// File:   learn-multi.cpp
// Author: Chris Eberle
// Copyright 2009 Securics, Inc

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <map>

#ifndef WIN32
# include <pthread.h>
#endif

#include "asciibar.h"
#include "learn-multi.h"

using namespace std;

static int incoming[2];
static int done[2];

// Macros
#define SAFE_FREE(ptr) do{if((ptr)!=NULL){free((ptr));(ptr)=NULL;}}while(0)
#define SAFE_DELETE(ptr)  do { if ((ptr) != NULL) { delete (ptr); (ptr) = NULL; } } while(0)
#define Malloc(type, n) (type*)malloc((n) * sizeof(type))

// Class constructor
learn_onevsrest* learn_onevsrest_create(int quiet, int progress, int num_procs)
{
    learn_onevsrest* self = (learn_onevsrest*)malloc(sizeof(learn_onevsrest));
    if(self == NULL)
        return NULL;

    self->gallery = NULL;
    self->gal_size = NULL;
    self->base_file_name = new string("");
    self->params = NULL;
    self->quiet = quiet;
    self->progress = progress;
    self->idxs = NULL;
    self->num_procs = num_procs;

    return self;
}

// Class destructor
void learn_onevsrest_destroy(learn_onevsrest* self)
{
    if(self == NULL)
        return;

    SAFE_FREE(self->params);
    SAFE_DELETE(self->base_file_name);
    SAFE_FREE(self->idxs);
    SAFE_FREE(self);
}

static int learn_onevsrest_trainone(learn_onevsrest* self, int catnum, unsigned int idx)
{
    if(self == NULL || catnum < 0)
        return -1;

    svm_parameter *params = self->params;
    if(params == NULL)
        return -1;

    struct gallery_entry* gallery = self->gallery;
    if(gallery == NULL)
        return -1;

    int size = self->gal_size;
    if(size <= 0)
        return -2;

    struct svm_problem prob;
    struct svm_node    *x_space;
    struct svm_model   *model;

    int elements, i, j, k;
    prob.l    = size;
    elements  = prob.l * (gallery[0].feature_count + 1);

    prob.y    = Malloc(double, prob.l);
    prob.x    = Malloc(struct svm_node*, prob.l);
    x_space   = Malloc(struct svm_node, elements);

    k = 0;

    for(i = 0; i < prob.l; i++)
    {
        prob.x[i] = &x_space[k];
        if(gallery[i].id == catnum)
            prob.y[i] = gallery[i].id;
        else
            prob.y[i] = -1;

        for(j = 0; j < gallery[i].feature_count; j++)
        {
            x_space[k].index = j + 1;
            x_space[k].value = gallery[i].pca_features[j];
            ++k;
        }

        x_space[k++].index = -1;
    }

    const char *error_msg = svm_check_parameter(&prob, params);
    if(error_msg)
    {
        fprintf(stderr, "SVM Error: %s\n", error_msg);
        return -1;
    }

    model = svm_train(&prob, params);

    if(model == NULL)
    {
        fprintf(stderr, "Error training!\n");
        return -1;
    }

    string fname;
    char* is = NULL;
    fname.assign(*(self->base_file_name));
    fname += "_";

    if(asprintf(&is, "%d", idx) <= 0)
    {
        fprintf(stderr, "Oh hell no!\n");
        return -1;
    }

    fname += is;
    SAFE_FREE(is);
    fname += ".svm";
    svm_save_model(fname.c_str(), model);
    svm_destroy_model(model);

    SAFE_FREE(prob.y);
    SAFE_FREE(prob.x);
    SAFE_FREE(x_space);

    return 0;
}

static void* learn_onevsrest_thread(void* p)
{
    learn_onevsrest* self = (learn_onevsrest*)p;

    int n, val = -1;

    while(1)
    {
        if(read(incoming[0], &n, sizeof(int)) != sizeof(int))
            perror("read");

        if(n < 0)
            break;

        if((val = learn_onevsrest_trainone(self, self->idxs[n].cat, self->idxs[n].idx)) < 0)
            fprintf(stderr, "\nPROBLEM\n");

        if(write(done[1], &n, sizeof(int)) != sizeof(int))
            perror("write");
    }

    return NULL;
}

int learn_onevsrest_train(learn_onevsrest* self, struct gallery_entry *gallery, unsigned int gal_size, const char* base_file_name)
{
    if(self == NULL || gallery == NULL)
        return -1;

    SAFE_FREE(self->params);

    self->gallery = gallery;
    self->gal_size = gal_size;
    self->base_file_name->assign(base_file_name);

    map<int,int> categories;
    int id, max_id = -1;

    for(unsigned int i = 0; i < gal_size; ++i)
    {
        int id = gallery[i].id;
        if(id < 0)
            continue;

        if(id > max_id)
            max_id = id;

        if(categories.count(id) == 0)
            categories[id] = 1;
        else
            categories[id] += 1;
    }

    unsigned int num_cats = 0;
    for(id = 0; id <= max_id; id++)
    {
        if(categories.count(id) == 0)
            continue;
        num_cats++;
    }

    if(self->num_procs > (int)num_cats)
        self->num_procs = (int)num_cats;

    self->idxs = (cat_idx*)malloc(sizeof(cat_idx) * num_cats);
    memset(self->idxs, 0, sizeof(cat_idx) * num_cats);

    self->params = (svm_parameter*)malloc(sizeof(svm_parameter));
    memset(self->params, 0, sizeof(svm_parameter));

    // Initialize the libsvm parameters
    self->params->svm_type     = C_SVC;
    self->params->kernel_type  = LINEAR;
    self->params->degree       = 2;
    self->params->gamma        = 0; // 1/k
    self->params->coef0        = 0;
    self->params->nu           = 0.1;
    self->params->cache_size   = 256;
    self->params->C            = 10;
    self->params->eps          = 0.01;
    self->params->p            = 0.1;
    self->params->shrinking    = 1;
    self->params->probability  = 1;
    self->params->nr_weight    = 0;
    self->params->weight_label = NULL;
    self->params->weight       = NULL;

    int max_index = gallery[0].feature_count;
    if(self->params->gamma == 0)
        self->params->gamma = 1.0 / max_index;

    unsigned int idx = 0;
    time_t start, end;

    // Start all of the threads
    pthread_t *threads = (pthread_t*)malloc(self->num_procs * sizeof(pthread_t));
    pthread_attr_t pthread_custom_attr;
    pthread_attr_init(&pthread_custom_attr);

    if(pipe(incoming) != 0)
    {
        perror("pipe");
        exit(1);
    }

    if(pipe(done) != 0)
    {
        perror("pipe");
        exit(1);
    }

    for(int c = 0; c < self->num_procs; c++)
        pthread_create(&threads[c], &pthread_custom_attr, learn_onevsrest_thread, (void*)self);

    if(self->quiet == 0)
        printf("Started %d worker%s\n", self->num_procs, (self->num_procs == 1) ? "" : "s");

    for(id = 0; id <= max_id; id++)
    {
        if(categories.count(id) == 0)
            continue;

        self->idxs[idx].cat = id;
        self->idxs[idx].idx = idx;

        if(write(incoming[1], &idx, sizeof(int)) != sizeof(int))
            perror("write");

        idx++;
    }

    // Write the end of the list
    int n = -1;
    for(int c = 0; c < self->num_procs; c++)
    {
        if(write(incoming[1], &n, sizeof(int)) != sizeof(int))
            perror("write");
    }

    start = time(NULL);

#ifndef HIDE_PROGRESS
    struct ascii_bar_params abp;
    if(self->progress)
    {
        init_ascii_bar(&abp, 0.0, (float)idx, 1.0f);
        abp.len = 50;
        abp.show_eta = 1;
        printf("SVM Multi: ");
        show_ascii_bar(&abp);
    }
    else
#endif
    {
        if(self->quiet)
        {
            printf("SVM Multi: ");
            fflush(stdout);
        }
        else
            printf("SVM One VS Rest Training...\n");
    }

    for(id = 0; id < (int)idx; id++)
    {
        if(read(done[0], &n, sizeof(int)) != sizeof(int))
            perror("read");

#ifndef HIDE_PROGRESS
        if(self->progress)
            step_ascii_bar(&abp);
        else
#endif
        {
            if(self->quiet == 0)
            {
                printf("[%5.1f%%] Category %d\n", 100. * (float)(id + 1) / (float)idx, self->idxs[n].cat);
                fflush(stdout);
            }
        }

        if(n < 0)
            fprintf(stderr, "\nError during training!\n");
    }

# ifndef HIDE_PROGRESS
    if(self->progress)
    {
        hide_ascii_bar(&abp);
        printf("done.\n");
        print_elapsed_time(&abp, "set");
    }
    else
# endif
    {
        if(self->quiet)
        {
            printf("done.\n");
            end = time(NULL);
            time_t sdiff = end - start;

            int min = (int)sdiff / 60;
            int sec = (int)sdiff % 60;

            if(self->quiet == 0)
            {
                printf("Time spent training SVM:");
                if(min > 0)
                    printf(" %d minute%s", min, (min != 1) ? "s" : "");
                if(sec > 0 || min == 0)
                    printf (" %d second%s", sec, (sec != 1) ? "s" : "");
                printf("\n");
            }
        }
    }

    for(int c = 0; c < self->num_procs; c++)
        pthread_join(threads[c], NULL);
    SAFE_FREE(threads);

    close(incoming[0]);
    close(incoming[1]);
    close(done[0]);
    close(done[1]);

    return 0;
}

int learn_onevsrest_cross_validate(learn_onevsrest* self)
{
    return 0;
}
