// recognize.h
// Copyright 2009 Securics, Inc

#ifndef _recognize_h_
#define _recognize_h_

// DO NOT include this file from anything but recognize.cpp.
// The only reason this file exists is to make the other a little
// more readable. Otherwise all of this would be at the top
// of recognize.

// Typedefs

// Command line parameters
typedef struct prog_params_t
{
    char *progName;
    char *file_name;
    char *pca_fname;
    char *sphere_fname;
    char *svm_model_name;
    char *easy_base;
    char *loadfilt_dir;
    char *map_name;
    char *failpredict_file;
    char *failmodel_name;
    char *save_int_dir;
    char *save_int_file;
    char *rpet_file;
    int  enable_alerts;
    int  keep_scores;
    int  use_groundtruth;
    int  fast_gabor;
    int  show_progress;
    int  show_normalized;
    int  use_cache;
    int  geo_norm;
    int  use_eye_detect;
    int  crop_face;
    int  multi_face;
    int  hist_norm;
    int  use_as_video;
    int  use_python;
    int  use_webcam;
    int  use_gige;
    int  use_atmos_deblur;
    int  use_motion_deblur;
    int  use_sqi_norm;
    int  webcam_trigger_time;
    int  demo_mode;
    int  quiet;
    int  fusion_type;
    int  force_id;
    int  onevsrest;

    double fp_atmos_thresh;
    double fp_motion1_thresh;
    double fp_motion2_thresh;
    double fp_motion3_thresh;
    double fp_default_thresh;
} prog_params;

// Various stateful variables needed by recognition
typedef struct recogvars_t
{
    v1_capture* cap;
    struct svm_model *model;
    map<int,string> cat_names;
    map<int,int> cat_correct;
    map<int,int> cat_total;
    map<int,int> rank_count;
    vector<string> problem_files;
    vector<id_score> top_ids;
    int rank_counter;
    int maxrank;
    int total;
    int correct;
    int face_fails;
    int face_detect_fails;
    CvHaarClassifierCascade* cascade;
    CvMemStorage* storage;
    CvMat* pca_training;
    MlClassification *fpmodel;
    FILE* fTrainFile;
    FILE* fInterFile;
    v1_float* mean;
    v1_float* std;
    void** filters;
    v1_float* features;
    classify_onevsrest* covr;

    RPET rpet;
    int top_rpet_num;
    map<string,int> rpet_nums;

    int fp_fail_correct;
    int fp_fail_incorrect;
    int fp_pass_correct;
    int fp_pass_incorrect;
} recogvars;

typedef struct multi_face_result_t
{
    vector<id_score> top_ids;
    int label;
    IplImage* faceImage;
} multi_face_result;

// Local prototypes
static void usage(const char* name, int ret);
static int tryCache(const char* file, v1_float* feat);
static void saveCache(const char* file, v1_float* feat);
static void parseCommandLine(int argc, char **argv, prog_params *parm);
static void printSummary(prog_params *parm);
static int runGeonorm(prog_params &params, recogvars &vars, IplImage **input);
static void init(prog_params &params, recogvars &vars);
static void uninit(prog_params &params, recogvars &vars);
static void fatal(const char* msg, ...);
static int recognize(prog_params &params, recogvars &vars);
static bool previewWebcam(prog_params &params, recogvars &vars);
static int showFace(IplImage* img, CvHaarClassifierCascade* cascade, CvMemStorage* storage, int use_python, const char* windowName);
static vector<multi_face_result> recognizeMulti(prog_params &params, recogvars &vars, IplImage* input);
static int recognizeBlurred(prog_params &params, recogvars &vars, IplImage* input, IplImage** normalized);
static int recognizeOne(prog_params &params, recogvars &vars, vector<id_score> &top, IplImage* input, IplImage** normalized,
                        const char* phase, double fp_thresh);
static void recordAnswer(prog_params &params, recogvars &vars, bool failed);
static void answerMulti(prog_params &params, recogvars &vars, vector<multi_face_result> &results);
static bool fuseCompare(id_score first, id_score second);
static void vote(map<int, int> &person_votes, int id);
static void fuseData(vector<id_score>& src, vector<id_score>& dst, int num);
IplImage** motionFaces(IplImage** motion_images, int size, recogvars &vars);
static void printRate(double elapsed, int total_processed);
static void printStats(prog_params &params, recogvars &vars);

static int runRecognition(prog_params &params, recogvars &vars);
static void saveRPET(prog_params &params, recogvars &vars);

static void onKill(int sig);
static void fatal(const char* msg, ...);

static const char* haarfile = "haarcascade_frontalface_alt2.xml";

// Lets us silence libsvm
extern int __verbose;


#endif /* _recognize_h_ */
