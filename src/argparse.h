// argparse.h
// Copyright 2009 Securics, Inc

#ifndef _argparse_h_
#define _argparse_h_

#include <iostream>
#include <map>
#include <vector>

typedef void (*usageFunc)(const char*, int);

class ArgParser
{
public:
    ArgParser(usageFunc usage, const char* progName, bool allowExtra);

    void addOption(const char* longName, bool wantArgument, const char* shortName = NULL, const char* defaultVal = NULL);
    bool isSet(const char* longName);
    bool setValue(const char* longName, const char* value);
    const char* getValue(const char* longName);
    int getBoolValue(const char* longName);
    size_t getExtraSize();
    const char* getExtra(size_t pos);
    bool parseCommandLine(int argc, char** argv, bool overwrite);
    bool parseConfigFile(const char* fileName, bool overwrite);
    bool writeConfigFile(const char* fileName, const std::vector<std::string>& excludes);

private:
    usageFunc usage;
    int argc;
    char** argv;
    bool allowExtra, overwrite;
    std::string progName;

    std::map<std::string,bool> longArgs;
    std::map<std::string,std::string> shortArgs;
    std::map<std::string,std::string> arguments;
    std::map<std::string,std::string> defaultValues;
    std::vector<std::string> extraArgs;

    bool parseShortOpt(const char* name, int &pos);
    bool parseLongOpt(const char* name, int &pos);
    bool parseConfigLine(std::string& line);

    static std::string upperToLower(std::string str);
};

#endif /* _argparse_h_ */
