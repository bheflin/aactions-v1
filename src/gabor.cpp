// File: gabor.cpp
// Authors: Ryan Thomas, Chris Eberle
// Copyright 2009 Securics, Inc
// reimplementation of the gabor filter from MIT

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sys/stat.h>
#include <stdlib.h>
#include "gabor.h"
#include "utils.h"

/** Get the highest number row / column pair that exist on disk for a given base name.
 * @param const char* base The base name of the filters
 */
static int get_max_rcnum(const char *base)
{
    struct stat rFileInfo, cFileInfo;
    char        rfilename[2048];
    char        cfilename[2048];
    int         num = 0, cres, rres;

    do
    {
        snprintf(rfilename, 2048, "%s_row_%d.xml", base, num + 1);
        snprintf(cfilename, 2048, "%s_col_%d.xml", base, num + 1);

        rres = stat(rfilename, &rFileInfo);
        cres = stat(cfilename, &cFileInfo);

        if(rres == 0 && cres == 0)
            num += 1;
    }
    while(rres == 0 && cres == 0);

    return num;
}

/**
 * Load filters from a directory.
 NOTE: Filters must be of naming format "/path/to/dir/gabor_filter_{fnum}_{row|col}_{rcnum}.xml"
* @param const char* dir The directory of the filters.
*/
gabor_filter**load_filters(const char *dir)
{
    gabor_filter **filterbank =
        (gabor_filter**)malloc(GABOR_FEATURES * sizeof(gabor_filter*));

    int  num;
    char filename[2048];
    char base[2048];
    int  total = 0;

    snprintf(base, 2048, "%s/gabor_filter", dir);

    // Load python's fast gabor filters
    for(int i = 0; i < 96; i++)
    {
        snprintf(filename, 2048, "%s_%d", base, i + 1);
        num = get_max_rcnum(filename);

        if(num <= 0)
        {
            fprintf(stderr, "Unable to find filter for base `%s'\n", base);
            exit(1);
        }

        filterbank[i] = (gabor_filter*)malloc(sizeof(gabor_filter));
        filterbank[i]->numEntries = num;
        filterbank[i]->first = (gabor_entry*)malloc(num * sizeof(gabor_entry));

        for(int j = 0; j < num; j++)
        {
            // Load the row
            snprintf(filename, 2048, "%s_%d_row_%d.xml", base, i + 1, j + 1);
            filterbank[i]->first[j].row = ipl_from_file(filename);

            if(filterbank[i]->first[j].row == NULL)
            {
                fprintf(stderr, "Unable to load filter file: `%s'\n", filename);
                return NULL; // memory leak
            }

            // Load the column
            snprintf(filename, 2048, "%s_%d_col_%d.xml", base, i + 1, j + 1);
            filterbank[i]->first[j].col = ipl_from_file(filename);

            if(filterbank[i]->first[j].col == NULL)
            {
                fprintf(stderr, "Unable to load filter file: `%s'\n", filename);
                return NULL; // memory leak
            }

            total += 2;
        }
    }

#ifndef TESTING
    printf("Loaded %d filters\n", total);
#endif

    return filterbank;
}


/**
 * Create the normal "fast" filters
 * @return gabor_filters The pca trimmed gabor_filters to use in v1
 */
gabor_filter**make_filters()
{
    gabor_filter **filterbank =
        (gabor_filter**)malloc(GABOR_FEATURES * sizeof(gabor_filter*));

    v1_float freqs[6]    = { 1. / 2., 1. / 3., 1. / 4., 1. / 6., 1. / 11., 1. / 18. };
    v1_float orients[16] =
    {
        0.0, M_PI / 16., M_PI / 8., 3. * M_PI / 16.,
        M_PI / 4., 5. * M_PI / 16., 3. * M_PI / 8., 7. * M_PI / 16.,
        M_PI / 2., 9. * M_PI / 16., 5. * M_PI / 8., 11. * M_PI / 16.,
        3. * M_PI / 4., 13. * M_PI / 16., 7. * M_PI / 8., 15. * M_PI / 16.
    };

    int      i = 0, j, k;
    v1_float gsw, gsh, xc, yc, wphase;

    gsw    = (v1_float)GABOR_SIZE / 5.;
    gsh    = (v1_float)GABOR_SIZE / 5.;
    xc     = GABOR_SIZE / 2;
    yc     = GABOR_SIZE / 2;
    wphase = 0;

    v1_float *result = (v1_float*)malloc(43 * 43 * sizeof(v1_float));

    for(j = 0; j < 6; j++)
    {
        for(k = 0; k < 16; k++)
        {
            filterbank[i] = gabor(gsw, gsh, xc, yc, freqs[j], orients[k], wphase, 43, 43, result);
            i++;
        }
    }

    free(result);

    return filterbank;
}

/**
 * Free the resources used by a single fast gabor filter
 * @param gabor_filter *filt The filters to free
 */
void free_gabor(gabor_filter *filt)
{
    if(filt == NULL)
        return;

    gabor_entry *e;

    for(int n = 0; n < filt->numEntries; n++)
    {
        e = &(filt->first[n]);

        if(e == NULL)
            continue;

        if(e->row != NULL)
            cvReleaseImage(&(e->row));

        if(e->col != NULL)
            cvReleaseImage(&(e->col));
    }

    free(filt->first);
    free(filt);
}

/**
 * Free the resources used by a fast gabor filterban
 * @param gabor_filter ** filters The filterbank to free
 */
void free_filters(gabor_filter **filters)
{
    if(filters == NULL)
        return;

    for(int i = 0; i < GABOR_FEATURES; ++i)
    {
        free_gabor(filters[i]);
        filters[i] = NULL;
    }

    free(filters);
}

/**
 * Create the slower full IplImage gabor filters not pca trimmed
 * @return IplImage** The gabor filters
 */
IplImage**make_ipl_filters()
{
    int      i = 0, j, k;
    v1_float gsw, gsh, xc, yc, wphase;

    IplImage **filterbank = (IplImage**)malloc(GABOR_FEATURES * sizeof(IplImage*));

    for(i = 0; i < GABOR_FEATURES; i++)
    {
        filterbank[i] = cvCreateImageHeader(cvSize(GABOR_SIZE, GABOR_SIZE), IPL_DEPTH_V1, 1);
        filterbank[i]->widthStep = filterbank[i]->width * sizeof(v1_float);
    }

    v1_float freqs[6]    = { 1. / 2., 1. / 3., 1. / 4., 1. / 6., 1. / 11., 1. / 18. };
    v1_float orients[16] =
    {
        0.0, M_PI / 16., M_PI / 8., 3. * M_PI / 16.,
        M_PI / 4., 5. * M_PI / 16., 3. * M_PI / 8., 7. * M_PI / 16.,
        M_PI / 2., 9. * M_PI / 16., 5. * M_PI / 8., 11. * M_PI / 16.,
        3. * M_PI / 4., 13. * M_PI / 16., 7. * M_PI / 8., 15. * M_PI / 16.
    };

    gsw    = (v1_float)GABOR_SIZE / 5.;
    gsh    = (v1_float)GABOR_SIZE / 5.;
    xc     = GABOR_SIZE / 2;
    yc     = GABOR_SIZE / 2;
    wphase = 0;

    i = 0;

    for(j = 0; j < 6; j++)
    {
        for(k = 0; k < 16; k++)
        {
            v1_float     *result = (v1_float*)malloc(43 * 43 * sizeof(v1_float));
            gabor_filter *tmp    = gabor(gsw, gsh, xc, yc, freqs[j], orients[k], wphase, 43, 43, result);
            free_gabor(tmp);
            filterbank[i]->imageData = (char*)result;
            i++;
        }
    }

    return filterbank;
}

/** 
 *Free the resources used by the slower filters
 * @param IplImage** filters The slow gabor filters to free.
 */
void free_ipl_filters(IplImage **filters)
{
    if(filters == NULL)
        return;

    for(int i = 0; i < GABOR_FEATURES; i++)
    {
        free(filters[i]->imageData);
        cvReleaseImageHeader(&filters[i]);
        filters[i] = NULL;
    }

    free(filters);
}

/*
 *preforms a dot product and a sqrt of it to get same value as fast norm in python
 * @param v1_float *mat The array to preform the fast norm on
 * @param int size The size of the array
 */
v1_float fastnorm(v1_float *mat, int size)
{
    v1_float return_val = 0;
    int      i;

    for(i = 0; i < size; i++)
    {
        return_val += mat[i] * mat[i];
    }

    return_val = v1_pow(return_val, .5);
    return return_val;
}
/**
 *Normalizes the svd same as python
 * @param CvMat * data The data to be normalized
 * @param int row The row in the data to work on
 * @return v1_float The normalization factor
 */

static v1_float svdnorm(CvMat *dat, int row)
{
    v1_float return_val = 0;
    int      i;

    for(i = 0; i < dat->cols; i++)
    {
        v1_float val = ((v1_float*)(dat->data.ptr + dat->step * row))[i];
        return_val += val * val;
    }

    return_val = v1_pow(return_val, .5);
    return return_val;
}

/*
 * Prforms a fast svd using the pca trimmed gabor features
 NOTE: Only designed to be called by gabor. For pca, see pca.c
 * @param int width The widht of the data
 * @param int height the height of the data
 * @param v1_float *gabor The gabor filters
 */
gabor_filter*gabor_fastsvd(int width, int height, v1_float *gabor)
{
    CvMat *data = cvCreateMat(height, width, CV_V1);
    int   i, j, k;

    k = 0;
    for(i = 0; i < height; i++)
    {
        for(j = 0; j < width; j++)
        {
            ((v1_float*)(data->data.ptr + data->step * i))[j] = gabor[k++];
        }
    }

    CvMat *dot = cvCreateMat(height, height, CV_V1);
    cvGEMM(data, data, 1, NULL, 1, dot, CV_GEMM_B_T);

    // U, S, V = N.linalg.svd(dot)
    CvMat *S = cvCreateMat(height, 1, CV_V1);
    CvMat *U = cvCreateMat(height, height, CV_V1);
    CvMat *V = cvCreateMat(height, height, CV_V1);
    cvSVD(dot, S, U, V, 0);

    cvGEMM(U, data, 1, NULL, 1, V, CV_GEMM_A_T);

    // Normalize, square, and calculate S.sum() at the same time
    v1_float S_sum = 0.0;

    for(i = 0; i < height; i++)
    {
        v1_float norm = svdnorm(V, i);
        ((v1_float*)(S->data.ptr + S->step * i))[0] = v1_pow(norm, 2.0);
        S_sum += v1_pow(norm, 2.0);

        for(j = 0; j < height; j++)
            ((v1_float*)(V->data.ptr + V->step * i))[j] /= norm;
    }

    if(S_sum == 0.0)
        S_sum = 1.0;

    v1_float tot = 0.0;
    int      num = 0;

    while(tot <= 0.9 && num < height)
    {
        v1_float S_i = ((v1_float*)(S->data.ptr + S->step * num))[0];
        tot += S_i / S_sum;
        num++;
    }

    gabor_filter *filt = (gabor_filter*)malloc(sizeof(gabor_filter));
    filt->numEntries = num;
    filt->first = (gabor_entry*)malloc(sizeof(gabor_entry) * num);

    IplImage *row, *col;
    v1_float U_j, V_j, S_i;

    // row = (U[:,idx]*S[idx])[:, newaxis]
    // col = (V[idx,:])[newaxis, :]
    // vectors += [(row,col)]
    for(i = 0; i < num; i++)
    {
        row = cvCreateImage(cvSize(1, height), IPL_DEPTH_V1, 1);
        col = cvCreateImage(cvSize(height, 1), IPL_DEPTH_V1, 1);

        S_i = ((v1_float*)(S->data.ptr + S->step * i))[0];

        for(j = 0; j < height; j++)
        {
            U_j = ((v1_float*)(U->data.ptr + U->step * j))[i];
            V_j = ((v1_float*)(V->data.ptr + V->step * i))[j];

            *((v1_float*)(row->imageData + row->widthStep * j)) = S_i * U_j;
            ((v1_float*)(col->imageData))[j] = V_j;
        }

        filt->first[i].row = row;
        filt->first[i].col = col;
    }

    cvReleaseMat(&data);
    cvReleaseMat(&dot);
    cvReleaseMat(&S);
    cvReleaseMat(&V);
    cvReleaseMat(&U);

    return filt;
}

/**
 * Same gabor function the python code uses
 * @param v1_float gsw standard deviation of the gaussian envelope (width)
 * @param v1_float  gsh standard deviation of the gaussian envelope (height)
 * @param v1_float gx0 x indice of center of the gaussian envelope
 * @param v1_float gy0 y indice of center of the gaussian envelope
 * @param v1_float wfreq frequency of the 2d wave
 * @param v1_float worient orientation of the 2d wave
 * @param v1_float  wphase phase of the 2d wave
 * @param int height height of gabor filter
 * @param int  width width of gabor filter
 * @param v1_float* gabor gabor filtered image
*/
gabor_filter* gabor(v1_float gsw, v1_float gsh, v1_float gx0, v1_float gy0, v1_float wfreq,
                    v1_float worient, v1_float wphase, int height, int width, v1_float *gabor)
{
    int      i, j, index;
    v1_float X, Y, env, wave, mean_val, fast_val;

    memset(gabor, 0, width * height * sizeof(v1_float));

    for(i = 0, index = 0; i < height; i++)
    {
        for(j = 0; j < width; j++)
        {
            Y    = i * sin(worient) * wfreq;
            X    = j * cos(worient) * wfreq;
            env  = expf(-.5 * ((v1_pow((i - gx0), 2) / (gsw * gsw)) + (v1_pow((j - gy0), 2) / (gsh * gsh))));
            wave = cos(2 * M_PI * (X + Y) + wphase);
            gabor[index] = env * wave;
            index++;
        }
    }

    mean_val = arrMean(gabor, width * height);

    for(i = 0, index = 0; i < height; i++)
    {
        for(j = 0; j < width; j++)
        {
            gabor[index] = (gabor[index] - mean_val);
            index++;
        }
    }

    fast_val = fastnorm(gabor, width * height);

    for(i = 0, index = 0; i < height; i++)
    {
        for(j = 0; j < width; j++)
        {
            gabor[index] /= fast_val;
            index++;
        }
    }

    return gabor_fastsvd(width, height, gabor);
}
