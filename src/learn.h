// learn.h
// Copyright 2009 Securics, Inc

#ifndef LEARN_H
#define LEARN_H

#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include "svm.h"
#include "structs.h"
using namespace std;

void learn(struct gallery_entry *gallery, int size,  svm_parameter *params, char* model_name, const char* train_out, int cross_validation);
void learn_validate(struct validate_gallery *gallery, int size,  svm_parameter *params, char* model_name, const char* train_out, int cross_validation);

#endif
