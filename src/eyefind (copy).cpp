// File:    eyefind.cpp
// Author:  Chris Eberle
// Created: Wed Oct 21 15:02:26 2009
// Copyright 2009 Securics, Inc
//
// A simple program to locate eyes in an image

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <ctype.h>
#include <libgen.h>
#include <stdarg.h>
#include <iostream>
#include <sys/stat.h>

#include "structs.h"
#include "utils.h"
#include "geo.h"
#include "eye_detector.h"
#include "facedetect.h"
#include "preprocessing.h"
#include "version.h"
#include "argparse.h"

#define SPACE 32
#define ESCAPE 27
#define ENTER 10

using namespace std;

// Typedefs
typedef struct params_t
{
    char* progName;
    char* file;
    int user_id;
    int confirm;
    int use_python;
} prog_params;

// Prototypes
static void usage(const char* name, int val);
static void parseCommandLine(int argc, char **argv, prog_params *parm);

static const char* haarfile = "haarcascade_frontalface_alt2.xml";

#define SAFE_RELEASE(img) do { if((img) != NULL) { cvReleaseImage(&(img)); (img) = NULL; } } while(0);
#define SAFE_FREE(ptr) do { if ((ptr) != NULL) { free((ptr)); (ptr) = NULL; } } while(0);

// Print a message to stderr and exit
static void fatal(const char* msg, ...)
{
    va_list ap;
    va_start(ap, msg);
    vfprintf(stderr, msg, ap);
    va_end(ap);

    exit(1);
}

int processImage(IplImage* img, CvHaarClassifierCascade* cascade, CvMemStorage* storage, prog_params& params)
{
    const int small_size = 180;
    if(img == NULL)
        return 1;

    IplImage* colorImg = cvCloneImage(img);
    IplImage* grayImg = getGrayscale(img);

    CvRect r = detect_face(cascade, storage, grayImg);
    /*
    if(r.x >=0)
      {
	r.x = floor(r.x - ((float)r.x * 0.20));
	if (r.x < 0)
	  r.x=0;
      }
    
    if(r.y >=0)
      {
	r.y = floor(r.y - ((float)r.y * 0.20));
	if(r.y < 0)
	  r.y = 0;
      }
    
    if(r.width >= 0)
      {
	r.width=floor(r.width + ((float)r.x * 0.53));
	if(r.width + r.x > img->width)
	  r.width=img->width - r.x;
      }
    
    if(r.height >= 0)
      {
	r.height = floor(r.height + ((float)r.y * 0.53));
	if (r.height + r.y  > img->height)
	  r.height=img->height - r.y;
      }
    */
    if(r.x >= 0 && r.y >=0 && r.width > 0 && r.height > 0)
    {
        CvPoint pt1 = cvPoint(r.x, r.y);
        CvPoint pt2 = cvPoint(r.x + r.width, r.y + r.height);

    





        cvRectangle(colorImg, pt1, pt2, cvScalar(0, 0, 255), 2, 8, 0);
    }
    else
    {
        fprintf(stderr, "ERROR: Face detection failed for image `%s'.\n", params.file);
        SAFE_RELEASE(colorImg);
        SAFE_RELEASE(grayImg);
        return 1;
    }

    IplImage* tmp = cropIpl(grayImg, r.x, r.y, r.width, r.height);
    SAFE_RELEASE(grayImg);
    grayImg = tmp;
    if(grayImg == NULL)
    {
        fprintf(stderr, "ERROR: Cropping failed for image `%s'.\n", params.file);
        SAFE_RELEASE(colorImg);
        return 1;
    }

    int beforeW, beforeH;
    beforeW = grayImg->width;
    beforeH = grayImg->height;

    tmp = get_image(grayImg, small_size, params.use_python, 1);
    
    //cvNamedWindow("CROP99",CV_WINDOW_AUTOSIZE);
    //cvShowImage("CROP99",grayImg);
    // cvWaitKey(0);
    //cvNamedWindow("CROP100",CV_WINDOW_AUTOSIZE);
    //cvShowImage("CROP100",tmp);
    // cvWaitKey(0);
    


    SAFE_RELEASE(grayImg);
    grayImg = tmp;
    if(grayImg == NULL)
    {
        fprintf(stderr, "ERROR: Resize filed for image `%s'.\n", params.file);
        SAFE_RELEASE(colorImg);
        return 1;
    }

    int lx = -1, ly = -1, rx = -1, ry = -1;
    int lx1 = -1, ly1 = -1, rx1 = -1, ry1 = -1;
    cvEqualizeHist(grayImg,grayImg);
    eye_detect(grayImg, &lx, &rx, &ly, &ry, &lx1, &rx1, &ly1, &ry1);
    if(lx < 0 || ly < 0 || rx < 0 || ry < 0 ||
       lx >= grayImg->width || ly >= grayImg->height ||
       rx >= grayImg->width || ry >= grayImg->height)
    {
        cvShowImage("Eyes", grayImg);
        SAFE_RELEASE(grayImg);
        SAFE_RELEASE(colorImg);
        fprintf(stderr, "ERROR: Eye detection failed for image `%s'.\n", params.file);
        return 1;
    }

    double scaleX, scaleY;
    scaleX = (double)beforeW / (double)grayImg->width;
    scaleY = (double)beforeH / (double)grayImg->height;

    CvPoint center;

    int flx = (int)((double)lx * scaleX) + r.x;
    int fly = (int)((double)ly * scaleY) + r.y;
    int frx = (int)((double)rx * scaleX) + r.x;
    int fry = (int)((double)ry * scaleY) + r.y;

    int flx1 = (int)((double)lx1 * scaleX) + r.x;
    int fly1 = (int)((double)ly1 * scaleY) + r.y;
    int frx1 = (int)((double)rx1 * scaleX) + r.x;
    int fry1 = (int)((double)ry1 * scaleY) + r.y;



    //center.x = flx; center.y = fly;
    //cvCircle(colorImg, center, 10, cvScalar(0, 0, 255), 2, 8, 0);

    //center.x = frx; center.y = fry;
    //cvCircle(colorImg, center, 10, cvScalar(0, 0, 255), 2, 8, 0);

    CvFont font;
    CvPoint LEFT_pt=cvPoint(flx,fly);
    CvPoint RIGHT_pt=cvPoint(frx,fry);
    cvInitFont(&font, CV_FONT_HERSHEY_COMPLEX,0.25,0.25,0,1,CV_AA);
    cvPutText(colorImg,"X",LEFT_pt,&font,CV_RGB(0xff,0xff,0xff));
    cvPutText(colorImg,"X",RIGHT_pt,&font,CV_RGB(0xff,0xff,0xff));
	
    CvFont font1;
    CvPoint LEFT_pt1=cvPoint(flx1,fly1);
    CvPoint RIGHT_pt1=cvPoint(frx1,fry1);
    cvInitFont(&font1, CV_FONT_HERSHEY_COMPLEX,0.25,0.25,0,1,CV_AA);
    cvPutText(colorImg,"O",LEFT_pt1,&font1,CV_RGB(0xff,0xff,0xff));
    cvPutText(colorImg,"O",RIGHT_pt1,&font1,CV_RGB(0xff,0xff,0xff));

    int key = -1;
    if(params.confirm)
    {
        tmp = get_image(colorImg, 300, params.use_python, 1);
        cvShowImage("Eyes", tmp);

        do
        {
            key = cvWaitKey(-1) & 255;
        } while(key != ENTER && key != ESCAPE);
        SAFE_RELEASE(tmp);
    }

    SAFE_RELEASE(grayImg);
    SAFE_RELEASE(colorImg);

    if(key == ESCAPE)
        return 99;

    if(params.user_id >= 0)
        printf("%s %d %d %d %d %d\n", params.file, params.user_id, flx, fly, frx, fry);
    else
        printf("%s %d %d %d %d\n", params.file, flx, fly, frx, fry);

    return 0;
}

// Entry point for program
int main(int argc, char **argv)
{
    // Parse the command line arguments
    prog_params params;
    parseCommandLine(argc, argv, &params);

    if(params.file == NULL)
        usage(params.progName, 1);

    CvHaarClassifierCascade* cascade = (CvHaarClassifierCascade*)cvLoad(haarfile, 0, 0, 0);
    if(cascade == NULL)
        fatal("Error: Unable to load haar classifier: `%s'\n", haarfile);
    CvMemStorage* storage = cvCreateMemStorage(0);

    IplImage* img = NULL;
    if((img = cvLoadImage(params.file, -1)) == NULL)
        fatal("Error: Unable to load image file: `%s'\n", params.file);

    if(params.confirm)
    {
        cvNamedWindow("Eyes", CV_WINDOW_AUTOSIZE);
        fprintf(stderr, "Press ENTER to confirm, and ESC to exit.\n");
    }

    int ret = processImage(img, cascade, storage, params);

    if(params.confirm)
        cvDestroyWindow("Eyes");
    cvReleaseImage(&img);

    SAFE_FREE(params.file);

    return ret;
}

// Print the program's usage
static void usage(const char* name, int val)
{
    fprintf(stderr,
            "Usage: %s [OPTIONS]\n"
            "  -C, --config=FILE        Load config options from a file.\n"
            "  -f, --image=FILE         Image to find eye coordinates within (required).\n"
            "  -u, --id=ID              User ID of subject (optional).\n"
            "  -c, --confirm=BOOL       Show window to confirm eye coordinates.\n"
            "  -P, --python-resize=BOOL Let python do the resize (default false).\n"
            "  -i, --info               Show file versions and exit.\n"
            "  -h, --help               Show this helpful message.\n"
            , name);
    exit(val);
}

// Parse the command line arguments and load them into a struct  
static void parseCommandLine(int argc, char **argv, prog_params *parm)
{
    argv[0] = basename(argv[0]);
    parm->progName = argv[0];
    parm->user_id = -1;
    parm->confirm = 0;
    parm->use_python = 0;
    parm->file = NULL;

    ArgParser ap(usage, parm->progName, false);

    // Simple switches
    ap.addOption("help", false, "h");
    ap.addOption("info", false, "i");

    // Regular options
    ap.addOption("config",        true, "C");
    ap.addOption("image",         true, "f");
    ap.addOption("id",            true, "u");

    // Boolean options
    ap.addOption("python-resize", true, "P", "yes");
    ap.addOption("confirm",       true, "c", "yes");

    if(!ap.parseCommandLine(argc, argv, true))
        exit(1);

    if(ap.isSet("help"))
        usage(argv[0], 0);
    
    if(ap.isSet("info"))
    {
        printFileVersions(stdout);
        exit(0);
    }

    // Parse the config file if specified
    if(ap.isSet("config"))
    {
        if(!ap.parseConfigFile(ap.getValue("config"), true))
            exit(1);
    }

    // Parse the options
    if(ap.isSet("image")) parm->file = strdup(ap.getValue("image"));
    if(ap.isSet("id"))  parm->user_id = atoi(ap.getValue("id"));
    if(ap.isSet("python-resize")) parm->use_python = ap.getBoolValue("python-resize");
    if(ap.isSet("confirm")) parm->confirm = ap.getBoolValue("confirm");
}
