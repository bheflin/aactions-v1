// alert.cpp
// Copyright 2009 Securics, Inc

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "alert.h"
#include "preprocessing.h"
#include "utils.h"

using namespace std;

static map<int,string> *acat_names = NULL;
static map<int,IplImage*> cat_imgs;

static const char* gwindowname = "Alert - Gallery";
static const char* pwindowname = "Alert - Probe";
static CvFont ft, bold;

// The most gallery items to display
#define MMIN(a, b) (((a) < (b)) ? a : b)
#define MAX_GAL_DISPLAY 5
#define MAX_WIDTH 1200
/**
 * Puts a string of text on an image
 * @param IplImage* img The image to put the text on
 * @param int x X location to start text
 * @param int y Y location to start text
 */
static void putOutlineText(IplImage* img, const char* text, int x, int y)
{
    cvPutText(img, text, cvPoint(x, y), &bold, CV_RGB(0, 0, 0));
    cvPutText(img, text, cvPoint(x, y), &ft, CV_RGB(255, 255, 255));
}

/**
 * Initializes the alert system
 * @param map<int,string>*cat_files Images are images to be shown
 * @param map<int, string>* cat_names The names of the catagories.
 */
void setup_alert_gallery(map<int,string> *cat_files, map<int,string> *cat_names)
{
    IplImage* tmp, *tmp2;
    int cat;
    string file;
    
    cvInitFont(&ft, CV_FONT_HERSHEY_PLAIN, 1.0, 1.0,
               0, 1, 10);
    cvInitFont(&bold, CV_FONT_HERSHEY_PLAIN, 1.0, 1.0,
               0, 2, 10);

    acat_names = cat_names;
    if(cat_files)
    {
        map<int,string>::iterator it;
        for(it = cat_files->begin(); it != cat_files->end(); it++)
        {
            cat = (*it).first;
            file = (*it).second;

            if((tmp = cvLoadImage(file.c_str(), CV_LOAD_IMAGE_COLOR)) == NULL)
            {
                fprintf(stderr, "WARNING: Unable to load gallery image: %s\n", file.c_str());
                cat_imgs[cat] = NULL;
                continue;
            }

            tmp2 = get_image(tmp, 200, 0, 0);
            if(tmp2 == NULL)
                tmp2 = tmp;
            else
                cvReleaseImage(&tmp);
            cat_imgs[cat] = tmp2;
        }
    }

    cvStartWindowThread();
}
/**
 *Frees all memory used for alerts.
 */
void cleanup_alert_gallery()
{
    IplImage *img;
    int cat;

    map<int,IplImage*>::iterator it;
    for(it = cat_imgs.begin(); it != cat_imgs.end(); it++)
    {
        cat = (*it).first;
        img = (*it).second;
        if(img) cvReleaseImage(&img);
        cat_imgs[cat] = NULL;
    }

    cvDestroyWindow(pwindowname);
    cvDestroyWindow(gwindowname);
}
/**
 *Puts a single person on larger gallery image.
 * @param int id The gallery id.
 * @param int pos The rank of the entry
 * @param IplImage img The image to draw
 * @param int woffset The offset of the image in the pane.
 */
static int draw_gitem(int id, int pos, IplImage* img, int woffset)
{
    IplImage *gitem = cat_imgs[id];
    if(gitem == NULL)
        return 0;

    char sid[1024];
    int sx, sy, dx, dy;

    for(sy = 0, dy = 0; sy < gitem->height; sy++, dy++)
    {
        for(sx = 0, dx = woffset; sx < gitem->width; sx++, dx++)
        {
            ((uchar*)(img->imageData + img->widthStep*dy))[dx*3] = ((uchar*)(gitem->imageData + gitem->widthStep*sy))[sx*3];
            ((uchar*)(img->imageData + img->widthStep*dy))[dx*3+1] = ((uchar*)(gitem->imageData + gitem->widthStep*sy))[sx*3+1];
            ((uchar*)(img->imageData + img->widthStep*dy))[dx*3+2] = ((uchar*)(gitem->imageData + gitem->widthStep*sy))[sx*3+2];
        }
    }

    if(acat_names != NULL && acat_names->count(id) > 0)
    {
        snprintf(sid, 1024, "%d: %s", pos, (*acat_names)[id].c_str());
        putOutlineText(img, sid, woffset + 10, 20);
    }
    else
    {
        snprintf(sid, 1024, "%d: %d", pos, id);
        putOutlineText(img, sid, woffset + 10, 20);
    }

    return gitem->width;
}
/**
 *Show the gallery
 * @param vector<id_score> &top_ids The top ids returned by recognition.
 */
static void show_gallery(vector<id_score> &top_ids)
{
    const unsigned int sep_width = 5; // 5px seperator

    unsigned int numtoshow = MMIN(MAX_GAL_DISPLAY, top_ids.size());
    unsigned int gwidth = 0, gheight = 0;
    int id, i;

    vector<int> final_ids;
    vector<int> final_pos;

    for(i = 0; i < (int)numtoshow; i++)
    {
        id = top_ids[i].id;
        if(cat_imgs.count(id) > 0 && cat_imgs[id] != NULL)
        {
            IplImage* img = cat_imgs[id];
            if(gwidth + (unsigned int)img->width > MAX_WIDTH)
                break;

            if(gwidth > 0)
                gwidth += sep_width;            
            if((unsigned int)img->height > gheight)
                gheight = (unsigned int)img->height;

            gwidth += (unsigned int)img->width;
            final_ids.push_back(id);
            final_pos.push_back(i + 1);
        }
    }

    if((numtoshow = final_ids.size()) == 0 || gwidth == 0 || gheight == 0)
    {
        cvDestroyWindow(gwindowname);
        return;
    }

    IplImage* img = cvCreateImage(cvSize(gwidth, gheight), IPL_DEPTH_8U, 3);
    if(img == NULL)
        return;
    cvSetZero(img);

    int x = 0, pos;
    for(i = 0; i < (int)numtoshow; i++)
    {
        if(x > 0)
            x += (int)sep_width;
        id = final_ids[i];
        pos = final_pos[i];
        x += draw_gitem(id, pos, img, x);
    }

    cvNamedWindow(gwindowname, CV_WINDOW_AUTOSIZE);
    cvShowImage(gwindowname, img);
    cvReleaseImage(&img);
}
/**
 *Show the alert for the probe
 * @param IplImage* probe Probe image to show.
 * @param vecotr<id_score> &topn The top n recognition results.
 */
int show_alert(IplImage* probe, vector<id_score> &topn)
{
    int id  = topn[0].id;
    if(probe == NULL)
        return 0;

    cvNamedWindow(pwindowname, CV_WINDOW_AUTOSIZE);

    if(topn.size() > 0)
        show_gallery(topn);

    IplImage* tmp = get_image(probe, 300, 0, 0);
    if(acat_names != NULL)
    {
        if(acat_names->count(id) > 0)
            putOutlineText(tmp, (*acat_names)[id].c_str(), 10, 20);
    }
    else
    {
        char sid[64];
        snprintf(sid, 64, "id: %d", id);
        putOutlineText(tmp, sid, 10, 20);
    }

    cvShowImage(pwindowname, tmp);
    //int key = cvWaitKey(60);
    cvReleaseImage(&tmp);

    //if(key == 27 /*esc*/)
    //    return 1;

    return 0;
}
/**
 *Destroies alert windows since not needed
 */
void hide_alert()
{
    cvDestroyWindow(pwindowname);
    cvDestroyWindow(gwindowname);    
}
