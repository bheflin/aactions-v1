// iplstr.h
// Author: Chris Eberle
// Copyright 2009 Securics, Inc
//
// Some functions to more easily convert an IplImage to and from a
// string for easier debugging and testing.

#ifndef _iplstr_h_
#define _iplstr_h_

#include "cvheaders.h"

IplImage* iplFromStr(const char* str, int depth, int channels);
IplImage* iplFromFile(const char* file, int depth, int channels);

int saveIplToMatlab(IplImage* img, const char* file);
int saveIplImage(IplImage* img, const char* file);
void printIplImage(IplImage* img, int pad);
void printIplSegment(IplImage* img, int xLeft, int yTop, int width, int height, int pad);

#endif
