// learn.cpp
// Copyright 2009 Securics, Inc

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "learn.h"
#include "utils.h"
#include <time.h>

#define Malloc(type, n) (type*)malloc((n) * sizeof(type))

/**
 *Method to preform svm training
 * @param struct gallery_entry gallery  The populated gallery of all images used to train
 * @param int size The number of entries in the gallery
 * @param svm_parameter  params The svm params that are used for training
 * @param char*  model_name The location the model file is to be stored
 * @param const char* train_out The location the feature file is to be saved, NULL no file saved
 * @param int  cross_validation 1 to run cross_valdation 0 to not
 */
void learn(struct gallery_entry *gallery, int size,  svm_parameter *params, char* model_name,
           const char* train_out, int cross_validation)
{
  struct svm_problem prob;
  struct svm_node    *x_space;
  struct svm_model   *model;
  int nr_fold = 3;

  //A line should not get bigger than this.
  const char       *error_msg;

  int elements, max_index, i, j, k;
  prob.l    = size;
  elements  = prob.l * (gallery[0].feature_count + 1);

  prob.y    = Malloc(double, prob.l);
  prob.x    = Malloc(struct svm_node*, prob.l);
  x_space   = Malloc(struct svm_node, elements);

  FILE* f = NULL;
  if(train_out != NULL)
    {
      fprintf(stdout,"Opening %s\n",train_out);
      FILE* f = NULL;
      if(train_out != NULL)
        f = fopen(train_out, "w");
      if(!f) perror(train_out);
      fprintf(stdout,"Finished opening %s\n",train_out);
    
      max_index = gallery[0].feature_count;
      k = 0;
      for(i = 0; i < prob.l; i++)
	{
	  if(f != NULL)
	    fprintf(f, "%d", gallery[i].id);

	  prob.x[i] = &x_space[k];
	  prob.y[i] = gallery[i].id;

	  for(j = 0; j < gallery[i].feature_count; j++)
	    {
	      if(f != NULL)
		fprintf(f, " %d:%f", j + 1, gallery[i].pca_features[j]);

	      x_space[k].index = j + 1;
	      x_space[k].value = gallery[i].pca_features[j];
	      ++k;
	    }

	  if(f != NULL)
	    fprintf(f, "\n");
	  x_space[k++].index = -1;
	}
    }
  if(f != NULL)
    fclose(f);

  if(params->gamma == 0)
    params->gamma = 1.0 / max_index;

  error_msg = svm_check_parameter(&prob, params);

  if(error_msg)
    {
      fprintf(stderr, "Error: %s\n", error_msg);
      exit(1);
    }

  if(cross_validation)
    {
      int    i;
      int    total_correct = 0;
      double total_error   = 0;
      double sumv    = 0, sumy = 0, sumvv = 0, sumyy = 0, sumvy = 0;
      double *target = Malloc(double, prob.l);

      svm_cross_validation(&prob, params, nr_fold, target);

      if(params->svm_type == EPSILON_SVR || params->svm_type == NU_SVR)
        {
	  for(i = 0; i < prob.l; i++)
            {
	      double y = prob.y[i];
	      double v = target[i];
	      total_error += (v - y) * (v - y);
	      sumv  += v;
	      sumy  += y;
	      sumvv += v * v;
	      sumyy += y * y;
	      sumvy += v * y;
            }

	  printf("Cross Validation Mean squared error = %g\n", total_error / prob.l);
	  printf("Cross Validation Squared correlation coefficient = %g\n",
		 ((prob.l * sumvy - sumv * sumy) * (prob.l * sumvy - sumv * sumy)) /
		 ((prob.l * sumvv - sumv * sumv) * (prob.l * sumyy - sumy * sumy)));
        }
      else
        {
	  for(i = 0; i < prob.l; i++)
	    if(target[i] == prob.y[i])
	      ++total_correct;

	  printf("Cross Validation Accuracy = %g%%\n", 100.0 * total_correct / prob.l);
        }

      free(target);
    }

  time_t start = time(NULL);

  printf("Training model: "); fflush(stdout);
  model = svm_train(&prob, params);
  printf("done.\n");

  time_t diff = time(NULL) - start;

  svm_save_model(model_name, model);
  svm_destroy_model(model);

  if(diff > 10)
    {
      int min = diff / 60;
      int sec = diff % 60;
      printf("Time spent training:");
      if(min > 0)
	printf(" %d minute%s", min, (min > 1) ? "s" : "");
      if(sec > 0)
	printf (" %d second%s", sec, (sec > 1) ? "s" : "");
      printf("\n");
    }

  free(prob.y);
  free(prob.x);
  free(x_space);
}

/**
 *Method to preform svm training
 * @param struct gallery_entry gallery  The populated gallery of all images used to train
 * @param int size The number of entries in the gallery
 * @param svm_parameter  params The svm params that are used for training
 * @param char*  model_name The location the model file is to be stored
 * @param const char* train_out The location the feature file is to be saved, NULL no file saved
 * @param int  cross_validation 1 to run cross_valdation 0 to not
 */
void learn_validate(struct validate_gallery *gallery, int size,  svm_parameter *params, char* model_name,
		    const char* train_out, int cross_validation)
{
  struct svm_problem prob;
  struct svm_node    *x_space;
  struct svm_model   *model;
  int nr_fold = 3;

  //A line should not get bigger than this.
  const char       *error_msg;

  int elements, max_index, i, j, k;
  prob.l    = size;
  elements  = prob.l * (gallery[0].feature_count + 1);

  prob.y    = Malloc(double, prob.l);
  prob.x    = Malloc(struct svm_node*, prob.l);
  x_space   = Malloc(struct svm_node, elements);

  FILE* f = NULL;
  if(train_out != NULL)
    {
      fprintf(stdout,"Opening %s\n",train_out);
      FILE* f = NULL;
      if(train_out != NULL)
        f = fopen(train_out, "w");
      if(!f) perror(train_out);
      fprintf(stdout,"Finished opening %s\n",train_out);
    }
  max_index = gallery[0].feature_count;
  k = 0;
  for(i = 0; i < prob.l; i++)
    {
      if(f != NULL)
	fprintf(f, "%d", gallery[i].id);

      prob.x[i] = &x_space[k];
      prob.y[i] = gallery[i].id;

      for(j = 0; j < gallery[i].feature_count; j++)
	{
	  if(f != NULL)
	    fprintf(f, " %d:%f", j + 1, gallery[i].pca_features[j]);

	  x_space[k].index = j + 1;
	  x_space[k].value = gallery[i].pca_features[j];
	  ++k;
	}

      if(f != NULL)
	fprintf(f, "\n");
      x_space[k++].index = -1;
    }

  if(f != NULL)
    fclose(f);

  if(params->gamma == 0)
    params->gamma = 1.0 / max_index;

  error_msg = svm_check_parameter(&prob, params);

  if(error_msg)
    {
      fprintf(stderr, "Error: %s\n", error_msg);
      exit(1);
    }

  if(cross_validation)
    {
      int    i;
      int    total_correct = 0;
      double total_error   = 0;
      double sumv    = 0, sumy = 0, sumvv = 0, sumyy = 0, sumvy = 0;
      double *target = Malloc(double, prob.l);

      svm_cross_validation(&prob, params, nr_fold, target);

      if(params->svm_type == EPSILON_SVR || params->svm_type == NU_SVR)
	{
	  for(i = 0; i < prob.l; i++)
	    {
	      double y = prob.y[i];
	      double v = target[i];
	      total_error += (v - y) * (v - y);
	      sumv  += v;
	      sumy  += y;
	      sumvv += v * v;
	      sumyy += y * y;
	      sumvy += v * y;
	    }

	  printf("Cross Validation Mean squared error = %g\n", total_error / prob.l);
	  printf("Cross Validation Squared correlation coefficient = %g\n",
		 ((prob.l * sumvy - sumv * sumy) * (prob.l * sumvy - sumv * sumy)) /
		 ((prob.l * sumvv - sumv * sumv) * (prob.l * sumyy - sumy * sumy)));
	}
      else
	{
	  for(i = 0; i < prob.l; i++)
	    if(target[i] == prob.y[i])
	      ++total_correct;

	  printf("Cross Validation Accuracy = %g%%\n", 100.0 * total_correct / prob.l);
	}

      free(target);
    }

  time_t start = time(NULL);

  printf("Training model: "); fflush(stdout);
  model = svm_train(&prob, params);
  printf("done.\n");

  time_t diff = time(NULL) - start;

  svm_save_model(model_name, model);
  svm_destroy_model(model);

  if(diff > 10)
    {
      int min = diff / 60;
      int sec = diff % 60;
      printf("Time spent training:");
      if(min > 0)
	printf(" %d minute%s", min, (min > 1) ? "s" : "");
      if(sec > 0)
	printf (" %d second%s", sec, (sec > 1) ? "s" : "");
      printf("\n");
    }

  free(prob.y);
  free(prob.x);
  free(x_space);
}
