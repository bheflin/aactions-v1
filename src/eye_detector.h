// eye_detector.h
// Copyright 2009 Securics, Inc

#ifndef EYE_DETECTOR_H
#define EYE_DETECTOR_H

#include "cvheaders.h"

void eye_detect(IplImage *input_img, int *x_pos_left, int *x_pos_right, int *y_pos_left, int *y_pos_right, int *x1_pos_left, int *x1_pos_right, int *y1_pos_left, int *y1_pos_right);
#endif
