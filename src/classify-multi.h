// classify-multi.h
// Copyright 2009 Securics, Inc

#ifndef _classify_multi_h_
#define _classify_multi_h_

#include <string>
#include <vector>

#include "svm.h"
#include "structs.h"

using namespace std;

typedef struct classify_onevsrest_t
{
    struct svm_model **models;
    unsigned int num_models;
    int quiet;
    int progress;
} classify_onevsrest;

// classify_onevsrest creation, destruction, and operations
classify_onevsrest* classify_onevsrest_create(int quiet, int progress);
void classify_onevsrest_destroy(classify_onevsrest* self);
int  classify_onevsrest_load(classify_onevsrest* self, const char* file_base);
int classify_onevsrest_classify(classify_onevsrest* self, v1_float* features, int feature_count, std::vector<id_score> &topids);

#endif /* _classify_multi_h_ */
