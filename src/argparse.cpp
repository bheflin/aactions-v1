// argparse.cpp
// Author: Chris Eberle
// Copyright 2009 Securics, Inc
// Because re-inventing the wheel never felt so right

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "argparse.h"
#include <assert.h>
#include <stdlib.h>
#include <fstream>
#include <errno.h>
#include <string.h>

using namespace std;

#define ISSHORT(s) ((s)[0] == '-' && (s)[1] != '-' && (s)[1] != '\0')
#define ISLONG(s) ((s)[0] == '-' && (s)[1] == '-' && (s)[2] != '\0')

// Constructor
ArgParser::ArgParser(usageFunc usage, const char* progName, bool allowExtra)
{
    assert(progName != NULL);

    this->argc = 0;
    this->argv = NULL;
    this->usage = usage;
    this->progName = progName;
    this->allowExtra = allowExtra;
    this->overwrite = false;

    assert(this->usage != NULL);
}

// Convert a string to all lower case
string ArgParser::upperToLower(string str)
{
    for(size_t i = 0; i < str.size(); i++)
    {
        if(str[i] >= 0x41 && str[i] <= 0x5A)
            str[i] = str[i] + 0x20;
    }

    return str;
}

// Add an option to the list
void ArgParser::addOption(const char* longName, bool wantArgument, const char* shortName, const char* defaultVal)
{
    assert(longName != NULL);
        
    string sLongName(longName);
    longArgs[sLongName] = wantArgument;

    if(shortName)
    {
        string sShortName(shortName);
        if(sShortName.size() != 1)
            cerr << "Hey, the short option `" << shortName << "' is a problem." << endl;
        else
            shortArgs[sShortName] = sLongName;
    }

    if(defaultVal)
        defaultValues[sLongName] = defaultVal;
}

// Set the value of an option. If value is NULL, a blank string will be used.
bool ArgParser::setValue(const char* longName, const char* value)
{
    string sValue("");
    assert(longName != NULL);

    // First make sure we're expecting this option
    string sLongName(longName);
    if(longArgs.count(sLongName) == 0)
    {
        cerr << progName << ": Unknown option `" << sLongName << "'" << endl;
        usage(progName.c_str(), 1);
        return false;
    }

    // Check to see if there's a required argument
    if(longArgs[sLongName])
    {
        if(value == NULL)
        {
            cerr << progName << ": Option `" << sLongName << "' expects an argument" << endl;
            usage(progName.c_str(), 1);
            return false;
        }

        sValue = value;
    }

    if(!this->overwrite && arguments.count(sLongName) > 0)
        return true;

    arguments[sLongName] = sValue;
    //cerr << "Set value of " << sLongName << " to '" << arguments[sLongName] << "'" << endl;

    return true;
}

// Get the value of an option. Returns NULL if the option wasn't set.
const char* ArgParser::getValue(const char* longName)
{
    assert(longName != NULL);
    string sLongName(longName);
    if(!isSet(longName))
        return NULL;

    return arguments[sLongName].c_str();
}

// Interpret a value as a boolean. User can specify 1/0 y/n yes/no or true/false.
int ArgParser::getBoolValue(const char* longName)
{
    const char* v = getValue(longName);
    if(v == NULL)
    {
        cerr << progName << ": Invalid value for option `" << longName << "'" << endl;
        exit(1);
    }

    string val(v);
    val = upperToLower(val);

    int num = -1;
    if(val == "yes" || val == "true" || val == "1" || val == "y" || val == "on" || val == "enable" || val == "enabled")
        num = 1;
    else if(val == "no" || val == "false" || val == "0" || val == "n" || val == "off" || val == "disable" || val == "disabled")
        num = 0;

    if(num == -1)
    {
        cerr << progName << ": Invalid value for option `" << longName << "'" << endl;
        exit(1);
    }

    return num;
}

// Return the number of non-option arguments
size_t ArgParser::getExtraSize()
{
    return extraArgs.size();
}

// Get the value of a non-option argument
const char* ArgParser::getExtra(size_t pos)
{
    size_t sz = getExtraSize();
    if(pos >= sz)
        return NULL;

    return extraArgs[pos].c_str();
}

// Determine if the user actually specified an option
bool ArgParser::isSet(const char* longName)
{
    assert(longName != NULL);
    string sLongName(longName);

    if(longArgs.count(sLongName) == 0)
        cerr << "WARNING: Use of undefined option `" << sLongName << "'" << endl;

    return (arguments.count(sLongName) > 0) ? true : false;
}

// Parse a short option (i.e. -n) from the command line
bool ArgParser::parseShortOpt(const char* name, int &pos)
{
    assert(name != NULL);

    string sfName(name);
    string sName = sfName.substr(1,1);

    if(shortArgs.count(sName) == 0)
    {
        cerr << progName << ": Unknown option `-" << sName << "' " << endl;
        usage(progName.c_str(), 1);
        return false;
    }

    pos++;

    string sLongName = shortArgs[sName];
    sName = sfName.substr(2);

    if(longArgs[sLongName] == false)
    {
        // no argument required
        if(sName.size() > 0)
            cerr << "WARNING: Ignoring value given to argument `-" << sfName.substr(1,1) << "'" << endl;
        return setValue(sLongName.c_str(), NULL);
    }
    else if(sName.size() > 0)
    {
        // argument found within same (i.e. "-n35")
        return setValue(sLongName.c_str(), sName.c_str());
    }
    else
    {
        if(pos >= argc)
        {
            if(defaultValues.count(sLongName) > 0)
                return setValue(sLongName.c_str(), defaultValues[sLongName].c_str());

            cerr << progName << ": Option `-" << sName << "' expects an argument" << endl;
            usage(progName.c_str(), 1);
            return false;
        }

        const char* val = argv[pos++];
        if(ISSHORT(val) || ISLONG(val))
        {
            // The next position is a new option, see if there's a default value first.
            if(defaultValues.count(sLongName) > 0)
            {
                pos--;
                return setValue(sLongName.c_str(), defaultValues[sLongName].c_str());
            }
        }

        return setValue(sLongName.c_str(), val);
    }
    
    return false;
}

// Parse a long option (i.e. --option) from the command line
bool ArgParser::parseLongOpt(const char* name, int &pos)
{
    assert(name != NULL);

    string sfName(name);
    string sName = sfName.substr(2);
    string sVal;

    string::size_type found = sName.find_first_of("=");
    if(found != string::npos)
    {
        sVal = sName.substr(found + 1);
        sName = sName.substr(0, found);
    }

    pos++;

    if(longArgs.count(sName) == 0)
    {
        cerr << progName << ": Unknown option `--" << sName << "'" << endl;
        usage(progName.c_str(), 1);
        return false;
    }

    if(longArgs[sName] == false)
    {
        // no argument required
        if(found != string::npos)
            cerr << "WARNING: Ignoring value given to argument `--" << sName << "'" << endl;
        return setValue(sName.c_str(), NULL);
    }
    else if(found != string::npos)
    {
        // argument required and supplied
        return setValue(sName.c_str(), sVal.c_str());
    }
    else if(defaultValues.count(sName) > 0)
    {
        // argument required and default is used
        return setValue(sName.c_str(), defaultValues[sName].c_str());
    }
    else
    {
        // argument required but not given
        cerr << progName << ": Option `--" << sName << "' expects an argument" << endl;
        usage(progName.c_str(), 1);
        return false;
    }

    return false;
}

// Parse a single line of a config file. Comments (using the # char) are allowed.
bool ArgParser::parseConfigLine(string& line)
{
    // First strip any comments
    string::size_type found = line.find_first_of("#");
    string sLine = line.substr(0, found);
    string val;

    if(sLine.size() == 0)
        return true;

    // Now strip whitespace
    found = sLine.find_first_not_of(" \t");
    string sName = (found == string::npos) ? sLine : sLine.substr(found);
    if(sName.size() == 0)
        return true;
    found = sName.find_first_of("=");
    string name = (found == string::npos) ? sName : sName.substr(0, found);
    name = name.substr(0, name.find_first_of(" \t"));
    if(name.size() == 0)
        return true;

    if(name == "%include")
    {
        found = sLine.find_first_not_of(" \t");
        string sVal = (found == string::npos) ? sLine : sLine.substr(found);
        found = sVal.find_first_of(" \t");
        sVal = sVal.substr(found + 1);
        found = sVal.find_first_not_of(" \t");
        sVal = sVal.substr(found);
        found = sVal.find_first_of(" \t");
        sVal = sVal.substr(0, found);

        return parseConfigFile(sVal.c_str(), true);
    }

    if(longArgs.count(name) == 0)
    {
        cerr << progName << ": Unknown config file option `" << name << "'" << endl;
        usage(progName.c_str(), 1);
        return false;
    }

    if(longArgs[name] == false)
    {
        // Don't bother to continue parsing, just use the name
        if(found != string::npos)
            cerr << "WARNING: Ignoring value given to config option `" << name << "'" << endl;
        return setValue(name.c_str(), NULL);
    }
    else if(found != string::npos)
    {
        val = sName.substr(found + 1);
        found = val.find_first_not_of(" \t");
        val = (found == string::npos) ? "" : val.substr(found);
        val = val.substr(0, val.find_first_of(" \t"));

        return setValue(name.c_str(), val.c_str());
    }
    else if(defaultValues.count(name) > 0)
    {
        // argument required and default is used
        return setValue(name.c_str(), defaultValues[name].c_str());
    }
    else
    {
        // argument required, but none given
        cerr << progName << ": Config option `" << name << "' expects an argument" << endl;
        usage(progName.c_str(), 1);
        return false;
    }

    return true;
}

// Parse options from the command line
bool ArgParser::parseCommandLine(int argc, char** argv, bool overwrite)
{
    this->argc = argc;
    this->argv = argv;
    this->overwrite = overwrite;

    int pos = 1;
    string arg;

    while(pos < argc)
    {
        arg = argv[pos];
        if(ISSHORT(argv[pos]))
        {
            if(!parseShortOpt(argv[pos], pos))
                return false;
        }
        else if(ISLONG(argv[pos]))
        {
            if(!parseLongOpt(argv[pos], pos))
                return false;
        }
        else if(allowExtra)
        {
            extraArgs.push_back(arg);
            ++pos;
        }
        else
        {
            cerr << progName << ": Unknown argument `" << arg << "' " << endl;
            usage(progName.c_str(), 1);
            return false;
        }
    }

    return true;
}

// Parse options from a config file
bool ArgParser::parseConfigFile(const char* fileName, bool overwrite)
{
    this->overwrite = overwrite;

    ifstream configFile;
    configFile.open(fileName);
    if(!configFile.is_open())
    {
        cerr << progName << ": Unable to open `" << fileName << "': " << strerror(errno) << endl;
        return false;
    }

    string line;
    while(!configFile.eof())
    {
        getline(configFile, line);
        if(!parseConfigLine(line))
        {
            configFile.close();
            return false;
        }
    }

    configFile.close();
    return true;
}

// Write the current options to a config file
bool ArgParser::writeConfigFile(const char* fileName, const vector<string>& excludes)
{
    ofstream configFile;
    configFile.open(fileName);

    if(!configFile.is_open())
    {
        cerr << progName << ": Unable to open `" << fileName << "': " << strerror(errno) << endl;
        return false;
    }

    string option;
    string value;
    map<string,string>::iterator it;
    vector<string>::const_iterator sit;
    bool print;

    for(it = arguments.begin(); it != arguments.end(); it++)
    {
        print = true;
        option = it->first;
        value = it->second;

        for(sit = excludes.begin(); sit != excludes.end(); sit++)
        {
            if(option == (*sit))
            {
                print = false;
                break;
            }
        }

        if(print)
        {
            if(longArgs[option])
                configFile << option << " = " << value << endl;
            else
                configFile << option << endl;
        }
    }

    configFile.close();
    return true;
}
