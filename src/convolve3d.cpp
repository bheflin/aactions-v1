// convolve3d.cpp
// Author: Chris Eberle
// Copyright 2009 Securics, Inc
//
// 3D convolution

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <math.h>
#include <stdio.h>
#include <limits.h>
#include "convolve3d.h"
#include "iplstr.h"
#include "asciibar.h"

// Macros
#define IMAX(a, b) ((a) > (b) ? (a) : (b))
#define GET_ELEM(mat, x, y, z) (*(((v1_float*)((mat[z])->imageData + (mat[z])->widthStep * y)) + x))

// Prototypes
static int convolve_same(IplImage **src, IplImage **dst, IplImage **kernel, int depth, int kdepth, convolve_bounds *out);
static int convolve_valid(IplImage **src, IplImage **dst, IplImage **kernel, int depth, int kdepth, convolve_bounds *out);
static int validate(IplImage **src, IplImage **dst, IplImage **kernel, unsigned int depth, unsigned int kdepth);

// Public convolve function
int convolve3d(IplImage **src, IplImage **dst, IplImage **kernel, unsigned int depth, unsigned int kdepth, convolve_mode mode, convolve_bounds *out)
{
    int ret = validate(src, dst, kernel, depth, kdepth);

    if(ret != 0)
        return ret;

    // Choose the convolve method based on the mode
    switch(mode)
    {
    case MODE_VALID:
        ret = convolve_valid(src, dst, kernel, depth, kdepth, out);
        break;
    case MODE_SAME:
        ret = convolve_same(src, dst, kernel, depth, kdepth, out);
        break;
    default:

        // Shouldn't happen
        ret = -99;
    }

    return ret;
}

// One gigantic sanity check
static int validate(IplImage **src, IplImage **dst, IplImage **kernel, unsigned int depth, unsigned int kdepth)
{
    unsigned int idx;
    int          width = -1, height = -1, kwidth = -1, kheight = -1;

    // Just a whole lot of sanity checks
    if(src == NULL || dst == NULL || kernel == NULL)
        return -1;

    if(depth < 1 || kdepth < 1)
        return -2;

    // Check the sanity of src and dst
    for(idx = 0; idx < depth; ++idx)
    {
        if(src[idx] == NULL || dst[idx] == NULL)
            return -3;

        if(idx == 0)
        {
            width  = src[idx]->width;
            height = src[idx]->height;
        }
        else
        {
            if(src[idx]->width != width || src[idx]->height != height ||
               dst[idx]->width != width || dst[idx]->height != height)
                return -5;
        }

        if(src[idx]->depth != IPL_DEPTH_V1 ||
           dst[idx]->depth != IPL_DEPTH_V1)
            return -6;

        if(src[idx]->nChannels != 1 ||
           dst[idx]->nChannels != 1)
            return -7;
    }

    // Check the sanity of kernel
    for(idx = 0; idx < kdepth; ++idx)
    {
        if(kernel[idx] == NULL)
            return -3;

        if(idx == 0)
        {
            kwidth  = kernel[idx]->width;
            kheight = kernel[idx]->height;

            if((kwidth * kheight) > (width * height))
                return -4;
        }
        else if(kernel[idx]->width != kwidth || kernel[idx]->height != kheight)
            return -5;

        if(kernel[idx]->depth != IPL_DEPTH_V1)
            return -6;

        if(kernel[idx]->nChannels != 1)
            return -7;
    }

    return 0;
}

// NOTE: We can assume that the depth is 32F and the number of channels is 1.
// TODO: Burn this thing with fire. It is slooooow.
// Unfortunately there don't seem to be too many good *fast* 3d convolve
// algorithms out there that don't rely on FFTs, so unless we want to go
// that direction, here is a nice and slow (but correct) 3d convolve.
//
// Sorry about the data types, it seems that we could have designed a
// better data type to encapsulate a 3d array, but for now this will
// have to do.
static int convolve_same(IplImage **src, IplImage **dst, IplImage **kernel, int depth, int kdepth, convolve_bounds *out)
{
    v1_float sum;

    int sheight = src[0]->height;
    int swidth  = src[0]->width;
    int kheight = kernel[0]->height;
    int kwidth  = kernel[0]->width;

    int edgL    = (kdepth - 1) / 2;
    int edgM    = (kheight - 1) / 2;
    int edgN    = (kwidth - 1) / 2;

#ifdef DEBUG

    // It takes for-friggin ever, so here's a progress bar
    struct ascii_bar_params params;
    init_ascii_bar(&params, 0.0, (float)depth, 1.0f);
    params.len = 65;
    printf("Convolve 3D: ");
    show_ascii_bar(&params);
#endif

    // Oh yeah, O(n^6). Sit back and make some popcorn.
    for(int sz = 0, mz = depth - 1; sz < depth; sz++, mz--)
    {
        for(int sy = 0; sy < sheight; sy++)
        {
            for(int sx = 0; sx < swidth; sx++)
            {
                sum = 0.0f;

                for(int kzb = kdepth - 1 - IMAX(0, edgL - sz), kza = IMAX(0, sz - edgL);
                    kzb >= 0 && kza < depth; kzb--, kza++)
                {
                    for(int kyb = kheight - 1 - IMAX(0, edgM - sy),
                        kya = IMAX(0, sy - edgM);
                        kyb >= 0 && kya < sheight; kyb--, kya++)
                    {
                        for(int kxb = kwidth - 1 - IMAX(0, edgN - sx),
                            kxa = IMAX(0, sx - edgN);
                            kxb >= 0 && kxa < swidth; kxb--, kxa++)
                            sum += GET_ELEM(src, kxa, kya, kza) * GET_ELEM(kernel, kxb, kyb, kzb);
                    }
                }

                *(((v1_float*)((dst[mz])->imageData + (dst[mz])->widthStep * sy)) + sx) = sum;
            }
        }

#ifdef DEBUG

        // Update the progress bar
        step_ascii_bar(&params);
#endif
    }

#ifdef DEBUG
    printf("\n");
#endif

    if(out)
    {
        out->startX = 0;
        out->startY = 0;
        out->startZ = 0;
        out->endX   = swidth - 1;
        out->endY   = sheight - 1;
        out->endZ   = depth - 1;
    }

    return 0;
}

// A convolve method which trims the edges of a convolve_same
// leaving only the values that were not interpolated.
//
// Unlike the 2d version of this function, this one can't simply set
// the ROI. Instead I've gone ahead and created a convolve_bounds
// struct whose members are populated with the inclusive min and max
// indices of the valid items in a convolution.
static int convolve_valid(IplImage **src, IplImage **dst, IplImage **kernel, int depth, int kdepth, convolve_bounds *out)
{
    if(out == NULL)
        return -8;

    int ret = convolve_same(src, dst, kernel, depth, kdepth, NULL);

    if(ret < 0)
        return ret;

    int kwidth  = kernel[0]->width;
    int kheight = kernel[0]->height;
    int swidth  = src[0]->width;
    int sheight = src[0]->height;

    // Calculate how much to trim from the edges
    int trim_w = (int)v1_floor((v1_float)kwidth / 2.0);
    int trim_h = (int)v1_floor((v1_float)kheight / 2.0);
    int trim_d = (int)v1_floor((v1_float)kdepth / 2.0);

    // Calculate the new bounds
    out->startX = trim_w;
    out->startY = trim_h;
    out->startZ = trim_d + 1;
    out->endX   = swidth - 1 - trim_w;
    out->endY   = sheight - 1 - trim_h;
    out->endZ   = (depth - 1 - trim_d) + 1;

    // Make sure we have enough to trim
    // TODO: Should this return failure? Is it enough to simply return
    // the image without setting the ROI?
    if(out->startZ > out->endZ)
        out->startZ = out->endZ;

    if(out->startX > out->endX ||
       out->startY > out->endY)
    {
        printf("Out of bounds!!\n");
        printf("--> (%d:%d, %d:%d, %d:%d)\n", out->startX, out->endX,
               out->startY, out->endY, out->startZ, out->endZ);

        out->startX = -1;
        out->startY = -1;
        out->startZ = -1;
        out->endX   = -1;
        out->endY   = -1;
        out->endZ   = -1;
        return -9;
    }

    return 0;
}

// This thing cheats like hell. DO NOT GIVE IT DIFFERENT KERNELS.
// IT WILL FAIL!!! Basically this does a "valid mode" convolve, and it
// assumes up front that the "valid" portion of the data will be
// confined to MxNx1 (since this is what happens in the python
// code). If we assume this, we can avoid processing 95 other layers
// and just give back the valid data, shaving many minutes off of
// processing time. Also since this is supposed to be "valid mode",
// this just trims the final image down rather than setting the ROI
// since... let's face it, no one in their right mind is going to try
// to allocate memory for this thing up-front. If that comment makes
// no sense, then read the comment for convolve2d_valid.
void fast_3d_convolve(IplImage **src, IplImage **dst, IplImage **kernel, int depth, int kdepth)
{
    v1_float sum;

    int sheight = src[0]->height;
    int swidth  = src[0]->width;
    int kheight = kernel[0]->height;
    int kwidth  = kernel[0]->width;

    int edgL    = (kdepth - 1) / 2;
    int edgM    = (kheight - 1) / 2;
    int edgN    = (kwidth - 1) / 2;

    // Calculate how much to trim from the edges
    int sz = (int)v1_ceil((v1_float)kdepth / 2.0) - 1;

    *dst = NULL;

    if(kdepth != depth) // HACK
        return;

    int start_x = (int)v1_floor((v1_float)kwidth / 2.0);
    int start_y = (int)v1_floor((v1_float)kheight / 2.0);
    int nwidth  = swidth - (start_x * 2);
    int nheight = sheight - (start_y * 2);

    *dst = cvCreateImage(cvSize(nwidth, nheight), IPL_DEPTH_V1, 1);

    for(int sy = 0, ty = -start_y; sy < sheight; sy++, ty++)
    {
        for(int sx = 0, tx = -start_x; sx < swidth; sx++, tx++)
        {
            if(tx >= 0 && tx < nwidth && ty >= 0 && ty < nheight)
            {
                sum = 0.0f;

                for(int kzb = kdepth - 1 - IMAX(0, edgL - sz), kza = IMAX(0, sz - edgL);
                    kzb >= 0 && kza < depth; kzb--, kza++)
                {
                    for(int kyb = kheight - 1 - IMAX(0, edgM - sy),
                        kya = IMAX(0, sy - edgM);
                        kyb >= 0 && kya < sheight; kyb--, kya++)
                    {
                        for(int kxb = kwidth - 1 - IMAX(0, edgN - sx),
                            kxa = IMAX(0, sx - edgN);
                            kxb >= 0 && kxa < swidth; kxb--, kxa++)
                            sum += GET_ELEM(src, kxa, kya, kza) * GET_ELEM(kernel, kxb, kyb, kzb);
                    }
                }

                *(((v1_float*)((*dst)->imageData + (*dst)->widthStep * ty)) + tx) = sum;
            }
        }
    }
}
