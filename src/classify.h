// classify.h
// Copyright 2009 Securics, Inc

#ifndef CLASSIFY_H
#define CLASSIFY_H

#include <map>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>

#include "structs.h"
#include "metarecog.h"
#include "svm.h"

int classify(v1_float* features, int feature_count, struct svm_model *model, std::vector<id_score> &topids);

#endif
