// v1s_funcs.h
// Copyright 2009 Securics, Inc

#ifndef V1S_FUNCS
#define V1S_FUNCS

#include "cvheaders.h"
#include "convolve2d.h"
#include "convolve3d.h"

void v1s_norm(IplImage** hin,IplImage** hout, int kh, int kw, v1_float threshold, int hin_d);
IplImage** v1s_dimr(IplImage** hin, int lsum_ksize, int outh, int outw, int hin_d);
IplImage** sresample(IplImage** src, int outh, int outw, int hin_d);

#endif
