#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/stat.h>
#include <string.h>
#include <libgen.h>
#include <vector>
#include "v1.h"
#include "structs.h"
#include "pca.h"
#include "asciibar.h"
#include "gabor.h"
#include "utils.h"
#include "geo.h"
#include "version.h"
#include "sqi.h"
#include "argparse.h"
#include "eye_detector.h"
#include "classify.h"
#include "facedetect.h"
#include "parser.h"

using namespace std;

// Structs
typedef struct validate_prog_params_t
{
    char* prog_name;
    char* file_name;
    char* pca_fname;
    char* svm_model_name;
    char* train_fname;
    char* sphere_fname;
    char* easy_base;
    char* loadfilt_dir;
    char* failmodel_name;
    char* failpredict_file;
    int   fast_gabor;
    int   show_progress;
    int   use_python;
    int   geo_norm;
    int   hist_norm;
    int   sqi_norm;
    int   quiet;
    int   use_eyedetect;
    int crop_face;
} validate_prog_params;

typedef struct recogvars_t
{
    struct svm_model *model;
    int total;
    int correct;
    int face_fails;
    int face_detect_fails;
    CvMat* pca_training;
    MlClassification *fpmodel;
    FILE* fTrainFile;
    v1_float* mean;
    v1_float* std;
    void** filters;
    v1_float* features;
    int fp_fail_correct;
    int fp_fail_incorrect;
    int fp_pass_correct;
    int fp_pass_incorrect;
    CvHaarClassifierCascade* cascade;
    CvMemStorage* storage;

} recogvars;



typedef struct probe_entry
{
  char* path;
  char* second_path;
  int ground_truth;
  int leyeX_file1;
  int leyeY_file1;
  int reyeX_file1;
  int reyeY_file1;

  int leyeX_file2;
  int leyeY_file2;
  int reyeX_file2;
  int reyeY_file2;

} probe_entry;

//global vars
static const char* haarfile = "haarcascade_frontalface_alt2.xml";
vector<probe_entry> probes;

//function signitures
static void usage(const char* name, int val);
static void fatal(const char* msg, ...);
static void parseCommandLine(int argc, char **argv, validate_prog_params *parm);
static void eye_detect_gallery(gallery_entry* gallery, int gallery_size, int thresh, int use_python);
static int  parseFile(char* file);
static void combine_features(v1_float* features_one, v1_float* features_two, v1_float* combined, int features);

// Macros
#define SAFE_RELEASE(img) do { if ((img) != NULL) { cvReleaseImage(&(img)); (img) = NULL; } } while(0);
#define SAFE_FREE(ptr)    do { if ((ptr) != NULL) { free((ptr)); (ptr) = NULL; } } while(0);
#define SAFE_DELETE(ptr)  do { if ((ptr) != NULL) { delete (ptr); (ptr) = NULL; } } while(0);

////////////////////////////////////////////////////////////////////////////////
// General utility functions
////////////////////////////////////////////////////////////////////////////////
/**
 * Allocate memory, or exit if none is available
 * @param size The number of bytes to malloc
 * @return The allocated memory
 */
static inline void* safe_malloc(size_t size)
{
    void* ptr = NULL;
    if((ptr = malloc(size)) == NULL)
        fatal("Fatal: safe_malloc failed.\n");
    return ptr;
}


/**
 * Print a message to stderr and exit
 * @param const char* msg The message to print
 */
static void fatal(const char* msg, ...)
{
    //    va_list ap;
    //    va_start(ap, msg);
    //    vfprintf(stderr, msg, ap);
    //    va_end(ap);

    exit(1);
}


static void combine_features(v1_float* features_one, v1_float* features_two, v1_float* combined, int features)
{
    int j;
    for(j = 0; j < features; j++)
    {
      combined[j] = (v1_float)pow((features_one[j]-features_two[j]),2.0);
    }
}
static void usage(const char* name, int val)
{
    fprintf(stderr, "Usage: %s [OPTIONS]\n"
            "General options:\n"
            "  -i, --info            Print out file versions and exit.\n"
            "  -h, --help            Show this helpful message.\n"
            "  -C, --config=FILE      Load extra configuration options from FILE.\n"
            "  -o, --save-config=FILE Dump all configuration options to FILE.\n"
            "V1 validation training options:\n"
            "  -f, --file=FILE       Probe File with ground truth.\n"
            "  -b, --base=NAME       Use easy file mode where -p, -s, and -v are\n"
            "                        automatically determined using NAME as the base.\n"
            "  -p, --pca=FILE        PCA subspace file. Required unless -b specified.\n"
            "  -s, --svm=FILE        SVM training file. Required unless -b specified.\n"
            "  -v, --value=FILE      Sphered values file. Required unless -b specified.\n"
            , name);
    exit(val);
}

//This is a really simple file reader that is inteded to get program going fast it expects prenormalized images and format is
// 1 path path for match
// -1 path path for non match
static int parseFile(char* file_name)
{
    FILE* file;
    char line_str[1024];
    char* line_no_newline;
    probe_entry probe;
    IplImage* check;
    vector<string> tokens;
    file = fopen(file_name,"r");
    if(!file)
    {
        printf("ERROR INPUT FILE NOT FOUND\n");
        return -1;
    }
    while(fgets(line_str, sizeof(line_str), file) !=NULL)
    {
        int len = strlen(line_str);
        line_no_newline = (char*) malloc((len) * sizeof(char));
        memcpy(line_no_newline, line_str, (len-1)*sizeof(char));
        line_no_newline[len-1] = '\0';
        tokenize(line_no_newline, tokens);
        SAFE_FREE(line_no_newline);
        if(tokens.size() != 3 && tokens.size() !=11)
        {
            printf("INVALID LINE %s\n Need either all ground truth or no ground truth\n", line_str);
            continue;
        }
        check = cvLoadImage(tokens[1].c_str());
      if(!check)
      {
          printf("Line %s is invalid\n",line_str); 
          continue;
      }
      else
      {
          cvReleaseImage(&check);
          check = cvLoadImage(tokens[2].c_str());
          if(!check)
          {
              printf("Line %s is invalid\n",line_str); 
              continue;
          }
          cvReleaseImage(&check);
          char* path;
	  char* second_path;
  
          probe.ground_truth = atoi(tokens[0].c_str());
          probe.path = strdup(tokens[1].c_str());
          probe.second_path = strdup(tokens[2].c_str());
          if(tokens.size() ==3)
          {
              probe.leyeX_file1 = -1;
              probe.leyeY_file1 = -1;
              probe.reyeX_file1 = -1;
              probe.reyeY_file1 = -1;
              
              probe.leyeX_file2 = -1;
              probe.leyeY_file2 = -1;
              probe.reyeX_file2 = -1;
              probe.reyeY_file2 = -1;
          }
          else
          {
              probe.leyeX_file1 = atoi(tokens[3].c_str());
              probe.leyeY_file1 = atoi(tokens[4].c_str());
              probe.reyeX_file1 = atoi(tokens[5].c_str());
              probe.reyeY_file1 = -atoi(tokens[6].c_str());
              
              probe.leyeX_file2 = atoi(tokens[7].c_str());
              probe.leyeY_file2 = atoi(tokens[8].c_str());
              probe.reyeX_file2 = atoi(tokens[9].c_str());
              probe.reyeY_file2 = atoi(tokens[10].c_str());

          }
          probes.push_back(probe);
      }
      tokens.clear();
    }

    fclose(file);
    return probes.size();
}



static void parseCommandLine(int argc, char **argv, validate_prog_params *parm)
{
    argv[0] = basename(argv[0]);

    // Set the default values
    parm->prog_name         = argv[0];
    parm->file_name        = NULL;
    parm->pca_fname        = NULL;
    parm->sphere_fname     = NULL;
    parm->svm_model_name   = NULL;
    parm->easy_base        = NULL;
    parm->loadfilt_dir     = NULL;
    parm->failpredict_file = NULL;
    parm->failmodel_name   = NULL;
    parm->fast_gabor       = 1;
    parm->show_progress    = 1;
    parm->geo_norm         = 0;
    parm->use_eyedetect   = 0;
    parm->crop_face        = 0;
    parm->hist_norm        = 0;
    parm->use_python       = 0;
    parm->quiet            = 0;

    ArgParser ap(usage, argv[0], false);

    // Simple switches
    ap.addOption("help", false, "h");
    ap.addOption("info", false, "i");

    // Regular options
    ap.addOption("config",       true, "C");
    ap.addOption("file",         true, "f");
    ap.addOption("pca",          true, "p");
    ap.addOption("value",        true, "v");
    ap.addOption("svm",          true, "s");
    ap.addOption("base",         true, "b");
    ap.addOption("filter-dir",   true, "d");
    ap.addOption("fp-save",      true, "O");
    ap.addOption("fp-model",     true, "F");
    ap.addOption("save-config",  true, "o");
    // Boolean options (rightmost column indicates that "-G" is the same as "-G true")
    ap.addOption("geo-norm",       true, "G", "true");
    ap.addOption("eye-detect",     true, "e", "true");
    ap.addOption("hist-norm",      true, "H", "true");
    ap.addOption("crop-face",      true, "c", "true");
    ap.addOption("groundtruth",    true, "g", "true");
    ap.addOption("python-resize",  true, "P", "true");
    ap.addOption("fast-gabor",     true, "S", "true");
    ap.addOption("progress",       true, "r", "true");
    ap.addOption("normalize-SQI",  true, "B", "true");
    ap.addOption("fusion-type",    true, "Y", "true");
    ap.addOption("quiet",          true, "q", "true");

    if(!ap.parseCommandLine(argc, argv, true))
        exit(1);

    if(ap.isSet("help"))
        usage(parm->prog_name, 0);

    if(ap.isSet("info"))
    {
        printFileVersions(stdout);
        exit(0);
    }

    // Parse the config file if specified
    if(ap.isSet("config"))
    {
        // The last option means that the command line always gets
        // preference if there is a conflict.
        if(!ap.parseConfigFile(ap.getValue("config"), false))
            exit(1);
    }

    // Extract the command line options
    if(ap.isSet("file")) parm->file_name = strdup(ap.getValue("file"));
    if(ap.isSet("pca")) parm->pca_fname = strdup(ap.getValue("pca"));
    if(ap.isSet("value")) parm->sphere_fname = strdup(ap.getValue("value"));
    if(ap.isSet("svm")) parm->svm_model_name = strdup(ap.getValue("svm"));
    if(ap.isSet("base")) parm->easy_base = strdup(ap.getValue("base"));
    if(ap.isSet("filter-dir")) parm->loadfilt_dir = strdup(ap.getValue("filter-dir"));
    if(ap.isSet("fp-model")) parm->failmodel_name = strdup(ap.getValue("fp-model"));
    if(ap.isSet("progress")) parm->show_progress = ap.getBoolValue("progress");
    if(ap.isSet("geo-norm")) parm->geo_norm = ap.getBoolValue("geo-norm");
    if(ap.isSet("fast-gabor")) parm->fast_gabor = ap.getBoolValue("fast-gabor");
    if(ap.isSet("eye-detect")) parm->use_eyedetect = ap.getBoolValue("eye-detect");
    if(ap.isSet("hist-norm")) parm->hist_norm = ap.getBoolValue("hist-norm");
    if(ap.isSet("crop-face")) parm->crop_face = ap.getBoolValue("crop-face");
    if(ap.isSet("python-resize")) parm->use_python = ap.getBoolValue("python-resize");
    if(ap.isSet("normalize-SQI")) parm->sqi_norm = ap.getBoolValue("normalize-SQI");
    if(ap.isSet("quiet")) parm->quiet = ap.getBoolValue("quiet");

    if(ap.isSet("fp-save"))
    {
        parm->failpredict_file = strdup(ap.getValue("fp-save"));
    }

    if(parm->quiet)
        parm->show_progress = 0;
    if(ap.isSet("save-config"))
    {
        vector<string> excludes;
        excludes.push_back("help");
        excludes.push_back("info");
        excludes.push_back("config");
        excludes.push_back("save-config");
        if(!ap.writeConfigFile(ap.getValue("save-config"), excludes))
            exit(1);
        printf("All options saved to %s.\n", ap.getValue("save-config"));
        exit(0);
    }


}

IplImage* crop_face(IplImage* img, recogvars vars, bool crop)
{
  //no need to crop again
  CvRect r = detect_face(vars.cascade, vars.storage, img);
  if(r.x < 0 || r.y < 0 || r.width < 0 || r.height < 0 )
    {
      printf("WARNING: Face detection failed, skipping.\n\n");
      vars.face_detect_fails++;
      return NULL;
    }

  if(crop)
    {
      IplImage* cropped = cropIpl(img, r.x, r.y,
				  r.width, r.height);
      SAFE_RELEASE(img);
      if(cropped == NULL)
        {
	  printf("WARNING: Crop to ROI failed, skipping.\n\n");
	  return NULL;
        }

      return cropped;
    }
  return NULL;
  
}
int recognize(struct probe_entry probe, validate_prog_params params, recogvars vars)
{
    IplImage* img;
    int lbl;
    int out_size = 0;
    v1_float* features_one;
    v1_float* features_two;
    v1_float* combined;
    v1_float* hold = NULL;
    int full_size = WINDOW_SIZE*WINDOW_SIZE*GABOR_FEATURES;
    features_one = (v1_float*)malloc(full_size * sizeof(v1_float));
    features_two = (v1_float*)malloc(full_size * sizeof(v1_float));
    combined = (v1_float*)malloc(full_size * sizeof(v1_float));
    img = cvLoadImage(probe.path,0);
    if(!img)
    {
        printf("VALIDATION FAILED\n");
        free(features_one);
        free(features_two);
        free(combined);
        return -100;
        
    }
    if(params.crop_face)
    {
        IplImage* face = crop_face(img, vars, true);
        if(face !=NULL)
        {
            cvReleaseImage(&img);
            img = face;
        }
    }
    if(params.use_eyedetect)
    {
        IplImage* resized = get_image(img, 150, params.use_python, 1);
        int lx, rx, ly, ry;
	int lx1, rx1, ly1, ry1;
        eye_detect(resized, &lx, &rx, &ly, &ry, &lx1, &rx1, &ly1, &ry1);

        // Now scale the points back to their proper size
        double scaleX = (double)(img->width) / (double)(resized->width);
        double scaleY = (double)(img->height) / (double)(resized->height);
        lx = (int)((double)lx * scaleX);
        ly = (int)((double)ly * scaleY);
        rx = (int)((double)rx * scaleX);
        ry = (int)((double)ry * scaleY);
        probe.leyeX_file1 = lx;
        probe.leyeY_file1 = ly;
        probe.reyeX_file1 = rx;
        probe.reyeY_file1 = ry;
        SAFE_RELEASE(resized);
    }
  if(params.geo_norm)
    {
      IplImage* geo = perform_geo_norm(img, probe.leyeX_file1, probe.leyeY_file1, probe.reyeX_file1,probe.reyeY_file1, params.hist_norm, 1);
      if(!geo)
	{
	  printf("GEO NORMALIZATION FAILED");
	}
      else
	{
	  cvReleaseImage(&img);
	  img = geo;      
	}
    }
  if(params.sqi_norm)
    {
      IplImage* sqi = securics_sqi_norm(img);
      if(!sqi)
	{
	  printf("sqi NORMALIZATION FAILED");
	}
      else
	{
	  cvReleaseImage(&img);
	  img = sqi;      
	}

    }

    get_features(img, features_one, vars.filters, params.fast_gabor, 1, params.use_python, 1);
    cvReleaseImage(&img);
    //Get second set
    img = cvLoadImage(probe.second_path,0);
    if(!img)
    {
        printf("VALIDATION FAILED\n");
        free(features_one);
        free(features_two);
        free(combined);
        return -100;
    }
    if(params.crop_face)
    {
        IplImage* face = crop_face(img, vars, true);
        if(face !=NULL)
        {
            cvReleaseImage(&img);
            img = face;
        }

    }
    if(params.use_eyedetect)
    {
        IplImage* resized = get_image(img, 150, params.use_python, 1);
        int lx, rx, ly, ry;
	int lx1, rx1, ly1, ry1;
        eye_detect(resized, &lx, &rx, &ly, &ry, &lx1, &rx1, &ly1, &ry1);

        // Now scale the points back to their proper size
        double scaleX = (double)(img->width) / (double)(resized->width);
        double scaleY = (double)(img->height) / (double)(resized->height);
        lx = (int)((double)lx * scaleX);
        ly = (int)((double)ly * scaleY);
        rx = (int)((double)rx * scaleX);
        ry = (int)((double)ry * scaleY);
        probe.leyeX_file2 = lx;
        probe.leyeY_file2 = ly;
        probe.reyeX_file2 = rx;
        probe.reyeY_file2 = ry;
        SAFE_RELEASE(resized);
    }
  if(params.geo_norm)
    {
      IplImage* geo = perform_geo_norm(img, probe.leyeX_file1, probe.leyeY_file1, probe.reyeX_file1,probe.reyeY_file1, params.hist_norm, 1);

      if(!geo)
	{
	  printf("GEO NORMALIZATION FAILED");
	}
      else
	{
	  cvReleaseImage(&img);
	  img = geo;      
	}
    }
  if(params.sqi_norm)
    {
      IplImage* sqi = securics_sqi_norm(img);
      if(!sqi)
	{
	  printf("sqi NORMALIZATION FAILED");
	}
      else
	{
	  cvReleaseImage(&img);
	  img = sqi;      
	}

    }
    get_features(img, features_two, vars.filters, params.fast_gabor, 1, params.use_python, 1);
    cvReleaseImage(&img);
    combine_features(features_one, features_two, combined,full_size);
    // Run recognition
    sphere_probe(combined, full_size, vars.mean, vars.std);

    hold = projectProbe(vars.pca_training, combined, full_size, &out_size);
    vector<id_score> top;
    lbl = classify(hold, out_size, vars.model, top);
    free(hold);
    free(features_one);
    free(features_two);
    free(combined);
    return lbl;
}

////////////////////////////////////////////////////////////////////////////////
// Program initialization and uninitialization
////////////////////////////////////////////////////////////////////////////////
/**
 *Load training, allocate memory, check parameters, etc
 *@param prog_params &params The program parameters
 *@param recogvars &vars The recognition variables.
 */
static void init(validate_prog_params &params, recogvars &vars)
{
    vars.total      = 0;
    vars.correct    = 0;
    vars.face_fails = 0;
    vars.face_detect_fails = 0;
    vars.fpmodel    = NULL;
    vars.fTrainFile = NULL;
    vars.mean       = NULL;
    vars.std        = NULL;
    vars.filters    = NULL;
    vars.pca_training = NULL;
    vars.features   = NULL;
    vars.model      = NULL;
    vars.fp_fail_correct   = 0;
    vars.fp_fail_incorrect = 0;
    vars.fp_pass_correct   = 0;
    vars.fp_pass_incorrect = 0;
    vars.storage = NULL;
    vars.cascade= NULL;
    int error = 0;

    if(params.easy_base && !error)
    {
        char tname[1024];
        if(params.pca_fname == NULL)
        {
            snprintf(tname, 1024, "%s.pca", params.easy_base);
            params.pca_fname = strdup(tname);
        }

        if(params.svm_model_name == NULL)
        {
	  snprintf(tname, 1024, "%s.svm", params.easy_base);
	  params.svm_model_name = strdup(tname);
	}

        if(params.sphere_fname == NULL)
        {
            snprintf(tname, 1024, "%s.val", params.easy_base);
            params.sphere_fname = strdup(tname);
        }
    }

    if(params.pca_fname == NULL)
        ++error;

    if(params.sphere_fname == NULL)
        ++error;

    if(params.svm_model_name == NULL)
        ++error;

    if(error)
        usage(params.prog_name, 1);

    if(params.loadfilt_dir != NULL)
        params.fast_gabor = 1;


    //    printSummary(&params);
    printf("Initializing: "); fflush(stdout);

    // Parse the value file
    FILE* f = fopenOrError(params.sphere_fname, "r");
    if(!f) exit(1);

    size_t sz = WINDOW_SIZE*WINDOW_SIZE*GABOR_FEATURES;
    vars.mean     = (v1_float*)safe_malloc(sz * sizeof(v1_float));
    vars.std      = (v1_float*)safe_malloc(sz * sizeof(v1_float));
    vars.features = (v1_float*)safe_malloc(sz * sizeof(v1_float));

    unsigned int i = 0;
#ifdef USE_DOUBLE
    while(fscanf(f, "%lf %lf", &(vars.mean[i]), &(vars.std[i])) !=EOF)
#else
    while(fscanf(f, "%f %f", &(vars.mean[i]), &(vars.std[i])) !=EOF)
#endif
    {
        if(i >= sz) fatal("\nOverflow detected, goodbye!\n");
        i++;
    }

    fclose(f);

    // Load the haar classifier if necessary
    // Load the haar classifier if necessary
    if(params.crop_face)
    {
        vars.cascade = (CvHaarClassifierCascade*)cvLoad(haarfile, 0, 0, 0);
        if(vars.cascade == NULL)
            fatal("\nError: Unable to load haar classifier: `%s'\n", haarfile);
        vars.storage = cvCreateMemStorage(0);
    }


    // Load the failure prediction model
    if(params.failmodel_name != NULL)
    {
        vars.fpmodel = new MlClassification();
        if(vars.fpmodel->LoadModel(params.failmodel_name) == false)
            fatal("\nUnable to load model file: %s\n", params.failmodel_name);
    }

    // Open the failpredict file for writing
    if(params.failpredict_file)
    {
        if((vars.fTrainFile = fopenOrError(params.failpredict_file, "w")) == NULL)
            exit(1);
    }

    // Generate the gabor filters
    if(params.loadfilt_dir != NULL)
    {
        vars.filters = (void**)load_filters(params.loadfilt_dir);
    }
    else
    {
        if(params.fast_gabor)
            vars.filters = (void**)make_filters();
        else
            vars.filters = (void**)make_ipl_filters();
    }

    if(vars.filters == NULL)
        fatal("\nError: nUnable to create gabor filters.\n");


    printf("done.\n");

    printf("Load PCA: "); fflush(stdout);

    // Load the PCA training data, may take a while
    if((vars.pca_training = loadTrainingData(params.pca_fname, params.show_progress)) == NULL)
        fatal("\nError: Unable to load PCA training.\n");

    printf("done.\n");

    // Load the main V1 SVM training file
    printf("Load SVM: "); fflush(stdout);
    if((vars.model = svm_load_model(params.svm_model_name)) == NULL)
        fatal("\nUnable to load model file: %s\n", params.svm_model_name);
    printf("done.\n\n");

}
/**
 * Free any allocated memory
 *@param prog_params &params The program parameters
 *@param recogvars &vars The recognition variables.
 */
static void uninit(validate_prog_params &params, recogvars &vars)
{

    if(vars.fTrainFile)
    {
        fclose(vars.fTrainFile);
        vars.fTrainFile = NULL;
    }

    if(vars.cascade != NULL)
    {
        cvReleaseHaarClassifierCascade(&(vars.cascade));
        vars.cascade = NULL;
    }

    if(vars.storage != NULL)
    {
        cvReleaseMemStorage(&(vars.storage));
        vars.storage = NULL;
    }

    if(vars.filters != NULL)
    {
        if(params.fast_gabor)
            free_filters((gabor_filter**)vars.filters);
        else
            free_ipl_filters((IplImage**)vars.filters);
        vars.filters = NULL;
    }

    SAFE_FREE(vars.mean);
    SAFE_FREE(vars.std);
    SAFE_FREE(vars.features);
    SAFE_DELETE(vars.fpmodel);

    if(vars.pca_training != NULL)
    {
        cvReleaseMat(&(vars.pca_training));
        vars.pca_training = NULL;
    }

    if(vars.model != NULL)
    {
        svm_destroy_model(vars.model);
        vars.model = NULL;
    }


    if(vars.fTrainFile != NULL)
    {
        fclose(vars.fTrainFile);
        vars.fTrainFile = NULL;
    }

    SAFE_FREE(params.file_name);
    SAFE_FREE(params.pca_fname);
    SAFE_FREE(params.sphere_fname);
    SAFE_FREE(params.svm_model_name);
    SAFE_FREE(params.easy_base);
    SAFE_FREE(params.loadfilt_dir);
    SAFE_FREE(params.failpredict_file);
    SAFE_FREE(params.failmodel_name);
}


int main(int argc, char** argv)
{
    validate_prog_params params;
    recogvars vars;
    int number_probes;
    int i;
    int error = 0;
    int correct = 0;
    int feat_size = WINDOW_SIZE * WINDOW_SIZE * GABOR_FEATURES;
    parseCommandLine(argc, argv, &params);
    parseFile(params.file_name);
    if(probes.size() > 0)
      {
	init(params, vars);
	float percent;
	int total;
	for(i = 0; i < probes.size(); i++)
	  {
	    int returned = recognize(probes[i], params, vars);
	    if(returned != -100)
	      {
		printf("%s %s ", probes[i].path, probes[i].second_path);
		if(returned == probes[i].ground_truth)
		  {
		    printf("--- CORRECT\n");
		    correct++;
		  }
		else
		  {
		    printf("--- INCORRECT\n");
		  }
		total = i+1;
		percent = 100.0*((float)correct/(float)(total));
		printf("Correct: %d Total: %d Percent: %f\n", correct, total, percent);
	      }
	  }
	percent = (float)correct/(float)(probes.size());
	printf("Correct: %d Total: %d Percent: %f\n", correct, probes.size(), percent);
	uninit(params, vars);    
	//free the probe names
	for(i = 0; i < probes.size(); i++)
	  {
	    SAFE_FREE(probes[i].path);
	    SAFE_FREE(probes[i].second_path);
	  }
      }
    return 0;
}
