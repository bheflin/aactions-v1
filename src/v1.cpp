// v1.cpp
// Copyright 2009 Securics, Inc

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "v1.h"
#include "iplstr.h"
#include "v1s_funcs.h"
#include "utils.h"

/**
 *Initilaze memory for program including all images
 *This cannot be done unless you remove dynamic feature reduction and set up all gabors willing to use at the time
 * @param struct gallery_entry ** gallery The gallery to initialize
 * @param int gallery_size The size of the gallery
 */
void init(struct gallery_entry **gallery, int gallery_size)
{
    int i = 0;
    *gallery = (struct gallery_entry*)malloc(gallery_size * sizeof(struct gallery_entry));
    for(i = 0; i < gallery_size; i++)
    {
        //for resample 30x30x96
        (*gallery)[i].features = (v1_float*)malloc(WINDOW_SIZE*WINDOW_SIZE*GABOR_FEATURES*sizeof(v1_float));
        (*gallery)[i].pca_features = NULL;
        (*gallery)[i].path = NULL;
        (*gallery)[i].id = 0;
        (*gallery)[i].feature_count = 0;
        (*gallery)[i].leyeX = -1;
        (*gallery)[i].leyeY = -1;
        (*gallery)[i].reyeX = -1;
        (*gallery)[i].reyeY = -1;
    }
}

/**
 *free all memory allocated in init
 * @param struct gallery_entry ** gallery The gallery to free
 * @param int gallery_size The size of the gallery
 */
void free_v1(struct gallery_entry **gallery, int gallery_size)
{
    int i;
    for(i = 0; i < gallery_size; i++)
    {
        free((*gallery)[i].features);
        if((*gallery)[i].pca_features != NULL)
            free((*gallery)[i].pca_features);
        if((*gallery)[i].path != NULL)
            free((*gallery)[i].path);
    }

    free(*gallery);
    *gallery = NULL;
}

/**spheres probe using gallery sphere values 
 *Tares array, subtracts mean, then finds std which it divides new array by
 * @param v1_float features The features to be sphered
 * @param int size The number of features
 * @param v1_float* mean The means of each column in the gallery
 * @param v1_float* std the standard deviations of each column in the gallery.
 */
void sphere_probe(v1_float* features, int size, v1_float* mean, v1_float* std)
{
    int i;
    for(i = 0; i < size; i++)
    {
        if(std[i] != 0)
            features[i] = (features[i] - mean[i]) / std[i];
        else
            features[i] = (features[i]- mean[i]);
    }
}

/**
 *spheres data same process as the python
 *Tares array, subtracts mean, then finds std which it divides new array by
 * @param struct gallery_entry* gallery The gallery to sphere
 * @param int gallery_size The size of the gallery
 * @param char* file_values Path to file to open and save the values
 */
void sphere_data(struct gallery_entry* gallery, int gallery_size, int size, char* file_values)
{
    int i, j;
    v1_float hold, mean, std;

    FILE* f = fopenOrError(file_values, "w");
    if(!f)
        return;

    for(i = 0; i < size; i++)
    {
        mean = 0.0;
        for(j = 0; j < gallery_size; j++)
            mean += gallery[j].features[i];

        mean = mean / gallery_size;
        hold = mean;

        mean = 0;
        for(j = 0; j < gallery_size; j++)
        {
            gallery[j].features[i] -= hold;
            mean += gallery[j].features[i];
        }

        mean = mean / gallery_size;

        std = 0;
        for(j = 0; j < gallery_size; j++)
            std += (gallery[j].features[i] - mean) * (gallery[j].features[i] - mean);

        std = v1_sqrt(std / gallery_size);

        fprintf(f, "%f %f\n", hold, std);

        if(std != 0)
        {
            for(j = 0; j < gallery_size; j++)
                gallery[j].features[i] /= std;
        }
    }

    fclose(f);
}


/*spheres data same process as the python
 *Tares array, subtracts mean, then finds std which it divides new array by
 * @param struct gallery_entry* gallery The gallery to sphere
 * @param int gallery_size The size of the gallery
 * @param char* file_values Path to file to open and save the values
 */
void sphere_data_validate(struct validate_gallery* gallery, int gallery_size, int size, char* file_values)
{
    int i, j;
    v1_float hold, mean, std;

    FILE* f = fopenOrError(file_values, "w");
    if(!f)
        return;

    for(i = 0; i < size; i++)
    {
        mean = 0.0;
        for(j = 0; j < gallery_size; j++)
            mean += gallery[j].features[i];

        mean = mean / gallery_size;
        hold = mean;

        mean = 0;
        for(j = 0; j < gallery_size; j++)
        {
            gallery[j].features[i] -= hold;
            mean += gallery[j].features[i];
        }

        mean = mean / gallery_size;

        std = 0;
        for(j = 0; j < gallery_size; j++)
            std += (gallery[j].features[i] - mean) * (gallery[j].features[i] - mean);

        std = v1_sqrt(std / gallery_size);

        fprintf(f, "%f %f\n", hold, std);

        if(std != 0)
        {
            for(j = 0; j < gallery_size; j++)
                gallery[j].features[i] /= std;
        }
    }

    fclose(f);
}

/**Runs convolve with correct type similar to python, 
 *
 * @param IplImage* src Source image
 * @param IplImage* dst Destionation image
 * @param IplImage* kernel Kernel to use for covolution
 * @param convolve_mode mode Mode for convolve (same, valid, full)
 */
static int do_convolve(IplImage *src, IplImage* dst, IplImage *kernel, convolve_mode mode)
{
    int ret = convolve2d(src, dst, kernel, mode);

    switch(ret)
    {
    case -1:
        fprintf(stderr, "Convolve error: Convolve parameter is null.\n");
        break;
    case -2:
        fprintf(stderr, "Convolve error: Source and destination images differ in size.\n");
        break;
    case -3:
        fprintf(stderr, "Convolve error: Kernel image is larger than source image.\n");
        break;
    case -4:
        fprintf(stderr, "Convolve error: Valid region is too small.\n");
        break;
    case -99:
        fprintf(stderr, "Convolve error: Unknown or unhandled mode.\n");
        break;
    }

    return ret;
}
/**Adds one image to another
 *
 * @param IplImage* dst First image to add, and store result
 * @param IplImage* add Second image to add to first
*/

void addIpl(IplImage* dst, IplImage* add)
{
    int i, j;
    for(i = 0; i < dst->height; i++)
    {
        for(j = 0; j < dst->width; j++)
        {
            CV_IMAGE_ELEM(dst, v1_float, i, j) += 
                CV_IMAGE_ELEM(add, v1_float, i, j);
        }
    }
}

/**
 *  Perform separable convolutions on an image with a set of filters
 * @param IplImage* img The image to preform the filter on.
 * @param gabor_filter** gilterbank The 96 gabor filters(PCA trimmed)
 * @param IplImage** results The resulting images one for each filter
 */
void v1s_filter_fast(IplImage* img, gabor_filter **filterbank, IplImage** results)
{
  /*
# -------------------------------------------------------------------------
def v1s_filter(hin, filterbank):
    """ V1S linear filtering
    Perform separable convolutions on an image with a set of filters
    
    Inputs:
      hin -- input image (a 2-dimensional array) 
      filterbank -- list of tuples with 1d filters (row, col)
                    used to perform separable convolution
     
    Outputs:
      hout -- a 3-dimensional array with outputs of the filters 
              (width X height X n_filters)

    """

    nfilters = len(filterbank)
    shape = list(hin.shape) + [nfilters]
    hout = N.empty(shape, 'f')
    for i in xrange(nfilters):
        vectors = filterbank[i]
        row0, col0 = vectors[0]
        result = conv(conv(hin[:], row0, 'same'), col0, 'same')
        for row, col in vectors[1:]:
            result += conv(conv(hin[:], row, 'same'), col, 'same')                  
        hout[:,:,i] = result

    return hout

    tmp1 = conv(hin[:], row0, 'same')
    result = conv(tmp1, col0, 'same')


  */

    int i, j, x, y;
    int nfilters = GABOR_FEATURES;
    IplImage* result = cvCreateImage(cvGetSize(img), IPL_DEPTH_V1, 1);
    IplImage* tmp1 = cvCreateImage(cvGetSize(img), IPL_DEPTH_V1, 1);
    IplImage* tmp2 = cvCreateImage(cvGetSize(img), IPL_DEPTH_V1, 1);
    gabor_filter* vectors;

    IplImage *row0, *col0, *row, *col;

    for(i = 0; i < nfilters; i++)
    {
        vectors = filterbank[i];
        row0 = vectors->first[0].row;
        col0 = vectors->first[0].col;
        //        cvZero(tmp1);
        //        cvZero(tmp2);
        //        cvZero(result);
        do_convolve(img,  tmp1, row0, MODE_SAME);
        do_convolve(tmp1, result, col0, MODE_SAME);
        
        for(j = 1; j < vectors->numEntries; j++)
        {
            row = vectors->first[j].row;
            col = vectors->first[j].col;

            do_convolve(img, tmp1, row, MODE_SAME);
            do_convolve(tmp1, tmp2, col, MODE_SAME);

            addIpl(result, tmp2);
        }

        for(x = 0; x < img->width; x++)
        {
            for(y = 0; y < img->height; y++)
            {
                v1_float value =  CV_IMAGE_ELEM(result, v1_float, y,x);
                if(value > 1.0f)
                    value = 1.0f;
                else if(value < 0.0f)
                    value = 0.0f;
                CV_IMAGE_ELEM(results[i], v1_float, y, x) = value;
            }
        }
    }

    cvReleaseImage(&result);
    cvReleaseImage(&tmp1);
    cvReleaseImage(&tmp2);
}
/**
 *  Perform separable convolutions on an image with a set of filters
 * @param IplImage* img The image to preform the filter on.
 * @param IplImage** filterbank_real The 96 gabor filters(NOT PCA trimmed)
 * @param IplImage** results The resulting images one for each filter
 */

void v1s_filter_slow(IplImage* img, IplImage**filterbank_real, IplImage** results)
{
/*
# -------------------------------------------------------------------------
def v1s_filter(hin, filterbank):
    """ V1S linear filtering
    Perform separable convolutions on an image with a set of filters

    Inputs:
      hin -- input image (a 2-dimensional array)
      filterbank -- list of tuples with 1d filters (row, col)
                    used to perform separable convolution

    Outputs:
      hout -- a 3-dimensional array with outputs of the filters
              (width X height X n_filters)

    """

    nfilters = len(filterbank)
    shape = list(hin.shape) + [nfilters]
    hout = N.empty(shape, 'f')
    for i in xrange(nfilters):
        vectors = filterbank[i]
        row0, col0 = vectors[0]
        result = conv(conv(hin[:], row0, 'same'), col0, 'same')
        for row, col in vectors[1:]:
            result += conv(conv(hin[:], row, 'same'), col, 'same')
        hout[:,:,i] = result

    return hout
*/

    int i, x, y;
    int nfilters = GABOR_FEATURES;
    IplImage* regabout = cvCreateImage(cvGetSize(img), IPL_DEPTH_V1, 1);

    for(i = 0; i < nfilters; i++)
    {
        do_convolve(img, regabout, filterbank_real[i], MODE_SAME);

        for(x = 0; x < img->width; x++)
        {
            for(y = 0; y < img->height; y++)
            {
                v1_float value =  CV_IMAGE_ELEM(regabout, v1_float, y,x);
                if(value > 1.0f)
                    value = 1.0f;
                else if(value < 0.0f)
                    value = 0.0f;
                CV_IMAGE_ELEM(results[i], v1_float, y,x) = value;
            }
        }
    }

    cvReleaseImage(&regabout);
}

/**This is the main method to call that will generate the features for a single image
 * @param IplImage* img The image to get features of.
 * @param v1_float* features The storage for the features
 * @param void gfilters** gilters The gabor filters
 * @param int fast_gabor 1 to preform fast gabor with PCA trimmed filters 0 for slow
 * @param int norm_image 1 to preform preprocessing 0 if already preprocessed
 * @param int use_python 1 to use python resize 0 otherwise
 * @param int num A number to help identify unique thread run
 */
void get_features(IplImage *img, v1_float *features, void** gfilters, int fast_gabor, int nomr_image, int use_python, int num)
{
    int      i, x, y, index;
    IplImage *dst;
    IplImage **results;
    IplImage **norm_results;
    IplImage **resampled;
    IplImage *norm = NULL;

    results      = (IplImage**)malloc(GABOR_FEATURES * sizeof(IplImage*));
    norm_results = (IplImage**)malloc(GABOR_FEATURES * sizeof(IplImage*));
    if(nomr_image == 1)
    {
        dst = get_image(img, 150, use_python, num);
        norm = image_preparation(dst);
    }
    else
    {
        norm = cvCloneImage(img);
    }

    for(i = 0; i < GABOR_FEATURES; i++)
    {
        results[i]  = cvCreateImage(cvGetSize(norm), IPL_DEPTH_V1, 1);
        norm_results[i] = cvCreateImage(cvGetSize(norm), IPL_DEPTH_V1, 1);
    }

    if(fast_gabor)
        v1s_filter_fast(norm, (gabor_filter**)gfilters, results);
    else
        v1s_filter_slow(norm, (IplImage**)gfilters, results);

#ifdef DEBUG
    // Show the final 96 features
    for(i = 0; i < GABOR_FEATURES; i++)
        showImage("Convolved", results[i]);
    cvDestroyWindow("Convolved");
#endif

    v1s_norm(results, norm_results, 3, 3, 1.0, GABOR_FEATURES);
    resampled = v1s_dimr(norm_results, 17, WINDOW_SIZE, WINDOW_SIZE, GABOR_FEATURES);

    //Need to unravel the matrix into a 1 deminsion.
    index = 0;
    for(y = 0; y < resampled[0]->height; y++)
    {
        for(x = 0; x < resampled[0]->width; x++)
        {
            for(i = 0; i < GABOR_FEATURES; i++)
                features[index++] = CV_IMAGE_ELEM(resampled[i], v1_float, y, x);
        }
    }

    cvReleaseImage(&norm);

    for(i = 0; i < GABOR_FEATURES; i++)
    {
        cvReleaseImage(&results[i]);
        cvReleaseImage(&norm_results[i]);
        cvReleaseImage(&resampled[i]);
    }

    free(results);
    free(norm_results);
    free(resampled);
    if(nomr_image == 1)
    {

        cvReleaseImage(&dst);
    }
}
