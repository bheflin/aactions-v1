// eye_detector.cpp 
// Copyright 2009 Securics, Inc 

#define LINUX

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef LINUX
#include "asciibar.h"
#include "geo.h"
#include <unistd.h>
#include <string.h>
#include <stdio.h>
using namespace std;
#else
#include <stdio.h>
#include <string>
#include <sstream> 
using namespace std;
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#include <time.h>
#endif

#include "eye_detector.h"
#include <fftw3.h>
#include "left_eye.h"
#include "right_eye.h"
#include "genericFaceMace.h"

#define FUDGE_FACTOR_X -3
#define FUDGE_FACTOR_Y 2

#ifndef LINUX
std::wstring s2ws(const std::string& s)
{  
  int len;      
  int slength = (int)s.length() + 1; 
  len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0); 
  wchar_t* buf = new wchar_t[len]; 
  MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len); 
  std::wstring r(buf); 
  delete[] buf; 
  return r; 
}  
#endif 


/** 
 *Preforms lighting normalization on an input image 
 * @param IplImage* img_in The image to be normalized 
 * @param int width The width of the image 
 * @param int height The height of the image 
 */ 
void LIGHTING_NORMALIZATION(IplImage *img_in, int width, int height) 
{ 
  static float         x; 
  static float         y[256]; 
  static int           i, j; 
  static unsigned char temp; 
  static float         temp_float; 
  static float         max_val; 
  static float         min_val; 
  static float         Factor; 
  
  
  static IplImage *smoothed_img=0; 
  static IplImage *q_img=0; 
  static IplImage *SQI_img=0; 
  
  uchar temp_s; 
  float temp_x, temp_y, temp_z; 
  float max, min, factor, mean; 
  
  
  
  IplImage *img1 = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1); 
    
  
  for(i = 0; i < height; i++) 
    { 
      for(j = 0; j < width; j++) 
        { 
	  (img1->imageData + i * img1->widthStep)[j] = (img_in->imageData + i * img_in->widthStep)[j]; 
        } 
    } 
  
  
  
  
  float *new_image = (float*)malloc(sizeof(float) * width * height); 
  /*     
	 x = 1; 
	 y[0] = 0; 
	 
	 for(i = 1; i <= 255; i++) 
	 { 
	 y[i] = log(x); 
	 x++; 
	 } 
	 
	 //log normalize the image 
	 for(i = 0; i < height; i++) 
	 { 
	 for(j = 0; j < width; j++) 
	 { 
	 temp = (img1->imageData + i * img1->widthStep)[j]; 
	 // temp +=1; 
	 //    new_image[i * width + j] = (y[(int)temp]); 
	 new_image[i * width + j] = temp; 
	 } 
	 } 
	 
	 
	 //gamma correction  
	 for(i = 0; i < height; i++) 
	 { 
	 for(j = 0; j < width; j++) 
	 { 
	 //temp = (new_image->imageData + i * new_image->widthStep)[j]; 
	 new_image[i * width + j] =pow(new_image[i * width + j] , 1/2.2); 
	 } 
	 } 
	 
	 //contrast stretch and convert to uint8 
	 max_val = 0; 
	 min_val = 10; 
	 
	 for(i = 0; i < height; i++) 
	 { 
	 for(j = 0; j < width; j++) 
	 { 
	 temp_float = new_image[i * width + j]; 
	 
	 if(temp_float > max_val) 
	 max_val = temp_float; 
	 
	 if(temp_float < min_val) 
	 min_val = temp_float; 
	 } 
	 } 
	 
	 Factor = floor((255) / (max_val - min_val)); 
	 
	 //subtract min value and multiply Factor 
	 for(i = 0; i < height; i++) 
	 { 
	 for(j = 0; j < width; j++) 
	 { 
	 temp_float = new_image[i * width + j]; 
	 new_image[i * width + j] = temp_float - min_val; 
	 
	 temp_float = new_image[i * width + j]; 
	 new_image[i * width + j] = (float)floor(temp_float * Factor); 
	 
	 if(new_image[i * width + j] > 255) 
	 new_image[i * width + j] = 255; 
	 
	 if(new_image[i * width + j] < 0) 
	 new_image[i * width + j] = 0; 
	 } 
	 } 
	 
    for(i = 0; i < height; i++) 
    { 
    for(j = 0; j < width; j++) 
    { 
    (img1->imageData + i * img1->widthStep)[j] = (unsigned char)new_image[i * width + j]; 
    } 
    } 
  */ 
  
  smoothed_img = cvCreateImage(cvSize(img1->width, img1->height), IPL_DEPTH_8U, 1);     
  cvSmooth(img1,smoothed_img,CV_GAUSSIAN,77,0,135); 
  q_img = cvCreateImage(cvSize(img1->width, img1->height), IPL_DEPTH_32F, 1); 
  SQI_img = cvCreateImage(cvSize(img1->width, img1->height), IPL_DEPTH_8U, 1);     
  max=0; 
  min=1000; 
  
  
  //form quotient image 
  for (i=0; i<img1->height; i++) 
    for (j=0; j<img1->width; j++) 
      { 
	//get pixel from input image 
	temp=((uchar *)(img1->imageData + i*img1->widthStep))[j]; 
	//divide by 255 to make values range from 0.0-1.0  
	temp_x=(float)temp/255; 
	
	//get pixel from smoothed image 
	temp_s=((uchar *)(smoothed_img->imageData + i*smoothed_img->widthStep))[j]; 
	//divide by 255 to make values range from 0.0-1.0  
	temp_y=(float)temp_s/255; 
	
	temp_z=temp_x/temp_y; 
	
	//divide the two pixel values 
	((float *)(q_img->imageData + i*q_img->widthStep))[j]=temp_z; 
	
	//find max and min of q_img 
	if (temp_z > max) 
	  max=temp_z; 
	if (temp_z < min) 
	  min=temp_z; 
      } 
  
  factor=(max-min); 
  
  //normalize q_image 
  for (i=0; i<q_img->height; i++) 
    for (j=0; j<q_img->width; j++) 
      {    
	temp_x =((float *)(q_img->imageData + i*q_img->widthStep))[j]; 
	temp_x = temp_x - min; 
	temp_x = temp_x / factor; 
	((float *)(q_img->imageData + i*q_img->widthStep))[j]=temp_x; 
      } 
  
  //find mean of the image 
  temp_x=0; 
  for (i=0; i<q_img->height; i++) 
    for (j=0; j<q_img->width; j++) 
      {    
	temp_x +=((float *)(q_img->imageData + i*q_img->widthStep))[j]; 
      } 
  
  mean=temp_x/(q_img->height*q_img->width); 
  
  //final normalize q_image 
  for (i=0; i<q_img->height; i++) 
    for (j=0; j<q_img->width; j++) 
      {    
	temp_x =((float *)(q_img->imageData + i*q_img->widthStep))[j]; 
	temp_x = (-1)*temp_x/mean; 
	temp_x = 1-exp((double)temp_x); 
	((float *)(q_img->imageData + i*q_img->widthStep))[j]=temp_x; 
      } 
  
  
  
  //convert back to uint8 
  for (i=0; i<q_img->height; i++) 
    for (j=0; j<q_img->width; j++) 
      { 
	//get pixel from quotient image 
	temp_x =((float *)(q_img->imageData + i*q_img->widthStep))[j]; 
	
	temp=(uchar)(temp_x*255); 
        
	((uchar *)(img1->imageData + i*img1->widthStep))[j]=temp; 
      } 
  
  
  for(i = 0; i < height; i++) 
    { 
      for(j = 0; j < width; j++) 
        { 
	  temp = (img1->imageData + i * img1->widthStep)[j]; 
	  // temp +=1; 
	  //    new_image[i * width + j] = (y[(int)temp]); 
	  new_image[i * width + j] = temp; 
        } 
    } 
  
  
  //cvSmooth( img1, img1,CV_GAUSSIAN,3,0,0,0 ); 
  
  //gamma correction  
  for(i = 0; i < height; i++) 
    { 
      for(j = 0; j < width; j++) 
        { 
	  // temp = (new_image->imageData + i * new_image->widthStep)[j]; 
	  new_image[i * width + j] =(pow(new_image[i * width + j],float(1/4.2))); 
        } 
    } 
  
  //contrast stretch and convert to uint8 
  max_val = 0; 
  min_val = 10; 
  
  for(i = 0; i < height; i++) 
    { 
      for(j = 0; j < width; j++) 
        { 
	  temp_float = new_image[i * width + j]; 
	  
	  if(temp_float > max_val) 
	    max_val = temp_float; 
	  
	  if(temp_float < min_val) 
	    min_val = temp_float; 
        } 
    } 
  
  Factor = floor((255) / (max_val - min_val)); 
  
  //subtract min value and multiply Factor 
  for(i = 0; i < height; i++) 
    { 
      for(j = 0; j < width; j++) 
        { 
	  temp_float = new_image[i * width + j]; 
	  new_image[i * width + j] = temp_float - min_val; 
	  
	  temp_float = new_image[i * width + j]; 
	  new_image[i * width + j] = (float)floor(temp_float * Factor); 
	  
	  if(new_image[i * width + j] > 255) 
	    new_image[i * width + j] = 255; 
	  
	  if(new_image[i * width + j] < 0) 
	    new_image[i * width + j] = 0; 
        } 
    } 
  
  for(i = 0; i < height; i++) 
    { 
      for(j = 0; j < width; j++) 
        { 
	  (img_in->imageData + i * img_in->widthStep)[j] = (unsigned char)new_image[i * width + j]; 
        } 
    } 
  
  
  cvReleaseImage(&smoothed_img); 
  cvReleaseImage(&q_img); 
  cvReleaseImage(&SQI_img); 
  cvReleaseImage(&img1); 
  free(new_image); 
  
} 


IplImage* LIGHTING_NORMALIZATION1(IplImage *img_in, int width, int height) 
{ 
    static float         x; 
    static float         y[256]; 
    static int           i, j; 
    static unsigned char temp; 
    static float         temp_float; 
    static float         max_val; 
    static float         min_val; 
    static float         Factor; 
 
 
    static IplImage *smoothed_img=0; 
    static IplImage *q_img=0; 
	static IplImage *SQI_img=0; 
	 
	uchar temp_s; 
	float temp_x, temp_y, temp_z; 
    float max, min, factor, mean; 
 
 
 
    IplImage *img1 = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1); 
    
 
    for(i = 0; i < height; i++) 
    { 
        for(j = 0; j < width; j++) 
        { 
            (img1->imageData + i * img1->widthStep)[j] = (img_in->imageData + i * img_in->widthStep)[j]; 
        } 
    } 
     
 
 
 
    float *new_image = (float*)malloc(sizeof(float) * width * height); 
    /*     
    x = 1; 
    y[0] = 0; 
     
    for(i = 1; i <= 255; i++) 
    { 
        y[i] = log(x); 
        x++; 
    } 
     
    //log normalize the image 
    for(i = 0; i < height; i++) 
    { 
        for(j = 0; j < width; j++) 
        { 
            temp = (img1->imageData + i * img1->widthStep)[j]; 
            // temp +=1; 
            //    new_image[i * width + j] = (y[(int)temp]); 
              new_image[i * width + j] = temp; 
        } 
    } 
 
 
 //gamma correction  
    for(i = 0; i < height; i++) 
    { 
        for(j = 0; j < width; j++) 
        { 
            //temp = (new_image->imageData + i * new_image->widthStep)[j]; 
            new_image[i * width + j] =pow(new_image[i * width + j] , 1/2.2); 
        } 
    } 
 
    //contrast stretch and convert to uint8 
    max_val = 0; 
    min_val = 10; 
 
    for(i = 0; i < height; i++) 
    { 
        for(j = 0; j < width; j++) 
        { 
            temp_float = new_image[i * width + j]; 
 
            if(temp_float > max_val) 
                max_val = temp_float; 
 
            if(temp_float < min_val) 
                min_val = temp_float; 
        } 
    } 
 
    Factor = floor((255) / (max_val - min_val)); 
 
    //subtract min value and multiply Factor 
    for(i = 0; i < height; i++) 
    { 
        for(j = 0; j < width; j++) 
        { 
            temp_float = new_image[i * width + j]; 
            new_image[i * width + j] = temp_float - min_val; 
 
            temp_float = new_image[i * width + j]; 
            new_image[i * width + j] = (float)floor(temp_float * Factor); 
 
            if(new_image[i * width + j] > 255) 
                new_image[i * width + j] = 255; 
 
            if(new_image[i * width + j] < 0) 
                new_image[i * width + j] = 0; 
        } 
    } 
 
    for(i = 0; i < height; i++) 
    { 
        for(j = 0; j < width; j++) 
        { 
            (img1->imageData + i * img1->widthStep)[j] = (unsigned char)new_image[i * width + j]; 
        } 
    } 
    */ 
     
	smoothed_img = cvCreateImage(cvSize(img1->width, img1->height), IPL_DEPTH_8U, 1);     
	cvSmooth(img1,smoothed_img,CV_GAUSSIAN,77,0,135); 
    q_img = cvCreateImage(cvSize(img1->width, img1->height), IPL_DEPTH_32F, 1); 
    SQI_img = cvCreateImage(cvSize(img1->width, img1->height), IPL_DEPTH_8U, 1);     
	max=0; 
	min=1000; 
 
 
	//form quotient image 
	for (i=0; i<img1->height; i++) 
       for (j=0; j<img1->width; j++) 
	   { 
		   //get pixel from input image 
		   temp=((uchar *)(img1->imageData + i*img1->widthStep))[j]; 
	       //divide by 255 to make values range from 0.0-1.0  
		   temp_x=(float)temp/255; 
		    
		   //get pixel from smoothed image 
		   temp_s=((uchar *)(smoothed_img->imageData + i*smoothed_img->widthStep))[j]; 
	       //divide by 255 to make values range from 0.0-1.0  
		   temp_y=(float)temp_s/255; 
		   
           temp_z=temp_x/temp_y; 
 
		   //divide the two pixel values 
		   ((float *)(q_img->imageData + i*q_img->widthStep))[j]=temp_z; 
	    
           //find max and min of q_img 
		   if (temp_z > max) 
			   max=temp_z; 
		   if (temp_z < min) 
			   min=temp_z; 
	   } 
     
   factor=(max-min); 
 
	//normalize q_image 
	for (i=0; i<q_img->height; i++) 
       for (j=0; j<q_img->width; j++) 
	   {    
		   temp_x =((float *)(q_img->imageData + i*q_img->widthStep))[j]; 
	       temp_x = temp_x - min; 
		   temp_x = temp_x / factor; 
	       ((float *)(q_img->imageData + i*q_img->widthStep))[j]=temp_x; 
	   } 
 
       //find mean of the image 
	   temp_x=0; 
  for (i=0; i<q_img->height; i++) 
       for (j=0; j<q_img->width; j++) 
	   {    
		   temp_x +=((float *)(q_img->imageData + i*q_img->widthStep))[j]; 
	   } 
	    
	   mean=temp_x/(q_img->height*q_img->width); 
        
//final normalize q_image 
	for (i=0; i<q_img->height; i++) 
       for (j=0; j<q_img->width; j++) 
	   {    
		   temp_x =((float *)(q_img->imageData + i*q_img->widthStep))[j]; 
            temp_x = (-1)*temp_x/mean; 
		   temp_x = 1-exp((double)temp_x); 
             ((float *)(q_img->imageData + i*q_img->widthStep))[j]=temp_x; 
	   } 
     
 
 
  //convert back to uint8 
for (i=0; i<q_img->height; i++) 
       for (j=0; j<q_img->width; j++) 
	   { 
		   //get pixel from quotient image 
		   temp_x =((float *)(q_img->imageData + i*q_img->widthStep))[j]; 
	        
		   temp=(uchar)(temp_x*255); 
            
		   ((uchar *)(img1->imageData + i*img1->widthStep))[j]=temp; 
	   } 
 
 
 for(i = 0; i < height; i++) 
    { 
        for(j = 0; j < width; j++) 
        { 
            temp = (img1->imageData + i * img1->widthStep)[j]; 
            // temp +=1; 
            //    new_image[i * width + j] = (y[(int)temp]); 
              new_image[i * width + j] = temp; 
        } 
    } 
 
 
 //cvSmooth( img1, img1,CV_GAUSSIAN,3,0,0,0 ); 
 
 //gamma correction  
    for(i = 0; i < height; i++) 
    { 
        for(j = 0; j < width; j++) 
        { 
            //temp = (new_image->imageData + i * new_image->widthStep)[j]; 
//	  new_image[i * width + j] =pow(new_image[i * width + j] , 1/3.5); 
  
        } 
    } 
 
    //contrast stretch and convert to uint8 
    max_val = 0; 
    min_val = 10; 
 
    for(i = 0; i < height; i++) 
    { 
        for(j = 0; j < width; j++) 
        { 
            temp_float = new_image[i * width + j]; 
 
            if(temp_float > max_val) 
                max_val = temp_float; 
 
            if(temp_float < min_val) 
                min_val = temp_float; 
        } 
    } 
 
    Factor = floor((255) / (max_val - min_val)); 
 
    //subtract min value and multiply Factor 
    for(i = 0; i < height; i++) 
    { 
        for(j = 0; j < width; j++) 
        { 
            temp_float = new_image[i * width + j]; 
            new_image[i * width + j] = temp_float - min_val; 
 
            temp_float = new_image[i * width + j]; 
            new_image[i * width + j] = (float)floor(temp_float * Factor); 
 
            if(new_image[i * width + j] > 255) 
                new_image[i * width + j] = 255; 
 
            if(new_image[i * width + j] < 0) 
                new_image[i * width + j] = 0; 
        } 
    } 
 
    for(i = 0; i < height; i++) 
    { 
        for(j = 0; j < width; j++) 
        { 
            (img1->imageData + i * img1->widthStep)[j] = (unsigned char)new_image[i * width + j]; 
        } 
    } 
     
 
    cvReleaseImage(&smoothed_img); 
    cvReleaseImage(&q_img); 
    cvReleaseImage(&SQI_img); 
    free(new_image); 
    return (img1); 
    cvReleaseImage(&img1); 
     
    
} 
 
 
IplImage* CROP_EYE (IplImage *img_temp, IplImage *img_in, int x_crop_start, int y_crop_start, int size) 
{ 
  
	static int i,j; 
	//IplImage        *img_temp = 0; 
	//img_temp = cvCreateImage( cvSize(size, size), IPL_DEPTH_8U, 1 ); 
	//crop out eye area 
	for( i = y_crop_start; i < y_crop_start + size; i++ ) { 
	  for( j = x_crop_start; j < x_crop_start + size ; j++ ) { 
	    
	    (img_temp->imageData + (int)(i-y_crop_start) * img_temp ->widthStep)[(int)(j-x_crop_start)]=(img_in->imageData + i * img_in ->widthStep)[j]; 
	  } 
	  
	} 
	
	return img_temp; 
} 


/** 
 *Preforms lighting normalization on an input image in low light 
 * @param IplImage* img_in The image to be normalized 
 * @param int width The width of the image 
 * @param int height The height of the image 
 */ 


/** 
 *Preforms eye detection on an image 
 * @param IplImage* input_img The croped face image to do eye detect ion 
 * @param int* x_pos_left The returned x position of the left eye 
 * @param int* x_pos_right The returned x position of the right eye 
 * @param int* y_pos_left The returned y position of the left eye 
 * @param int* y_pos_right The returned y position of the right eye 
 */ 
void eye_detect(IplImage *input_img, int *x_pos_left, int *x_pos_right, int *y_pos_left, int *y_pos_right, int *x1_pos_left, int *x1_pos_right, int *y1_pos_left, int *y1_pos_right) 
{ 
  double QCC_Threshold = 0.80; 
  
  
  int inner_loop; 
  int column_loop, row_loop; 
  int counter = 0; 
  int loop; 
  int ifftCounter = 0; 
  int filter_loops; 
  int rows, columns; 
  int maxi, maxj; 
  int rowCounter = 0; 
  int columnCounter = 0; 
  int newRow; 
  int fftCounter = 0; 
  double temp_D; 
  fftw_complex *data_in_sym     = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * 65); 
  fftw_complex *tempfft     = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * 65); 
  fftw_complex *fftArray    = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * 65 * 150); 
  fftw_complex *fftTempRow  = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * 150); 
  fftw_complex *tempfft1     = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * 150); 
  fftw_complex *tempifft1     = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * 150); 
  fftw_complex *tempifft     = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * 150); 
  fftw_complex *ifftArray    = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * 65 * 150); 
  fftw_complex *fftArrayFinal = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * 65 * 150); 
  fftw_complex *ifftArrayFinal = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * 65 * 150); 
  
  fftw_plan     plan_f_sym     = fftw_plan_dft_1d(65, data_in_sym, tempfft, FFTW_FORWARD, FFTW_ESTIMATE); 
  fftw_plan     plan_f1_sym    = fftw_plan_dft_1d(150, fftTempRow, tempfft1, FFTW_FORWARD, FFTW_ESTIMATE); 
  fftw_plan     plan_r_sym     = fftw_plan_dft_1d(65, data_in_sym, tempifft, FFTW_BACKWARD, FFTW_ESTIMATE); 
  fftw_plan     plan_r1_sym    = fftw_plan_dft_1d(150, fftTempRow, tempifft1, FFTW_BACKWARD, FFTW_ESTIMATE); 
  IplImage *imgOut = cvCreateImage(cvSize(65, 150), IPL_DEPTH_32F, 1); 
  double* D = (double*)malloc(65 * 150* sizeof(double)); 
  fftw_complex *UMACE_Filter = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * 65 * 150); 
  unsigned char* tempRow = (unsigned char*)malloc(65 * sizeof(unsigned char)); 
  fftw_complex *tempScore = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * 65 * 150); 
  float  *img4     = (float*)malloc(sizeof(float) * 150 * 65); 
  IplImage* originalImage; 
  
  float symmetryArray[2][2]; 
  int symLoop; 
  
  //    int x_pos_left0; 
  //    int y_pos_left0; 
  //    int x_pos_left1; 
  //    int y_pos_left1; 
  
  //    int x_pos_right0; 
  //    int y_pos_right0; 
  //    int x_pos_right1; 
  //    int y_pos_right1; 
  
  int y_pos_left_gt; 
  int x_pos_left_gt; 
  int y_pos_right_gt; 
  int x_pos_right_gt; 
  
  //    int y_pos_left_gt1; 
  //    int x_pos_left_gt1; 
  //    int y_pos_right_gt1; 
  //    int x_pos_right_gt1; 
  
  //    int y_pos_left_gttemp; 
  //    int x_pos_left_gttemp; 
  //    int y_pos_right_gttemp; 
  //    int x_pos_right_gttemp; 
  
  
  
  int second_peak_left=0; 
  int second_peak_right=0; 
  
  double max_left = -1000000000; 
  double max_left1 = -1000000000; 
  double max_right = -1000000000; 
  double max_right1 = -1000000000; 
  
  //    float temp_left; 
  //    float temp1_left; 
  //    float temp_right; 
  //    float temp1_right; 
  
  
  int WIPE_OUT_SIZE=5; 
  
  int WIPE_OUT_X_START=0; 
  int WIPE_OUT_Y_START=0; 
  int WIPE_OUT_X_FINISH=0; 
  int WIPE_OUT_Y_FINISH=0; 
  
  int COLUMN_OFFSET=-8; 
  
  float PERCENTAGE_Y=0.80; 
  float PERCENTAGE_X=1-PERCENTAGE_Y; 
  // float PERCENTAGE_THRESHOLD=0.65; 
  // float CHANGE_THRESHOLD=.1; 
  float PERCENTAGE_THRESHOLD=0.85; 
  float CHANGE_THRESHOLD=.0001; 
  int i, j, k; 
  
  IplImage *blurred_image = NULL; 
  IplImage *img1          = NULL; 
  IplImage *img_light     = NULL; 
  
  float  *img3; 
  char   *input_img_data; 
  int    width, height, step; 
  int    new_width, new_height, new_step; 
  int    rows_half, columns_half; 
  double temp1_d, temp2_d, temp3_d, temp4_d; 
  double dd; 
  double temp; 
  
  float left_eye_crop_x_start; 
  float left_eye_crop_y_start; 
  float right_eye_crop_x_start; 
  float right_eye_crop_y_start; 
  int   ptr_var = 0; 
  
  char image_name [99]; 
  
  fftw_complex *data_in; 
  fftw_plan    plan_f; 
  fftw_plan    plan_r; 
  fftw_plan    plan_eye; 
  fftw_complex *fft; 
  fftw_complex *ifft; 
  
  fftw_complex *eye_in; 
  fftw_complex *eye_out; 
  
  width  = 64; 
  height = 64; 
  
  // initialize arrays for fftw operations 
  data_in  = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * width * height); 
  fft      = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * width * height); 
  ifft     = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * width * height); 
  eye_in   = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * width * height); 
  eye_out  = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * width * height); 
  
  plan_f   = fftw_plan_dft_1d(64 * 64, data_in, fft, FFTW_FORWARD, FFTW_ESTIMATE); 
  plan_eye = fftw_plan_dft_1d(64 * 64, eye_in, eye_out, FFTW_FORWARD, FFTW_ESTIMATE); 
  plan_r   = fftw_plan_dft_1d(64 * 64, fft, ifft, FFTW_BACKWARD, FFTW_ESTIMATE); 
  
  img3     = (float*)malloc(sizeof(float) * 64 * 64); 
  
  // load original image 
  blurred_image  = cvCreateImageHeader(cvSize(input_img->width, 
					      input_img->height), IPL_DEPTH_8U, 1); 
  blurred_image->widthStep = input_img->width; 
  input_img_data = (char*)malloc(input_img->width * input_img->height * sizeof(char)); 
  int index = 0; 
  
  for(i = 0; i < input_img->height; i++) 
    { 
      for(j = 0; j < input_img->width; j++) 
	{ 
	  input_img_data[index] = CV_IMAGE_ELEM(input_img, uchar, i, j); 
	  index++; 
	} 
    } 
  
  blurred_image->imageData = input_img_data; 
  
  // get image properties 
  width  = blurred_image->width; 
  height = blurred_image->height; 
  step   = blurred_image->widthStep; 
  
  // normalize 
  
  //!!!!!!!!!!!!!!!!!!!!!!!memory leak!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
  //img_light = LIGHTING_NORMALIZATION1(blurred_image, width, height); 
  
  ////////////////// LEFT EYE /////////////////////////////////// 
  
  // crop left eye out or original image 
  left_eye_crop_x_start = floor(width * .1); 
  left_eye_crop_y_start = floor(height * .15); 
  
  static int size_crop=64; 
  
  img1 = cvCreateImage( cvSize(size_crop, size_crop), IPL_DEPTH_8U, 1 ); 
  
  CROP_EYE (img1, blurred_image, (int) left_eye_crop_x_start, (int) left_eye_crop_y_start, size_crop); 
  
  // get new image properties 
  new_width  = img1->width; 
  new_height = img1->height; 
  new_step   = img1->widthStep; 
  
  
  LIGHTING_NORMALIZATION(img1, new_width, new_height); 
  
  //cvNamedWindow( "CROP_LEFT", CV_WINDOW_AUTOSIZE ); 
  //cvShowImage( "CROP_LEFT", img1 ); 	 
  //cvWaitKey( 0 );  
  
  // load eye data to fftw input 
  for(i = 0, k = 0; i < new_height; i++) 
    { 
      for(j = 0; j < new_width; j++) 
	{ 
	  data_in[k][0] = (double)((uchar*)(img1->imageData + i * img1->widthStep))[j]; 
	  data_in[k][1] = 0.0; 
            k++; 
	} 
    } 
  
  // perform FFT 
  fftw_execute(plan_f); 
  
  ptr_var = 0; 
    
  // load eye filter data 
  for(i = 0, k = 0; i < new_height; i++) 
    { 
      for(j = 0; j < new_width; j++) 
	{ 
	  eye_out[k][0] = (double)eye_data[ptr_var]; 
	  ptr_var++; 
	  eye_out[k][1] = (double)eye_data[ptr_var]; 
	  k++; 
	  ptr_var++; 
	} 
    } 
  
  // perfrom correlation 
  
  //eye filter already preprocessed with hanning window and conjugated 
  for(k = 0; k < new_height * new_width; k++) 
    { 
      fft[k][0] = ((fft[k][0] * eye_out[k][0]) - (fft[k][1] * eye_out[k][1])); 
      fft[k][1] = ((fft[k][0] * eye_out[k][1]) + (fft[k][1] * eye_out[k][0])); 
    } 
  
  // perfrom ifft 
  fftw_execute(plan_r); 
  
  // normalize IFFT result 
  // copy IFFT result to img_float's data 
  
  //added abs to get rid of errors in conversion 
  for(i = 0, k = 0; i < new_height; i++) 
    { 
      for(j = 0; j < new_width; j++) 
	{ 
	  (img3[i * new_width + j]) = ((float)ifft[i * new_width + j][0]); 
	} 
    } 
  
  //IFFT SHIFT 
  //!!!!image rows and columns have to be even!!!!! 
  
  //int rows_half, columns_half; 
  rows_half    = new_height / 2; 
  columns_half = new_width / 2; 
  
  for(i = 0; i < 32; i++) 
    { 
      for(j = 0; j < 32; j++) 
	{ 
	  temp1_d = img3[(i * new_width) + j]; 
	  temp2_d = img3[(i * new_width) + (j + columns_half)]; 
	  temp3_d = img3[((i + rows_half) * new_width) + (j + columns_half)]; 
	  temp4_d = img3[((i + rows_half) * new_width) + j]; 
	  
	  //1=3 
	  img3[(i * new_width) + j] = temp3_d; 
	  
	  //2=4 
	  img3[(i * new_width) + (j + columns_half)] = temp4_d; 
	  
	  //3=1 
	  img3[((i + rows_half) * new_width) + (j + columns_half)] = temp1_d; 
	  
	  //4=2 
	  img3[((i + rows_half) * new_width) + j] = temp2_d; 
	} 
    } 
  
  //set borders to 0 
  for(i = 0; i < new_height; i++) 
    { 
      for(j = 0; j < new_width; j++) 
	{ 
	  if(i > 10 && i < 50 && j > 10 && j < 50) 
	    dd = 0; 
	  else 
	    img3[i * new_width + j] = 0; 
	} 
    } 
  
  
  ////////////////////////NEW CC!!!!!!!!!!!!!!!!!!!!////////////////////////////// 
  IplImage *detectedImage = 0; 
  detectedImage = cvCreateImage( cvSize(64, 64), IPL_DEPTH_8U, 1 ); 
  
  //Perfrom Connected Components on the Image 
  
  
  //find max value 
  double maxVal=-100000000000; 
  //find max value which corresponds to eye location 
  for( i = 0; i < new_height ; i++ ) { 
    for( j = 0 ; j < new_width ; j++ ) { 
      
      temp=img3[i*new_width+j]; 
      
      if (temp > maxVal) 
	{ 
	  maxVal=temp; 
	} 
    } 
  } 
  
  //Create Thresholded Image 
  int THRESHOLD = maxVal * QCC_Threshold; 
  
  for( i = 0; i < new_height ; i++ ) { 
    for( j = 0 ; j < new_width ; j++ ) { 
      
      temp=img3[i*new_width+j]; 
      
      if (temp > THRESHOLD) 
	{ 
	  ((unsigned char *)(detectedImage->imageData + i*detectedImage->widthStep))[j]=255; 
	} 
      
      else 
	((unsigned char *)(detectedImage->imageData + i*detectedImage->widthStep))[j]=0; 
      
    } 
  } 
  
  //    int QCCwidth, QCCheight; //QCCstep; 
  unsigned char tempQCCPixel; 
  
  IplImage *QCC_img = 0; 
  
  unsigned short connectionLabels [4000]; 
  unsigned short NW, N, NE, W, X; 
  unsigned short NW_Label, N_Label, NE_Label, W_Label; 
  unsigned short labelCounter; 
  unsigned short tempParent; 
  bool NW_Flag, N_Flag, NE_Flag, W_Flag; 
  
  unsigned short minVal, minLabel; 
  
  int QCC_THRESHOLD = 0; 
  
  QCC_img = cvCreateImage( cvSize(64, 64), IPL_DEPTH_16U, 1 ); 
  
  for (i=0; i<4000; i++) 
    { 
      connectionLabels[i] = 0; 
    } 
  
  for (i=0; i<new_height; i++) 
    for (j=0; j<new_width; j++) 
      { 
	tempQCCPixel = ((unsigned char *)(detectedImage->imageData + i*detectedImage->widthStep))[j]; 
	
	if (tempQCCPixel > QCC_THRESHOLD) 
	  tempQCCPixel = 1; 
	else 
	  tempQCCPixel = 0; 
	
	((unsigned short *)(QCC_img->imageData + i*QCC_img->widthStep))[j] = tempQCCPixel; 
      } 
  
  //perfrom Connected Components 
  NW = 0; 
  N = 0; 
  NE = 0; 
  W = 0; 
  X = 0; 
  labelCounter = 1; 
  
  for (i=2; i<new_height; i++) 
    for (j=2; j<new_width; j++) 
      { 
	NW_Flag = false; 
	N_Flag = false; 
	NE_Flag = false; 
	W_Flag = false; 
	
	NW = ((unsigned short *)(QCC_img->imageData + (i-1)*QCC_img->widthStep))[j-1]; 
	
	if (NW > 0) 
	  NW_Label = connectionLabels[NW]; 
	else 
	  NW_Label = 0; 
	
	if (NW != NW_Label && NW_Label > 0) 
	  NW = NW_Label; 
	
	N = ((unsigned short *)(QCC_img->imageData + (i-1)*QCC_img->widthStep))[j]; 
	
	if (N > 0) 
	  N_Label = connectionLabels[N]; 
	else 
	  N_Label = 0; 
	
	if (N != N_Label && N_Label > 0) 
	  N = N_Label; 
	
	NE = ((unsigned short *)(QCC_img->imageData + (i-1)*QCC_img->widthStep))[j+1]; 
	
	if (NE > 0) 
	  NE_Label = connectionLabels[NE]; 
	else 
	  NE_Label = 0; 
	
	if (NE != NE_Label && NE_Label > 0) 
	  NE = NE_Label; 
	
	W = ((unsigned short *)(QCC_img->imageData + (i)*QCC_img->widthStep))[j-1]; 
	
	if (W > 0) 
	  W_Label = connectionLabels[W]; 
	else 
	  W_Label = 0; 
	
	if (W != W_Label && W_Label > 0) 
	  W = W_Label; 
	
	X = ((unsigned short *)(QCC_img->imageData + i*QCC_img->widthStep))[j]; 
	
	if (X > 0) 
	  { 
	    minVal = 65000; 
	    
	    if (NW > 0 && NW < minVal) 
	      minVal = NW; 
	    if (N > 0 && N < minVal) 
	      minVal = N; 
	    if (NE > 0 && NE < minVal) 
	      minVal = NE; 
	    if (W > 0 && W < minVal) 
	      minVal = W; 
	    
	    minLabel = minVal; 
	    
	    //Does pixel to N have same value but not same Label 
	    //assign pixel to min label 
	    if (N > 0 || NE > 0 || NW > 0 || W > 0) 
	      { 
		((unsigned short *)(QCC_img->imageData + i*QCC_img->widthStep))[j] = minLabel; 
		X = minLabel; 
		
		//check NW 
		if (NW > 0 && minLabel < connectionLabels[NW]) 
		  { 
		    connectionLabels[NW] = minLabel; 
		    ((unsigned short *)(QCC_img->imageData + (i-1)*QCC_img->widthStep))[j-1] = minLabel; 
		  } 
		
		//check N 
		if (N > 0 && minLabel < connectionLabels[N]) 
		  { 
		    connectionLabels[N] = minLabel; 
		    ((unsigned short *)(QCC_img->imageData + (i-1)*QCC_img->widthStep))[j] = minLabel; 
		  } 
		
		//check NE 
		if (NE > 0 && minLabel < connectionLabels[NE]) 
		  { 
		    connectionLabels[NE] = minLabel; 
		    ((unsigned short *)(QCC_img->imageData + (i-1)*QCC_img->widthStep))[j+1] = minLabel; 
		  } 
		
		//check W 
		if (W > 0 && minLabel < connectionLabels[W]) 
		  { 
		    connectionLabels[W] = minLabel; 
		    ((unsigned short *)(QCC_img->imageData + (i)*QCC_img->widthStep))[j-1] = minLabel; 
		  } 
		
	      }//if (N > 0) 
	    
	    //neighbors are empty 
	    else 
	      { 
		((unsigned short *)(QCC_img->imageData + i*QCC_img->widthStep))[j] = labelCounter; 
		connectionLabels[labelCounter] = labelCounter; 
		labelCounter++; 
	      } 
	    
	    //check NW 
	    if (NW > 0 && NW > X && connectionLabels[NW] < X) 
	      { 
		connectionLabels[X] = connectionLabels[NW]; 
		((unsigned short *)(QCC_img->imageData + i*QCC_img->widthStep))[j] = connectionLabels[NW]; 
		NW_Flag = true; 
	      } 
	    
	    //check N 
	    if (N > 0 && N > X && connectionLabels[N] < X) 
	      { 
		connectionLabels[X] = connectionLabels[N]; 
		((unsigned short *)(QCC_img->imageData + i*QCC_img->widthStep))[j] = connectionLabels[N]; 
		N_Flag = true; 
	      } 
	    
	    //check NE 
	    if (NE > 0 && NE > X && connectionLabels[NE] < X) 
	      { 
		connectionLabels[X] = connectionLabels[NE]; 
		((unsigned short *)(QCC_img->imageData + i*QCC_img->widthStep))[j] = connectionLabels[NE]; 
		NE_Flag = true; 
	      } 
	    
	    //check W 
	    if (W > 0 && W > X && connectionLabels[W] < X) 
	      { 
		connectionLabels[X] = connectionLabels[W]; 
		((unsigned short *)(QCC_img->imageData + i*QCC_img->widthStep))[j] = connectionLabels[W]; 
		W_Flag = true; 
	      } 
	    
	    //reassign root parents 
	    if (X > 0) 
	      { 
		tempParent = connectionLabels[X]; 
		if (tempParent > 0 && tempParent != connectionLabels[tempParent]) 
		  { 
		    while (tempParent != connectionLabels[tempParent]) 
		      { 
			tempParent = connectionLabels[tempParent]; 
		      } 
		    connectionLabels[X] = tempParent; 
		  } 
	      } 
	    
	    if (NW_Flag == true) 
	      { 
		tempParent = connectionLabels[NW]; 
		if (tempParent > 0 && tempParent != connectionLabels[tempParent]) 
		  { 
		    while (tempParent != connectionLabels[tempParent]) 
		      { 
			tempParent = connectionLabels[tempParent]; 
		      } 
		    connectionLabels[NW] = tempParent; 
		  } 
	      } 
	    
	    if (N_Flag == true) 
	      { 
		tempParent = connectionLabels[N]; 
		if (tempParent > 0 && tempParent != connectionLabels[tempParent]) 
		  { 
		    while (tempParent != connectionLabels[tempParent]) 
		      { 
			tempParent = connectionLabels[tempParent]; 
		      } 
		    connectionLabels[NW] = tempParent; 
		  } 
	      } 
	    
	    if (NE_Flag == true) 
	      { 
		tempParent = connectionLabels[NE]; 
		if (tempParent > 0 && tempParent != connectionLabels[tempParent]) 
		  { 
		    while (tempParent != connectionLabels[tempParent]) 
		      { 
			tempParent = connectionLabels[tempParent]; 
		      } 
		    connectionLabels[NW] = tempParent; 
		  } 
	      } 
	    
	    if (W_Flag == true) 
	      { 
		tempParent = connectionLabels[W]; 
		if (tempParent > 0 && tempParent != connectionLabels[tempParent]) 
		  { 
		    while (tempParent != connectionLabels[tempParent]) 
		      { 
			tempParent = connectionLabels[tempParent]; 
		      } 
		    connectionLabels[NW] = tempParent; 
		  } 
	      } 
	    
	  }//if X > 0 
	
      }//end for loops 
  
  //find root parent for each entry in connectionLabels 
  for (i=1; i<=labelCounter-1; i++) 
    { 
      tempParent = connectionLabels[i]; 
      if (tempParent > 0 && tempParent != connectionLabels[tempParent]) 
	{ 
	  while (tempParent != connectionLabels[tempParent]) 
	    { 
	      tempParent = connectionLabels[tempParent]; 
	    } 
	  connectionLabels[i] = tempParent; 
	} 
    } 
  
  //relabel based on roots 
  for (i=0; i<new_height; i++) 
    for (j=0; j<new_width; j++) 
      { 
	X = ((unsigned short *)(QCC_img->imageData + i*QCC_img->widthStep))[j]; 
	if (X > 0) 
	  { 
	    ((unsigned short *)(QCC_img->imageData + i*QCC_img->widthStep))[j]= connectionLabels[X]; 
	  } 
      } 
  
  
  //cvNamedWindow( "blurred", CV_WINDOW_AUTOSIZE ); 
  //cvShowImage( "blurred", QCC_img ); 
  
  //cvWaitKey( 0 ); 
  
  ////Determine the Number of Labels/////// 
#define MAXLABELS 300 
  int labelCounter1 = 0; 
  int targetArray [MAXLABELS]; 
  int tempArrayVal; 
  int numLabelsLeft; 
  for (i=0; i<MAXLABELS; i++) 
    targetArray[i]=0; 
  
  for (i=0; i<new_height; i++) 
    for (j=0; j<new_width; j++) 
      { 
	X = ((unsigned short *)(QCC_img->imageData + i*QCC_img->widthStep))[j]; 
	if (X != 0) 
	  { 
	    tempArrayVal = targetArray[X]; 
	    if (tempArrayVal == 0) 
	      { 
		//mark entry 
		targetArray[X]=1; 
		labelCounter1++; 
	      } 
	  } 
	
      } 
  
  numLabelsLeft = (int)labelCounter1; 
  
  /////////////////////////////////////Done with Labels/////////////////////////////////////// 
  
  /////create left and right array of x and y coordinates///// 
  int *leftXArr; 
  int *leftYArr; 
  int maxCCLabel; 
  
  leftXArr = (int *)malloc(sizeof(int)*numLabelsLeft+100); 
  leftYArr = (int *)malloc(sizeof(int)*numLabelsLeft+100); 
  
  float max=-100000000000; 
  //find max value which corresponds to eye location 
  for( i = 0; i < new_height ; i++ ) { 
    for( j = 0 ; j < new_width ; j++ ) { 
      
      temp=img3[i*new_width+j]; 
      X = ((unsigned short *)(QCC_img->imageData + i*QCC_img->widthStep))[j]; 
      if (temp > max) 
	{ 
	  max=temp; 
	  maxCCLabel = X; 
	  
	  *y_pos_left=i; 
	  *x_pos_left=j; 
	} 
    } 
  } 
  
  
  x_pos_left_gt= *x_pos_left + (int)left_eye_crop_x_start; 
  y_pos_left_gt= *y_pos_left + (int)left_eye_crop_y_start; 
  
  leftXArr[0] = x_pos_left_gt + FUDGE_FACTOR_X; 
  leftYArr[0] = y_pos_left_gt + FUDGE_FACTOR_Y; 
  
  int secondPeak = 0; 
  int labelCCLoops; 
  
  // numLabelsLeft = 1; 
  
  if (numLabelsLeft > 1) 
    { 
      for (labelCCLoops = 1; labelCCLoops <= numLabelsLeft; labelCCLoops++) 
	{ 
	  
	  secondPeak = 1; 
	  
	  //wipe out maxCCLabel Area 
	  for( i = 0; i < new_height ; i++ ) { 
	    for( j = 0 ; j < new_width ; j++ ) { 
	      
	      X = ((unsigned short *)(QCC_img->imageData + i*QCC_img->widthStep))[j]; 
	      
	      if (X == maxCCLabel) 
		((unsigned short *)(QCC_img->imageData + i*QCC_img->widthStep))[j] = 0; 
	      
	    } 
	  } 
	  
	  //search for next peak 
	  
	  max=-100000000000; 
	  //find max value which corresponds to eye location 
	  for( i = 0; i < new_height ; i++ ) { 
	    for( j = 0 ; j < new_width ; j++ ) { 
	      
	      temp=img3[i*new_width+j]; 
	      X = ((unsigned short *)(QCC_img->imageData + i*QCC_img->widthStep))[j]; 
	      
	      if (temp > max && X !=0) 
		{ 
		  max=temp; 
		  maxCCLabel = X; 
		  
		  *y_pos_left=i; 
		  *x_pos_left=j; 
		} 
	    } 
	  } 
	  
	  *x_pos_left=*x_pos_left + FUDGE_FACTOR_X; 
	  *y_pos_left=*y_pos_left + FUDGE_FACTOR_Y; 
	  
	  x_pos_left_gt=*x_pos_left + (int)left_eye_crop_x_start; 
	  y_pos_left_gt=*y_pos_left + (int)left_eye_crop_y_start; 
	  
	  leftXArr[labelCCLoops] = x_pos_left_gt; 
	  leftYArr[labelCCLoops] = y_pos_left_gt; 
	} 
    } 
  //cvNamedWindow( "detected_left", CV_WINDOW_AUTOSIZE ); 
  //cvShowImage( "detected_left", detectedImage ); 
  
  //cvWaitKey( 0 ); 
  
  
  
  
  
  ////////////////// RIGHT EYE /////////////////////////////////// 
  right_eye_crop_x_start = floor(width * .1); 
  right_eye_crop_y_start = floor(height * .15); 
  
  IplImage* flipOriginal = cvCreateImage(cvGetSize(blurred_image),blurred_image->depth,blurred_image->nChannels); 
  cvFlip(blurred_image,flipOriginal,1); 
  
  CROP_EYE (img1, flipOriginal, (int) right_eye_crop_x_start, (int) right_eye_crop_y_start, size_crop); 
  
  //cvNamedWindow( "fl", CV_WINDOW_AUTOSIZE ); 
  //cvShowImage( "fl", flipOriginal ); 
  
  // cvWaitKey( 0 ); 
  cvReleaseImage (&flipOriginal); 
  
  
  LIGHTING_NORMALIZATION(img1, new_width, new_height); 
  
  //cvNamedWindow("CROP1",CV_WINDOW_AUTOSIZE); 
  //cvShowImage("CROP1",img1); 
  //cvWaitKey(0); 
  // load eye data to fftw input 
  for(i = 0, k = 0; i < new_height; i++) 
    { 
      for(j = 0; j < new_width; j++) 
	{ 
	  data_in[k][0] = (double)((uchar*)(img1->imageData + i * img1->widthStep))[j]; 
	  data_in[k][1] = 0.0; 
	  k++; 
	} 
    } 
  
  // perform FFT 
  fftw_execute(plan_f); 
  
  ptr_var = 0; 
  
  // load eye filter data 
  for(i = 0, k = 0; i < new_height; i++) 
    { 
      for(j = 0; j < new_width; j++) 
	{ 
	  eye_out[k][0] = (double)eye_data_right[ptr_var]; 
	  ptr_var++; 
	  eye_out[k][1] = (double)eye_data_right[ptr_var]; 
	  k++; 
	  ptr_var++; 
	} 
    } 
  
  // perfrom correlation 
  
  //eye filter already preprocessed with hanning window and conjugated 
  for(k = 0; k < new_height * new_width; k++) 
    { 
      fft[k][0] = ((fft[k][0] * eye_out[k][0]) - (fft[k][1] * eye_out[k][1])); 
      fft[k][1] = ((fft[k][0] * eye_out[k][1]) + (fft[k][1] * eye_out[k][0])); 
    } 
  
  // perfrom ifft 
  fftw_execute(plan_r); 
  
  // normalize IFFT result 
  
  // copy IFFT result to img_float's data 
  
  //added abs to get rid of errors in conversion 
  for(i = 0, k = 0; i < new_height; i++) 
    { 
      for(j = 0; j < new_width; j++) 
	(img3[i * new_width + j]) = ((float)ifft[i * new_width + j][0]); 
    } 
  
  //IFFT SHIFT 
  //!!!!image rows and columns have to be even!!!!! 
  
  //int rows_half, columns_half; 
  rows_half    = new_height / 2; 
  columns_half = new_width / 2; 
  
  for(i = 0; i < 32; i++) 
    { 
      for(j = 0; j < 32; j++) 
	{ 
	  temp1_d = img3[(i * new_width) + j]; 
	  temp2_d = img3[(i * new_width) + (j + columns_half)]; 
	  temp3_d = img3[((i + rows_half) * new_width) + (j + columns_half)]; 
	  temp4_d = img3[((i + rows_half) * new_width) + j]; 
	  
	  //1=3 
	  img3[(i * new_width) + j] = temp3_d; 
	  
	  //2=4 
	  img3[(i * new_width) + (j + columns_half)] = temp4_d; 
	  
	  //3=1 
	  img3[((i + rows_half) * new_width) + (j + columns_half)] = temp1_d; 
	  
	  //4=2 
	  img3[((i + rows_half) * new_width) + j] = temp2_d; 
	} 
    } 
  
  //set borders to 0 
  for(i = 0; i < new_height; i++) 
    { 
      for(j = 0; j < new_width; j++) 
	{ 
	  if(i > 10 && i < 50 && j > 10 && j < 50) 
	    dd = 0; 
	  else 
	    img3[i * new_width + j] = 0; 
	} 
    } 
  
  max_right = -1000000000; 
  
  /////////////////////////////////////////////CC////////////////////////////////////// 
  IplImage *detectedImage1 = 0; 
  detectedImage1 = cvCreateImage( cvSize(64, 64), IPL_DEPTH_8U, 1 ); 
  
  //find max value 
  maxVal=-100000000000; 
  //find max value which corresponds to eye location 
  for( i = 0; i < new_height ; i++ ) { 
    for( j = 0 ; j < new_width ; j++ ) { 
      
      temp=img3[i*new_width+j]; 
      
      if (temp > maxVal) 
	{ 
	  maxVal=temp; 
	} 
    } 
  } 
  
  //Create Thresholded Image 
  THRESHOLD = maxVal * QCC_Threshold; 
  
  for( i = 0; i < new_height ; i++ ) { 
    for( j = 0 ; j < new_width ; j++ ) { 
      
      temp=img3[i*new_width+j]; 
      
      if (temp > THRESHOLD) 
	{ 
	  ((unsigned char *)(detectedImage1->imageData + i*detectedImage1->widthStep))[j]=255; 
	} 
      
      else 
	((unsigned char *)(detectedImage1->imageData + i*detectedImage1->widthStep))[j]=0; 
      
    } 
  } 
  
  
  //cvNamedWindow( "detected_right", CV_WINDOW_AUTOSIZE ); 
  //cvShowImage( "detected_right", detectedImage1 ); 
  //cvWaitKey(0); 
  //////////////////////Connected Components///////////////////////////// 
  
  IplImage *QCC_img1 = 0; 
  
  unsigned short connectionLabels1 [4000]; 
  
  QCC_THRESHOLD = 0; 
  
  QCC_img1 = cvCreateImage( cvSize(64, 64), IPL_DEPTH_16U, 1 ); 
  
  for (i=0; i<4000; i++) 
    { 
      connectionLabels1[i] = 0; 
    } 
  unsigned short QCC_debug; 
  for (i=0; i<new_height; i++) 
    for (j=0; j<new_width; j++) 
      { 
	tempQCCPixel = ((unsigned char *)(detectedImage1->imageData + i*detectedImage1->widthStep))[j]; 
	
	if (tempQCCPixel > QCC_THRESHOLD) 
	  //tempQCCPixel = 1; 
	  QCC_debug=1; 
	else 
	  //tempQCCPixel = 0; 
	  QCC_debug=0; 
	((unsigned short *)(QCC_img1->imageData + i*QCC_img1->widthStep))[j] = QCC_debug; 
      } 
  
  //cvNamedWindow( "detected_right_QCC", CV_WINDOW_AUTOSIZE ); 
  //cvShowImage( "detected_right_QCC", QCC_img1 ); 
  //cvWaitKey(0); 
  //perfrom Connected Components 
  NW = 0; 
  N = 0; 
  NE = 0; 
  W = 0; 
  X = 0; 
  labelCounter = 1; 
  
  for (i=2; i<new_height; i++) 
    for (j=2; j<new_width; j++) 
      { 
	NW_Flag = false; 
	N_Flag = false; 
	NE_Flag = false; 
	W_Flag = false; 
	
	NW = ((unsigned short *)(QCC_img1->imageData + (i-1)*QCC_img1->widthStep))[j-1]; 
	
	if (NW > 0) 
	  NW_Label = connectionLabels1[NW]; 
	else 
	  NW_Label = 0; 
	
	if (NW != NW_Label && NW_Label > 0) 
	  NW = NW_Label; 
	
	N = ((unsigned short *)(QCC_img1->imageData + (i-1)*QCC_img1->widthStep))[j]; 
	
	if (N > 0) 
	  N_Label = connectionLabels1[N]; 
	else 
	  N_Label = 0; 
	
	if (N != N_Label && N_Label > 0) 
	  N = N_Label; 
	
	NE = ((unsigned short *)(QCC_img1->imageData + (i-1)*QCC_img1->widthStep))[j+1]; 
	
	if (NE > 0) 
	  NE_Label = connectionLabels1[NE]; 
	else 
	  NE_Label = 0; 
	
	if (NE != NE_Label && NE_Label > 0) 
	  NE = NE_Label; 
	
	W = ((unsigned short *)(QCC_img1->imageData + (i)*QCC_img1->widthStep))[j-1]; 
	
	if (W > 0) 
	  W_Label = connectionLabels1[W]; 
	else 
	  W_Label = 0; 
	
	if (W != W_Label && W_Label > 0) 
	  W = W_Label; 
	
	X = ((unsigned short *)(QCC_img1->imageData + i*QCC_img1->widthStep))[j]; 
	
	if (X > 0) 
	  { 
	    minVal = 65000; 
	    
	    if (NW > 0 && NW < minVal) 
	      minVal = NW; 
	    if (N > 0 && N < minVal) 
	      minVal = N; 
	    if (NE > 0 && NE < minVal) 
	      minVal = NE; 
	    if (W > 0 && W < minVal) 
	      minVal = W; 
	    
	    minLabel = minVal; 
	    
	    //Does pixel to N have same value but not same Label 
	    //assign pixel to min label 
	    if (N > 0 || NE > 0 || NW > 0 || W > 0) 
	      { 
		((unsigned short *)(QCC_img1->imageData + i*QCC_img1->widthStep))[j] = minLabel; 
		  X = minLabel; 
		  
		  //check NW 
		  if (NW > 0 && minLabel < connectionLabels1[NW]) 
		    { 
		      connectionLabels1[NW] = minLabel; 
		      ((unsigned short *)(QCC_img1->imageData + (i-1)*QCC_img1->widthStep))[j-1] = minLabel; 
		    } 
		  
		  //check N 
		  if (N > 0 && minLabel < connectionLabels1[N]) 
		    { 
		      connectionLabels1[N] = minLabel; 
		      ((unsigned short *)(QCC_img1->imageData + (i-1)*QCC_img1->widthStep))[j] = minLabel; 
		    } 
		  
		  //check NE 
		  if (NE > 0 && minLabel < connectionLabels1[NE]) 
		    { 
		      connectionLabels1[NE] = minLabel; 
		      ((unsigned short *)(QCC_img1->imageData + (i-1)*QCC_img1->widthStep))[j+1] = minLabel; 
		    } 
		  
		  //check W 
		  if (W > 0 && minLabel < connectionLabels1[W]) 
		    { 
		      connectionLabels1[W] = minLabel; 
		      ((unsigned short *)(QCC_img1->imageData + (i)*QCC_img1->widthStep))[j-1] = minLabel; 
		    } 
		  
		}//if (N > 0) 
	    
	    //neighbors are empty 
	    else 
	      { 
		((unsigned short *)(QCC_img1->imageData + i*QCC_img1->widthStep))[j] = labelCounter; 
		connectionLabels1[labelCounter] = labelCounter; 
		labelCounter++; 
	      } 
	    
	    //check NW 
	    if (NW > 0 && NW > X && connectionLabels1[NW] < X) 
	      { 
		connectionLabels1[X] = connectionLabels1[NW]; 
		((unsigned short *)(QCC_img1->imageData + i*QCC_img1->widthStep))[j] = connectionLabels[NW]; 
		NW_Flag = true; 
	      } 
	    
	    //check N 
	    if (N > 0 && N > X && connectionLabels1[N] < X) 
	      { 
		connectionLabels1[X] = connectionLabels1[N]; 
		((unsigned short *)(QCC_img1->imageData + i*QCC_img1->widthStep))[j] = connectionLabels[N]; 
		N_Flag = true; 
	      } 
	    
	    //check NE 
	    if (NE > 0 && NE > X && connectionLabels1[NE] < X) 
	      { 
		connectionLabels1[X] = connectionLabels[NE]; 
		((unsigned short *)(QCC_img1->imageData + i*QCC_img1->widthStep))[j] = connectionLabels[NE]; 
		NE_Flag = true; 
	      } 
	    
	    //check W 
	    if (W > 0 && W > X && connectionLabels1[W] < X) 
	      { 
		connectionLabels1[X] = connectionLabels1[W]; 
		((unsigned short *)(QCC_img1->imageData + i*QCC_img1->widthStep))[j] = connectionLabels[W]; 
		  W_Flag = true; 
	      } 
	    
	    //reassign root parents 
	    if (X > 0) 
	      { 
		tempParent = connectionLabels1[X]; 
		if (tempParent > 0 && tempParent != connectionLabels1[tempParent]) 
		  { 
		    while (tempParent != connectionLabels1[tempParent]) 
		      { 
			tempParent = connectionLabels1[tempParent]; 
		      } 
		    connectionLabels1[X] = tempParent; 
		  } 
		} 
	    
	    if (NW_Flag == true) 
	      { 
		tempParent = connectionLabels1[NW]; 
		if (tempParent > 0 && tempParent != connectionLabels1[tempParent]) 
		  { 
		    while (tempParent != connectionLabels1[tempParent]) 
		      { 
			tempParent = connectionLabels1[tempParent]; 
		      } 
		    connectionLabels1[NW] = tempParent; 
		  } 
	      } 
	    
	    if (N_Flag == true) 
	      { 
		tempParent = connectionLabels1[N]; 
		if (tempParent > 0 && tempParent != connectionLabels1[tempParent]) 
		  { 
		    while (tempParent != connectionLabels1[tempParent]) 
		      { 
			tempParent = connectionLabels1[tempParent]; 
		      } 
		    connectionLabels1[NW] = tempParent; 
		  } 
	      } 
	    
	    if (NE_Flag == true) 
	      { 
		tempParent = connectionLabels1[NE]; 
		if (tempParent > 0 && tempParent != connectionLabels1[tempParent]) 
		  { 
		    while (tempParent != connectionLabels1[tempParent]) 
		      { 
			tempParent = connectionLabels1[tempParent]; 
		      } 
		    connectionLabels[NW] = tempParent; 
		  } 
	      } 
	    
	    if (W_Flag == true) 
	      { 
		tempParent = connectionLabels1[W]; 
		if (tempParent > 0 && tempParent != connectionLabels1[tempParent]) 
		  { 
		      while (tempParent != connectionLabels1[tempParent]) 
			{ 
			  tempParent = connectionLabels1[tempParent]; 
			} 
		      connectionLabels1[NW] = tempParent; 
		  } 
	      } 
	    
	  }//if X > 0 
	
      }//end for loops 
     
  //find root parent for each entry in connectionLabels 
  for (i=1; i<=labelCounter-1; i++) 
    { 
      tempParent = connectionLabels1[i]; 
      if (tempParent > 0 && tempParent != connectionLabels1[tempParent]) 
	{ 
	  while (tempParent != connectionLabels1[tempParent]) 
	    { 
	      tempParent = connectionLabels1[tempParent]; 
	    } 
	  connectionLabels1[i] = tempParent; 
	} 
    } 
  
  //relabel based on roots 
    for (i=0; i<new_height; i++) 
      for (j=0; j<new_width; j++) 
	{ 
	  X = ((unsigned short *)(QCC_img1->imageData + i*QCC_img1->widthStep))[j]; 
	  if (X > 0) 
	    { 
	      ((unsigned short *)(QCC_img1->imageData + i*QCC_img1->widthStep))[j]= connectionLabels1[X]; 
	    } 
	} 
    
    
     // cvNamedWindow( "blurred", CV_WINDOW_AUTOSIZE ); 
     // cvShowImage( "blurred", QCC_img1 ); 
    
    //  cvWaitKey( 0 ); 
    
    ////Determine the Number of Labels/////// 
    int numLabelsRight; 
    
    labelCounter1 = 0; 
    
    for (i=0; i < MAXLABELS; i++) 
      targetArray[i]=0; 
    
    for (i=0; i<new_height; i++) 
      for (j=0; j<new_width; j++) 
	{ 
	  X = ((unsigned short *)(QCC_img1->imageData + i*QCC_img1->widthStep))[j]; 
	  if (X != 0) 
	    { 
	      tempArrayVal = targetArray[X]; 
	      
	      if (tempArrayVal == 0) 
		{ 
		  //mark entry 
		  targetArray[X]=1; 
		  labelCounter1++; 
		} 
	    } 
	   
	} 
    
    numLabelsRight = (int)labelCounter1; 
    
    /////////////////////////////////////Done with Labels/////////////////////////////////////// 
    
    /////create left and right array of x and y coordinates///// 
    int *rightXArr; 
    int *rightYArr; 
    
    rightXArr = (int *)malloc(sizeof(int)*numLabelsRight+100); 
    rightYArr = (int *)malloc(sizeof(int)*numLabelsRight+100); 
    
    maxCCLabel = 0; 
    max=-100000000000; 
    //find max value which corresponds to eye location 
    for( i = 0; i < new_height ; i++ ) { 
      for( j = 0 ; j < new_width ; j++ ) { 
	
	temp=img3[i*new_width+j]; 
	X = ((unsigned short *)(QCC_img1->imageData + i*QCC_img1->widthStep))[j]; 
	if (temp > max) 
	  { 
	    max=temp; 
	    maxCCLabel = X; 
	    
	    *y_pos_right=i; 
	    *x_pos_right=j; 
	  } 
      } 
    } 
    
    //	x_pos_right=x_pos_right + FUDGE_FACTOR_X; 
    //y_pos_right=y_pos_right + FUDGE_FACTOR_Y; 
    
    x_pos_right_gt=blurred_image->width - *x_pos_right - right_eye_crop_x_start; 
    y_pos_right_gt=*y_pos_right + right_eye_crop_y_start; 
    
    rightXArr[0] = x_pos_right_gt + FUDGE_FACTOR_X; 
    rightYArr[0] = y_pos_right_gt + FUDGE_FACTOR_Y; 
    
    int secondPeakRight = 0; 
    
    if (numLabelsRight > 1) 
      { 
	for (labelCCLoops = 1; labelCCLoops <= numLabelsRight; labelCCLoops++) 
	  { 
	    
	    secondPeakRight = 1; 
	     
	    //wipe out maxCCLabel Area 
	    for( i = 0; i < new_height ; i++ ) { 
	      for( j = 0 ; j < new_width ; j++ ) { 
		 
		X = ((unsigned short *)(QCC_img1->imageData + i*QCC_img1->widthStep))[j]; 
		
		if (X == maxCCLabel) 
		  ((unsigned short *)(QCC_img1->imageData + i*QCC_img1->widthStep))[j] = 0; 
		
	      } 
	    } 
	    
	    //search for next peak 
	    
	    max=-100000000000; 
	    //find max value which corresponds to eye location 
	    for( i = 0; i < new_height ; i++ ) { 
	      for( j = 0 ; j < new_width ; j++ ) { 
		
		temp=img3[i*new_width+j]; 
		X = ((unsigned short *)(QCC_img1->imageData + i*QCC_img1->widthStep))[j]; 
		
		if (temp > max && X !=0) 
		  { 
		    max=temp; 
		    maxCCLabel = X; 
		    
		    *y_pos_right=i; 
		    *x_pos_right=j; 
		  } 
	      } 
	    } 
	    
	    
	    x_pos_right_gt = blurred_image->width - *x_pos_right - right_eye_crop_x_start; 
	    y_pos_right_gt = *y_pos_right + right_eye_crop_y_start; 
	    
	    rightXArr[labelCCLoops] = x_pos_right_gt - FUDGE_FACTOR_X; 
	    rightYArr[labelCCLoops] = y_pos_right_gt + FUDGE_FACTOR_Y; 
	  } 
      } 
	 
    /////////////////////PERTURBATIONS////////////////////////// 
    
#ifdef LINUX
    for (i=1; i<=numLabelsLeft*numLabelsRight; i++)
      {
	sprintf(image_name,"./input/%d.pgm",i);
	  cvSaveImage(image_name,blurred_image);
      }
    
    //geo norm the images
    char command[1024];
    char buffer[32];
    char file_name[64];
    char pgm_name[64];
    string hold;
    string::size_type pos;
    
    
    int imageCounter = 1;
    for (i=0; i<numLabelsLeft; i++)
      for (j=0; j<numLabelsRight; j++)
	{
	  sprintf(buffer, "./input/%d", imageCounter);
	  sprintf(file_name,"%s.file", buffer);
	  sprintf(pgm_name, "%s.pgm", buffer);
	  
	  hold = buffer;
	  pos = hold.rfind("/");
	  FILE* f = fopen(file_name, "w");
	  
	  fprintf(f, "%s %d %d %d %d\n", hold.substr(pos+1, hold.size()-1).c_str(), leftXArr[i], leftYArr[i], rightXArr[j], rightYArr[j]);
	  fclose(f);
	  
	  snprintf(command, 1024, "../csuFaceIdEval.5.0/bin/csuPreprocessNormalize -pgm ./output -mask NO ");
	  strcat(command, "-hist POST ");
	  sprintf(command, "%s %s ./input", command, file_name);
	  
	  if(system(command) != 0)
	    fprintf(stderr, "WARNING: command returned non-zero: %s\n", command);
	  
	  imageCounter++;
	}
    
    
#else 
    //write out the images  
    for (i=1; i<=numLabelsLeft*numLabelsRight; i++) 
      { 
	sprintf(image_name,"C:/newEyeDetectorBase/SecuricsGeoNorm/SecuricsGeoNorm/input/%d.jpg",i); 
	cvSaveImage(image_name,blurred_image); 
      } 
    
    //geo norm the images 
    int imageCounter = 1; 
    
    
    
    
    //	  LPCWSTR tempInfo ; 
    for (i=0; i<numLabelsLeft; i++) 
      { 
	
	for (j=0; j<numLabelsRight; j++) 
	  { 
	    
	    std::stringstream out; 			 
	    out << "C:/newEyeDetectorBase/SecuricsGeoNorm/SecuricsGeoNorm/input/" << imageCounter << ".jpg "  << "C:/newEyeDetectorBase/SecuricsGeoNorm/SecuricsGeoNorm/output/" << imageCounter << ".jpg " << leftXArr[i] << " " << leftYArr[i] << " " <<  rightXArr[j] << " " <<  rightYArr[j];  
	    std::string s = out.str();  
	    std::wstring stemp = s2ws(s); // Temporary buffer is required 
	    LPCWSTR tempInfo; 
	    tempInfo = stemp.c_str(); 
	    SHELLEXECUTEINFO sei; 
	    LPCWSTR tempFile = L"C:/geo/geo-norm.exe"; 
	    LPCWSTR tempOpen = L"open"; 
	    //SHELLEXECUTEINFO sei; 
	    sei.cbSize = sizeof(SHELLEXECUTEINFO); 
	    sei.fMask = NULL;  
	    sei.hwnd = NULL;  
	    sei.lpVerb = tempOpen; 
	    sei.lpFile = tempFile; 
	    sei.lpParameters= tempInfo;  
	    sei.nShow = SW_SHOWNORMAL;  
	    sei.hInstApp = NULL;  
	    sei.lpIDList = NULL;  
	    sei.lpClass = NULL;  
	    sei.hkeyClass = NULL;  
	    sei.dwHotKey = NULL;  
	    sei.hIcon = NULL;  
	    sei.hProcess = NULL;  
	    sei.lpDirectory = NULL; 
	    int ReturnCode = ::ShellExecuteEx(&sei);  
	    
	    imageCounter++; 
	  } 
      } 
    //need beacuse GEONORM.exe not finished before next for loop 
    //wrong first image displayed 
    _sleep(1000); 
#endif 
    //Determine the best image 
    int totalImages = numLabelsLeft * numLabelsRight; 
    
    
    float* scoreArray; 
    scoreArray = (float *) malloc(sizeof(float)*totalImages+10); 
    
    //test MACE filter 
    int    rows_half_face, columns_half_face; 
    double temp1_d_face, temp2_d_face, temp3_d_face, temp4_d_face; 
    //	double dd_face; 
    //double temp_face; 
    
    fftw_complex *data_in_face; 
    fftw_plan    plan_f_face; 
    fftw_plan    plan_r_face; 
    fftw_complex *fft_face; 
    fftw_complex *ifft_face; 
    fftw_complex *face_out; 
    
    //fftw_plan    plan_face; 
    fftw_complex *face_in; 
    
    
    
    int    width_face, height_face, step_face;
    
#ifdef LINUX
    sprintf(image_name,"./generic_face.pgm");
#else 
    sprintf(image_name,"C:/newEyeDetectorBase/SecuricsGeoNorm/SecuricsGeoNorm/generic_face.pgm");
#endif 
    IplImage* tempImage = cvLoadImage(image_name,0); 
    width_face  = tempImage->width; 
    height_face = tempImage->height; 
    step_face   = tempImage->widthStep; 
    
    float  *img3_face; 
    img3_face     = (float*)malloc(sizeof(float) * width_face * height_face); 
    
    data_in_face  = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * width_face * height_face); 
    fft_face      = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * width_face * height_face); 
    ifft_face     = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * width_face * height_face); 
    face_out      = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * width_face * height_face); 
    face_in       = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * width_face * height_face); 
    
    plan_f_face   = fftw_plan_dft_1d(width_face * height_face, data_in_face, fft_face, FFTW_FORWARD, FFTW_ESTIMATE); 
    plan_r_face   = fftw_plan_dft_1d(width_face * height_face, fft_face, ifft_face, FFTW_BACKWARD, FFTW_ESTIMATE); 
    int ii; 
    float maxCorrVal = -99999; 
    int xloc=0;  
    int yloc=0; 
    //	int kloc; 
 
    
    IplImage* tempImage1A; 
    
    
    IplImage* tempImage1 = cvCreateImage(cvSize(tempImage->width,tempImage->height),tempImage->depth,tempImage->nChannels); 
    
    cvReleaseImage(&tempImage);
    
    for (ii=0; ii<=totalImages-1; ii++) 
      { 
	//read image
#ifdef LINUX
	sprintf(image_name,"./output/%d.pgm",ii+1);
#else 
	sprintf(image_name,"output/%d.jpg",ii+1);
#endif 
	tempImage1A = cvLoadImage(image_name,0); 
	
	
	
	width_face  = tempImage1A->width; 
	height_face = tempImage1A->height; 
	step_face   = tempImage1A->widthStep; 
	
	cvEqualizeHist(tempImage1A,tempImage1); 
	
	
	// load eye data to fftw input 
	for(i = 0, k = 0; i < width_face; i++) 
	  { 
	    for(j = 0; j < height_face; j++) 
	      { 
		data_in_face[k][0] = (double)((uchar*)(tempImage1->imageData + j * tempImage1->widthStep))[i]; 
		data_in_face[k][0] /= 255; 
		data_in_face[k][1] = 0.0; 
		k++; 
	      } 
	  } 
	
	//cvNamedWindow( "temp image", CV_WINDOW_AUTOSIZE ); 
	//cvShowImage( "temp image", tempImage1 ); 
	//cvWaitKey(0); 
	// perform FFT 
	fftw_execute(plan_f_face); 
	
	int ptr_var = 0; 
	
	// load eye filter data 
	for(k = 0; k < width_face*height_face; k++) 
	  { 
	    face_out[k][0] = (double)face_data2[ptr_var]; 
	    ptr_var++; 
	    face_out[k][1] = (double)face_data2[ptr_var]; 
	    ptr_var++; 
	  } 
	
	for(k = 0; k < height_face * width_face; k++) 
	  { 
	    fft_face[k][0] = ((fft_face[k][0] * face_out[k][0]) - (fft_face[k][1] * face_out[k][1])); 
	    fft_face[k][1] = ((fft_face[k][0] * face_out[k][1]) + (fft_face[k][1] * face_out[k][0])); 
	  } 
	
	// perfrom ifft 
	fftw_execute(plan_r_face); 
	
	k=0; 
	for (i=0; i<height_face; i++) 
	  { 
	    for (j=0; j<width_face; j++) 
	      { 
		img3_face[k] = (float)((ifft_face[k][0])/(150*130)); 
		k++; 
	      } 
	  } 
	
	//int rows_half, columns_half; 
	rows_half_face    = width_face / 2; 
	columns_half_face = height_face / 2; 
	
	for(i = 0; i < 65; i++) 
	  { 
	    for(j = 0; j < 75; j++) 
	      { 
		temp1_d_face = img3_face[(i * height_face) + j]; 
		temp2_d_face = img3_face[(i * height_face) + (j + columns_half_face)]; 
		temp3_d_face = img3_face[((i + rows_half_face) * height_face) + (j + columns_half_face)]; 
		temp4_d_face = img3_face[((i + rows_half_face) * height_face) + j]; 
		
		//1=3 
		img3_face[(i * height_face) + j] = temp3_d_face; 
		
		//2=4 
		img3_face[(i * height_face) + (j + columns_half_face)] = temp4_d_face; 
		
		//3=1 
		img3_face[((i + rows_half_face) * height_face) + (j + columns_half_face)] = temp1_d_face; 
		
		//4=2 
		img3_face[((i + rows_half_face) * height_face) + j] = temp2_d_face; 
	      } 
	  } 
	
	k=0; 
	float temp1; 
	//added abs to get rid of errors in conversion 
	maxCorrVal = -99999; 
	xloc=0;  
	yloc=0; 
	
	int PEAK_SEARCH_RANGE = 15; 
	
	//need to add PSR computation 
	
	for(i = (width_face/2)-PEAK_SEARCH_RANGE; i < (width_face/2)+PEAK_SEARCH_RANGE; i++) 
	  { 
	    for(j = (height_face/2)-PEAK_SEARCH_RANGE; j < (height_face/2)+PEAK_SEARCH_RANGE; j++) 
	      { 
		temp1=((float)(img3_face[i * height_face + j])); 
		
		if (temp1 > maxCorrVal) 
		  { 
		    maxCorrVal =temp1; 
		    //kloc=k; 
		    xloc=i;		 
		    yloc=j; 
		  } 
		//k++; 
	      } 
	  } 
	
	//put value into scoreArray 
	scoreArray[ii] = maxCorrVal; 
	// cvReleaseImage (&tempImage1); 
      } 
    
    for (i=0; i<totalImages; i++) 
      { 
	fprintf(stderr,"Score Array[%d]: %f\r\n",i,scoreArray[i]); 
      } 
    ///////////////////SYMMETRY TEST 2 Images///////////////////////////// 
    
    float newScoreArray[2][2];  
    
    //find the max Indice of the scoreArray 
    int maxIndice = 0; 
    float maxScore = -10000000; 
    float tempMaxScore; 
    for (i=0; i<totalImages; i++) 
      { 
	
	tempMaxScore = scoreArray[i]; 
	if (tempMaxScore > maxScore) 
	  { 
	    maxScore = tempMaxScore; 
	    maxIndice = i; 
	  } 
      } 
    float maxScoreSave = maxScore; 
    
    //assign to newScoreArray 
    newScoreArray[0][0]=maxScore/maxScoreSave; 
    newScoreArray[0][1]=maxIndice; 
    
    //clear max entry 
    scoreArray[maxIndice]=0; 
    
    //get 2nd max image 
    maxIndice = 0; 
    maxScore = -10000000; 
    tempMaxScore; 
    for (i=0; i<totalImages; i++) 
      { 
	
	tempMaxScore = scoreArray[i]; 
	if (tempMaxScore > maxScore) 
	  { 
	    maxScore = tempMaxScore; 
	    maxIndice = i; 
	  } 
      } 
    
    //assign to newScoreArray 
    newScoreArray[1][0]=maxScore/maxScoreSave; 
    newScoreArray[1][1]=maxIndice; 
    for(i=0; i<2; i++) 
      printf("newScoreArray[%d]: index: %f score: %f\r\n",i,newScoreArray[i][1],newScoreArray[i][0]); 
    ///////////////////////////////////perform symmetry test/////////////////////////// 
    
    //sprintf(image_name,"output/%d.jpg",int(newScoreArray[symLoop][1])+1); 
    for (symLoop=0; symLoop <=1; symLoop++) 
      { 
#ifdef LINUX
	sprintf(image_name,"./output/%d.pgm",int(newScoreArray[symLoop][1])+1);
#else
	sprintf(image_name,"output/%d.jpg",int(newScoreArray[symLoop][1])+1);
#endif 
	//load original image 
	originalImage = cvLoadImage(image_name,0); 
	cvEqualizeHist(originalImage,originalImage); 
	width  = originalImage->width; 
	height = originalImage->height; 
	
	//crop half of image 
	IplImage* tempImage1 = cvCreateImage(cvGetSize(originalImage),originalImage->depth,originalImage->nChannels); 
	cvCopy(originalImage,tempImage1,NULL); 
	
	//first half of image 
	cvSetImageROI(tempImage1,  cvRect(0, 0, 65, 150)); 
	IplImage* crop1 = cvCreateImage(cvGetSize(tempImage1),tempImage1->depth,tempImage1->nChannels); 
	cvCopy(tempImage1,crop1,NULL); 
	
	//second half of image 
	cvSetImageROI(tempImage1, cvRect(65, 0, 65, 150)); 
	IplImage* crop2 = cvCreateImage(cvGetSize(tempImage1),tempImage1->depth,tempImage1->nChannels); 
	cvCopy(tempImage1,crop2,NULL); 
	
	//flip crop2 
	IplImage* crop3 = cvCreateImage(cvGetSize(crop2),crop2->depth,crop2->nChannels); 
	cvFlip(crop2,crop3,1); 
	
	//cvNamedWindow( "crop1", CV_WINDOW_AUTOSIZE ); 
	//cvShowImage( "crop1", crop1 ); 	 
	
	//cvNamedWindow( "crop2", CV_WINDOW_AUTOSIZE ); 
	//cvShowImage( "crop2", crop2 ); 	 
	
	//cvNamedWindow( "crop3", CV_WINDOW_AUTOSIZE ); 
	//cvShowImage( "crop3", crop3 ); 	 
	//cvWaitKey(0); 
	
	
	//transpose row and column and 1-D FFT of image 
	
	rows = crop1->height; 
	columns = crop1->width; 
	
	
	newRow = columns; 
	fftCounter = 0; 
	
	
	
	for (i = 0; i<rows; i++) 
	  { 
	    for(j = 0; j<columns; j++) 
	      { 
		tempRow[fftCounter] = CV_IMAGE_ELEM(crop3, uchar, i, j); 
		fftCounter++; 
		
		if (rowCounter%newRow == 0) 
		  { 
		    columnCounter++; 
		    rowCounter = 1; 
		  } 
		else 
		  { 
		    rowCounter++; 
		  } 
	      } 
	    
	    //fft of tempRow 
	    fftCounter= 0; 
	    for(k = 0; k < crop1->width; k++) 
	      { 
		data_in_sym[k][0] = (double)tempRow[k]; 
		data_in_sym[k][1] = 0.0; 
	      } 
	    
	    // perform FFT (results in tempfft) 
	    fftw_execute(plan_f_sym); 
	    
	    //transpose and write out temp results 
	    for (inner_loop=0; inner_loop < columns; inner_loop++) 
	      { 
		fftArray[((i*columns)+inner_loop)][0] = tempfft[inner_loop][0]; 
		fftArray[((i*columns)+inner_loop)][1] = tempfft[inner_loop][1]; 
	      } 
	  } 
	
	
	//column wise fft of fftArray 
	counter = 0; 
	for (column_loop=0; column_loop < columns; column_loop++) 
	  { 
	    for(row_loop=0; row_loop < rows; row_loop++) 
	      { 
		//get 1 column of fftArray 
		index = (row_loop*columns+column_loop); 
		fftTempRow [row_loop][0] = fftArray[index][0]; 
		fftTempRow [row_loop][1] = fftArray[index][1]; 
	      } 
	    
	    // perform FFT (results in tempfft1) 
	    fftw_execute(plan_f1_sym); 
	    
	    
	    //assign tempfft1 to fftArrayFinal 
	    for (inner_loop=0; inner_loop < rows; inner_loop++) 
	      { 
		fftArrayFinal[counter][0] = tempfft1[inner_loop][0]; 
		//complex conjugate 
		fftArrayFinal[counter][1] = -1 * (tempfft1[inner_loop][1]); 
		counter++; 
	      } 
	  } 
	
	//ABS of FFTArrayFinal; 
	
	
	for (loop=0; loop < crop1->width*crop1->height; loop++) 
	  { 
	    D[loop] = sqrt((fftArrayFinal[loop][0] * fftArrayFinal[loop][0]) + (fftArrayFinal[loop][1] * fftArrayFinal[loop][1]));  
	  } 
	
	//h = D^-1*m UMACE 
	
	for (loop=0; loop < crop1->width*crop1->height; loop++) 
	  { 
	    if (D==0) 
	      temp_D=0; 
	    else 
	      temp_D = 1/D[loop]; 
	    
	    UMACE_Filter[loop][0] = temp_D * fftArrayFinal[loop][0]; 
	    UMACE_Filter[loop][1] = temp_D * fftArrayFinal[loop][1]; 
	    
	  } 
	
	//////2D fft of Test Image////// 
	rowCounter = 0; 
	columnCounter = 0; 
	newRow = columns; 
	fftCounter=0; 
	for (i = 0; i<rows; i++) 
	  { 
	    for(j = 0; j<columns; j++) 
	      { 
		tempRow[fftCounter] = CV_IMAGE_ELEM(crop1, uchar, i, j); 
		fftCounter++; 
		
		if (rowCounter%newRow == 0) 
		  { 
		    columnCounter++; 
		    rowCounter = 1; 
		  } 
		else 
		  { 
		    rowCounter++; 
		  } 
	      } 
	    
	    //fft of tempRow 
	    fftCounter= 0; 
	    for(k = 0; k < crop1->width; k++) 
	      { 
		data_in_sym[k][0] = (double)tempRow[k]; 
		data_in_sym[k][1] = 0.0; 
	      } 
 
	    // perform FFT (results in tempfft) 
	    fftw_execute(plan_f_sym); 
	    
	    //transpose and write out temp results 
	    for (inner_loop=0; inner_loop < columns; inner_loop++) 
	      { 
		fftArray[((i*columns)+inner_loop)][0] = tempfft[inner_loop][0]; 
		fftArray[((i*columns)+inner_loop)][1] = tempfft[inner_loop][1]; 
	      } 
	  } 
	
	//column wise fft of fftArray 
	counter = 0; 
	for (column_loop=0; column_loop < columns; column_loop++) 
	  { 
	    for(row_loop=0; row_loop < rows; row_loop++) 
	      { 
		//get 1 column of fftArray 
		index = (row_loop*columns+column_loop); 
		fftTempRow [row_loop][0] = fftArray[index][0]; 
		fftTempRow [row_loop][1] = fftArray[index][1]; 
	      } 
	    
	    
	    
	    //perform FFT (results in tempfft1) 
	    fftw_execute(plan_f1_sym); 
	    
	    //assign tempfft1 to fftArrayFinal 
	    for (inner_loop=0; inner_loop < rows; inner_loop++) 
	      { 
		fftArrayFinal[counter][0] = tempfft1[inner_loop][0]; 
		//complex conjugate 
		fftArrayFinal[counter][1] = tempfft1[inner_loop][1]; 
		counter++; 
	      } 
	  } 
	
	////////////Filter UMACE AND Crop3/////////////////// 
	
	for (filter_loops = 0; filter_loops < crop1->width*crop1->height; filter_loops++) 
	  { 
	    tempScore[filter_loops][0] = ((fftArrayFinal[filter_loops][0] * UMACE_Filter[filter_loops][0]) - (fftArrayFinal[filter_loops][1] * UMACE_Filter[filter_loops][1])); 
	    tempScore[filter_loops][1] = ((fftArrayFinal[filter_loops][0] * UMACE_Filter[filter_loops][1]) + (fftArrayFinal[filter_loops][1] * UMACE_Filter[filter_loops][0])); 
	    //printf("tempScore[%d]: %f %f\r\n",filter_loops+1,tempScore[filter_loops][0],tempScore[filter_loops][1]); 
	  } 
	
	//////////2D Inverse FFT/////////////// 
	rows = 150; 
	columns = 65; 
	newRow = columns; 
	ifftCounter = 0; 
	rowCounter=0; 
	columnCounter = 0; 
	for (i = 0; i<rows; i++) 
	  { 
	    for(j = 0; j<columns; j++) 
	      { 
		index = (j * rows + i); 
		fftTempRow [ifftCounter][0] = tempScore[index][0]; 
		fftTempRow [ifftCounter][1] = tempScore[index][1]; 
		ifftCounter++; 
		
		if (rowCounter%newRow == 0) 
		  { 
		    columnCounter++; 
		    rowCounter = 1; 
		  } 
		else 
		  { 
		    rowCounter++; 
		  } 
	      } 
	    
	    //ifft of tempRow 
	    ifftCounter= 0; 
	    for(k = 0; k < crop1->width; k++) 
	      { 
		data_in_sym[k][0] = fftTempRow[k][0]; 
		data_in_sym[k][1] = fftTempRow[k][1]; 
	      } 
	    
	    // perform IFFT (results in tempifft) 
	    fftw_execute(plan_r_sym); 
	    
	    //transpose and write out temp results 
	    for (inner_loop=0; inner_loop < columns; inner_loop++) 
	      { 
		ifftArray[((i*columns)+inner_loop)][0] = tempifft[inner_loop][0]; 
		ifftArray[((i*columns)+inner_loop)][1] = tempifft[inner_loop][1]; 
	      } 
	  } 
	
	//column wise fft of fftArray 
	counter = 0; 
	for (column_loop=0; column_loop < columns; column_loop++) 
	  { 
	    for(row_loop=0; row_loop < rows; row_loop++) 
	      { 
		//get 1 column of fftArray 
		index = (row_loop*columns+column_loop); 
		fftTempRow [row_loop][0] = ifftArray[index][0]; 
		fftTempRow [row_loop][1] = ifftArray[index][1]; 
	      } 
	    
	    //perform FFT (results in tempfft1) 
	    fftw_execute(plan_r1_sym); 
	    
	    //assign tempfft1 to fftArrayFinal 
	    for (inner_loop=0; inner_loop < rows; inner_loop++) 
	      { 
		ifftArrayFinal[counter][0] = tempifft1[inner_loop][0]; 
		//complex conjugate 
		ifftArrayFinal[counter][1] = tempifft1[inner_loop][1]; 
		counter++; 
	      } 
	  } 
	
	//reshape into 2D image 
	
	//added abs to get rid of errors in conversion 
	for(i = 0, k = 0; i < rows; i++) 
	  { 
	    for(j = 0; j < columns; j++) 
	      { 
		(img4[i * columns + j]) = ((float)ifftArrayFinal[i * columns + j][0]); 
		
	      } 
	  } 
	
	//ifft shift 
	//IFFT SHIFT 
	//!!!!image rows and columns have to be even!!!!! 
	
	//int rows_half, columns_half; 
	
	new_height = rows; 
	new_width  = columns; 
	rows_half    = new_height / 2; 
	columns_half = new_width / 2; 
	
	for(i = 0; i < rows_half; i++) 
	  { 
	    for(j = 0; j < columns_half; j++) 
	      { 
		temp1_d = img4[(i * new_width) + j]; 
		temp2_d = img4[(i * new_width) + (j + columns_half)]; 
		temp3_d = img4[((i + rows_half) * new_width) + (j + columns_half)]; 
		temp4_d = img4[((i + rows_half) * new_width) + j]; 
		
		//1=3 
		img4[(i * new_width) + j] = temp3_d; 
		
		//2=4 
		img4[(i * new_width) + (j + columns_half)] = temp4_d; 
		
		//3=1 
		img4[((i + rows_half) * new_width) + (j + columns_half)] = temp1_d; 
		
		//4=2 
		img4[((i + rows_half) * new_width) + j] = temp2_d; 
	      } 
	  } 
	
	
	maxVal=0; 
	int PEAK_SEARCH_RANGE = 7; 
	
	//need to add PSR computation 
	
	for(i = (crop1->height/2)-PEAK_SEARCH_RANGE; i < (crop1->height/2)+PEAK_SEARCH_RANGE; i++) 
	  { 
	    for(j = (crop1->width/2)-PEAK_SEARCH_RANGE; j < (crop1->width/2)+PEAK_SEARCH_RANGE; j++) 
	      { 
		
		CV_IMAGE_ELEM(imgOut, float, i, j)=(img4[i * columns + j])/(crop1->width*crop1->height); 
		if (CV_IMAGE_ELEM(imgOut, float, i, j) > maxVal) 
		  { 
		    maxVal = CV_IMAGE_ELEM(imgOut, float, i, j); 
		    maxi=i; 
		    maxj=j; 
		  } 
		
	      } 
	  } 
	
	symmetryArray[symLoop][0]=maxVal; 
	symmetryArray[symLoop][1]=newScoreArray[symLoop][1]; 
	cvReleaseImage(&tempImage1); 
	cvReleaseImage(&crop1); 
	cvReleaseImage(&crop2); 
	cvReleaseImage(&crop3); 
 
      }//end symLoop 
    
    
    //normalize the symmetry Array 
    maxVal = (symmetryArray[0][0] >= symmetryArray[1][0]) ? symmetryArray[0][0] : symmetryArray[1][0]; 
    symmetryArray[0][0] /= maxVal; 
    symmetryArray[1][0] /= maxVal; 
    
    float finalScoreArray[2][2]; 
    finalScoreArray[0][0] = ((newScoreArray[0][0]) + (symmetryArray[0][0])); 
    finalScoreArray[0][1] = newScoreArray[0][1]; 
    
    
    finalScoreArray[1][0] = ((newScoreArray[1][0]) + (symmetryArray[1][0])); 
    //finalScoreArray[1][1] = newScoreArray[1][0]; 
    
    for (i=0; i<2; i++) 
      fprintf(stderr,"symmetryArray[%d]: index: %f  score: %f\r\n",i,symmetryArray[i][1],symmetryArray[i][0]); 
    
    maxIndice = (finalScoreArray[0][0] >= finalScoreArray[1][0]) ? newScoreArray[0][1] : newScoreArray[1][1]; 
    int loop_count = 0; 
    
    
    //get eye coords of the max Indice 
    for (int geoLoopsOuter = 0; geoLoopsOuter < numLabelsLeft; geoLoopsOuter++) 
      { 
	for (int geoLoopsInner = 0; geoLoopsInner < numLabelsRight; geoLoopsInner++) 
	  { 
	    if (loop_count == maxIndice) 
	      { 
		x_pos_left_gt = leftXArr[geoLoopsOuter]; 
		y_pos_left_gt = leftYArr[geoLoopsOuter]; 
		
		x_pos_right_gt = rightXArr[geoLoopsInner]; 
		y_pos_right_gt = rightYArr[geoLoopsInner]; 
		//break; 
	      } 
	    
	    loop_count++; 
	  } 
	
      } 
    
    
    
    //	cvWaitKey(0); 
    //y_pos_left_gt+=4; 
    //y_pos_right_gt+=4; 
    //y_pos_left_gt1+=4; 
    //y_pos_right_gt1+=4; 
    
    //	x_pos_left_gt-=2; 
    //x_pos_right_gt-=2; 
    //	x_pos_left_gt1-=2; 
    //x_pos_right_gt1-=2; 
    
    int y_diff1=0; 
    int y_diff2=0; 
    int x_diff1=0; 
    int x_diff2=0; 
    
    *x_pos_left  = x_pos_left_gt; 
    *y_pos_left  = y_pos_left_gt; 
    *x_pos_right = x_pos_right_gt; 
    *y_pos_right = y_pos_right_gt; 
    
    *x1_pos_left  = x_pos_left_gt; 
    *y1_pos_left  = y_pos_left_gt; 
    *x1_pos_right = x_pos_right_gt; 
    *y1_pos_right = y_pos_right_gt; 
    
    
    //cvDestroyWindow("CROP"); 
    //cvDestroyWindow("CROP1"); 
    CvFont font; 
    CvPoint LEFT_pt=cvPoint(x_pos_left_gt,y_pos_left_gt); 
    CvPoint RIGHT_pt=cvPoint(x_pos_right_gt,y_pos_right_gt); 
    cvInitFont(&font, CV_FONT_HERSHEY_COMPLEX,0.25,0.25,0,1,CV_AA); 
    cvPutText(input_img,"X",LEFT_pt,&font,CV_RGB(0xff,0xff,0xff)); 
    cvPutText(input_img,"X",RIGHT_pt,&font,CV_RGB(0xff,0xff,0xff)); 
    
    //      CvFont font1; 
    //      CvPoint LEFT_pt1=cvPoint(x_pos_left_gt1,y_pos_left_gt1); 
    //      CvPoint RIGHT_pt1=cvPoint(x_pos_right_gt1,y_pos_right_gt1); 
    //      cvInitFont(&font1, CV_FONT_HERSHEY_COMPLEX,0.25,0.25,0,1,CV_AA); 
    //if (second_peak_left==1) 
    //cvPutText(input_img,"O",LEFT_pt1,&font1,CV_RGB(0xff,0xff,0xff)); 
    //if (second_peak_right==1) 
    //cvPutText(input_img,"O",RIGHT_pt1,&font1,CV_RGB(0xff,0xff,0xff)); 
    
    
    
    
    
    //cvNamedWindow("RESIZE",CV_WINDOW_AUTOSIZE); 
    //cvShowImage("RESIZE",input_img); 
    //cvWaitKey(0); 
    
    free(leftXArr); 
    free(leftYArr); 
    free(rightXArr); 
    free(rightYArr); 
    free(input_img_data); 
    //free(new_image); 
    
    
    
    
    cvReleaseImageHeader(&blurred_image); 
    cvReleaseImage(&img1); 
    //free(blurred_image->imageData); 
    //cvReleaseImage(&img_light); 
    
    cvReleaseImage(&QCC_img); 
    cvReleaseImage(&QCC_img1); 
    
    cvReleaseImage(&detectedImage); 
    cvReleaseImage(&detectedImage1);// get new image properties 
    
    
    fftw_free(data_in); 
    fftw_free(fft); 
    fftw_free(ifft); 
    fftw_free(eye_in); 
    fftw_free(eye_out); 
    
    fftw_destroy_plan(plan_f); 
    fftw_destroy_plan(plan_eye); 
    fftw_destroy_plan(plan_r); 
    free(img3); 
    
    //generic face  
    free(scoreArray); 
    fftw_free(data_in_face); 
    fftw_free(fft_face); 
    fftw_free(ifft_face); 
    fftw_free(face_out); 
    fftw_free(face_in); 
    fftw_destroy_plan(plan_f_face); 
    fftw_destroy_plan(plan_r_face); 
    //    fftw_destroy_plan(plan_face); 
    
    
    //symmetry variables 
    fftw_free(data_in_sym); 
    fftw_free(tempfft); 
    fftw_free(fftArray); 
    fftw_free(fftTempRow); 
    fftw_free(tempfft1); 
    fftw_free(tempifft1); 
    fftw_free(tempifft); 
    fftw_free(ifftArray); 
    fftw_free(fftArrayFinal); 
    fftw_free(ifftArrayFinal); 
    fftw_free(UMACE_Filter); 
    fftw_free(tempScore); 
    fftw_destroy_plan(plan_f_sym); 
    fftw_destroy_plan(plan_f1_sym); 
    fftw_destroy_plan(plan_r_sym); 
    fftw_destroy_plan(plan_r1_sym); 
    cvReleaseImage(&flipOriginal); 
    cvReleaseImage(&imgOut); 
    free(D); 
    cvReleaseImage(&originalImage); 
    free(img4); 
    free(tempRow); 
    free(img3_face); 
    cvReleaseImage(&tempImage1A); 
    fftw_cleanup();
    //_CrtDumpMemoryLeaks();    
} 
 
 
 
 
