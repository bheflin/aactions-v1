// File:   verify.cpp
// Author: Chris Eberle
// Copyright 2009 Securics, Inc
//
// Verifies the labels between a gallery and probe set. Assumes a
// naming convention like 101_ObjectCategories

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <iostream>
#include <map>

#include "parser.h"

using namespace std;

string getclass(char* name)
{
    char* tok = strtok(name, "/");
    tok = strtok(NULL, "/");
    return string(tok);
}

int main(int argc, char **argv)
{
    if(argc < 3)
    {
        printf("Verify the label consistency between a gallery and probe file\n");
        printf("Usage: %s gallery_file probe_file\n", argv[0]);
        return 1;
    }

    filelist* fl_gal = parseFileList(argv[1]);
    if(fl_gal == NULL)
        return 1;

    struct stat stFileInfo;
    string myclass, myfile;
    int cls;
    map<string, int> class_id;
    map<string, int> file_id;
    int problems = 0;
    bool skip_probe = false;
    char file[4096];

    // First populate the categories from the gallery
    fileitem* gitem = fl_gal->begin;

    while(gitem != NULL)
    {
        cls = gitem->label;
        myfile = gitem->file;
        strncpy(file, myfile.c_str(), 4096);
        myclass = getclass(file);

        if(stat(myfile.c_str(), &stFileInfo) != 0)
        {
            printf("Gallery file does not exist: %s\n", myfile.c_str());
            ++problems;
        }

        if(class_id.count(myclass) == 0)
        {            
            class_id[myclass] = cls;
        }
        else if(class_id[myclass] != cls)
        {
            printf("Found an inconsistency in the gallery!\n");
            printf("Offensive file: %s\n", myfile.c_str());
            printf("Category claims %d, but should be %d\n\n", cls, class_id[myclass]);
            ++problems;
        }

        if(file_id.count(myfile) == 0)
        {
            file_id[myfile] = cls;
        }
        else
        {
            printf("Found a duplicate file in the gallery!\n");
            printf("File is %s\n", myfile.c_str());
            ++problems;
        }

        gitem = gitem->next;
    }

    printf("Done processing gallery\n");
    freeList(fl_gal);

    if(strcmp(argv[2], "-") == 0)
        skip_probe = true;
    
    filelist* fl_probe = NULL;
    if(!skip_probe)
    {
        fl_probe = parseFileList(argv[2]);
        if(fl_probe == NULL)
            return 1;
    }

    // Now verify the training
    fileitem* pitem = NULL;
    if(!skip_probe)
        pitem = fl_probe->begin;

    while(pitem != NULL)
    {
        cls     = pitem->label;
        myfile  = pitem->file;
        strncpy(file, myfile.c_str(), 4096);
        myclass = getclass(file);

        if(stat(myfile.c_str(), &stFileInfo) != 0)
        {
            printf("Probe file does not exist: %s\n", myfile.c_str());
            ++problems;
        }

        if(class_id.count(myclass) == 0)
        {
            printf("Unknown category used: %s\n", myclass.c_str());
        }
        else if(class_id[myclass] != cls)
        {
            printf("Found an inconsistency in the probe!\n");
            printf("Offensive file: %s\n", myfile.c_str());
            printf("Category claims %d, but should be %d\n\n", cls, class_id[myclass]);
            ++problems;
        }

        if(file_id.count(myfile.c_str()) != 0)
        {
            printf("Duplicate file found in the probe set: %s\n", myfile.c_str());
            ++problems;
        }

        pitem = pitem->next;
    }

    printf("Done processing probes\n");
    if(fl_probe != NULL)
        freeList(fl_probe);

    if(problems == 0)
    {
        printf("\nHere are the categories and labels\n");
        map<string ,int>::iterator it;
        for(it = class_id.begin(); it != class_id.end(); it++)
        {
            string cat = (*it).first;
            int id = (*it).second;

            printf("%d %s\n", id, cat.c_str());
        }
        printf("\n");
    }

    printf("Found %d problem%s\n", problems, (problems == 1) ? "" : "s");

    return problems;
}
