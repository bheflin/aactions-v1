// convolve3d.h
// Copyright 2009 Securics, Inc

#ifndef _convolve3d_h_
#define _convolve3d_h_

#include "cvheaders.h"
#include "structs.h"

int convolve3d(IplImage **src, IplImage **dst, IplImage **kernel, unsigned int depth, unsigned int kdepth, convolve_mode mode, convolve_bounds* out);
void fast_3d_convolve(IplImage **src, IplImage **dst, IplImage **kernel, int depth, int kdepth);

#endif
