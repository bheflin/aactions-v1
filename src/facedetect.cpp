// facedetect.cpp
// Copyright 2009 Securics, Inc

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "facedetect.h"
#include "structs.h"
#include "utils.h"

using namespace std;

/**
 *Preforms face detection on an image
 * @param CvHaarClassifierCascade* cascade The haar classifier used by opencv
 * @param CvMemStorage* storage The storage to put the results of the classifier
 * @param IplImage* img The image to preform face detection on
 * @return CvRect The rectange encompassing the face
 */
CvRect detect_face(CvHaarClassifierCascade* cascade, CvMemStorage* storage, IplImage* img)
{
    CvRect r = cvRect(-1, -1, -1, -1);
    if(cascade == NULL || storage == NULL || img == NULL)
        return r;

    double scale = 1.0;
    IplImage* small_img = cvCreateImage(cvSize(cvRound(img->width/scale),
                                               cvRound(img->height/scale)),
                                        8, 1);

    cvResize(img, small_img, CV_INTER_LINEAR);
    cvEqualizeHist(small_img, small_img);
    cvClearMemStorage(storage);

    CvSeq* faces = cvHaarDetectObjects(small_img, cascade, storage,
                                       1.1, 2, CV_HAAR_DO_CANNY_PRUNING,
                                       cvSize(20, 20));

    for(int i = 0; i < (faces ? faces->total : 0); i++)
    {
        CvRect* rc = (CvRect*)cvGetSeqElem(faces, i);
        int height = cvRound(rc->height * scale);
        int width = cvRound(rc->width * scale);

        // Threshold empirically determined
        if((double)height / (double)img->height < 0.1 ||
           (double)width  / (double)img->width  < 0.1)
            continue;

        r.x = cvRound(rc->x * scale);
        r.y = cvRound(rc->y * scale);
        r.width = width;
        r.height = height;
        break;
    }

    cvReleaseImage(&small_img);

    return r;
}

/**
 * Performs face detection on an image, returning multiple results
 * @param CvHaarClassifierCascade* cascade The haar classifier used by opencv
 * @param CvMemStorage* storage The storage to put the results of the classifier
 * @param IplImage* img The image to preform face detection on
 * @return a vector of CvRects describing the faces
 */
vector<CvRect> detect_face_multi(CvHaarClassifierCascade* cascade, CvMemStorage* storage, IplImage* img)
{
    vector<CvRect> regions;
    
    CvRect r = cvRect(-1, -1, -1, -1);
    if(cascade == NULL || storage == NULL || img == NULL)
        return regions;

    double scale = 1.3;
    IplImage* small_img = cvCreateImage(cvSize(cvRound(img->width/scale),
                                               cvRound(img->height/scale)),
                                        8, 1);

    cvResize(img, small_img, CV_INTER_LINEAR);
    cvEqualizeHist(small_img, small_img);
    cvClearMemStorage(storage);

    CvSeq* faces = cvHaarDetectObjects(small_img, cascade, storage,
                                       1.1, 2, 0,
                                       cvSize(20, 20));

    for(int i = 0; i < (faces ? faces->total : 0); i++)
    {
        CvRect* rc = (CvRect*)cvGetSeqElem(faces, i);
        int height = cvRound(rc->height * scale);
        int width = cvRound(rc->width * scale);

        // Threshold empirically determined
        if((double)height / (double)img->height < 0.1 ||
           (double)width  / (double)img->width  < 0.1)
            continue;

        r.x = cvRound(rc->x * scale);
        r.y = cvRound(rc->y * scale);
        r.width = width;
        r.height = height;

        regions.push_back(r);
    }

    cvReleaseImage(&small_img);

    return regions;
}
