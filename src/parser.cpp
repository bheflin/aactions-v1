// parser.cpp
// Copyright 2009 Securics, Inc

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "parser.h"

using namespace std;

void tokenize(const string& str, vector<string>& tokens, const string& delimiters)
{
    tokens.clear();

    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    string::size_type pos     = str.find_first_of(delimiters, lastPos);

    while(string::npos != pos || string::npos != lastPos)
    {
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        lastPos = str.find_first_not_of(delimiters, pos);
        pos = str.find_first_of(delimiters, lastPos);
    }
}

static int strtoint(string& str)
{
    const char* s = str.c_str();
    char* end = NULL;
    long int n = strtol(s, &end, 10);
    if(end == s)
        return -1;
    return (int)n;
}

static fileitem* readLine(vector<string>& tokens, int lineno)
{
    int numtok = tokens.size();
    if(numtok == 0)
        return NULL;

    // Don't include lines beginning with a hash
    if(tokens[0].at(0) == '#')
        return NULL;

    int i;
    string fname;
    struct stat stFileInfo;
    int mode = S_IFREG; // regular file
    bool good = false;

    for(i = 0; i < numtok; ++i)
    {
        fname = "";
        for(int j = 0; j <= i; ++j)
        {
            if(j > 0)
                fname += " ";
            fname += tokens[j];
        }

        if(stat(fname.c_str(), &stFileInfo) == 0)
        {
            if(((int)stFileInfo.st_mode & mode) == mode)
            {
                good = true;
                break;
            }
        }
    }

    if(!good)
    {
        cerr << "Warning: Invalid file on line " << lineno << endl;
        return NULL;
    }

    ++i;
    fileitem* item = (fileitem*)malloc(sizeof(fileitem));
    item->file = strdup(fname.c_str());

    item->label = -1;
    item->leyeX = -1;
    item->leyeY = -1;
    item->reyeX = -1;
    item->reyeY = -1;
    item->cropX = -1;
    item->cropY = -1;
    item->cropW = -1;
    item->cropH = -1;
    item->next  = NULL;

    int remain = numtok - i;
    if(remain == 1 || remain == 5 || remain >= 9)
    {
        // It's a groundtruth label
        item->label = strtoint(tokens[i]);
        if(item->label < 0)
        {
            free(item->file);
            free(item);
            cerr << "Warning: Invalid label on line " << lineno << endl;
            return NULL;
        }

        remain--;
        i++;
    }

    // The next field is the eye coordinates
    if(remain >= 4)
    {
        item->leyeX = strtoint(tokens[i]);
        item->leyeY = strtoint(tokens[i+1]);
        item->reyeX = strtoint(tokens[i+2]);
        item->reyeY = strtoint(tokens[i+3]);

        if(item->leyeX < 0 || item->leyeY < 0 ||
           item->reyeX < 0 || item->reyeY < 0)
        {
            free(item->file);
            free(item);
            cerr << "Warning: Invalid geonorm coordinates on line " << lineno << endl;
            return NULL;
        }

        remain -= 4;
        i += 4;
    }

    // Finally is the cropping bounds
    if(remain >= 4)
    {
        item->cropX = strtoint(tokens[i]);
        item->cropY = strtoint(tokens[i+1]);
        item->cropW = strtoint(tokens[i+2]);
        item->cropH = strtoint(tokens[i+3]);

        if(item->cropX < 0 || item->cropY < 0 ||
           item->cropW <= 0 || item->cropH <= 0)
        {
            free(item->file);
            free(item);
            cerr << "Warning: Invalid cropping bounds on line " << lineno << endl;
            return NULL;
        }

        remain -= 4;
        i += 4;
    }

    if(remain > 0)
        cerr << "Warning: Extra tokens ignored on line " << lineno << endl;

    return item;
}

filelist* parseFileList(const char* file_name)
{
    ifstream myfile;
    myfile.open(file_name);
    if(!myfile.is_open())
    {
        fprintf(stderr, "Unable to open `%s': %s\n",
                file_name, strerror(errno));
        return NULL;
    }

    filelist* files = (filelist*)malloc(sizeof(filelist));
    files->begin = NULL;
    files->num   = 0;

    int lineno = 0;
    string line;
    vector<string> tokens;
    fileitem* item;
    fileitem* cur = NULL;

    while(!myfile.eof())
    {
        ++lineno;
        getline(myfile, line);
        tokenize(line, tokens);
        item = readLine(tokens, lineno);
        if(item != NULL)
        {
            if(cur == NULL)
            {
                cur = item;
                files->begin = cur;
            }
            else
            {
                cur->next = item;
                cur = item;
            }
            files->num++;
        }
    }

    myfile.close();

    return files;
}

void freeList(filelist* list)
{
    fileitem* cur  = list->begin;
    fileitem* next = NULL;

    while(cur != NULL)
    {
        next = cur->next;
        free(cur->file);
        free(cur);
        cur = next;
    }

    free(list);
}
