// utils.cpp
// Copyright 2009 Securics, Inc

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include <errno.h>
#include "structs.h"
#include "utils.h"

/**
 *Converts and IplImage to a CvMat
 * @param const IplImage* img Image to convert
 * @return CvMat* The image in matrix format
 */
CvMat* mat_from_ipl(const IplImage* img)
{
    int i, j;

    if(img== NULL)
        return NULL;

    CvMat* mat = cvCreateMat(img->height, img->width, CV_V1);
    if(mat == NULL)
        return NULL;

    for(i = 0; i < mat->rows; ++i)
    {
        for(j = 0; j < mat->cols; ++j)
        {
            ((v1_float*)(mat->data.ptr + mat->step * i))[j] =
                CV_IMAGE_ELEM(img, v1_float, i, j);
        }
    }

    return mat;
}

/**
 * Converts a CvMat into an IplImage
 * @param const CvMat* m Matrix to be converted
 * @return IplImage* The image from the matrix
 */
IplImage* ipl_from_mat(const CvMat* m)
{
    int i, j;

    if(m == NULL)
        return NULL;

    IplImage* img = cvCreateImage(cvGetSize(m), IPL_DEPTH_V1, 1);
    if(img == NULL)
        return NULL;

    for(i = 0; i < img->height; i++)
    {
        for(j = 0; j < img->width; j++)
        {
            CV_IMAGE_ELEM(img, v1_float, i, j) = 
                ((v1_float*)(m->data.ptr + m->step * i))[j];
        }
    }

    return img;
}

/**
 * Loads a CvMat from file
 * @param const char* file The file containing the matrix
 * @return CvMat* The loaded matrix
 */
CvMat* mat_from_file(const char* file)
{
    CvMat* m = (CvMat*)cvLoad(file, NULL, NULL, NULL);
    if(m == NULL)
        return NULL;

    // Convert if necessary
    if(CV_MAT_TYPE(m->type) != CV_V1)
    {
        CvMat* nm = cvCreateMat(m->height, m->width, CV_V1);
        if(nm == NULL)
        {
            cvReleaseMat(&m);
            return NULL;
        }

        cvConvert(m, nm);
        cvReleaseMat(&m);
        m = nm;
    }

    return m;
}

/**
 * Loads an IplImage from file
 * @param const char* file The file containing the image
 * @return IplImage* The loaded image
 */
IplImage* ipl_from_file(const char* file)
{
    CvMat* m = mat_from_file(file);
    if(m == NULL)
        return NULL;

    IplImage* img = ipl_from_mat(m);
    cvReleaseMat(&m);

    return img;
}
/**
 * Displays an image to a cvWindow
 * @param const char* window The highgui window name to display in.
 * @param IplImage* img The image to display
 * @param int wait The time to wait before continuing to process
 */

void showImage(const char* window, IplImage* img, int wait)
{
    cvNamedWindow(window, 0);
    cvShowImage(window, img);
    if(wait)
        cvWaitKey(-1);
}

/** Crop an IplImage down. Creates a new IplImage and copies the region
 * @param IplImage* in The image to crop
 * @param int x The x coordinate of the upper left coner of the region to crop.
 * @param int y The y coordinate of the upper left coner of the region to crop.
 * @param int width The width of the region to crop.
 * @param int height The height of the region to crop.
 * @return IplImage* The image containing the cropped region.
 */
IplImage* cropIpl(IplImage* in, int x, int y, int width, int height)
{
    if(in == NULL || width == 0 || height == 0 || x < 0 || y < 0 || x >= in->width || y >= in->height)
        return NULL;

    IplImage* cropped = cvCreateImage(cvSize(width, height), in->depth, in->nChannels);
    if(cropped == NULL)
        return NULL;

    int nx, ny, tx, ty;
    if(in->depth == IPL_DEPTH_8U)
    {
        for(ty = 0, ny = y; ty < height; ty++, ny++)
        {
            for(tx = 0, nx = x; tx < width; tx++, nx++)
            {
                CV_IMAGE_ELEM(cropped, uchar, ty, tx) =
                    CV_IMAGE_ELEM(in, uchar, ny, nx);
            }
        }        
    }
    else if(in->depth == IPL_DEPTH_V1)
    {
        for(ty = 0, ny = y; ty < height; ty++, ny++)
        {
            for(tx = 0, nx = x; tx < width; tx++, nx++)
            {
                CV_IMAGE_ELEM(cropped, v1_float, ty, tx) =
                    CV_IMAGE_ELEM(in, v1_float, ny, nx);
            }
        }
    }

    return cropped;
}

/*
 * Trim an IplImage down to the desired dimensions. Automatically  calculates the dimensions to crop.
 * @param IplImage* in The image to trim.
 * @param CvSize newsize The size of the trimed image.
 * @return IplImage* The trimed image.
 */
IplImage* trimIpl(IplImage* in, CvSize newsize)
{
    if(in == NULL)
        return NULL;

    int trim_w = in->width - newsize.width;
    int trim_h = in->height - newsize.height;

    if(trim_w < 0 || trim_h < 0)
        return NULL;

    int start_x = (int)v1_floor((v1_float)trim_w / 2.0);
    int start_y = (int)v1_floor((v1_float)trim_h / 2.0);

    return cropIpl(in, start_x, start_y, newsize.width, newsize.height);
}
/**
 *This takes an image with a set ROI and crops out the ROI.
 * @param IplImage* in The image to crop.
 * @return IplImage* The cropped image.
 */
IplImage* cropIplToROI(IplImage* in)
{
    if(in == NULL)
        return NULL;

    CvRect r = cvGetImageROI(in);
    cvResetImageROI(in);

    IplImage* cropped = cropIpl(in, r.x, r.y, r.width, r.height);
    cvSetImageROI(in, r);

    return cropped;
}
/**
 *Calculates the mean of an image
 * @param IplImage* img The image to find the mean of.
 * @return v1_float The mean of the image.
 */
v1_float iplMean(IplImage* img)
{
    v1_float mean = 0.0;
    for(int y = 0; y < img->height; ++y)
    {
        for(int x = 0; x < img->width; ++x)
            mean += CV_IMAGE_ELEM(img, v1_float, y, x);
    }

    mean /= (v1_float)(img->height * img->width);
    return mean;
}
/**
 *Calculates the mean of a matrix
 * @param CvMat* mat The matrix to find the mean of.
 * @return v1_float The mean of the matrix.
 */

v1_float matMean(CvMat* mat)
{
    v1_float mean = 0.0;
    for(int i = 0; i < mat->rows; ++i)
    {
        for(int j = 0; j < mat->cols; ++j)
            mean += ((v1_float*)(mat->data.ptr + mat->step * i))[j];
    }

    mean /= (v1_float)(mat->rows * mat->cols);
    return mean;
}
/**
 *Calculates the mean of an array
 * @param v1_float* arr The arr to find the mean of.
 * @param size_t size The size of the array.
 * @return v1_float The mean of the array.
 */

v1_float arrMean(v1_float* arr, size_t size)
{
    v1_float mean = 0.0;
    for(size_t i = 0; i < size; ++i)
    {
        mean += *arr;
        ++arr;
    }

    mean /= (v1_float)size;
    return mean;
}

/**
 *Calculates the standard devation of an image
 * @param IplImage* img The image to find the standard devation of.
 * @return v1_float The standard devation of the image.
 */
v1_float iplStdDev(IplImage* img)
{
    v1_float mean = iplMean(img);
    v1_float std = 0.0;

    for(int y = 0; y < img->height; ++y)
    {
        for(int x = 0; x < img->width; ++x)
            std += v1_pow((CV_IMAGE_ELEM(img, v1_float, y, x) - mean), 2.0);
    }

    return v1_sqrt(std / ((v1_float)(img->height * img->width)));
}

/**
 *Calculates the standard devation of a matrix
 * @param CvMat* mat The matrix to find the standard devation of.
 * @return v1_float The standard devation of the matrix.
 */

v1_float matStdDev(CvMat* mat)
{
    v1_float mean = matMean(mat);
    v1_float std = 0.0;

    for(int i = 0; i < mat->rows; ++i)
    {
        for(int j = 0; j < mat->cols; ++j)
            std += v1_pow((((v1_float*)(mat->data.ptr + mat->step * i))[j] - mean), 2.0);
    }

    return v1_sqrt(std / ((v1_float)(mat->rows * mat->cols)));
}
/**
 *Calculates the standard devation of an array
 * @param v1_float* arr The arr to find the standard devation of.
 * @param size_t size The size of the array.
 * @return v1_float The standard devation of the array.
 */
v1_float arrStdDev(v1_float* arr, size_t size)
{
    v1_float mean = arrMean(arr, size);
    v1_float std = 0.0;

    for(size_t i = 0; i < size; ++i)
    {
        std += v1_pow((*arr) - mean, 2.0);
        ++arr;
    }

    return v1_sqrt(std / (v1_float)size);
}
/**
 *
 */
FILE* fopenOrError(const char *path, const char *mode)
{
    FILE *fp = fopen(path, mode);
    if(fp == NULL)
    {
        fprintf(stderr, "Unable to open `%s': %s\n",
                path, strerror(errno));
    }

    return fp;
}

/**
 *
 */
int compareIpl(IplImage* A, IplImage* B)
{
    if(A == NULL && B == NULL)
        return 0;
    if(A == NULL || B == NULL)
        return 1;
    if(A->width != B->width || A->height != B->height)
        return 1;
    if(A->depth != IPL_DEPTH_V1 || B->depth != IPL_DEPTH_V1)
        return 1;

    int wrong = 0;
    v1_float diff;

    for(int row = 0; row < A->height; row++)
    {
        for(int col = 0; col < A->width; col++)
        {
            diff = v1_abs(CV_IMAGE_ELEM(B, v1_float, row, col) - CV_IMAGE_ELEM(A, v1_float, row, col));
            if(diff > 0.00001)
                wrong++;
        }
    }

    return wrong;
}
/**
 *
 */
int compareMat(CvMat* A, CvMat* B)
{
    if(A == NULL && B == NULL)
        return 0;
    if(A == NULL || B == NULL)
        return 1;
    if(A->cols != B->cols || A->rows != B->rows)
        return 1;

    int wrong = 0;
    v1_float diff;

    for(int row = 0; row < A->rows; row++)
    {
        for(int col = 0; col < A->cols; col++)
        {
            diff = v1_abs((((v1_float*)(A->data.ptr + A->step * row))[col]) - (((v1_float*)(B->data.ptr + B->step * row))[col]));
            if(diff > 0.00001)
                wrong++;
        }
    }

    return wrong;
}
/**
 *
 */

int compareArr(v1_float* A, size_t Asize, v1_float* B, size_t Bsize)
{
    if(A == NULL && B == NULL)
        return 0;
    if(A == NULL || B == NULL)
        return 1;
    if(Asize != Bsize)
        return 1;

    int wrong = 0;
    v1_float diff;

    for(size_t i = 0; i < Asize; ++i)
    {
        diff = v1_abs(A[i] - B[i]);
        if(diff > 0.00001)
            wrong++;
    }

    return wrong;
}
/**
 *
 */
v1_float iplDist(IplImage* A, IplImage* B)
{
    if(A == NULL || B == NULL)
        return -1.0;
    if(A->width != B->width || A->height != B->height)
        return -1.0;    

    v1_float total_diff = 0.0;
    v1_float total = (v1_float)(A->height * A->width);

    for(int row = 0; row < A->height; row++)
    {
        for(int col = 0; col < A->width; col++)
            total_diff += v1_abs(CV_IMAGE_ELEM(B, v1_float, row, col) - CV_IMAGE_ELEM(A, v1_float, row, col));
    }

    return total_diff / total;
}

/**
 *
 */
v1_float matDist(CvMat* A, CvMat* B)
{
    if(A == NULL || B == NULL)
        return -1.0;
    if(A->rows != B->rows || A->cols != B->cols)
        return -1.0;    

    v1_float total_diff = 0.0;
    v1_float total = (v1_float)(A->rows * A->cols);

    for(int row = 0; row < A->rows; row++)
    {
        for(int col = 0; col < A->cols; col++)
            total_diff += v1_abs(
                ((v1_float*)(A->data.ptr + A->step * row))[col] -
                ((v1_float*)(B->data.ptr + B->step * row))[col]
            );
    }

    return total_diff / total;
}

/**
 *
 */
v1_float arrDist(v1_float* A, size_t Asize, v1_float* B, size_t Bsize)
{
    if(A == NULL || B == NULL)
        return -1.0;
    if(Asize != Bsize)
        return -1.0;

    v1_float total_diff = 0.0;
    v1_float total = (v1_float)Asize;

    for(size_t i = 0; i < Asize; i++)
        total_diff += v1_abs(A[i] - B[i]);

    return total_diff / total;
}

/**
 *
 */
static int comp_v1(const void* a, const void* b)
{
    v1_float A = *((v1_float*)a);
    v1_float B = *((v1_float*)b);

    if(A > B)
        return 1;
    else if(A < B)
        return -1;
    return 0;
}

/**
 *
 */
v1_float iplMedian(IplImage *img)
{
    if(img == NULL)
        return 0.0;

    v1_float median;
    size_t sz = img->width * img->height;
    v1_float* tmpArr = (v1_float*)malloc(sz * sizeof(v1_float));
    unsigned int idx = 0;

    for(int row = 0; row < img->height; row++)
    {
        for(int col = 0; col < img->width; col++)
            tmpArr[idx++] = CV_IMAGE_ELEM(img, v1_float, row, col);
    }

    qsort(tmpArr, sz, sizeof(v1_float), comp_v1);

    if(sz % 2 == 1)
        median = tmpArr[(sz - 1) / 2];
    else
        median = (tmpArr[(sz / 2) - 1] + tmpArr[(sz / 2)]) / 2.;

    free(tmpArr);

    return median;
}

/**
 *
 */
v1_float matMedian(CvMat *mat)
{
    if(mat == NULL)
        return 0.0;

    v1_float median;
    size_t sz = mat->rows * mat->cols;
    v1_float* tmpArr = (v1_float*)malloc(sz * sizeof(v1_float));
    unsigned int idx = 0;

    for(int row = 0; row < mat->rows; row++)
    {
        for(int col = 0; col < mat->cols; col++)
            tmpArr[idx++] = ((v1_float*)(mat->data.ptr + mat->step * row))[col];
    }

    qsort(tmpArr, sz, sizeof(v1_float), comp_v1);

    if(sz % 2 == 1)
        median = tmpArr[(sz - 1) / 2];
    else
        median = (tmpArr[(sz / 2) - 1] + tmpArr[(sz / 2)]) / 2.;

    free(tmpArr);

    return median;
}

/**
 *
 */
v1_float arrMedian(v1_float* arr, size_t size)
{
    if(arr == NULL)
        return 0.0;

    v1_float median;
    v1_float* tmpArr = (v1_float*)malloc(size * sizeof(v1_float));
    unsigned int idx;

    for(idx = 0; idx < size; idx++)
        tmpArr[idx] = arr[idx];
    qsort(tmpArr, size, sizeof(v1_float), comp_v1);

    if(size % 2 == 1)
        median = tmpArr[(size - 1) / 2];
    else
        median = (tmpArr[(size / 2) - 1] + tmpArr[(size / 2)]) / 2.;

    free(tmpArr);
    return median;
}

/**
 *
 */
void printCompareIpl(IplImage* A, IplImage* B, const char* Aname, const char* Bname)
{
    printf("IplImage %s:\n", Aname);
    printf("  => mean: %f\n", iplMean(A));
    printf("  => med:  %f\n", iplMedian(A));
    printf("  => std:  %f\n\n", iplStdDev(A));

    printf("IplImage %s:\n", Bname);
    printf("  => mean: %f\n", iplMean(B));
    printf("  => med:  %f\n", iplMedian(B));
    printf("  => std:  %f\n\n", iplStdDev(B));

    printf("Difference between %s and %s:\n", Aname, Bname);
    printf("  => Number differing: %d\n", compareIpl(A, B));
    printf("  => Average distance: %f\n\n", iplDist(A, B));
}

/**
 *
 */
void printCompareMat(CvMat* A, CvMat* B, const char* Aname, const char* Bname)
{
    printf("CvMat %s:\n", Aname);
    printf("  => mean: %f\n", matMean(A));
    printf("  => med:  %f\n", matMedian(A));
    printf("  => std:  %f\n\n", matStdDev(A));

    printf("CvMat %s:\n", Bname);
    printf("  => mean: %f\n", matMean(B));
    printf("  => med:  %f\n", matMedian(B));
    printf("  => std:  %f\n\n", matStdDev(B));

    printf("Difference between %s and %s:\n", Aname, Bname);
    printf("  => Number differing: %d\n", compareMat(A, B));
    printf("  => Average distance: %f\n\n", matDist(A, B));
}

/**
 *
 */
void printCompareArr(v1_float* A, size_t Asize, v1_float* B, size_t Bsize,
                     const char* Aname, const char* Bname)
{
    printf("Array %s:\n", Aname);
    printf("  => mean: %f\n", arrMean(A, Asize));
    printf("  => med:  %f\n", arrMedian(A, Asize));
    printf("  => std:  %f\n\n", arrStdDev(A, Asize));

    printf("Array %s:\n", Bname);
    printf("  => mean: %f\n", arrMean(B, Bsize));
    printf("  => med:  %f\n", arrMedian(B, Bsize));
    printf("  => std:  %f\n\n", arrStdDev(B, Bsize));

    printf("Difference between %s and %s:\n", Aname, Bname);
    printf("  => Number differing: %d\n", compareArr(A, Asize, B, Bsize));
    printf("  => Average distance: %f\n\n", arrDist(A, Asize, B, Bsize));
}

/**
 *
 */
void printIplInfo(const char* name, IplImage* img)
{
    if(img == NULL)
    {
        printf("IplImage* %s = NULL;\n", name);
        return;
    }

    printf("IplImage* %s = {\n", name);

    printf("    int  nSize = %d;\n", img->nSize);
    printf("    int  ID = %d;\n", img->ID);
    printf("    int  nChannels = %d;\n", img->nChannels);
    printf("    int  alphaChannel = %d;\n", img->alphaChannel);
    printf("    int  depth = ");

    switch(img->depth)
    {
        case IPL_DEPTH_8U:
            printf("IPL_DEPTH_8U");
            break;
        case IPL_DEPTH_8S:
            printf("IPL_DEPTH_8S");
            break;
        case IPL_DEPTH_16U:
            printf("IPL_DEPTH_16U");
            break;
        case IPL_DEPTH_16S:
            printf("IPL_DEPTH_16S");
            break;
        case IPL_DEPTH_32S:
            printf("IPL_DEPTH_32S");
            break;
        case IPL_DEPTH_32F:
            printf("IPL_DEPTH_32F");
            break;
        case IPL_DEPTH_64F:
            printf("IPL_DEPTH_64F");
            break;
    }

    printf(";\n");
    printf("    char colorModel[4] = { %d, %d, %d, %d };\n", img->colorModel[0],
           img->colorModel[1], img->colorModel[2], img->colorModel[3]);
    printf("    char channelSeq[4]  = { %d, %d, %d, %d };\n", img->channelSeq[0],
           img->channelSeq[1], img->channelSeq[2], img->channelSeq[3]);
    printf("    int  dataOrder = %d;\n", img->dataOrder);
    printf("    int  origin = %d;\n", img->origin);
    printf("    int  align = %d;\n", img->align);
    printf("    int  width = %d;\n", img->width);
    printf("    int  height = %d;\n", img->height);
    printf("    struct _IplROI *roi = ");

    if(img->roi == NULL)
        printf("NULL;\n");
    else
    {
        printf("{\n");
        printf("            int  coi = %d;\n", img->roi->coi);
        printf("            int  xOffset = %d;\n", img->roi->xOffset);
        printf("            int  yOffset = %d;\n", img->roi->yOffset);
        printf("            int  width = %d;\n", img->roi->width);
        printf("            int  height = %d\n", img->roi->height);
        printf("        };\n");
    }

    printf("    struct _IplImage *maskROI = %p;\n", (void*)img->maskROI);
    printf("    void  *imageId = %p;\n", img->imageId);
    printf("    struct _IplTileInfo *tileInfo = %p;\n", (void*)img->tileInfo);
    printf("    int  imageSize = %d;\n", img->imageSize);
    printf("    char *imageData = %p;\n", (void*)img->imageData);
    printf("    int  widthStep = %d;\n", img->widthStep);
    printf("    int  BorderMode[4] = { %d, %d, %d, %d };\n", img->BorderMode[0],
           img->BorderMode[1], img->BorderMode[2], img->BorderMode[3]);
    printf("    int  BorderConst[4] = { %d, %d, %d, %d };\n", img->BorderConst[0],
           img->BorderConst[1], img->BorderConst[2], img->BorderConst[3]);
    printf("    char *imageDataOrigin = %p;\n", (void*)img->imageDataOrigin);

    printf("};\n\n");
}

#define MAKECVMNAME(TYPE) \
    case TYPE: \
        printf(#TYPE); \
        break
/**
 * Prints what is contained in a matrix for debugging purposes
 * @param const char* name The name of the matrix.
 * @param CvMat* mat The matrix to have displayed.
 */
void printMatInfo(const char* name, CvMat* mat)
{
    if(mat == NULL)
    {
        printf("CvMat* %s = NULL;\n", name);
        return;
    }

    printf("CvMat* %s = {\n", name);
    printf("    int type = ");

    switch(CV_MAT_TYPE(mat->type))
    {
        MAKECVMNAME(CV_8UC1);
        MAKECVMNAME(CV_8UC2);
        MAKECVMNAME(CV_8UC3);
        MAKECVMNAME(CV_8UC4);
        MAKECVMNAME(CV_8SC1);
        MAKECVMNAME(CV_8SC2);
        MAKECVMNAME(CV_8SC3);
        MAKECVMNAME(CV_8SC4);
        MAKECVMNAME(CV_16UC1);
        MAKECVMNAME(CV_16UC2);
        MAKECVMNAME(CV_16UC3);
        MAKECVMNAME(CV_16UC4);
        MAKECVMNAME(CV_16SC1);
        MAKECVMNAME(CV_16SC2);
        MAKECVMNAME(CV_16SC3);
        MAKECVMNAME(CV_16SC4);
        MAKECVMNAME(CV_32SC1);
        MAKECVMNAME(CV_32SC2);
        MAKECVMNAME(CV_32SC3);
        MAKECVMNAME(CV_32SC4);
        MAKECVMNAME(CV_32FC1);
        MAKECVMNAME(CV_32FC2);
        MAKECVMNAME(CV_32FC3);
        MAKECVMNAME(CV_32FC4);
        MAKECVMNAME(CV_64FC1);
        MAKECVMNAME(CV_64FC2);
        MAKECVMNAME(CV_64FC3);
        MAKECVMNAME(CV_64FC4);
    default:
        printf("%d", CV_MAT_TYPE(mat->type));
        break;
    }

    printf(";\n");
    printf("    int step = %d;\n", mat->step);
    printf("    int* refcount = %p;\n", (void*)mat->refcount);
    printf("    uchar* data.ptr = %p;\n", (void*)mat->data.ptr);
    printf("    int rows = %d;\n", mat->rows);
    printf("    int cols = %d;\n", mat->cols);
    printf("};\n\n");
}
/**
 *This image takes an image and returns a grayscale version of it
 * @param IplImage* img The image to have a grayscale version of.
 * @return IplImage* The grayscale image.
 */
IplImage* getGrayscale(IplImage* img)
{
    IplImage* tmp = NULL;
    if(img == NULL)
        return NULL;

    if(img->nChannels > 1)
    {
        // Clone it
        tmp = cvCreateImage(cvGetSize(img), img->depth, 1);
        if(tmp == NULL)
            return NULL;

        // Convert to grayscale
        cvCvtColor(img, tmp, CV_BGR2GRAY);
    }
    else
    {
        tmp = cvCloneImage(img);
    }

    return tmp;
}
