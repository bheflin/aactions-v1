// facedetect.h
// Copyright 2009 Securics, Inc

#ifndef _facedetect_h_
#define _facedetect_h_

#include "cvheaders.h"
#include <vector>

CvRect detect_face(CvHaarClassifierCascade* cascade, CvMemStorage* storage, IplImage* input);
std::vector<CvRect> detect_face_multi(CvHaarClassifierCascade* cascade, CvMemStorage* storage, IplImage* input);

#endif /* _facedetect_h_ */
