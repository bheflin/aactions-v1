// File:    gentest.c
// Author:  Chris Eberle
// Copyright 2009 Securics, Inc

// Generate a random gallery and probe file
// using a directory as a source

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <ctype.h>
#include <libgen.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <dirent.h>
#include <time.h>
#include <iostream>
#include <map>
#include <vector>

#include "argparse.h"

using namespace std;

#define DOT_OR_DOTDOT(s) ((s)[0] == '.' && (!(s)[1] || ((s)[1] == '.' && !(s)[2])))

// Typedefs
typedef int (*fwanted)(const char* path);

typedef struct params_t
{
    char* progName;
    char* galleryFile;
    char* probeFile;
    char* imageDir;
    char* mapFile;
    int catNum;
    int galFileNum;
    int probFileNum;
} params;

// Prototypes
static void usage(const char* name, int val);
static void parseCommandLine(int argc, char **argv, params *parm);
static FILE* tryOpen(const char* name);
static int find(const char* path, fwanted wanted_function);
static int wanted(const char* path);
static int iendsWith(const char* str, const char* postfix);
static int isRegularFile(const char* path);
static vector<string> split(string path);
static string randCat();
static string randFile(vector<string>& files);

// Global variables
static vector<string> file_list;
static map<string, vector<string> > category_files;

// Entry point for program
int main(int argc, char **argv)
{
    // Parse the command line arguments
    params myparams;
    parseCommandLine(argc, argv, &myparams);

    srand(time(NULL));

    // Make sure we have all of our parameters
    if(myparams.galleryFile == NULL ||
       myparams.probeFile   == NULL ||
       myparams.imageDir    == NULL ||
       myparams.mapFile     == NULL ||
       myparams.catNum      == 0 ||
       myparams.galFileNum  == 0 ||
       myparams.probFileNum == 0)
        usage(myparams.progName, 1);

    // Find all valid files
    find(myparams.imageDir, wanted);
    if(file_list.size() == 0)
    {
        fprintf(stderr, "Found no valid files, quitting now. Bleargh. *Gasp*. kill -9 self.\n");
        exit(1);
    }

    vector<string>::iterator it;
    vector<string> paths;
    string file, cat;

    // Split files into categories
    for(it = file_list.begin(); it < file_list.end(); it++)
    {
        file = *it;
        paths = split(file);
        cat = *(paths.end() - 2);
        category_files[cat].push_back(file);
    }

    printf("Found %d categories, and %d files\n", (int)category_files.size(), (int)file_list.size());
    if(myparams.catNum < 0)
        myparams.catNum = category_files.size();

    // Open the gallery and probes
    FILE* fGallery = tryOpen(myparams.galleryFile);
    FILE* fProbe   = tryOpen(myparams.probeFile);
    FILE* fMap     = tryOpen(myparams.mapFile);

    // Build the gallery and probes
    int count, probe_count;

    for(int i = 1; i <= myparams.catNum; i++)
    {
        cat = randCat();
        vector<string> catfile = category_files[cat];

        probe_count = myparams.probFileNum;
        if(probe_count < 0)
            probe_count = 1;

        while((int)catfile.size() - ((int)myparams.galFileNum + probe_count) < 0)
        {
            fprintf(stderr, "Skipping category %s because there are too few entries\n", cat.c_str());
            category_files.erase(cat);
            cat = randCat();
            catfile = category_files[cat];
        }

        for(count = 0; count < myparams.galFileNum; ++count)
        {
            file = randFile(catfile);
            fprintf(fGallery, "%s %d\n", file.c_str(), i);
        }

        probe_count = myparams.probFileNum;
        if(probe_count < 0)
            probe_count = catfile.size();

        for(count = 0; count < probe_count; ++count)
        {
            file = randFile(catfile);
            fprintf(fProbe, "%s %d\n", file.c_str(), i);
        }

        fprintf(fMap, "%d %s\n", i, cat.c_str());
        printf("  => %s\n", cat.c_str());

        category_files.erase(cat);
    }

    // Clean up
    fclose(fGallery);
    fclose(fProbe);
    fclose(fMap);

    printf("Created gallery: %s\n", myparams.galleryFile);
    printf("Created probe:   %s\n", myparams.probeFile);
    printf("Created map:     %s\n", myparams.mapFile);

    // All done
    return 0;
}

static int wanted(const char* path)
{
    if(isRegularFile(path) &&
       (iendsWith(path, ".png") || iendsWith(path, ".jpg")))
        file_list.push_back(string(path));

    return 0;
}

static string randFile(vector<string>& files)
{
    if(files.size() == 0)
        return string("");

    int rnum = rand() % files.size();
    vector<string>::iterator it = files.begin();
    vector<string>::iterator hold;
    string file;
    for(int i = 0; ;)
    {
        hold = it;
        file = *it;
        ++i;
        ++it;

        if(i == (rnum + 1) || it == files.end())
        {
            files.erase(hold);
            break;
        }
    }
    
    return file;
}

static string randCat()
{
    if(category_files.size()  == 0)
    {
        fprintf(stderr, "Oh noes! We ran out of categories :(\n");
        exit(1);
    }

    int rnum = rand() % category_files.size();
    map<string, vector<string> >::iterator it = category_files.begin();
    string cat;
    for(int i = 0; i <= rnum && it != category_files.end(); i++, it++)
        cat = (*it).first;

    return cat;
}

static int iendsWith(const char* str, const char* postfix)
{
    int slen = strlen(str);
    int plen = strlen(postfix);

    if(plen > slen)
        return 0;

    char* nstr = strdup(&str[slen - plen]);

    for(int i = 0; i < plen; i++)
        nstr[i] = (char)tolower(nstr[i]);

    int res = strncmp(nstr, postfix, plen);
    free(nstr);
    return res == 0;
}

static FILE* tryOpen(const char* name)
{
    FILE *fp = fopen(name, "w");
    if(fp == NULL)
    {
        fprintf(stderr, "Unable to open `%s': %s\n",
                name, strerror(errno));
        exit(1);
    }

    return fp;
}

static vector<string> split(string path)
{
    vector<string> comp;
    char* cpath = strdup(path.c_str());

    char* tok = strtok(cpath, "/");
    comp.push_back(string(tok));

    while((tok = strtok(NULL, "/")) != NULL)
        comp.push_back(string(tok));

    free(cpath);
    return comp;
}

// Check the existence of a file or directory
static int pathExists(const char* path)
{
    struct stat stFileInfo;
    return (lstat(path, &stFileInfo) == 0);
}

static int isFileType(const char* path, const int mode)
{
    struct stat stFileInfo;
    if(lstat(path, &stFileInfo) != 0)
        return 0;

    return (((int)stFileInfo.st_mode & mode) == mode) ? 1 : 0;
}

// Check the existence of a file or directory
static int isDirectory(const char* path)
{
    return isFileType(path, S_IFDIR);
}

static int isRegularFile(const char* path)
{
    return isFileType(path, S_IFREG);
}

static char* concat_subpath_file(const char *path, const char *filename)
{
    if(filename && DOT_OR_DOTDOT(filename))
        return NULL;

    int lc = 1;
    if (!path) path = "";
    size_t sz = strlen(path);
    if(path[sz - 1] != '/')
    {
        ++sz;
        lc = 0;
    }

    while (*filename == '/')
        filename++;
    sz += strlen(filename) + 1;
    char* newpath = (char*)malloc(sz);
    if(newpath == NULL)
        return NULL;

    snprintf(newpath, sz, "%s%s%s", path, (lc == 0 ? "/" : ""), filename);
    return newpath;
}

static int find(const char* path, fwanted wanted_function)
{
    if(!pathExists(path))
        return -1;

    if(isDirectory(path))
    {
        if(wanted_function(path) < 0)
            return -1;

        DIR *dp = opendir(path);
        struct dirent *d;
        int status = 0;

        if(dp == NULL)
            return 0;

        while((d = readdir(dp)) != NULL)
        {
            char *new_path = concat_subpath_file(path, d->d_name);
            if(new_path == NULL)
                continue;
            if(find(new_path, wanted_function) < 0)
                status = -1;
            free(new_path);
        }

        if(closedir(dp) < 0)
            return -1;

        return status;
    }

    return wanted_function(path);
}

// Print the program's usage and exit
static void usage(const char* name, int val)
{
    FILE* f = (val == 0) ? stdout : stderr;
    fprintf(f,
            "Gentest: create a valid gallery, probe, and map of random testing data\n"
            "Usage: %s [OPTIONS]\n"
            "  -c cnum        Number of categories (-1 means all).\n"
            "  -j gnum        Files per category (gallery).\n"
            "  -k pnum        Files per category (probes) (-1 means use all remaining).\n"
            "  -g galfile     Gallery output file.\n"
            "  -p probfile    Probe output file.\n"
            "  -m mapfile     Category map output file.\n"
            "  -d dir         Image directory.\n"
            "  -h             Show this helpful message.\n"
            , name);
    exit(val);
}

// Parse the command line arguments and load them into a struct
static void parseCommandLine(int argc, char **argv, params *parm)
{
    int c = -1;
    
    argv[0] = basename(argv[0]);

    parm->progName    = argv[0];
    parm->galleryFile = NULL;
    parm->probeFile   = NULL;
    parm->imageDir    = NULL;
    parm->mapFile     = NULL;
    parm->catNum      = 0;
    parm->galFileNum  = 0;
    parm->probFileNum = 0;

    ArgParser ap(usage, parm->progName, false);
    ap.addOption("help", false, "h");
    ap.addOption("num-cat", true, "c");
    ap.addOption("num-gal", true, "j");
    ap.addOption("num-probe", true, "k");
    ap.addOption("gal-file", true, "g");
    ap.addOption("probe-file", true, "p");
    ap.addOption("map-file", true, "m");
    ap.addOption("image-dir", true, "d");

    if(!ap.parseCommandLine(argc, argv, true))
        exit(1);

    if(ap.isSet("help"))
        usage(parm->progName, 0);

    if(ap.isSet("gal-file")) parm->galleryFile = strdup(ap.getValue("gal-file"));
    if(ap.isSet("probe-file")) parm->probeFile = strdup(ap.getValue("probe-file"));
    if(ap.isSet("image-dir")) parm->imageDir   = strdup(ap.getValue("image-dir"));
    if(ap.isSet("num-cat")) parm->catNum = atoi(ap.getValue("num-cat"));
    if(ap.isSet("num-gal")) parm->galFileNum = atoi(ap.getValue("num-gal"));
    if(ap.isSet("num-probe")) parm->probFileNum = atoi(ap.getValue("num-probe"));
    if(ap.isSet("map-file")) parm->mapFile = strdup(ap.getValue("map-file"));
}
