// utils.h
// Copyright 2009 Securics, Inc

#ifndef _utils_h_
#define _utils_h_

#include <stdio.h>
#include "cvheaders.h"

CvMat* mat_from_ipl(const IplImage* img);
IplImage* ipl_from_mat(const CvMat* m);
CvMat* mat_from_file(const char* file);
IplImage* ipl_from_file(const char* file);
void showImage(const char* window, IplImage* img, int wait = 1);
IplImage* cropIpl(IplImage* in, int x, int y, int width, int height);
IplImage* trimIpl(IplImage* in, CvSize newsize);
IplImage* cropIplToROI(IplImage* in);

void printIplInfo(const char* name, IplImage* img);
void printMatInfo(const char* name, CvMat* mat);

v1_float iplMean(IplImage* img);
v1_float matMean(CvMat* mat);
v1_float arrMean(v1_float* arr, size_t size);

v1_float iplMedian(IplImage *img);
v1_float matMedian(CvMat *mat);
v1_float arrMedian(v1_float* arr, size_t size);

v1_float iplStdDev(IplImage* img);
v1_float matStdDev(CvMat* mat);
v1_float arrStdDev(v1_float* arr, size_t size);

FILE* fopenOrError(const char *path, const char *mode);

int compareIpl(IplImage* A, IplImage* B);
int compareMat(CvMat* A, CvMat* B);
int compareArr(v1_float* A, size_t Asize, v1_float* B, size_t Bsize);

v1_float iplDist(IplImage* A, IplImage* B);
v1_float matDist(CvMat* A, CvMat* B);
v1_float arrDist(v1_float* A, size_t Asize, v1_float* B, size_t Bsize);

void printCompareIpl(IplImage* A, IplImage* B, const char* Aname, const char* Bname);
void printCompareMat(CvMat* A, CvMat* B, const char* Aname, const char* Bname);
void printCompareArr(v1_float* A, size_t Asize, v1_float* B, size_t Bsize,
                     const char* Aname, const char* Bname);

IplImage* getGrayscale(IplImage* img);

#endif /* _utils_h_ */
