// File:   classify-multi.cpp
// Author: Chris Eberle
// Copyright 2009 Securics, Inc

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#include "asciibar.h"
#include "classify-multi.h"

using namespace std;

// Macros
#define SAFE_FREE(ptr) do{if((ptr)!=NULL){free((ptr));(ptr)=NULL;}}while(0)
#define SAFE_DELETE(ptr)  do{if((ptr)!=NULL){delete(ptr); (ptr)=NULL;}}while(0)

// A struct used for sorting the top N
struct multi_label_and_score
{
    int label;
    double score;
};

// Class constructor
classify_onevsrest* classify_onevsrest_create(int quiet, int progress)
{
    // Allocate memory for the C class
    classify_onevsrest* self = (classify_onevsrest*)malloc(sizeof(classify_onevsrest));
    if(self == NULL)
        return NULL;

    self->quiet = quiet;
    self->progress = progress;
    self->models = NULL;
    self->num_models = 0;

    return self;
}

static void classify_onevsrest_svm_destroy(classify_onevsrest* self)
{
    if(self->models != NULL)
    {
        for(unsigned int i = 0; i < self->num_models; ++i)
        {
            if(self->models[i] != NULL)
            {
                svm_destroy_model(self->models[i]);
                self->models[i] = NULL;
            }
        }    
    }

    SAFE_FREE(self->models);
    self->num_models = 0;
}

// Class destructor
void classify_onevsrest_destroy(classify_onevsrest* self)
{
    if(self == NULL)
        return;

    classify_onevsrest_svm_destroy(self);
    SAFE_FREE(self);
}

static int classify_onevsrest_getmaxnum(classify_onevsrest* self, const char* file_base)
{
    int num = -1, result;
    struct stat fileInfo;
    string fname;
    char* is = NULL;

    do
    {
        fname.assign(file_base);
        fname += "_";
        if(asprintf(&is, "%d", num + 1) <= 0)
            return -2;
        fname += is;
        SAFE_FREE(is);
        fname += ".svm";

        if((result = stat(fname.c_str(), &fileInfo)) == 0)
            num += 1;
    }
    while(result == 0);

    return num;
}

static int classify_onevsrest_loadone(classify_onevsrest* self, const char* file_base, int idx)
{
    char* is = NULL;
    if(asprintf(&is, "%d", idx) <= 0)
        return -1;

    string fname(file_base);
    fname += "_";
    fname += is;
    fname += ".svm";

    SAFE_FREE(is);

    struct svm_model *model = NULL;
    if((model = svm_load_model(fname.c_str())) == NULL)
    {
        fprintf(stderr, "Can't open file `%s': "
                "Go stand in the corner and think about what you've done.\n", fname.c_str());
        return -1;
    }

    self->models[idx] = model;

    return 0;
}

int classify_onevsrest_load(classify_onevsrest* self, const char* file_base)
{
    if(self == NULL || file_base == NULL)
        return -1;

    int top_num = classify_onevsrest_getmaxnum(self, file_base);
    if(top_num < 0)
    {
        fprintf(stderr, "Unable to load any training data. Is your basename right?\n");
        return -1;
    }

    classify_onevsrest_svm_destroy(self);
    self->models = (struct svm_model**)malloc(sizeof(struct svm_model*) * (top_num + 1));

    if(self->models == NULL)
    {
        fprintf(stderr, "Sorry chief, it looks like your memory has the swine flu.\n");
        return -1;
    }

#ifndef HIDE_PROGRESS
    struct ascii_bar_params abp;
    if(self->progress)
    {
        init_ascii_bar(&abp, 0.0, (float)top_num, 1.0f);
        abp.len = 50;
        abp.show_eta = 1;
        show_ascii_bar(&abp);
    }
#endif

    int val = -1;
    for(int i = 0; i <= top_num; i++)
    {
        if((val = classify_onevsrest_loadone(self, file_base, i)) < 0)
            return val;
        self->num_models++;

#ifndef HIDE_PROGRESS
        if(self->progress)
            step_ascii_bar(&abp);
#endif

    }

# ifndef HIDE_PROGRESS
    if(self->progress)
        hide_ascii_bar(&abp);
#endif

    return 0;
}

int classify_onevsrest_one(classify_onevsrest* self, v1_float* features, int feature_count, unsigned int cidx, int &lbl, float &dist)
{
    dist = 0.0;
    lbl = -1;

    struct svm_model *model = self->models[cidx];
    struct svm_node *x;
    int i = 0;
    int nr_class = svm_get_nr_class(model);

    if(nr_class != 2)
    {
        fprintf(stderr, "Not a valid classifier!\n");
        return -1;
    }

    x = (struct svm_node*)malloc((feature_count + 1) * sizeof(struct svm_node));

    int *labels = (int*)malloc(sizeof(int) * nr_class);
    svm_get_labels(model, labels);

    if(labels[0] == -1)
        lbl = labels[1];
    else
        lbl = labels[0];
    SAFE_FREE(labels);

    for(i = 0; i < feature_count; i++)
    {
        x[i].index = i + 1;
        x[i].value = features[i];
    }

    x[feature_count].index = -1;
    double score = 0.0;

    svm_predict(model, x, &score);
    SAFE_FREE(x);
    dist = score;

    return 0;
}

static int cmpmulti(const void *p1, const void *p2)
{
    struct multi_label_and_score *s1 = (struct multi_label_and_score*)p1;
    struct multi_label_and_score *s2 = (struct multi_label_and_score*)p2;

    if(s1->score < s2->score)
        return 1;
    else if(s1->score > s2->score)
        return -1;
    else
        return 0;
}

int classify_onevsrest_classify(classify_onevsrest* self, v1_float* features, int feature_count, vector<id_score> &topids)
{
    if(self == NULL || features == NULL || feature_count <= 0 || self->models == NULL || self->num_models == 0)
        return -1;

    int lbl = -1;
    float dist = 0.0;

    vector<struct multi_label_and_score> vscores;
    struct multi_label_and_score score;

    for(unsigned int i = 0; i < self->num_models; ++i)
    {
        if(self->models[i] != NULL)
        {
            if(classify_onevsrest_one(self, features, feature_count, i, lbl, dist) == 0)
            {
                score.label = lbl;
                score.score = dist;
                vscores.push_back(score);
            }
            else
            {
                printf("Classifier %d had an error\n", i);
            }
        }
    }

    size_t sz = vscores.size();
    if(sz == 0)
    {
        fprintf(stderr, "No good data!\n");
        return -1;
    }

    struct multi_label_and_score* sorted = (struct multi_label_and_score*)malloc(sz * sizeof(struct multi_label_and_score));
    if(sorted == NULL)
    {
        fprintf(stderr, "Your memory is exhausted, give it some air!\n");
        return -1;
    }

    for(unsigned int n = 0; n < sz; n++)
    {
        sorted[n].label = vscores[n].label;
        sorted[n].score = vscores[n].score;
    }

    vscores.clear();
    qsort(sorted, sz, sizeof(struct multi_label_and_score), cmpmulti);

    topids.clear();
    id_score entry;

    for(unsigned int n = 0; n < sz; n++)
    {
        entry.id = sorted[n].label;
        entry.score = sorted[n].score;
        entry.algo = 0;
        topids.push_back(entry);
    }

    SAFE_FREE(sorted);
    return topids[0].id;
}
