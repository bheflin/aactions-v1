// File:    rawenroll.cpp
// Author:  Chris Eberle
// Created: Wed Nov 18 10:45:32 2009
// Copyright 2009 Securics, Inc
//
// Train an SVM classifier directly from raw data. Useful for
// testing the SVM classifier with python data.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <ctype.h>
#include <libgen.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <errno.h>
#include <sstream>
#include <map>

#include "argparse.h"
#include "structs.h"
#include "learn.h"
#include "learn-multi.h"

using namespace std;

// Typedefs
typedef struct prog_params_t
{
    char* progName;
    char* fileName;
    char* svmFile;
    int   oneVSRest;
    int   numProcs;
} prog_params;

extern int __verbose;

// Prototypes
static void usage(const char* name, int val);
static void parseCommandLine(int argc, char **argv, prog_params *params);

// Macros
#define SAFE_FREE(ptr) do{if((ptr)!=NULL){free((ptr));(ptr)=NULL;}}while(0)

static int strtoint(string& str)
{
    int num;
    istringstream stm;
    stm.str(str);
    stm >> num;
    return num;
}

static double strtodouble(string& str)
{
    double num;
    istringstream stm;
    stm.str(str);
    stm >> num;
    return num;    
}

static void tokenize(const string& str, vector<string>& tokens, const string& delimiters = " ")
{
    tokens.clear();

    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    string::size_type pos     = str.find_first_of(delimiters, lastPos);

    while(string::npos != pos || string::npos != lastPos)
    {
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        lastPos = str.find_first_not_of(delimiters, pos);
        pos = str.find_first_of(delimiters, lastPos);
    }
}

void processLine(string& line, vector<double>& data, int& label, int lineno)
{
    label = -1;
    data.clear();
    
    vector<string> tokens;
    vector<string> mtok;
    double val;
    tokenize(line, tokens);

    if(tokens.size() == 0)
        return;

    label = strtoint(tokens[0]);
    if(label < 0)
    {
        cerr << "Inavlid label on line " << lineno << endl;
        return;
    }

    for(int i = 1; i < (int)tokens.size(); i++)
    {
        mtok.clear();
        tokenize(tokens[i], mtok, ":");

        if(mtok.size() != 2)
        {
            data.clear();
            label = -1;
            cerr << "Inavlid syntax on line " << lineno << endl;
            return;
        }

        val = strtodouble(mtok[1]);
        data.push_back(val);
    }
}

// Entry point for program
int main(int argc, char **argv)
{
    string line;
    ifstream myfile;
    vector<double> data;
    vector<gallery_entry*> gallery;
    gallery_entry* gal;
    gallery_entry* sgal;

    int label = -1;
    int lineno = 0;
    int n;
    size_t size;

    // Parse the command line arguments
    prog_params params;
    parseCommandLine(argc, argv, &params);
    if(params.fileName == NULL || params.svmFile == NULL)
        usage(params.progName, 1);

    // Open the input file
    myfile.open(params.fileName);
    if(!myfile.is_open())
    {
        fprintf(stderr, "Unable to open `%s': %s\n",
                params.fileName, strerror(errno));
        exit(1);
    }

    // Process the input file line-by-line
    while(!myfile.eof())
    {
        ++lineno;
        getline(myfile, line);
        processLine(line, data, label, lineno);

        if(data.size() == 0 || label == -1)
            continue;

        if((gal = (gallery_entry*)malloc(sizeof(gallery_entry))) == NULL)
        {
            cerr << "Oops, ran out of memory." << endl;
            exit(1);
        }

        size = data.size() * sizeof(v1_float);
        gal->features = NULL;
        gal->pca_features = (v1_float*)malloc(size);
        memset(gal->pca_features, 0, size);
        gal->feature_count = data.size();
        gal->id = label;
        gal->path = NULL;
        gal->leyeX = -1;
        gal->leyeY = -1;
        gal->reyeX = -1;
        gal->reyeY = -1;

        for(n = 0; n < (int)data.size(); n++)
            gal->pca_features[n] = (v1_float)data[n];
        gallery.push_back(gal);
        gal = NULL;
    }

    myfile.close();

    if(gallery.size() == 0)
    {
        cerr << "Gallery is empty!" << endl;
        exit(1);
    }

    size = gallery.size() * sizeof(gallery_entry);
    if((sgal = (gallery_entry*)malloc(size)) == NULL)
    {
        cerr << "Oops, ran out of memory when creating static gallery." << endl;
        exit(1);
    }

    memset(sgal, 0, size);
    size = gallery.size();
    for(n = 0; n < (int)size; n++)
        memcpy(&sgal[n], gallery[n], sizeof(gallery_entry));
    cout << "Read in " << size << " entries." << endl;
    gallery.clear();

    //__verbose = 1;
    if(params.oneVSRest)
    {
        // Train the PCA-projected data using a one-vs-rest classifier
        learn_onevsrest* onevrest = NULL;
        if((onevrest = learn_onevsrest_create(0, 1, params.numProcs)) == NULL)
        {
            fprintf(stderr, "ERROR: Unable to create one vs rest svm learning model.\n");
            exit(42);
        }

        if(learn_onevsrest_train(onevrest, sgal, (unsigned int)size, params.svmFile) < 0)
        {
            fprintf(stderr, "ERROR: One vs rest training failed.\n");
            exit(43);
        }

        learn_onevsrest_destroy(onevrest);
        onevrest = NULL;
    }
    else
    {
        // Initialize the libsvm parameters
        svm_parameter svm_params;
        svm_params.svm_type     = C_SVC;
        svm_params.kernel_type  = LINEAR;
        svm_params.degree       = 2;
        svm_params.gamma        = 0; // 1/k
        svm_params.coef0        = 0;
        svm_params.nu           = 0.1;
        svm_params.cache_size   = 256;
        svm_params.C            = 10;
        svm_params.eps          = 0.01;
        svm_params.p            = 0.1;
        svm_params.shrinking    = 1;
        svm_params.probability  = 1;
        svm_params.nr_weight    = 0;
        svm_params.weight_label = NULL;
        svm_params.weight       = NULL;

        // Train the PCA-projected data
        learn(sgal, size, &svm_params, params.svmFile, NULL, 0);
    }

    // TODO: free sgal memory here (this means all actual gallery entries)

    SAFE_FREE(params.fileName);
    SAFE_FREE(params.svmFile);

    return 0;
}

// Parse the command line arguments and load them into a struct
static void parseCommandLine(int argc, char **argv, prog_params *params)
{
    argv[0] = basename(argv[0]);
    params->progName = argv[0];
    params->fileName = NULL;
    params->svmFile  = NULL;
    params->oneVSRest = 0;
    params->numProcs = 2;

    ArgParser ap(usage, params->progName, false);
    ap.addOption("help",        false, "h");
    ap.addOption("file",        true,  "f");
    ap.addOption("svm",         true,  "s");
    ap.addOption("one-vs-rest", true,  "M", "yes");

    if(!ap.parseCommandLine(argc, argv, false))
        exit(1);

    if(ap.isSet("help"))
        usage(params->progName, 0);
    if(ap.isSet("file"))
        params->fileName = strdup(ap.getValue("file"));
    if(ap.isSet("svm"))
        params->svmFile = strdup(ap.getValue("svm"));
    if(ap.isSet("one-vs-rest"))
        params->oneVSRest = ap.getBoolValue("one-vs-rest");
}

// Print the program's usage
static void usage(const char* name, int val)
{
    FILE* f = (val == 0) ? stdout : stderr;
    fprintf(f,
        "Usage: %s [OPTIONS]\n"
        "  -h, --help             Show this helpful message.\n"
        "  -f, --file=FILE        An SVM feature list.\n"
        "  -s, --svm=FILE         Save an SVM model to FILE.\n"
        "  -M, --one-vs-rest=BOOL Use one vs rest (default false).\n"
        , name);
    exit(val);
}

