#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "atmos_deblur.h"
#include <stdio.h>
#include <stdlib.h>

#define PI 3.14159265

//User Parameters
#define GAMMA_VALUE 0.001925
#define NUMBER_OF_OUTPUT_IMAGES 15 
//#define ATMOSPHERE_KERNAL_SIZE 41


float COMPUTE_IMAGE_MEAN(IplImage *imageIn)
{
    int i,j;
    int width, height, step;
    
    IplImage *floatImg;

    floatImg=cvCreateImage(cvSize(imageIn->width,imageIn->height),IPL_DEPTH_32F,1);
   
    width  	  = imageIn->width;
    height 	  = imageIn->height;
    step	  = imageIn->widthStep;
    
    float mean=0;
    float temp_mean;
    
    
  
for( i = 0; i < height ; i++ ) {
        for( j = 0 ; j < width ; j++ ) {
            
	  ((float*)(floatImg->imageData+floatImg->widthStep*i))[j]=((unsigned char*)(imageIn->imageData + imageIn->widthStep*i))[j];       
	  temp_mean=((float*)(floatImg->imageData+floatImg->widthStep*i))[j];
      temp_mean/=255;	  
      mean+=temp_mean;
        }
    }
    
 mean/=(width*height);
 cvReleaseImage( &floatImg );
 return mean;
    
}

float COMPUTE_IMAGE_STD(IplImage *imageIn, float image_mean)
{
  int i,j;
  int width, height, step;

  IplImage *floatImg;
  float temp_std,temp_std1, temp_std2;

  floatImg=cvCreateImage(cvSize(imageIn->width,imageIn->height),IPL_DEPTH_32F,1);
   
  width  	  = imageIn->width;
  height 	  = imageIn->height;
  step	  = imageIn->widthStep;
  
  float std=0;
  temp_std2=0;
  for( i = 0; i < height ; i++ ) {
    for( j = 0 ; j < width ; j++ ) {
      ((float*)(floatImg->imageData+floatImg->widthStep*i))[j]=((unsigned char*)(imageIn->imageData + imageIn->widthStep*i))[j];
      temp_std=((float*)(floatImg->imageData+floatImg->widthStep*i))[j];
      temp_std/=255;
      temp_std1 = temp_std - image_mean;
      temp_std1=temp_std1 * temp_std1;
      temp_std2+=temp_std1;
    }
  }
  
  
  std=sqrt(temp_std2/(width*height));
  cvReleaseImage( &floatImg );
  return std;
  
}

int FLAG_SUB_CROP_ATMOS(IplImage *original_crop, IplImage *deblurred_crop, float original_mean, float deblurred_mean, float original_std, float deblurred_std)
{

  IplImage *floatImgOrig;
  IplImage *floatImgDeblur;

  int i,j;
  int width, height, step;

  float tempOrig, tempDeblur;
  float temp_std=original_std*deblurred_std;
  float temp1;
  float norm_xcorr_value=0;
  width  	  = original_crop->width;
  height 	  = original_crop->height;
  step	          = original_crop->widthStep;
  
  floatImgOrig=cvCreateImage(cvSize(original_crop->width,original_crop->height),IPL_DEPTH_32F,1);
  floatImgDeblur=cvCreateImage(cvSize(original_crop->width,original_crop->height),IPL_DEPTH_32F,1);

  int flag;

  //norm_corr_value=norm_corr_value+((X(ii,jj)-mean_f)*(Blurred(ii,jj)-mean_t))/(std_f*std_t);

 for( i = 0; i < height ; i++ ) {
    for( j = 0 ; j < width ; j++ ) {

    ((float*)(floatImgOrig->imageData+floatImgOrig->widthStep*i))[j]=((unsigned char*)(original_crop->imageData + original_crop->widthStep*i))[j];
((float*)(floatImgDeblur->imageData+floatImgDeblur->widthStep*i))[j]=((unsigned char*)(deblurred_crop->imageData + deblurred_crop->widthStep*i))[j];

 tempOrig=((float*)(floatImgOrig->imageData+floatImgOrig->widthStep*i))[j];
 tempDeblur=((float*)(floatImgDeblur->imageData+floatImgDeblur->widthStep*i))[j];

 tempOrig/=255;
 tempDeblur/=255;

 tempOrig-=original_mean;
 tempDeblur-=deblurred_mean;
 temp1=tempOrig*tempDeblur;
 temp1/=temp_std;
 norm_xcorr_value+=temp1;

    }
 }

 norm_xcorr_value/=(width*height-1);


 if (norm_xcorr_value <= 0.80 || norm_xcorr_value >= 0.90)
     flag=1;
 else
     flag=0;
 // fprintf(stderr,"xcorr: %f\r\n",norm_xcorr_value);
 cvReleaseImage( &floatImgOrig );
 cvReleaseImage( &floatImgDeblur );

 return flag;

}

int BACKGROUND_SUB_CROP_ATMOS(IplImage *original_crop, IplImage *deblurred_crop)
{
  
  
  int flag=0;
  int width, height, step;
  int i,j;
  unsigned char  THRESHOLD=10;
  unsigned char* temp_value_original;
  unsigned char* temp_value_deblurred;
  unsigned char abs_value;
  int above_threshold_counter=0;
  
   width  	  = original_crop->width;
   height 	  = original_crop->height;
   step	  = original_crop->widthStep;
   
   //subtract original crop and deblurred
   for( i = 0; i < height ; i++ ) {
     for( j = 0 ; j < width ; j++ ) {
       
         temp_value_original=((unsigned char*) (original_crop->imageData + i * step) [j]);
         temp_value_deblurred=((unsigned char*) (deblurred_crop->imageData + i * step) [j]);
         abs_value=abs(temp_value_original-temp_value_deblurred);
         if (abs_value > THRESHOLD)
             above_threshold_counter++;
     }
   }
   
   if (above_threshold_counter > ((width*height)/1.5) || above_threshold_counter < (width*height)/6.3)
       flag=1;
   else
       flag=0;
   
   //printf("FLAG: %d BACK_DEBUG ABOVE_COUNTER: %d  CUTOFF: %d\n",flag,above_threshold_counter,((width*height)/3));
   // cvWaitKey(0);
   
   return (flag);
}



void CROP_REGION_ATMOS (IplImage * img_out, IplImage *img_in, int x_crop_start, int y_crop_start, int size_x, int size_y)
{
  
  static int i,j;
  
  //crop out eye area
  for( i = y_crop_start; i < y_crop_start + size_y; i++ ) {
    for( j = x_crop_start; j < x_crop_start + size_x ; j++ ) {
      
      (img_out->imageData + (int)(i-y_crop_start) * img_out ->widthStep)[(int)(j-x_crop_start)]=(img_in->imageData + i * img_in ->widthStep)[j];
    }
    
  }
  
}


float IMAGE_QUALITY_ATMOS(IplImage *input_image, int apertureSize, float Threshold)
{
  
  float temp_sum;
  float temp_mean;
  float temp_sobel;
  float temp_2u;
  IplImage        *sobel_x;
  IplImage        *sobel_y;
  IplImage        *sobel_z;
  int dx;
  int dy;
  short temp_x, temp_y;
  int temp_x_sq, temp_y_sq;
  int temp_z;
  float Image_Quality;
  int i,j;


  sobel_x=cvCreateImage( cvSize( input_image->width, input_image->height ), IPL_DEPTH_16S, 1 );
  sobel_y=cvCreateImage( cvSize( input_image->width, input_image->height ), IPL_DEPTH_16S, 1 );
  sobel_z=cvCreateImage( cvSize( input_image->width, input_image->height ), IPL_DEPTH_32F, 1 );
  
  dx=1;
  dy=0;
 
  cvSobel(input_image,sobel_x,dx,dy,apertureSize);
 
  dx=0;
  dy=1;
  
  cvSobel(input_image,sobel_y,dx,dy,apertureSize);
  
  //combine sobel_x and sobel_y
  for (i=0; i< sobel_x->height; i++)
    for (j=0; j < sobel_x->width; j++)
      {
	temp_x=((short *)(sobel_x->imageData + i*sobel_x->widthStep))[j];
	temp_y=((short *)(sobel_y->imageData + i*sobel_y->widthStep))[j];
	temp_x_sq=((int)temp_x * (int)temp_x);
	temp_y_sq=((int)temp_y * (int)temp_y);
	//	temp_z=pow((float)temp_x_sq+(float)temp_y_sq,(float).5);
	temp_z=pow((float)temp_x_sq,(float).5);

	((float *)(sobel_z->imageData + i*sobel_z->widthStep))[j]=temp_z;
      }
  
  //find sum and mean of edge pixels
  temp_sum=0;
  temp_sobel=0;
  
  for (i=0; i< sobel_z->height; i++)
    for (j=0; j < sobel_z->width; j++)
      {
	temp_sobel=((float *)(sobel_z->imageData + i*sobel_z->widthStep))[j];
	temp_sum+=temp_sobel;
      }
  
  temp_mean=(temp_sum)/(sobel_z->width*sobel_z->height);
  
  //find sum of pixels above 2U of temp_mean
  temp_mean*=Threshold;
  temp_2u=0;
  for (i=0; i< sobel_z->height; i++)
    for (j=0; j < sobel_z->width; j++)
      {
	temp_sobel=((float *)(sobel_z->imageData + i*sobel_z->widthStep))[j];
	if (temp_sobel > temp_mean)
	  temp_2u+=temp_sobel;
      }
  
  Image_Quality=(float)(temp_2u/temp_mean);
  
  
  
  cvReleaseImage (&sobel_x);
  cvReleaseImage (&sobel_y);
  cvReleaseImage (&sobel_z);
  
    
  return (Image_Quality);

}

/**
 *Preform fft on input image
 * @param IplImage* img1 input image
 * @param int width image width
 * @param int height image height
 * @param int step image widthstep
 * @return fftw_complex* fft of the image containing real and imaginary parts
 */
fftw_complex* FFT_Image(IplImage* img1, int width, int height, int step)
{
  int i,j,k;
  uchar			*img1_data;
  img1_data = ( uchar* ) img1->imageData; 
  fftw_complex    *data_in;
  fftw_plan       plan_f;
  fftw_complex    *fft;
  
  /*initialize arrays for fftw operations */
  data_in = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width * height );
  fft     = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width * height );
  plan_f = fftw_plan_dft_1d( width * height, data_in, fft,  FFTW_FORWARD,  FFTW_ESTIMATE );
  
  /* load img1's data to fftw input */
  for( i = 0, k = 0 ; i < height ; i++ ) {
    for( j = 0 ; j < width ; j++ ) {
      data_in[k][0] = ( double )img1_data[i * step + j];
      data_in[k][1] = 0.0;
      k++;
    }
  }
  
  /* perform FFT */
  fftw_execute( plan_f );
  fftw_destroy_plan(plan_f);
  fftw_free(data_in);
  return fft;
}
/**
 *Computes the atmospheric point spread function(PSF)
 * @param float* PSF_array array of 0's to be populated
 * @param float c controls The blur severity
 * @param int kernal_size The size of kernel
 * @param int kernal_middle Basis for x and y offsets
 */
float* Compute_PSF (float* PSF_array, float c, int kernal_size, int kernal_middle)
{
  int i,j;
  float temp, sum;
  float kernal_temp1, kernal_temp2, kernal_temp3, kernal_temp4;
  float offset_x, offset_y;
  
  //initilize array
  for (i=0; i<kernal_size; i++)
    for (j=0; j<kernal_size;j++)
      {  
	PSF_array[i*kernal_size+j]=0;
      }
  
  for (i=0; i<kernal_size; i++)
    for (j=0; j<kernal_size;j++)
      {
	if ( i < kernal_middle)
	  offset_x=abs(kernal_middle-i);
	else
	  offset_x=(i-kernal_middle);
	
	if (j < kernal_middle)
	  offset_y=abs(kernal_middle-j);
	else
	  offset_y=(j-kernal_middle);
	
	kernal_temp1=((offset_x * offset_x) + (offset_y * offset_y));
	kernal_temp2=powf(kernal_temp1,0.833333);
	kernal_temp3=-c*(kernal_temp2);
	kernal_temp4=exp(kernal_temp3);
	PSF_array[i*kernal_size+j]=kernal_temp4;
      }
  
  //find sum of the kernal
  sum=0;
  for (i=0; i<kernal_size; i++)
    for (j=0; j<kernal_size;j++)
      {
	temp=PSF_array[i*kernal_size+j];
	sum+=temp;
      }
  
  //normalize kernal
  for (i=0; i<kernal_size; i++)
    for (j=0; j<kernal_size;j++)
      {
	temp=PSF_array[i*kernal_size+j];
	temp/=sum;
	PSF_array[i*kernal_size+j]=temp;
      }
  
  return PSF_array;
}
/**
 *Computes the optical transfer function(OTF) or the fft of the psf
 * @param float* OTF_array Array of 0's to be populated
 * @param float* temp_OTF_array Used for intermediate computation
 * @param float* PSF_array atmospheric point spread function
 * @param int kernal_size The size of the kernal
 * @param int kernal_middle Basis for x and y offsets
 * @param int height The height of the image
 * @param int width The widht of the image
 */
float* Compute_OTF (float* OTF_array, float* temp_OTF_array, float* PSF_array, int kernal_size, int kernal_middle, int height, int width)
{
  int i,j;
  int shift_up;
  int shift_left;
  float temp;
  //initilize OTF Array
  for (i=0; i<height; i++)
    for (j=0; j<width;j++)
      {
	OTF_array[i*width+j]=0;
	temp_OTF_array[i*width+j]=0;
      }
  
  //insert PSF into OTF array
  for (i=0; i<kernal_size; i++)
    for (j=0; j<kernal_size;j++)
      {
	temp=PSF_array[i*kernal_size+j];
	OTF_array[i*width+j]=temp;	
      }
  
  //circularly shift the OTF so that the center of 
  //the PSF is at the (0,0) element of the array
  shift_up=kernal_middle;
  shift_left=shift_up;
  
  ///CIRCULAR SHIFT UP///////////////// 
  for (i=shift_up; i < height; i++)
    for (j=0; j < width; j++)
      {
	temp_OTF_array[((i-shift_up)*width)+j]=OTF_array[i*width+j];
      }
  
  
  for (i=(height)-(shift_up); i < height; i++)
    for (j=0; j < width; j++)
      {
	temp_OTF_array[i*width+j]=OTF_array[((i-(height-shift_up))*width)+j];
      }
  
  //CIRCULAR SHIFT LEFT
  for (i=shift_left; i < width; i++)
    for (j=0; j < height; j++)
      {
	OTF_array[(j*width)+(i-shift_left)]=temp_OTF_array[(j*width)+i];	  
      }
  
  for (i=width-shift_left; i < width; i++)
    for (j=0; j < height; j++)
      {
	OTF_array[((j*width)+i)]=temp_OTF_array[(j*width)+(i-(width-shift_left))];   
      }
  
  return OTF_array;
}
/**
 * Deconvolves the image with estimated blur(OTF)
 * @param IplImage* img2 The original Blurred image
 * @param fftw_complex **fft The fft of the original blurred image.
 * @param float* OTF_array The blur OTF.
 * @param int height The height of the image.
 * @param int widht The width of the image.
 */
//IplImage* Wiener_Filter_Image(IplImage *img2, fftw_complex *fft, float* OTF_array, int height, int width)
inline void Wiener_Filter_Image(IplImage *img2, fftw_complex *fft, float* OTF_array, int height, int width, float SNR_ACF)
{
  int i,j,k;
  float temp;
  float SNR, SNR_temp, SNR_temp1;
  
  fftw_complex    *OTF_in;
  fftw_complex    *OTF_fft;        
  fftw_complex    *temp_wiener;
  fftw_complex    *recovered;
  fftw_complex    *recovered_out;
  
  fftw_plan       plan_OTF;
  fftw_plan       plan_ifft_wiener;
  
  
  /*initialize arrays for fftw operations */
  recovered     = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width * height );
  recovered_out     = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width * height );
  OTF_in  = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width * height );
  OTF_fft = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width * height );
  temp_wiener = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width * height );
  
  /* create plans */
  plan_OTF = fftw_plan_dft_1d( width * height, OTF_in, OTF_fft,  FFTW_FORWARD,  FFTW_ESTIMATE );
  plan_ifft_wiener = fftw_plan_dft_1d( width * height, recovered, recovered_out, FFTW_BACKWARD, FFTW_ESTIMATE );
  
  /* load OTF data to fftw input */
  for( i = 0, k = 0 ; i < height ; i++ ) {
    for( j = 0 ; j < width ; j++ ) {
      temp=OTF_array[i*width+j];
      OTF_in[k][0] = ( double )temp;
      OTF_in[k][1] = 0.0;
      k++;
    }
  }
  
  /* perform FFT */
  fftw_execute(plan_OTF);
  
  //modify OTF to avoid divide by 0 error
  for (k=0; k < height*width; k++)
    {
      if(OTF_fft[k][0]==0)
	OTF_fft[k][0]=0.0000000000001;
    }
  
  for (k=0; k < height*width; k++)
    {
      temp_wiener[k][0]=OTF_fft[k][0]/((OTF_fft[k][0]*OTF_fft[k][0]) + (SNR_ACF));
    }
  
  //Convolve Image and Wiener Kernal in Frequency Domain
  for (k=0; k < height*width; k++)
    {
      recovered[k][0]=fft[k][0]*temp_wiener[k][0];
      recovered[k][1]=fft[k][1]*temp_wiener[k][0];
    }
  
  /* perform IFFT */
  fftw_execute( plan_ifft_wiener );
  
  /* normalize IFFT result */
  for( k = 0 ; k < ( width * height ) ; k++ ) {
    recovered_out[k][0] = (recovered_out[k][0])/(width*height);
    if (recovered_out[k][0] > 255)
      recovered_out[k][0]=255;
    if (recovered_out[k][0] < 0)
      recovered_out[k][0]= 0;
  }
  
  /* copy IFFT result to img_float's data */
  //added abs to get rid of errors in conversion
  for( i = 0, k = 0 ; i < height ; i++ ) {
    for( j = 0 ; j < width ; j++ ) {
      ((uchar *)(img2->imageData + i*img2->widthStep))[j]= ((uchar)recovered_out[k++][0]);
    }
  }

  fftw_destroy_plan( plan_OTF );
  fftw_destroy_plan( plan_ifft_wiener );
  fftw_free( recovered );
  fftw_free( recovered_out );
  fftw_free( OTF_in );
  fftw_free( OTF_fft );
  fftw_free( temp_wiener );

}
void Edge_Taper_Atmos(IplImage *img1, int width, int height)
{
  

  float alpha=0.8;
  int i,j;
  float temp1_hanning, temp2_hanning;
  float temp1, temp2, temp3, temp4, temp5, temp6, temp7;
  float Tl, Th;
  float factor;

  float *hanning_window_array, *hanning_window_array2;
  IplImage *smoothed_img;
  unsigned char temp_hanning, temp_smooth;
  
  smoothed_img=cvCreateImage(cvSize(img1->width,img1->height),IPL_DEPTH_8U,1);
  cvSmooth(img1,smoothed_img,CV_GAUSSIAN,7,0,10);
 
  hanning_window_array = (float *)malloc(sizeof(float)* width);
  if (!hanning_window_array) {printf("MALLOC Failed hanning_window_array\r\n");}
  hanning_window_array2 =(float *)malloc(sizeof(float)* height);
  if (!hanning_window_array2) {printf("MALLOC Failed hanning_window_array2\r\n");}
 
 //vertical Tukey window
  factor=(1/(float)width);
  Tl=floor((alpha*(float)width)/2);
 Th=floor(width-Tl);
 
 for( j = 0 ; j < width; j++ ) { 
   
   if (j <= Tl)
     {
       temp1=(factor*j)-factor;
       temp2=temp1-(alpha/2);
       temp3=temp2/(alpha/2);
       temp4=PI*temp3;
       temp5=cos(temp4);
       temp6=1+temp5;
       temp7=temp6/2;
       hanning_window_array[j]=temp7;
     }
   else if (j > Tl && j < Th)
     {
       hanning_window_array[j]=1;
     }
   else
     {
       temp1=(factor*j)-factor;
       temp2=temp1+(alpha/2);
       temp3=temp2/(alpha/2);
       temp4=PI*temp3;
       temp5=cos(temp4);
       temp6=1+temp5;
       temp7=temp6/2;
       hanning_window_array[j]=temp7;
     }
   
 }
  
  //horizontal Tukey  window
 factor=(1/(float)height);
 Tl=floor((alpha*(float)height)/2);
 Th=floor(height-Tl);
  
for( j = 0 ; j < height; j++ ) { 
   
if (j <= Tl)
     {
       temp1=(factor*j)-factor;
       temp2=temp1-(alpha/2);
       temp3=temp2/(alpha/2);
       temp4=PI*temp3;
       temp5=cos(temp4);
       temp6=1+temp5;
       temp7=temp6/2;
       hanning_window_array2[j]=temp7;
     }
   else if (j > Tl && j < Th)
     {
       hanning_window_array2[j]=1;
     }
   else
     {
       temp1=(factor*j)-factor;
       temp2=temp1+(alpha/2);
       temp3=temp2/(alpha/2);
       temp4=PI*temp3;
       temp5=cos(temp4);
       temp6=1+temp5;
       temp7=temp6/2;
       hanning_window_array2[j]=temp7;
     }
  }
  
  
  //multiply windows to image//
  for( i = 0; i < height ; i++ ) {
    for( j = 0 ; j < width ; j++ ) {
      temp_hanning =((uchar *)(img1->imageData + i*img1->widthStep))[j];
      temp_smooth =((uchar *)(smoothed_img->imageData + i*smoothed_img->widthStep))[j];
      temp1_hanning=hanning_window_array[j]*hanning_window_array2[i];
      temp2_hanning=(temp_hanning*temp1_hanning)+(temp_smooth*(1-temp1_hanning));
      ((uchar *)(img1->imageData + i*img1->widthStep))[j]= temp2_hanning;
     
    }
  }
  
  
  free(hanning_window_array);
  free(hanning_window_array2);
  cvReleaseImage(&smoothed_img);
}


/**
 *Preforms atmospheric deblurring of an image
 * @param IplImage* input_img the blurred image.
 * @return IplImage* The deblurred image.
 */
//int main(int argc, char *argv[])
IplImage* atmos_deblur(IplImage* input_img, int alerts)
{
  
  int ATMOSPHERE_KERNAL_SIZE = 41;

  int kernal_size=ATMOSPHERE_KERNAL_SIZE;
 
  float* PSF_array;
  float* OTF_array;
  float* temp_OTF_array;
  
  int width, height, step;
  int loops;
  
  IplImage *blurred_image = 0; 
  
  fftw_complex *fft_image;
  fftw_complex *sqi_fft_image;
  
  float* c_atmos;
  
  float parameter2;
  
  static float previous_parameter2;
  float* image_metric;
  
  static int parameter_flag=0;
  int kernal_middle=(kernal_size/2);	
  
  int apertureSize;
  float max_score;
  float temp_score;
  int ii;

  static float* c_past;
  static float* c_sort;
  static char indicator=0;
  
  static int index;
  static int previous_index;
  static int all_failed_flag=0;
  static int initial_loop=0;
  
  float crop_start=0.20;
  float c=0;
  
  //ACF Variables
  static float max_acf_val;
  static float nf;
  static float factor1, factor2;
  static float SNR_ACF;
  static float previous_SNR_ACF=0.0003;
  static float temp_SNR;
  static float ABS_temp_SNR;
  static float SNR_negative_flag;
  

  static float mean_acf;
  static float temp_acf;
 

  //new variables
  static float mean_array[15];
  static float std_array[15];
  static int kernalArray[15];

  IplImage *deblurred_image = 0;
  IplImage *sqi_image = 0;
  IplImage *crop_img0 = 0;
  IplImage *crop_img1 = 0;
  IplImage *crop_img2 = 0;
  IplImage *crop_img3 = 0;
  IplImage *crop_img4 = 0;		
  IplImage *crop_img5 = 0;
  IplImage *crop_img6 = 0;
  IplImage *crop_img7 = 0;
  IplImage *crop_img8 = 0;
  IplImage *crop_img9 = 0;
  IplImage *crop_img10 = 0;
  IplImage *crop_img11 = 0;
  IplImage *crop_img12 = 0;
  IplImage *crop_img13 = 0;
  IplImage *crop_img14 = 0;
  
  static float* SNR_past;
  int*   background_sub_flag = (int *)malloc(sizeof(int) * NUMBER_OF_OUTPUT_IMAGES);
  
  int APERTURE=7;
  int gt_zero_counter;
  float THRESHOLD =2.5;
  int    all_zero=1;
  int i,j,k;

int size_filter=8;

  if (indicator==0)
    {
      c_past =(float *)malloc(sizeof(float)*size_filter);	
      c_sort =(float *)malloc(sizeof(float)*size_filter);			
      SNR_past = (float*) malloc(sizeof(float)*size_filter);

	for(i=0; i<size_filter; i++)
	{      
		c_past[i]=0.00;
 	} 
    
      previous_parameter2=0.00;
    }
  
  /* load original image */
  blurred_image = cvCreateImageHeader(cvSize(input_img->width, input_img->height), IPL_DEPTH_8U, 1);    
  blurred_image->imageData = input_img->imageData;
  deblurred_image = cvCreateImage( cvSize( blurred_image->width, blurred_image->height ), IPL_DEPTH_8U, 1 );   
  sqi_image = cvCreateImage( cvSize( blurred_image->width, blurred_image->height ), IPL_DEPTH_8U, 1 );   
  
  /* get image properties */
  width  	  = blurred_image->width;
  height 	  = blurred_image->height;
  step	          = blurred_image->widthStep;
  
  //Edge_Taper_Atmos(blurred_image,width,height);
  
  
  
  /* FFT Input Image */
  
  fft_image=FFT_Image(blurred_image,width,height,step);
  
  sqi_fft_image=FFT_Image(blurred_image,width,height,step);
  fftw_complex    *ifft;	
  fftw_plan       plan_r;  
  ifft   =  ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width * height );
  plan_r = fftw_plan_dft_1d( width*height, sqi_fft_image, ifft,  FFTW_BACKWARD,  FFTW_ESTIMATE );
  
  /* perfrom correlation */
  //eye filter already preprocessed with hanning window and conjugated
  for( k = 0 ; k < height * width ; k++ ) {
    sqi_fft_image[k][0]=((sqi_fft_image[k][0]*sqi_fft_image[k][0])-(sqi_fft_image[k][1]*(-1*sqi_fft_image[k][1])));
    sqi_fft_image[k][1]=((sqi_fft_image[k][0]* (-1*sqi_fft_image[k][1]))+(sqi_fft_image[k][1]* sqi_fft_image[k][0]));
  }
  
  /* perfrom ifft */
  fftw_execute( plan_r );
  
  IplImage        *img_float = 0;
  img_float = cvCreateImage( cvSize( blurred_image->width, blurred_image->height ), IPL_DEPTH_32F, 1 );
  
  /* copy IFFT result to 2D float data */
  for( i = 0, k = 0 ; i < height ; i++ ) {
    for( j = 0 ; j < width ; j++ ) {
      ((float *)(img_float->imageData + i*img_float->widthStep))[j]=((float)ifft[i*width+j][0]/(width*height));
    }
  }
  float temp_pixel;
  float* average_array = (float *)malloc(sizeof(float)*width);
  
  //initilize average array
  for (i=0; i<width; i++)
    average_array[i]=0;
  
  //average columns (collapse 2D power spectrum)
  for (i=0; i < width; i++)
    {
      for (j=0; j < height; j++)
	{
	  temp_pixel=((float *)(img_float->imageData + j*img_float->widthStep))[i];
	  average_array[i]+=temp_pixel;
	}     
      average_array[i]/=height;  
    }	
  
  float max_value=0;
  int max_ind=0;
  mean_acf=0;
  float temp;
  for (i=0; i < width; i++){
    temp=average_array[i];
    mean_acf+=temp;
    if (temp > max_value)
      {
      max_value=temp;
      max_ind=i;
      }
  }
  
   mean_acf/=width;
   //max_value=average_array[0];
   //	float nf;
   

   if(max_ind==0)  
       nf=average_array[max_ind+1];
   else
       nf=average_array[max_ind-1];
   //	float factor1, factor2;
   
   factor1=nf-mean_acf;
   factor2=max_value-nf;
   //float SNR;
   if (mean_acf > nf)
       //  fprintf(stderr,"BAD SNR ESTIMATE!!!!!!!!!!\r\n");
   
   
   // fprintf(stderr,"max_acf_val: %f mean_acf: %f nf: %f factor1: %f factor2: %f\r\n",max_value,mean_acf,nf,factor1,factor2);
  
   //some feedback to prevent wild SNR value swings
   float snr_temp;
   float abs_snr_temp;
   if (initial_loop > size_filter-1)
     {
       previous_SNR_ACF=SNR_ACF;
       SNR_ACF=factor2/factor1;
       //SNR_ACF*=.1;
       //shift parameter buffer
       for (ii=0; ii <= size_filter-2; ii++)
       {
           SNR_past[ii]=SNR_past[ii+1];
           //  fprintf(stderr,"SNR[%d]: %f\r\n",ii,SNR_past[ii]);
       }
       
       SNR_past[size_filter-1]=SNR_ACF;
       //  fprintf(stderr,"SNR[7]: %f\r\n",SNR_past[size_filter-1]);

       SNR_ACF=((SNR_past[7]*.25)+(SNR_past[6]*.20)+(SNR_past[5]*.18)+(SNR_past[4]*.14)+(SNR_past[3]*.08)+(SNR_past[2]*.06)+(SNR_past[1]*.05)+(SNR_past[0]*.04));
       fprintf(stderr,"SNR OUT: %f\r\n\n",SNR_ACF);
       //   fprintf(stderr,"snr_temp: %f abs_temp: %f new_SNR_ACF: %f\r\n",snr_temp,abs_snr_temp,SNR_ACF*c*4);
       if (SNR_ACF >= 0.1)       
           SNR_ACF=0.1;
       // if (SNR_ACF < 0.0001)
       //   SNR_ACF=0.0001;
     


     }

   //first value
   else
   {
       SNR_ACF=factor2/factor1;
       //SNR_ACF*=.1;
       //put value in snr buffer
       SNR_past[initial_loop]=SNR_ACF;
       //  fprintf(stderr,"INITAL SNR[%d] : %f\r\n",initial_loop,SNR_past[initial_loop]);
       //  fprintf(stderr,"WORKING ACF: %f factor1: %f factor2: %f\r\n",SNR_ACF,factor1,factor2);
   }
   crop_img0=cvCreateImage(cvSize(blurred_image->width,int(64)),IPL_DEPTH_8U,1);    
   crop_img1=cvCreateImage(cvSize(blurred_image->width,int(64)),IPL_DEPTH_8U,1);    
   crop_img2=cvCreateImage(cvSize(blurred_image->width,int(64)),IPL_DEPTH_8U,1);    
   crop_img3=cvCreateImage(cvSize(blurred_image->width,int(64)),IPL_DEPTH_8U,1);    
   crop_img4=cvCreateImage(cvSize(blurred_image->width,int(64)),IPL_DEPTH_8U,1);    
   crop_img5=cvCreateImage(cvSize(blurred_image->width,int(64)),IPL_DEPTH_8U,1);    
   crop_img6=cvCreateImage(cvSize(blurred_image->width,int(64)),IPL_DEPTH_8U,1);    
   crop_img7=cvCreateImage(cvSize(blurred_image->width,int(64)),IPL_DEPTH_8U,1);    
   crop_img8=cvCreateImage(cvSize(blurred_image->width,int(64)),IPL_DEPTH_8U,1);    
   crop_img9=cvCreateImage(cvSize(blurred_image->width,int(64)),IPL_DEPTH_8U,1);    
   crop_img10=cvCreateImage(cvSize(blurred_image->width,int(64)),IPL_DEPTH_8U,1);    
   crop_img11=cvCreateImage(cvSize(blurred_image->width,int(64)),IPL_DEPTH_8U,1);    
   crop_img12=cvCreateImage(cvSize(blurred_image->width,int(64)),IPL_DEPTH_8U,1);    
   crop_img13=cvCreateImage(cvSize(blurred_image->width,int(64)),IPL_DEPTH_8U,1);    
   crop_img14=cvCreateImage(cvSize(blurred_image->width,int(64)),IPL_DEPTH_8U,1); 
   
   
   c_atmos      =(float *)malloc(sizeof(float)*NUMBER_OF_OUTPUT_IMAGES);
   image_metric =(float *)malloc(sizeof(float)*NUMBER_OF_OUTPUT_IMAGES);
   
   
   
   
   
   //compute c parameter
   if (parameter_flag==0)
     {
       //initial c_parameter
       c_atmos[0]=0.02;
       c_atmos[1]=0.03;
       c_atmos[2]=0.04;
       c_atmos[3]=0.05;
       c_atmos[4]=0.06;
       c_atmos[5]=0.07;
       c_atmos[6]=0.08;
       c_atmos[7]=0.09;
       c_atmos[8]=0.1;
       c_atmos[9]=0.2;
       c_atmos[10]=0.3;
       c_atmos[11]=0.4;
       c_atmos[12]=0.5;
       c_atmos[13]=0.6;
       c_atmos[14]=0.7;

       kernalArray[0]=91;
       kernalArray[1]=91;
       kernalArray[2]=91;
       kernalArray[3]=41;
       kernalArray[4]=41;
       kernalArray[5]=41;
       kernalArray[6]=41;
       kernalArray[7]=41;
       kernalArray[8]=41;
       kernalArray[9]=41;
       kernalArray[10]=41;
       kernalArray[11]=41;
       kernalArray[12]=41;
       kernalArray[13]=41;
       kernalArray[14]=41;

     }
   
   //SNR_ACF=0.009330;
   
   float original_mean;
   float original_std;
   original_mean=COMPUTE_IMAGE_MEAN(blurred_image);
   original_std=COMPUTE_IMAGE_STD(blurred_image,original_mean);


   for (loops=0; loops < NUMBER_OF_OUTPUT_IMAGES; loops++)
     {
       c=c_atmos[loops];
       kernal_size=kernalArray[loops];
       kernal_middle=(kernal_size/2);
      
       /* Allocate Memory for PSF/OTF arrays */
       PSF_array      =(float *)malloc(sizeof(float)*kernal_size*kernal_size);
       OTF_array      =(float *)malloc(sizeof(float)*width*height);
       temp_OTF_array =(float *)malloc(sizeof(float)*width*height);

       PSF_array=Compute_PSF(PSF_array,c,kernal_size,kernal_middle);
       OTF_array=Compute_OTF(OTF_array,temp_OTF_array,PSF_array,kernal_size,kernal_middle,height,width);
       Wiener_Filter_Image(deblurred_image,fft_image,OTF_array,height,width,SNR_ACF);
       
       free(PSF_array);
       free(OTF_array);
       free(temp_OTF_array);

       //add new normxcorr metric
       mean_array[loops] = COMPUTE_IMAGE_MEAN(deblurred_image);
       std_array[loops] = COMPUTE_IMAGE_STD(deblurred_image,mean_array[loops]);
	 
       background_sub_flag[loops]=FLAG_SUB_CROP_ATMOS(blurred_image,deblurred_image, original_mean,mean_array[loops],original_std, std_array[loops]);

       background_sub_flag[loops]=BACKGROUND_SUB_CROP_ATMOS(blurred_image,deblurred_image);
       
       
       switch(loops){
	 
       case 0: 
	 CROP_REGION_ATMOS (crop_img0, deblurred_image, int (0), deblurred_image->height*crop_start, deblurred_image->width, 64);
	 break;
	 
       case 1: 
	 CROP_REGION_ATMOS (crop_img1, deblurred_image, int (0), deblurred_image->height*crop_start, deblurred_image->width, 64);	  
	 break;
	 
       case 2: 
	 CROP_REGION_ATMOS (crop_img2, deblurred_image, int (0), deblurred_image->height*crop_start, deblurred_image->width, 64);  
	 break;
	 
       case 3:
	 CROP_REGION_ATMOS (crop_img3, deblurred_image, int (0), deblurred_image->height*crop_start , deblurred_image->width, 64); 
	 break;
	 
       case 4: 
	 CROP_REGION_ATMOS (crop_img4, deblurred_image, int (0), deblurred_image->height*crop_start, deblurred_image->width, 64);	  
	 break;
	 
       case 5: 
	 CROP_REGION_ATMOS (crop_img5, deblurred_image, int (0), deblurred_image->height*crop_start, deblurred_image->width, 64); 
	 break;
	 
       case 6:
	 CROP_REGION_ATMOS (crop_img6, deblurred_image, int (0), deblurred_image->height*crop_start, deblurred_image->width, 64);	  
	 break;
	 
       case 7:
	 CROP_REGION_ATMOS (crop_img7, deblurred_image, int (0), deblurred_image->height*crop_start, deblurred_image->width, 64);	  
	 break;
	 
       case 8: 
	 CROP_REGION_ATMOS (crop_img8, deblurred_image, int (0), deblurred_image->height*crop_start, deblurred_image->width, 64);
	 break;
	 
       case 9:
	 CROP_REGION_ATMOS (crop_img9, deblurred_image, int (0), deblurred_image->height*crop_start, deblurred_image->width, 64);	
	 break;
	 
       case 10: 
	 CROP_REGION_ATMOS (crop_img10, deblurred_image, int (0), deblurred_image->height*crop_start, deblurred_image->width, 64);	
	 break;
	 
       case 11:
	 CROP_REGION_ATMOS (crop_img11, deblurred_image, int (0), deblurred_image->height*crop_start, deblurred_image->width, 64);
	 break;
	 
       case 12:
	 CROP_REGION_ATMOS (crop_img12, deblurred_image, int (0), deblurred_image->height*crop_start, deblurred_image->width, 64);	
	 break;
	 
       case 13: 
	 CROP_REGION_ATMOS (crop_img13, deblurred_image, int (0), deblurred_image->height*crop_start, deblurred_image->width, 64);   	
	 break;
	 
       case 14:
	 CROP_REGION_ATMOS (crop_img14, deblurred_image, int (0), deblurred_image->height*crop_start, deblurred_image->width, 64);	
	 break;
	 
	}
       
    }
        


   
  //while loop
  while (all_zero==1){
    
    
    THRESHOLD-=0.5;
    for (loops=0; loops < NUMBER_OF_OUTPUT_IMAGES; loops++)
      {
	
	switch(loops){
	  
	case 0:  image_metric[loops]=IMAGE_QUALITY_ATMOS(crop_img0,APERTURE,(float)THRESHOLD);
	  break;
	  
	case 1:  image_metric[loops]=IMAGE_QUALITY_ATMOS(crop_img1,APERTURE,(float)THRESHOLD);
	  break;
	  
	case 2:  image_metric[loops]=IMAGE_QUALITY_ATMOS(crop_img2,APERTURE,(float)THRESHOLD);
	  break;
	  
	case 3:  image_metric[loops]=IMAGE_QUALITY_ATMOS(crop_img3,APERTURE,(float)THRESHOLD);
	  break;
	  
	case 4:  image_metric[loops]=IMAGE_QUALITY_ATMOS(crop_img4,APERTURE,(float)THRESHOLD);
	  break;
	  
	case 5:  image_metric[loops]=IMAGE_QUALITY_ATMOS(crop_img5,APERTURE,(float)THRESHOLD);
	  break;
	  
	case 6:  image_metric[loops]=IMAGE_QUALITY_ATMOS(crop_img6,APERTURE,(float)THRESHOLD);
	  break;
	  
	case 7:  image_metric[loops]=IMAGE_QUALITY_ATMOS(crop_img7,APERTURE,(float)THRESHOLD);
	  break;
	  
	case 8:  image_metric[loops]=IMAGE_QUALITY_ATMOS(crop_img8,APERTURE,(float)THRESHOLD);
	  break;
	  
	case 9:  image_metric[loops]=IMAGE_QUALITY_ATMOS(crop_img9,APERTURE,(float)THRESHOLD);
	  break;
	  
	case 10: image_metric[loops]=IMAGE_QUALITY_ATMOS(crop_img10,APERTURE,(float)THRESHOLD);
	  break;
	  
	case 11: image_metric[loops]=IMAGE_QUALITY_ATMOS(crop_img11,APERTURE,(float)THRESHOLD);
	  break;
	  
	case 12:   image_metric[loops]=IMAGE_QUALITY_ATMOS(crop_img12,APERTURE,(float)THRESHOLD);
	  break;
	  
	case 13:  image_metric[loops]=IMAGE_QUALITY_ATMOS(crop_img13,APERTURE,(float)THRESHOLD);
	  break;
	  
	case 14:   image_metric[loops]=IMAGE_QUALITY_ATMOS(crop_img14,APERTURE,(float)THRESHOLD);
	  break;
	  
	  
	}//end case
	
      }//end for
    
    gt_zero_counter=0;
    //check to see if all values are 0
    for (ii=0; ii < NUMBER_OF_OUTPUT_IMAGES; ii++)
      {
	//	printf("SCORE[%d]: %f background_flag[%d]: %d \r\n",ii,image_metric[ii],ii,background_sub_flag[ii]);
	//	cvWaitKey(0);
	if (image_metric[ii] > 0)
	  {
	    gt_zero_counter++;
	  }
	
      }
    
    if (gt_zero_counter >=13 || THRESHOLD==0)
      { 
	all_zero=0;
	//	printf("FINAL THRESHOLD: %f\n",THRESHOLD);
      }
  }//end while
  
  // cvWaitKey(0);
  
  //check to see if all values are 0
  for (ii=0; ii < NUMBER_OF_OUTPUT_IMAGES; ii++)
    {
      printf("SCORE[%d]: %f background_flag[%d]: %d \r\n",ii,image_metric[ii],ii,background_sub_flag[ii]);
      
    }
  
  //cvWaitKey(0);
  //find best image based on metric
  max_score=-1;
  
  parameter2=999;	
  //look for max score
  for (ii=1; ii < NUMBER_OF_OUTPUT_IMAGES; ii++){
    temp_score=image_metric[ii];
    if (temp_score > max_score && temp_score !=0 &&  background_sub_flag[ii]==0)
      {     
	if (ii < 8)
	  {
	    parameter2=c_atmos[ii];
	    index=ii;
	    max_score=temp_score;
	  }	
	else if (ii >=8 && ii < 14)
	  { 
	    parameter2=c_atmos[ii];
	      index=ii;
	      max_score=temp_score;
	  }
	else
	  {
	    parameter2=c_atmos[ii];
	    index=ii;
	    max_score=temp_score;
	  }
  	
      }
  }
  
  if (parameter2==999)
    
    all_failed_flag=1;  
  
  else
    all_failed_flag=0;
  
  //fprintf(stderr,"PRELIMINARY Atmospheric Parameter: %f PREVIOUS PARAMETER: %f\r\n",parameter2,c_atmos[previous_index]);
  // fprintf(stderr,"SNR_ACF: %f\r\n",SNR_ACF);
  
  int change_threshold=3;
  
  if (indicator !=0 && all_failed_flag==0)
    {
      if (abs(index - previous_index) >= change_threshold && (index-previous_index) > 0)
	{
	
	  //no change
	  parameter2=c_atmos[previous_index+change_threshold-1];
	  
	  index=previous_index + change_threshold-1;
	  
	}      
      else if (abs(index - previous_index) >= change_threshold && (index-previous_index) < 0)
	{	

	  parameter2=c_atmos[previous_index-change_threshold+1];
	  
	  
	
	  index=previous_index-change_threshold+1;

	} 
    }
  else
    {
      indicator=1;
    }
  
  // fprintf(stderr,"BEST Atmospheric Parameter: %f Index: %d Previous Index: %d\r\n",parameter2,index,previous_index);
  
  if (all_failed_flag==0)
    previous_index=index;
  else{
    
    previous_index=14;
    parameter2=0.7;  
  }
  
  //shift parameter buffer
  for (ii=0; ii <= size_filter-2; ii++)
    {
      c_past[ii]=c_past[ii+1];
    }
  
  c_past[size_filter-1]=parameter2;
  
  
  for (ii=0; ii <= size_filter-1; ii++)
    {
      c_sort[ii]=c_past[ii];
    }
  
  if (initial_loop > size_filter-1)
    {
      //sort buffer
      int k,l,m;
      float hold;
      /*
      for (k=0; k<=3; k++)
	{
	  m=k;
	  for (l=k+1; l<=4; l++)
	    {
	      if (c_sort[l] < c_sort[m])
		m=l;
	    }
	  hold=c_sort[m];
	  c_sort[m]=c_sort[k];
	  c_sort[k]=hold;
	}
      */
      
      
      temp_score=0;
      for (ii=0; ii <= size_filter-1; ii++)
	{
	  temp_score+=c_past[ii];
	}
      
     // parameter2=temp_score/5;
      
    }//if initial loop > 4
  
  for (ii=0; ii <= size_filter-1; ii++)
    {
        //  fprintf(stderr,"c_past[%d]: %f\n",ii,c_sort[ii]);
    }
  
  if (all_failed_flag==0 && initial_loop > size_filter-1)
    {
     
       parameter2=((c_sort[7]*.35)+(c_sort[6]*.20)+(c_sort[5]*.15)+(c_sort[4]*.1)+(c_sort[3]*.07)+(c_sort[2]*.05)+(c_sort[1]*.04)+(c_sort[0]*.04));
       previous_parameter2=parameter2; 
       c_past[size_filter-1]=parameter2;	
    }  
  else if (all_failed_flag==0 && initial_loop <= size_filter-1)
    {
      temp_score=0;
      for (ii=0; ii <= size_filter-1; ii++)
	{
	  temp_score+=c_past[ii];
	}
      parameter2=parameter2;
      previous_parameter2=parameter2; 
    }  
  
  else
    {
      parameter2=3.0;
      previous_parameter2=0.7;
    }
  
  //put best deblurred back into image
  

  c = parameter2;
  
  
  //  *best_atmos_parameter=c;
 
  if (c < 0.04)
    kernal_size=91;
  else
    kernal_size=41;


 /* Allocate Memory for PSF/OTF arrays */
  PSF_array      =(float *)malloc(sizeof(float)*kernal_size*kernal_size);
  OTF_array      =(float *)malloc(sizeof(float)*width*height);
  temp_OTF_array =(float *)malloc(sizeof(float)*width*height);



  PSF_array=Compute_PSF(PSF_array,c,kernal_size,kernal_middle);
  OTF_array=Compute_OTF(OTF_array,temp_OTF_array,PSF_array,kernal_size,kernal_middle,height,width);
  Wiener_Filter_Image(deblurred_image,fft_image,OTF_array,height,width,SNR_ACF);
  
  
  initial_loop++;
  if (initial_loop > size_filter)
    initial_loop=size_filter;
  
  
  cvNamedWindow("INPUT", CV_WINDOW_AUTOSIZE );
  cvShowImage("INPUT", input_img ); 
 

  
  cvReleaseImageHeader( &blurred_image );
  cvReleaseImage( &sqi_image ); 
  cvReleaseImage( &img_float );  
  cvReleaseImage( &crop_img0 );
  cvReleaseImage( &crop_img0 );
  cvReleaseImage( &crop_img1 );
  cvReleaseImage( &crop_img2 );
  cvReleaseImage( &crop_img3 );
  cvReleaseImage( &crop_img4 );
  cvReleaseImage( &crop_img5 );
  cvReleaseImage( &crop_img6 );
  cvReleaseImage( &crop_img7 );
  cvReleaseImage( &crop_img8 );
  cvReleaseImage( &crop_img9 );
  cvReleaseImage( &crop_img10 );
  cvReleaseImage( &crop_img11 );
  cvReleaseImage( &crop_img12 );
  cvReleaseImage( &crop_img13 );
  cvReleaseImage( &crop_img14 );
  
  fftw_destroy_plan( plan_r );
  fftw_free( sqi_fft_image );   
  fftw_free(ifft);
  //fftw_free(IFFT_ACF);
  fftw_free( fft_image );   
  free(average_array); 
  free(image_metric);
  free(c_atmos);  
  free(PSF_array);
  free(OTF_array);
  free(temp_OTF_array);
  free(background_sub_flag);
  fftw_cleanup();
  fprintf(stderr,"DONE DEBLUR: %f SNR: %f\r\n",parameter2,SNR_ACF);
  //return input_img;
 
  return deblurred_image;
  
}
    
