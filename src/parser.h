// parser.h
// Copyright 2009 Securics, Inc

#ifndef _parser_h_
#define _parser_h_
#include <iostream>
#include <fstream>
#include <vector>
#include <sys/stat.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
using namespace std;
typedef struct fileitem_t
{
    char* file;
    int label;
    int leyeX;
    int leyeY;
    int reyeX;
    int reyeY;
    int cropX;
    int cropY;
    int cropW;
    int cropH;
    fileitem_t* next;
} fileitem;

typedef struct filelist_t
{
    fileitem* begin;
    int num;
} filelist;

void tokenize(const string& str, vector<string>& tokens, const string& delimiters = " ");
filelist* parseFileList(const char* file_name);
void freeList(filelist* list);

#endif /* _parser_h_ */
