// File:    webcamgal.cpp
// Author:  Chris Eberle
// Created: Tue Oct 20 15:34:32 2009
// Copyright 2009 Securics, Inc
//
// A simple program to create a gallery using a webcam

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <ctype.h>
#include <libgen.h>
#include <stdarg.h>
#include <iostream>
#include <sys/stat.h>
#include <string.h>

#include "structs.h"
#include "utils.h"
#include "geo.h"
#include "eye_detector.h"
#include "facedetect.h"
#include "preprocessing.h"
#include "version.h"
#include "argparse.h"

#define SPACE 32
#define ESCAPE 27
#define ENTER 10

using namespace std;

// Typedefs
typedef struct params_t
{
    char* progName;
    char* saveDir;
    char* user_name;
    int user_id;
    int use_geonorm;
    int enable_histnorm;
    int use_python;
} prog_params;

// Prototypes
static void usage(const char* name, int val);
static void parseCommandLine(int argc, char **argv, prog_params *parm);

static const char* haarfile = "haarcascade_frontalface_alt2.xml";

#define SAFE_RELEASE(img) do { if((img) != NULL) { cvReleaseImage(&(img)); (img) = NULL; } } while(0);
#define SAFE_FREE(ptr) do { if ((ptr) != NULL) { free((ptr)); (ptr) = NULL; } } while(0);

// Print a message to stderr and exit
static void fatal(const char* msg, ...)
{
    va_list ap;
    va_start(ap, msg);
    vfprintf(stderr, msg, ap);
    va_end(ap);

    exit(1);
}

int processImage(IplImage* img, CvHaarClassifierCascade* cascade, CvMemStorage* storage, prog_params& params)
{
    static int capnum = 0;
    const int small_size = 150;
    int key = -1, good = 0;
    if(img == NULL)
        return -1;

    IplImage* colorImg = cvCloneImage(img);
    IplImage* grayImg = getGrayscale(img);

    CvRect r = detect_face(cascade, storage, grayImg);
    if(r.x >= 0 && r.y >=0 && r.width > 0 && r.height > 0)
    {
        CvPoint pt1 = cvPoint(r.x, r.y);
        CvPoint pt2 = cvPoint(r.x + r.width, r.y + r.height);
        cvRectangle(colorImg, pt1, pt2, cvScalar(0, 0, 255), 2, 8, 0);
        good = 1;
    }

    if(good == 0)
    {
        cvShowImage("Webcam", colorImg);
        SAFE_RELEASE(colorImg);
        SAFE_RELEASE(grayImg);
        return cvWaitKey(100) & 255;
    }

    IplImage* tmp = cropIpl(grayImg, r.x, r.y, r.width, r.height);
    SAFE_RELEASE(grayImg);
    grayImg = tmp;
    if(grayImg == NULL)
    {
        cvShowImage("Webcam", colorImg);
        SAFE_RELEASE(colorImg);
        return cvWaitKey(100) & 255;
    }

    int beforeW, beforeH;
    beforeW = grayImg->width;
    beforeH = grayImg->height;

    tmp = get_image(grayImg, small_size, params.use_python, 1);
    SAFE_RELEASE(grayImg);
    grayImg = tmp;
    if(grayImg == NULL)
    {
        cvShowImage("Webcam", colorImg);
        SAFE_RELEASE(colorImg);
        return cvWaitKey(100) & 255;
    }

    int lx = -1, ly = -1, rx = -1, ry = -1;
    int lx1 = -1, ly1 = -1, rx1 = -1, ry1 = -1;
    eye_detect(grayImg, &lx, &rx, &ly, &ry ,&lx1, &rx1, &ly1, &ry1);
    if(lx < 0 || ly < 0 || rx < 0 || ry < 0 ||
       lx >= grayImg->width || ly >= grayImg->height ||
       rx >= grayImg->width || ry >= grayImg->height)
    {
        cvShowImage("Webcam", colorImg);
        SAFE_RELEASE(grayImg);
        SAFE_RELEASE(colorImg);
        fprintf(stderr, "Eye detection failed.\n");
        return cvWaitKey(100) & 255;
    }

    double scaleX, scaleY;
    scaleX = (double)beforeW / (double)grayImg->width;
    scaleY = (double)beforeH / (double)grayImg->height;

    CvPoint center;

    int flx = (int)((double)lx * scaleX) + r.x;
    int fly = (int)((double)ly * scaleY) + r.y;
    int frx = (int)((double)rx * scaleX) + r.x;
    int fry = (int)((double)ry * scaleY) + r.y;


    /*
    center.x = flx; center.y = fly;
    cvCircle(colorImg, center, 10, cvScalar(0, 0, 255), 2, 8, 0);

    center.x = frx; center.y = fry;
    cvCircle(colorImg, center, 10, cvScalar(0, 0, 255), 2, 8, 0);
    */
    

    CvFont font;
    CvPoint LEFT_pt=cvPoint(flx,fly);
    CvPoint RIGHT_pt=cvPoint(frx,fry);
    cvInitFont(&font, CV_FONT_HERSHEY_COMPLEX,0.25,0.25,0,1,CV_AA);
    cvPutText(colorImg,"X",LEFT_pt,&font,CV_RGB(0xff,0x00,0x00));
    cvPutText(colorImg,"X",RIGHT_pt,&font,CV_RGB(0xff,0x00,0x00));

    if(!params.use_geonorm)
    {
        lx = flx;
        ly = fly;
        rx = frx;
        ry = fry;
    }

    cvShowImage("Webcam", colorImg);

    key = cvWaitKey(100) & 255;
    if(key == SPACE)
    {
        do
        {
            key = cvWaitKey(-1) & 255;
            if(key == ENTER)
            {
                char filename[4096];
                struct stat stFileInfo;

                do
                {
                    capnum++;
                    snprintf(filename, 4096, "%s/%s_%04d.png", params.saveDir, params.user_name, capnum);
                } while(stat(filename, &stFileInfo) == 0);

                if(params.use_geonorm)
                {
                    IplImage* fullGrayImg = getGrayscale(img);
                    IplImage* geo_normed = perform_geo_norm(fullGrayImg, flx, fly, frx, fry, params.enable_histnorm, 1);
                    SAFE_RELEASE(fullGrayImg);

                    if(geo_normed != NULL)
                    {
                        cvSaveImage(filename, geo_normed);
                        printf("%s %d\n", filename, params.user_id);
                        fprintf(stderr, "Saved %s\n", filename);
                        SAFE_RELEASE(geo_normed);
                    }
                    else
                    {
                        fprintf(stderr, "Geo-norm failed!\n");
                    }
                }
                else
                {
                    cvSaveImage(filename, img);
                    printf("%s %d %d %d %d %d\n", filename, params.user_id, lx, ly, rx, ry);
                    fprintf(stderr, "Saved %s\n", filename);
                }
            }
        } while(key != ENTER && key != ESCAPE);
        key = -1;
    }

    SAFE_RELEASE(grayImg);
    SAFE_RELEASE(colorImg);

    return key;
}

// Entry point for program
int main(int argc, char **argv)
{
    IplImage* img = NULL;
    int key = -1;

    // Parse the command line arguments
    prog_params params;
    parseCommandLine(argc, argv, &params);

    if(params.saveDir == NULL || params.user_id <= 0 || params.user_name == NULL)
        usage(params.progName, 1);

    {
        char cmd[1024];
        snprintf(cmd, 1024, "mkdir -p \"%s\"", params.saveDir);
        if(system(cmd) != 0)
            fprintf(stderr, "Command returned non-zero: %s\n", cmd);
    }

    CvHaarClassifierCascade* cascade = (CvHaarClassifierCascade*)cvLoad(haarfile, 0, 0, 0);
    if(cascade == NULL)
        fatal("Error: Unable to load haar classifier: `%s'\n", haarfile);
    CvMemStorage* storage = cvCreateMemStorage(0);

    CvCapture* capture = NULL;
    if((capture = cvCaptureFromCAM(0 /*whatever's there*/)) == NULL)
        fatal("Error: Unable to use webcam.\n");

    cvNamedWindow("Webcam", CV_WINDOW_AUTOSIZE);
    cvStartWindowThread();

    if(cvGrabFrame(capture))
        img = cvRetrieveFrame(capture);

    if(img != NULL)
    {
        fprintf(stderr, "Press SPACE to pause the camera. Once paused, press ENTER to save the entry.\n");
        fprintf(stderr, "To quit, press ESCAPE.\n");
    }

    while(img != NULL && key != ESCAPE)
    {
        key = processImage(img, cascade, storage, params);
        if(!cvGrabFrame(capture))
            break;
        img = cvRetrieveFrame(capture);
    }

    cvDestroyWindow("Webcam");
    cvReleaseCapture(&capture);

    SAFE_FREE(params.saveDir);
    SAFE_FREE(params.user_name);

    return 0;
}

// Print the program's usage
static void usage(const char* name, int val)
{
    fprintf(stderr,
            "Usage: %s [OPTIONS]\n"
            "  -C, --config=FILE        Load config options from a file.\n"
            "  -d, --dir=DIR            Image save directory (required).\n"
            "  -u, --id=ID              User ID of webcam subject (required).\n"
            "  -n, --name=NAME          User name of webcam subject (required).\n"
            "  -G, --geo-norm=BOOL      Enable geo-norm (default false).\n"
            "  -H, --hist-norm=BOOL     Enable hist-norm (default true).\n"
            "  -P, --python-resize=BOOL Let python do the resize (default false).\n"
            "  -i, --info               Show file versions and exit.\n"
            "  -h, --help               Show this helpful message.\n"
            , name);
    exit(val);
}

// Parse the command line arguments and load them into a struct  
static void parseCommandLine(int argc, char **argv, prog_params *parm)
{
    argv[0] = basename(argv[0]);
    parm->progName = argv[0];
    parm->saveDir = NULL;
    parm->user_id = 0;
    parm->user_name = NULL;
    parm->use_geonorm = 0;
    parm->enable_histnorm = 1;
    parm->use_python = 0;

    ArgParser ap(usage, parm->progName, false);

    // Simple switches
    ap.addOption("help", false, "h");
    ap.addOption("info", false, "i");

    // Regular options
    ap.addOption("config",        true, "C");
    ap.addOption("dir",           true, "d");
    ap.addOption("id",            true, "u");
    ap.addOption("name",          true, "n");

    // Boolean options
    ap.addOption("geo-norm",      true, "G", "yes");
    ap.addOption("hist-norm",     true, "H", "yes");
    ap.addOption("python-resize", true, "P", "yes");

    if(!ap.parseCommandLine(argc, argv, true))
        exit(1);

    if(ap.isSet("help"))
        usage(argv[0], 0);
    
    if(ap.isSet("info"))
    {
        printFileVersions(stdout);
        exit(0);
    }

    // Parse the config file if specified
    if(ap.isSet("config"))
    {
        // The last option means that the command line always gets
        // preference if there is a conflict.
        if(!ap.parseConfigFile(ap.getValue("config"), true))
            exit(1);
    }

    // Parse the options
    if(ap.isSet("dir")) parm->saveDir = strdup(ap.getValue("dir"));
    if(ap.isSet("id"))  parm->user_id = atoi(ap.getValue("id"));
    if(ap.isSet("name")) parm->user_name = strdup(ap.getValue("name"));
    if(ap.isSet("geo-norm")) parm->use_geonorm = ap.getBoolValue("geo-norm");
    if(ap.isSet("hist-norm")) parm->enable_histnorm = ap.getBoolValue("hist-norm");
    if(ap.isSet("python-resize")) parm->use_python = ap.getBoolValue("python-resize");
}
