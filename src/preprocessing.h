// preprocessing.h
// Copyright 2009 Securics, Inc

#ifndef PREPROCESSING_H
#define PREPROCESSING_H

#include <stdio.h>
#include "cvheaders.h"

IplImage* get_image(IplImage *img, int max_edge, int call_python, int num);
IplImage* image_preparation(IplImage *img);

#endif
