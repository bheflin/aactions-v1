#!/usr/bin/python
# Author: Chris Eberle
# Called by enroll and recognize when using the -x switch

import sys, os
import Image

def get_image(img_fname, max_edge):
    # -- open image
    img = Image.open(img_fname)
    iw, ih = img.size

    # -- resize so that the biggest edge is max_edge (keep aspect ratio)
    if iw > ih:
        new_iw = max_edge
        new_ih = int(round(1.* max_edge * ih/iw))
    else:
        new_iw = int(round(1.* max_edge * iw/ih))
        new_ih = max_edge
    img = img.resize((new_iw, new_ih), Image.BICUBIC)
    return img

def resize(filename, newfilename):
    img = get_image(filename, 150)
    img.save(newfilename);

try:
    filein = sys.argv[1]
    fileout = sys.argv[2]
except IndexError:
    progname = sys.argv[0]
    print "Usage: %s filein fileout" % progname
    sys.exit(1)

resize(filein, fileout)
