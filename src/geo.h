// geo.h
// Copyright 2009 Securics, Inc

#ifndef GEO_H
#define GEO_H

#include "cvheaders.h"
#include <stdio.h>
#include <iostream>
#include <string.h>
#include "structs.h"

IplImage* perform_geo_norm(IplImage* src, int eye1X, int eye1Y, int eye2X, int eye2Y, int hist_norm, int thread_num);
int perform_geo_norm_gallery(struct gallery_entry* gallery, int size, int hist_norm, int show_progress, int geo_norm_after);

#endif
