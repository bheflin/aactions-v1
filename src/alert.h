// alert.h
// Copyright 2009 Securics, Inc

#ifndef _alert_h_
#define _alert_h_

#include "cvheaders.h"
#include <map>
#include <iostream>
#include <vector>
#include "structs.h"

void setup_alert_gallery(std::map<int,std::string> *cat_files, std::map<int,std::string> *cat_names);
int show_alert(IplImage* probe, std::vector<id_score> &topn);
void cleanup_alert_gallery();
void hide_alert();

#endif /* _alert_h_ */
