// File:   capture.cpp
// Author: Chris Eberle
// Handle all kinds of capture for the v1 program
// Copyright 2009 Securics, Inc

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "capture.h"


// Macros
#define SAFE_FREE(ptr) do{if((ptr)!=NULL){free((ptr));(ptr)=NULL;}}while(0)
#define SAFE_RELEASE(img) do{if((img)!=NULL){cvReleaseImage(&(img));(img)=NULL;}} while(0);

// Class constructor
v1_capture* v1_capture_create(v1_capture_type ftype, const v1_capture_param param)
{
    // Allocate memory for the C class
    v1_capture* self = (v1_capture*)malloc(sizeof(v1_capture));
    if(self == NULL)
        return NULL;

    self->curImage   = NULL;
    self->curName    = NULL;
    self->cacheName  = NULL;
    self->saveName   = NULL;
    self->framenum   = 0;
    self->frametotal = 0;
    self->type       = ftype;
    self->eof        = false;
    self->tfList     = NULL;
    self->listItem   = NULL;
    self->capture    = NULL;
    self->filename   = NULL;
    self->gige_cam   = NULL;
    self->vgtlabel   = -1;
    self->shouldFree = false;

    switch(ftype)
    {
    case FILE_LIST:
        if((self->tfList = parseFileList(param.filename)) == NULL)
        {
            SAFE_FREE(self);
            return NULL;
        }

        self->filename = strdup(param.filename);
        self->frametotal = (unsigned int)self->tfList->num;

        break;

    case VIDEO:
        if((self->capture = cvCaptureFromFile(param.filename)) == NULL)
        {
            SAFE_FREE(self);
            return NULL;
        }

        {
            int numframes = (int)cvGetCaptureProperty(self->capture, CV_CAP_PROP_FRAME_COUNT);
            if(numframes <= 0)
                self->frametotal = 0;
            else
                self->frametotal = (unsigned int)numframes;
        }

        self->filename = strdup(param.filename);

        break;

    case WEBCAM:
        if((self->capture = cvCaptureFromCAM(param.cam_index)) == NULL)
        {
            SAFE_FREE(self);
            return NULL;
        }

        self->filename = strdup("(webcam)");

        break;

    case GIGE:
        if((self->gige_cam = findCamera()) == NULL)
        {
            fprintf(stderr, "ERROR: No GigE cameras found on the network!\n");
            SAFE_FREE(self);
            return NULL;
        }

        if(initializeCamera(self->gige_cam) != 0)
        {
            fprintf(stderr, "ERROR: Unable to initialize GigE camera!\n");
            SAFE_FREE(self->gige_cam);
            SAFE_FREE(self);
            return NULL;
        }

        resetTimestamp(self->gige_cam);
        writeRegister(self->gige_cam, 0xD33C, 0x800000, NULL);

        if(beginStreaming(self->gige_cam) != 0)
        {
            fprintf(stderr, "ERROR: Unable to begin stream from GigE camera!\n");
            uninitializeCamera(self->gige_cam);
            SAFE_FREE(self->gige_cam);
            SAFE_FREE(self);
            return NULL;
        }

        self->filename = strdup("(gige)");

        break;

    default:
        fprintf(stderr, "Unknown capture type.\n");
        SAFE_FREE(self);

        return NULL;

    }

    if(v1_capture_next(self) == false)
    {
        fprintf(stderr, "Unable to fetch first frame from capture source.\n");
        v1_capture_destroy(self);
        return NULL;
    }

    return self;
}

// Class destructor
void v1_capture_destroy(v1_capture* self)
{
    if(self == NULL)
        return;

    if(self->tfList != NULL)
    {
        freeList(self->tfList);
        self->tfList = NULL;
    }

    if(self->capture != NULL)
    {
        cvReleaseCapture(&(self->capture));
        self->capture = NULL;
    }

    if(self->gige_cam != NULL)
    {
        uninitializeCamera(self->gige_cam);
        SAFE_FREE(self->gige_cam);
    }

    SAFE_FREE(self->filename);
    SAFE_FREE(self->curName);
    SAFE_FREE(self->cacheName);
    SAFE_FREE(self->saveName);

    // cvCapture sources aren't freed
    if(self->type == FILE_LIST)
        SAFE_RELEASE(self->curImage);

    SAFE_FREE(self);
}

static int getPostfixPosition(const char* str)
{
    // Find the last instance of "."
    unsigned int len = strlen(str);
    unsigned int cpos = len - 1;
    int pos = -1;

    while(cpos > 0 && str[cpos] != '/' && str[cpos] != '\\')
    {
        if(str[cpos] == '.')
        {
            pos = (int)cpos;
            break;
        }

        cpos--;
    }

    if(pos == 0 || pos == (int)len)
        pos = -1;

    return pos;
}

static bool v1_capture_next_file(v1_capture* self)
{
    do
    {
        SAFE_FREE(self->curName);
        SAFE_FREE(self->cacheName);
        SAFE_FREE(self->saveName);
        SAFE_RELEASE(self->curImage);

        if(self->listItem == NULL)
            self->listItem = self->tfList->begin;
        else
            self->listItem = self->listItem->next;

        if(self->listItem == NULL)
        {
            self->eof = true;
            return false;
        }
        else
        {
            self->curName = strdup(self->listItem->file);
            self->cacheName = strdup(self->listItem->file);

            int pos = getPostfixPosition(self->listItem->file);
            if(pos > 0)
            {
                self->saveName = (char*)malloc((pos + 1) * sizeof(char));
                if(self->saveName == NULL)
                    self->saveName = strdup(self->listItem->file);
                else
                {
                    strncpy(self->saveName, self->listItem->file, pos);
                    self->saveName[pos] = '\0';
                }
            }
            else
            {
                self->saveName = strdup(self->listItem->file);
            }

            if((self->curImage = cvLoadImage(self->curName, 1)) == NULL)
                printf("WARNING: Unable to open file for reading: `%s'\n\n", self->curName);
        }
    } while(self->curImage == NULL);

    return true;
}

static bool v1_capture_next_video(v1_capture* self)
{
    SAFE_FREE(self->curName);
    SAFE_FREE(self->cacheName);
    SAFE_FREE(self->saveName);

    if(self->shouldFree)
    {
        SAFE_RELEASE(self->curImage);
        self->shouldFree = false;
    }

    if((self->curImage = cvQueryFrame(self->capture)) == NULL)
    {
        self->eof = true;
        return false;
    }

    // Get a valid filename
    char* fname = NULL;
    int pos = -1;

    if(self->type == WEBCAM)
        fname = strdup("webcam");
    else
    {
        fname = strdup(self->filename);
        pos = getPostfixPosition(fname);
    }

    if(pos > 0)
    {
        char* nosuffix = (char*)malloc((pos + 1) * sizeof(char));
        if(nosuffix == NULL)
        {
            if(asprintf(&(self->saveName), "%s_frame_%u", fname, (self->framenum + 1)) <= 0)
                self->saveName = NULL;
        }
        else
        {
            strncpy(nosuffix, fname, pos);
            nosuffix[pos] = '\0';
            if(asprintf(&(self->saveName), "%s_frame_%u", nosuffix, (self->framenum + 1)) <= 0)
                self->saveName = NULL;
            free(nosuffix);
        }
    }
    else
    {
        if(asprintf(&(self->saveName), "%s_frame_%u", fname, (self->framenum + 1)) <= 0)
            self->saveName = NULL;
    }

    if(fname != NULL)
        free(fname);

    if(asprintf(&(self->curName), "%s (frame %u)", self->filename, (self->framenum + 1)) <= 0)
        self->curName = NULL;

    // For now we won't even think about caching video frames
    self->cacheName = NULL;

    return true;
}

static bool v1_capture_next_gige(v1_capture* self)
{
    SAFE_FREE(self->curName);
    SAFE_FREE(self->cacheName);
    SAFE_FREE(self->saveName);

    if(self->curImage != NULL)
    {
        cvReleaseImageHeader(&(self->curImage));
        self->curImage = NULL;
    }

    g_image* img = getFrameFromCamera(self->gige_cam, 60 /* timeout in seconds */);
    if(img == NULL)
    {
        fprintf(stderr, "NULL video frame returned from GigE camera.\n");
        self->eof = true;
        return false;
    }

    /*
    printf("img->pix_format = %d\n", img->pix_format);
    printf("img->width = %d\n", img->width);
    printf("img->height = %d\n", img->height);
    printf("img->bpp = %d\n", img->bpp);
    printf("img->channels = %d\n", img->channels);
    printf("img->timestamp = %d\n", img->timestamp);
    printf("img->payload_type = %d\n", img->payload_type);
    printf("img->block_id = %d\n", img->block_id);
    printf("img->frame_size = %d\n", img->frame_size);
    printf("img->frame = %p\n", img->frame);
    */

    unsigned int depth = 0;
    if(img->bpp == 8)
        depth = IPL_DEPTH_8U;
    else if(img->bpp >= 10)
        depth = IPL_DEPTH_16U;
    else
    {
        fprintf(stderr, "Unknown depth returned from GigE camera.\n");
        self->eof = true;
        return false;
    }

    self->curImage = cvCreateImageHeader(cvSize(img->width, img->height), depth, img->channels);
    if(self->curImage == NULL)
    {
        fprintf(stderr, "Call to cvCreateImageHeader failed!\n");
        self->eof = true;
        return false;
    }

    self->curImage->imageData = (char*)img->frame;
    //self->curImage->widthStep = img->width;

    if(asprintf(&(self->saveName), "gige_frame_%u", (self->framenum + 1)) <= 0)
        self->saveName = NULL;

    if(asprintf(&(self->curName), "GigE camera (frame %u)", (self->framenum + 1)) <= 0)
        self->curName = NULL;

    // For now we won't even think about caching gige video frames
    self->cacheName = NULL;

    return true;
}

bool v1_capture_next(v1_capture* self)
{
    if(self == NULL)
        return false;
    if(self->eof)
        return false;

    switch(self->type)
    {
    case FILE_LIST:
        if(!v1_capture_next_file(self))
            return false;
        break;
    case VIDEO:
    case WEBCAM:
        if(!v1_capture_next_video(self))
            return false;
        break;
    case GIGE:
        if(!v1_capture_next_gige(self))
            return false;
    }

    self->framenum++;
    return true;
}

IplImage* v1_capture_get(v1_capture* self)
{
    if(self == NULL)
        return NULL;
    if(self->eof || self->curImage == NULL)
        return NULL;
    return cvCloneImage(self->curImage);
}

unsigned int v1_capture_pos(v1_capture* self)
{
    if(self == NULL)
        return (unsigned int)-1;
    return self->framenum;
}

float v1_capture_progress(v1_capture* self)
{
    if(self == NULL)
        return -1.;
    if(self->frametotal == 0)
        return -1.;
    return 100. * ((float)self->framenum / (float)self->frametotal);
}

bool v1_capture_getdata(v1_capture* self, v1_capture_gtdata* data)
{
    if(self == NULL || data == NULL)
        return false;
    if(self->eof)
        return false;

    switch(self->type)
    {
    case FILE_LIST:
        data->label = self->listItem->label;
        data->lx = self->listItem->leyeX;
        data->ly = self->listItem->leyeY;
        data->rx = self->listItem->reyeX;
        data->ry = self->listItem->reyeY;
        data->cropX = self->listItem->cropX;
        data->cropY = self->listItem->cropY;
        data->cropW = self->listItem->cropW;
        data->cropH = self->listItem->cropH;
        break;
    case VIDEO:
    case WEBCAM:
    case GIGE:
        if(self->vgtlabel == -1)
            return false;
        data->label = self->vgtlabel;
        data->lx = -1;
        data->ly = -1;
        data->rx = -1;
        data->ry = -1;
        data->cropX = -1;
        data->cropY = -1;
        data->cropW = -1;
        data->cropH = -1;
    }

    return true;
}

bool v1_capture_setdata(v1_capture* self, v1_capture_gtdata* data)
{
    if(self == NULL || data == NULL)
        return false;
    if(self->eof)
        return false;

    switch(self->type)
    {
    case FILE_LIST:
        self->listItem->label = data->label;
        self->listItem->leyeX = data->lx;
        self->listItem->leyeY = data->ly;
        self->listItem->reyeX = data->rx;
        self->listItem->reyeY = data->ry;
        self->listItem->cropX = data->cropX;
        self->listItem->cropY = data->cropY;
        self->listItem->cropW = data->cropW;
        self->listItem->cropH = data->cropH;
        break;
    case VIDEO:
    case WEBCAM:
    case GIGE:
        self->vgtlabel = data->label;
    }

    return true;
}

bool v1_capture_setimg(v1_capture* self, IplImage* img)
{
    if(self == NULL || img == NULL)
        return false;
    if(self->eof)
        return false;

    switch(self->type)
    {
    case FILE_LIST:
    case GIGE:
        SAFE_RELEASE(self->curImage);
        break;
    case VIDEO:
    case WEBCAM:
        if(self->shouldFree)
            SAFE_RELEASE(self->curImage);
        break;
    }

    if((self->curImage = cvCloneImage(img)) == NULL)
        return false;
    self->shouldFree = true;
    return true;
}

void v1_capture_setvlabel(v1_capture* self, int vlabel)
{
    if(self == NULL)
        return;
    self->vgtlabel = vlabel;
}

bool v1_capture_eof(v1_capture* self)
{
    if(self == NULL)
        return true;
    return self->eof;
}

const char* v1_capture_name(v1_capture* self)
{
    if(self == NULL)
        return NULL;
    return self->curName;
}

const char* v1_capture_cache(v1_capture* self)
{
    if(self == NULL)
        return NULL;
    return self->cacheName;
}

const char* v1_capture_savename(v1_capture* self)
{
    if(self == NULL)
        return NULL;
    return self->saveName;
}
