// structs.h
// Copyright 2009 Securics, Inc

#ifndef STRUCTS_H
#define STRUCTS_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef USE_DOUBLE
  typedef double v1_float;
  #define IPL_DEPTH_V1 IPL_DEPTH_64F
  #define CV_V1 CV_64FC1
  #define v1_pow pow
  #define v1_sqrt sqrt
  #define v1_round round
  #define v1_floor floor
  #define v1_ceil ceil
  #define v1_abs fabs
#else
  typedef float v1_float;
  #define IPL_DEPTH_V1 IPL_DEPTH_32F
  #define CV_V1 CV_32FC1
  #define v1_pow powf
  #define v1_sqrt sqrtf
  #define v1_round roundf
  #define v1_floor floorf
  #define v1_ceil ceilf
  #define v1_abs fabsf
#endif

#define GABOR_FEATURES 96
#define GABOR_SIZE 43
#define WINDOW_SIZE 30

/// A structure to hold information about a single gallery entry
/// (i.e. an image).
typedef struct gallery_entry
{
    char* path;             //< The path to the gallery file
    int id;                 //< The class id of the gallery entry
    int feature_count;      //< The number of PCA-projected features
    int atmos_deblur;       //< 
    int motion_deblur;      //< 
    v1_float* features;     //< The V1 feature data
    v1_float* pca_features; //< PCA-projected feature data
    int leyeX;              //< The x coordinate of the left eye's position
    int leyeY;              //< The y coordinate of the left eye's position
    int reyeX;              //< The x coordinate of the right eye's position
    int reyeY;              //< The y coordinate of the right eye's position
    int perturb_value;      //< The number of perturbation, used for geo norm
} gallery_entry;
//Gallery type for validateion
typedef struct validate_gallery
{
    char* path;             //< The path to the first image
    char* second_path;      //< The path to the second image. 
    int id;                 //< 1 if the images match 2 if not
    int feature_count;      //< The number of PCA-projected features
    v1_float* features;     //< The V1 feature data
    v1_float* pca_features; //< PCA-projected feature data
    int leyeX_file1;              //< The x coordinate of the left eye's position
    int leyeY_file1;              //< The y coordinate of the left eye's position
    int reyeX_file1;              //< The x coordinate of the right eye's position
    int reyeY_file1;              //< The y coordinate of the right eye's position
    int leyeX_file2;              //< The x coordinate of the left eye's position
    int leyeY_file2;              //< The y coordinate of the left eye's position
    int reyeX_file2;              //< The x coordinate of the right eye's position
    int reyeY_file2;              //< The y coordinate of the right eye's position

} validate_gallery_entry;

/// Which convolve mode is used when calling convolve2d or convolve3d
typedef enum convolvemode_t
{
    MODE_VALID = 0,
    MODE_SAME  = 1
} convolve_mode;

/// Used by convolve3d to set the bounds of a MODE_VALID convolution
typedef struct conv_bounds_t
{
    int startX;
    int startY;
    int startZ;
    int endX;
    int endY;
    int endZ;
} convolve_bounds;

/// Mostly used for sorting purposes, associates an ID with a score.
typedef struct id_score
{
    int algo;            //< to identify which algorithm came up with the answer
    int id;              //< The class ID
    float score;         //< The confidence score
    float failure_score; //< The failure marginal distance
} id_score;

/// Data to pass on to the worker threads
struct tparam
{
    struct gallery_entry *gallery; //< The full gallery
    void **filters;      //< The gabor filters
    int fast_gabor;      //< Which gabor method to use
    int use_cache;       //< Whether or not to use cache files
    int use_python;      //< Whether or not to call python's resize
    int tnum;            //< A unique thread number
    int geo_norm_after;  //< Whether to geo-norm before or after atmospheric
    int geo_norm;        //< Whether or not to geo-norm
    int hist_norm;       //< When geo-normalizing, whether or not to use histogram normalization
    int sqi_norm;        //< Whether or not to use SQI normalization
};

#endif
