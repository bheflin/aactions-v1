// geo.cpp
// Copyright 2009 Securics, Inc
// Performs the geo normalization on the images for either enrollment or recognition

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "asciibar.h"
#include "geo.h"
#include <unistd.h>

using namespace std;

/**
 * Saves the image as a pgm so CSU can be called externally to geo-normalize
 * @param IplImage* img An ipl image to save off in binary pgm format.
 * @param char* save The path to where the pgm should be saved. 
 */
void convert_to_pgm(IplImage* img, char* save)
{
    if(img->nChannels != 1)
    {
        printf("ERROR Input image is three channels");
        return;
    }
    else
    {
        cvSaveImage(save, img);
    }
}

/**
 * Loads an image and saves the geo_norm image to path_geo.pgm
 * @param char* path The path of the image to load, and the save will be path_geo.pgm
 */
void convert_to_pgm_file(char* path)
{
    char name[128];
    IplImage* img = cvLoadImage(path, 0);
    string tmp = path;
    string::size_type pos = tmp.rfind(".");
    string first = tmp.substr(0, pos);
    sprintf(name, "%s_geo.pgm", first.c_str());
    cvSaveImage(name, img);
    cvReleaseImage(&img);
}

/**
 * Loads an image and saves the geo_norm image to path_geo.pgm
 * @param char* path The path of the image to load, and the save will be path_geo.pgm
 */
void convert_to_pgm_file_name(char* path, const char* name)
{
    IplImage* img = cvLoadImage(path, 0);
    cvSaveImage(name, img);
    cvReleaseImage(&img);
}

/**
 * perform geo normalization on a single image
 * @param IplImage* src The loaded image to perform geo normalization on.
 * @param int eye1X The x coordinate of the left eye
 * @param int eye1Y The y coordinate of the left eye
 * @param int eye2X The x coordinate of the right eye
 * @param int eye2Y The y coordinate of the right eye
 * @param int hist_norm Should histogram normalization be performed, 1 yes 0 no. 
 * @param int thread_num which thread to make it thread safe. 
 */
IplImage* perform_geo_norm(IplImage* src, int eye1X, int eye1Y, int eye2X, int eye2Y, int hist_norm, int thread_num)
{
    IplImage* dst;
    char command[1024];
    char buffer[32];
    char file_name[64];
    char pgm_name[64];
    string hold;

    string::size_type pos;
    int pid = getpid();
    //never threaded just needs to be process safe for now
    /*    if(tmpnam_r(buffer) == NULL)
    {
        fprintf(stderr, "Unable to generate a unique name.\n");
        return NULL;
        }*/
    sprintf(buffer, "/tmp/%d_%d", pid, thread_num);
    sprintf(file_name,"%s.file", buffer);
    sprintf(pgm_name, "%s.pgm", buffer);
    //    printf("%s file %s image %s\n", buffer, file_name, pgm_name);
    hold = buffer;
    pos = hold.rfind("/");
    FILE* f = fopen(file_name, "w");
    if(!f)
    {
        printf("FAILED TO OPEN FILE FOR GEO_NORM, GEO_NORM IS NOT BEING PERFORMED\n");
        dst = cvCloneImage(src);
        return dst;
    }
    fprintf(f, "%s %d %d %d %d\n", hold.substr(pos+1, hold.size()-1).c_str(), eye1X, eye1Y, eye2X, eye2Y);
    fclose(f);
    convert_to_pgm(src, pgm_name);
    snprintf(command, 1024, "../csuFaceIdEval.5.0/bin/csuPreprocessNormalize -pgm /tmp -mask NO ");
    if(hist_norm)
        strcat(command, "-hist POST ");
    else
        strcat(command, "-hist NONE ");
    sprintf(command, "%s %s /tmp > /dev/null", command, file_name);
    //    strcat(command, "%s . > /dev/null", file_name);
    //    printf("Command: %s\n", command);
    if(system(command) != 0)
        fprintf(stderr, "WARNING: command returned non-zero real geo: %s\n", command);
    dst = cvLoadImage(pgm_name, 0);
    sprintf(command, "rm %s.file %s.pgm", buffer, buffer);
    if(system(command) != 0)
        fprintf(stderr, "WARNING: command returned non-zero: %s\n", command);
    return dst;
}

struct entry_data
{
    char path[128];
    char file_name[128];
};

/**
 * perform geonormalization on many images at once, used for enrollment
 * @param struct gallery_entry* Gallery is the gallery that is used for enrollment
 * @param int size The size of the gallery
 * @param int hist_nomr Should histogram normalization be performed 1 yes 0 no.
 * @param int show_progress 1 show progress bar 0 hide progress bar
 * @param int geo_norm_after If the image is to be enrolled with atmoshperic deblurring, should geo_norm happen before or after geo_norm.
 */
int perform_geo_norm_gallery(struct gallery_entry* gallery, int size, int hist_norm, int show_progress, int geo_norm_after)
{
    char command[1024];
    char tmp[1024];
    char current_path[512];
    int i, j;
    string hold;
    string name;
    string convert_name;
    string path;
    string new_path;
    string::size_type pos;
    size_t npos = -1;
    FILE* f;
    //malloc enough for the worse case each image is in its own directory
    struct entry_data* entries = (entry_data*)malloc(size *sizeof(entry_data));
    int num_different = 0;

    printf("Prepare geo-norm: "); fflush(stdout);

#ifndef HIDE_PROGRESS
    ascii_bar_params params;
    if(show_progress)
    {
        init_ascii_bar(&params, 0.0f, (float)size, 1.0f);
        params.len = 50;
        show_ascii_bar(&params);
    }
#endif

    //First case sets up for potential repeats
    if(gallery[0].leyeX != -1)
    {
        //        convert_to_pgm_file(gallery[0].path);
        sprintf(entries[0].file_name, "geo_file_%d",num_different);
        f = fopen(entries[0].file_name, "w");
        if(!f)
        {
            printf("FAILED TO OPEN FILE FOR GEO_NORM, GEO_NORM IS NOT BEING PERFORMED\n");
            return -1;
        }

        hold = gallery[0].path;
        pos = hold.rfind("/");
        if(pos == npos)
        {
            pos = hold.rfind(".");
            name = hold.substr(0, pos);
            name.append("_geo");
            char temp[64];
            sprintf(temp,"_%d",gallery[0].perturb_value); 
            name.append(temp);
            path = ".";
        }
        else
        {
            path = hold.substr(0, pos+1);
            name = hold.substr(pos+1, hold.size()-1);
            pos = name.rfind(".");
            name = name.substr(0, pos);
            name.append("_geo");
            char temp[64];
            sprintf(temp,"_%d",gallery[0].perturb_value); 
            name.append(temp);
           
        }

        convert_name = path;
        convert_name.append(name);
        convert_name.append(".pgm");
        convert_to_pgm_file_name(gallery[0].path, convert_name.c_str());
        sprintf(entries[0].path, "%s", path.c_str());
        sprintf(current_path, "%s", path.c_str());
        fprintf(f, "%s %d %d %d %d\n", name.c_str(), gallery[0].leyeX, gallery[0].leyeY, gallery[0].reyeX, gallery[0].reyeY);
        name.append(".pgm");
        new_path.clear();
        new_path = path;
        new_path.append(name.c_str());
        free(gallery[0].path);
        gallery[0].path = strdup(new_path.c_str());

#ifndef HIDE_PROGRESS
        if(show_progress)
            step_ascii_bar(&params);
#endif

    }
    else
    {
        printf("EYE COORDS NOT GIVEN FOR ALL ENTRIES EXITING RERUN WITHOUT GEONORM\n");
        free(entries);
        return -1;
    }
    int index = 0;
    int no_add;
    for(i = 1; i < size; i++)
    {
        no_add = 0;
        if(geo_norm_after == 1 && gallery[i].atmos_deblur == 1)
        {
            continue;
        }
        else if(gallery[i].atmos_deblur ==1)
        {
            no_add = 1;
        }
        if(gallery[i].leyeX != -1)
        {
            //            convert_to_pgm_file(gallery[i].path);
            hold = gallery[i].path;
            pos = hold.rfind("/");
            if(pos == npos)
            {
                pos = hold.rfind(".");
                name = hold.substr(0, pos);
                name.append("_geo");
                char temp[64];
                sprintf(temp,"_%d",gallery[i].perturb_value); 
                name.append(temp);
                path = ".";
                }
                else
                {
                    path = hold.substr(0, pos+1);
                    name = hold.substr(pos+1, hold.size()-1);
                    pos = name.rfind(".");
                    name = name.substr(0, pos);
                    name.append("_geo");
                    char temp[64];
                    sprintf(temp,"_%d",gallery[i].perturb_value); 
                    name.append(temp);
                    
                }
            convert_name = path;
            convert_name.append(name);
            
            convert_name.append(".pgm");
            convert_to_pgm_file_name(gallery[i].path, convert_name.c_str());
        
            if(strcmp(path.c_str(), current_path) != 0)
            {
                sprintf(current_path, "%s", path.c_str());
                fclose(f);
                int found = 0;
                for(j = 0; j < num_different; j++)
                {
                    if(strcmp(entries[j].path, path.c_str()) == 0)
                    {
                        found = 1;
                        index = j;
                        f = fopen(entries[j].file_name, "a");
                        if(!f)
                        {
                            printf("FAILED TO OPEN FILE FOR GEO_NORM, GEO_NORM IS NOT BEING PERFORMED\n");
                            return -1;
                        }
                        
                        break;
                    }
                }
                if(found ==0)
                {
                    num_different++;
                    index = num_different;
                    sprintf(entries[num_different].file_name, "geo_file_%d", num_different);
                    f = fopen(entries[num_different].file_name, "w");
                    if(!f)
                    {
                        printf("FAILED TO OPEN FILE FOR GEO_NORM, GEO_NORM IS NOT BEING PERFORMED\n");
                        return -1;
                    }
                    
                    sprintf(entries[num_different].path, "%s", path.c_str());
                }
            }
            if(no_add == 0)
            {
                fprintf(f, "%s %d %d %d %d\n", name.c_str(), gallery[i].leyeX, gallery[i].leyeY, gallery[i].reyeX, gallery[i].reyeY);
            }
            name.append(".pgm");
            new_path.clear();
            new_path = path;
            new_path.append(name.c_str());
            free(gallery[i].path);
            gallery[i].path = strdup(new_path.c_str());
            
#ifndef HIDE_PROGRESS
            if(show_progress)
                step_ascii_bar(&params);
#endif
        }
        else
        {
            fclose(f);
            printf("EYE COORDS NOT GIVEN FOR ALL ENTRIES EXITING RERUN WITHOUT GEONORM\n");
            free(entries);
            return -1;
        }
        
    }
    fclose(f);
    
#ifndef HIDE_PROGRESS
    if(show_progress)
        hide_ascii_bar(&params);
#endif
    
    printf("done.\n");
    printf("Geo-normalize: "); fflush(stdout);
    
    for(i = 0; i <= num_different; i++)
    {
        snprintf(tmp, 1024, "csuFaceIdEval.5.0/bin/csuPreprocessNormalize -pgm %s -mask NO ", entries[i].path);
        if(hist_norm)
            strcat(tmp, "-hist POST");
        else
            strcat(tmp, "-hist NONE");
        snprintf(command, 1024, "%s %s %s > /dev/null", tmp, entries[i].file_name, entries[i].path);
        if(system(command) != 0)
            fprintf(stderr, "WARNING: command returned non-zero: %s\n", command);
        snprintf(command, 1024, "rm geo_file_%d", i);
        if(system(command) != 0)
            fprintf(stderr, "WARNING: command returned non-zero: %s\n", command);
    }

    printf("done.\n");

    free(entries);
    return 0;
}



