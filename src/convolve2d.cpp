// convolve2d.cpp
// Author: Chris Eberle
// Copyright 2009 Securics, Inc
//
// Basic convolution

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <math.h>
#include <stdio.h>
#include <limits.h>
#include "convolve2d.h"
#include "iplstr.h"

// Macros
#define IMAX(a, b) ((a) > (b) ? (a) : (b))
#define GET_ELEM(mat, x, y) (*(((v1_float*)(mat->imageData + mat->widthStep * y)) + x))

// Prototypes
static int convolve2d_same(IplImage *src, IplImage *dst, IplImage *kernel);
static int convolve2d_valid(IplImage *src, IplImage *dst, IplImage *kernel);

// Public convolve function
int convolve2d(IplImage *src, IplImage *dst, IplImage *kernel, convolve_mode mode)
{
    int ret = -99;

    // Sanity check
    if(src == NULL || dst == NULL || kernel == NULL)
        return -1;

    // Another sanity check
    if(src->width != dst->width || src->height != dst->height)
        return -2;

    // Check the dimensions
    if((kernel->width * kernel->height) > (src->width * src->height))
        return -3;

    IplImage *real_source = src;
    IplImage *real_kernel = kernel;
    IplImage *real_dst    = dst;

    if(src->depth != IPL_DEPTH_V1)
    {
        real_source = cvCreateImage(cvGetSize(src), IPL_DEPTH_V1, 1);
        cvConvert(src, real_source);
    }

    if(kernel->depth != IPL_DEPTH_V1)
    {
        real_kernel = cvCreateImage(cvGetSize(kernel), IPL_DEPTH_V1, 1);
        cvConvert(kernel, real_kernel);
    }

    if(dst->depth != IPL_DEPTH_V1)
        real_dst = cvCreateImage(cvGetSize(dst), IPL_DEPTH_V1, 1);

    // Choose the convolve method based on the mode
    switch(mode)
    {
    case MODE_VALID:
        ret = convolve2d_valid(real_source, real_dst, real_kernel);
        break;
    case MODE_SAME:
        ret = convolve2d_same(real_source, real_dst, real_kernel);
        break;
    default:
        ret = -99;
    }

    if(real_dst != dst)
    {
        if(ret != -99)
            cvConvert(real_dst, dst);

        cvReleaseImage(&real_dst);
    }

    if(real_source != src)
        cvReleaseImage(&real_source);

    if(real_kernel != kernel)
        cvReleaseImage(&real_kernel);

    // Shouldn't happen
    return ret;
}

// Optimized version, verified to be correct with some very limited
// test data. Much faster than the vanilla version.
//
// A convolve method which keeps the dimensions of the incoming data
// Based off of octave code in [octave]/src/DLD-FUNCTIONS/conv2.cc.
// NOTE: Assumes all IplImage types are of depth IPL_DEPTH_V1 and one
// channel. This is taken care of automatically by the public convolve
// function defined above.
static int convolve2d_same(IplImage *src, IplImage *dst, IplImage *kernel)
{
    v1_float sum;
    int      edgM   = (kernel->height - 1) / 2;
    int      edgN   = (kernel->width - 1) / 2;

    v1_float *src_p = NULL;
    v1_float *krn_p = NULL;
    v1_float *dst_p = NULL;

    for(int oi = 0; oi < dst->height; oi++)
    {
        dst_p = (v1_float*)(dst->imageData + dst->widthStep * oi);

        for(int oj = 0; oj < dst->width; oj++)
        {
            sum = 0;
            int ai = IMAX(0, oi - edgM);
            int bi = kernel->height - 1 - IMAX(0, edgM - oi);

            for(; bi >= 0 && ai < src->height; bi--, ai++)
            {
                int aj = IMAX(0, oj - edgN);
                int bj = kernel->width - 1 - IMAX(0, edgN - oj);

                src_p = ((v1_float*)(src->imageData + src->widthStep * ai)) + aj;
                krn_p = ((v1_float*)(kernel->imageData + kernel->widthStep * bi)) + bj;

                for(; bj >= 0 && aj < src->width; bj--, aj++, krn_p--, src_p++)
                    sum += (*src_p) * (*krn_p);
            }

            *dst_p++ = sum;
        }
    }

    return 0;
}

// A convolve method which trims the edges of a convolve_same
// leaving only the values that were not interpolated.
//
// Going to hack our way through this by setting the ROI instead of
// actually trimming the image itself done. This will allow us to
// statically allocate the memory up-front.
static int convolve2d_valid(IplImage *src, IplImage *dst, IplImage *kernel)
{
    int ret = convolve2d_same(src, dst, kernel);

    if(ret < 0)
        return ret;

    // Calculate how much to trim from the edges
    int trim_w = (int)v1_floor((v1_float)kernel->width / 2.0);
    int trim_h = (int)v1_floor((v1_float)kernel->height / 2.0);

    // Calculate the new width and height for the ROI
    int new_width  = dst->width - (trim_w * 2);
    int new_height = dst->height - (trim_h * 2);

    // Make sure we have enough to trim
    // TODO: Should this return failure? Is it enough to simply return
    // the image without setting the ROI?
    if(new_width <= 0 || new_height <= 0)
        return -4;

    // Set the ROI
    CvRect rect = cvRect(trim_w, trim_h, new_width, new_height);
    cvSetImageROI(dst, rect);

    return 0;
}
