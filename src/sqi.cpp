// sqi.cpp
// Copyright 2009 Securics, Inc

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "sqi.h"
/**
 * Preforms self quotient image
 * @param IplImage* input_img The input image
 * @return IplImage* The normalized image
 */
IplImage* securics_sqi_norm(IplImage* input_img)
{

    IplImage *smoothed_img=0;
    IplImage *q_img=0;
    IplImage *SQI_img=0;
    int i,j;
    uchar temp, temp_s;
    
    float temp_x, temp_y, temp_z, temp_val;
    float max, min, factor, mean;
    float my_float;
    
    smoothed_img = cvCreateImage(cvSize(input_img->width, input_img->height), IPL_DEPTH_8U, 1);    
    cvSmooth(input_img,smoothed_img,CV_GAUSSIAN,15,0,4);
    q_img = cvCreateImage(cvSize(input_img->width, input_img->height), IPL_DEPTH_32F, 1);
    SQI_img = cvCreateImage(cvSize(input_img->width, input_img->height), IPL_DEPTH_8U, 1);    
    max=0;
    min=1000;
    
    //form quotient image
    for (i=0; i<input_img->height; i++)
      for (j=0; j<input_img->width; j++)
	{
	  //get pixel from input image
	  temp=((uchar *)(input_img->imageData + i*input_img->widthStep))[j];
	  //divide by 255 to make values range from 0.0-1.0 
	  my_float=(float)temp;		  
	  temp_x=my_float/(float)255;
	  
	  
	  //get pixel from smoothed image
	   temp_s=((uchar *)(smoothed_img->imageData + i*smoothed_img->widthStep))[j];
	  my_float=(float)temp_s;
	  //divide by 255 to make values range from 0.0-1.0 
	  temp_y=my_float/(float)255;
	  

	  if (temp_y==0)

	  (((float *)(q_img->imageData + i*q_img->widthStep))[j])=0;
	  else
	    (((float *)(q_img->imageData + i*q_img->widthStep))[j])=temp_x/temp_y;
	 
	  
	  
	  //divide the two pixel values
	   temp_z=(((float *)(q_img->imageData + i*q_img->widthStep))[j]);
	  
	  //find max and min of q_img
	   if (temp_z > max)
	     max=temp_z;
	    if (temp_z < min)
	     min=temp_z;
	}
    
    // cvNamedWindow("INITIAL QUOTIENT", CV_WINDOW_AUTOSIZE);
    // cvShowImage("INITIAL QUOTIENT",q_img);
    
    //	fprintf(stderr,"MAX: %f MIN: %f \r\n",max,min); 
    
	/* 
    for (i=0; i<q_img->height; i++)
      for (j=0; j<q_img->width; j++)
	{   
	  temp=((float *)(q_img->imageData + i*q_img->widthStep))[j];
	    if (temp==0)
	      fprintf(stderr,"image_data[%d][%d]: %f\r\n",i,j,temp);
	}
    
	*/
    
    // cvWaitKey(0);    
    
    factor=(max-min);
    
    //normalize q_image
    for (i=0; i<q_img->height; i++)
      for (j=0; j<q_img->width; j++)
	{   
	  temp_x =((float *)(q_img->imageData + i*q_img->widthStep))[j];
	  temp_x = temp_x - min;
	  temp_z = temp_x / factor;
	  // fprintf(stderr,"temp_data: %f  temp-min: %f  temp/factor: %f\r\n",temp_x,temp_y,temp_z);
	  
	  ((float *)(q_img->imageData + i*q_img->widthStep))[j]=temp_z;
	}
    
    
    //  cvNamedWindow("FIRST NORM QUOTIENT", CV_WINDOW_AUTOSIZE);
    //   cvShowImage("FIRST NORM QUOTIENT",q_img);
    
    
    
    
    
    //find mean of the image
    temp_x=0;
    for (i=0; i<q_img->height; i++)
      for (j=0; j<q_img->width; j++)
	{   
	  temp_val=((float *)(q_img->imageData + i*q_img->widthStep))[j];
	  
	  temp_x+=temp_val;   
	  // temp_x +=((float *)(q_img->imageData + i*q_img->widthStep))[j];
	}
    
    mean=temp_x/(q_img->height*q_img->width);
    
    //   fprintf(stderr,"temp_x: %f MEAN: %f \r\n",temp_x,mean); 
    
    
    
    
    //final normalize q_image
    for (i=0; i<q_img->height; i++)
      for (j=0; j<q_img->width; j++)
	{   
	  temp_x =((float *)(q_img->imageData + i*q_img->widthStep))[j];
	  temp_x = (-1)*temp_x/mean;
	  temp_x = 1-exp((double)temp_x);
	  ((float *)(q_img->imageData + i*q_img->widthStep))[j]=temp_x;
	}
    
    //convert back to uint8
    for (i=0; i<q_img->height; i++)
      for (j=0; j<q_img->width; j++)
	{
	  //get pixel from quotient image
	  temp_x =((float *)(q_img->imageData + i*q_img->widthStep))[j];
	  //divide by 255 to make values range from 0.0-1.0 
	  temp=(uchar)(temp_x*255);
          
	  ((uchar *)(SQI_img->imageData + i*SQI_img->widthStep))[j]=temp;
	}
    
    
    //    cvNamedWindow("INPUT", CV_WINDOW_AUTOSIZE);
    //    cvShowImage("INPUT",input_img);
    
    //    cvNamedWindow("SMOOTH", CV_WINDOW_AUTOSIZE);
    //    cvShowImage("SMOOTH",smoothed_img);
    
    //    cvNamedWindow("QUOTIENT", CV_WINDOW_AUTOSIZE);
    //     cvShowImage("QUOTIENT",q_img);
    
    //     cvNamedWindow("NORMALIZED", CV_WINDOW_AUTOSIZE);
    //    cvShowImage("NORMALIZED",SQI_img);
    //   cvWaitKey(0);
    cvReleaseImage(&smoothed_img);
    cvReleaseImage(&q_img);
    return SQI_img;
}
