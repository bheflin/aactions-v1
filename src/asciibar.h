// alert.h
// Copyright 2009, Chris Eberle
// You may use this code however the hell you want, just keep the
// header in tact. No warranties, some people may experience random
// outbreaks of death, etc.

#ifndef __asciibar_h__
#define __asciibar_h__

#include <time.h>

// ASCII bar structures
struct ascii_bar_params
{
    // Display fields
    int len;            // The length of the bar in characters
    int show_txt;       // Whether or not to show percentage as text
    int show_eta;       // Show the estimated completion time
    char left_char;     // The character used for the left character
    char right_char;    // The character used for the right character
    char progress_char; // The character used to show progress
    char leading_char;  // The character used at the rightmost edge of progress
    char incom_char;    // The character used to show uncompleted progress

    // Progress fields
    float begin;        // The starting value for the progress bar
    float end;          // The end value for the progress bar
    float stepsize;     // The step size
    float pos;          // The current progress

    // Time fields
    int el;             // Extra length to remove upon next update
    time_t eta;         // Time until completion
    time_t epoch;       // The time that the progress bar was first shown
    time_t start_time;  // The beginning of statistical significance
    time_t last_time;   // Used for calculating ETA
    time_t next_time;   // The expected next update time
    time_t end_time;    // The time 100% was reached
    float left;         // The amount remaining
    float rate;         // The processing rate
    int broke;          // Is it "broke"

    // Internal use
    int called;
};

// ASCII progress bar functions
void init_ascii_bar(struct ascii_bar_params *params, float min, float max, float step);
void show_ascii_bar(struct ascii_bar_params *params);
void set_ascii_bar(struct ascii_bar_params *params, float val);
void step_ascii_bar(struct ascii_bar_params *params);
void hide_ascii_bar(struct ascii_bar_params *params);
void print_elapsed_time(struct ascii_bar_params *params, const char *metric_name);

#endif // __asciibar_h__
