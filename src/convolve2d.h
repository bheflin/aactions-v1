// convolve2d.h
// Copyright 2009 Securics, Inc

#ifndef _convolve2d_h_
#define _convolve2d_h_

#include "structs.h"
#include "cvheaders.h"

int convolve2d(IplImage *src, IplImage *dst, IplImage *kernel, convolve_mode mode);

#endif
