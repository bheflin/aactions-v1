// learn-multi.h
// Copyright 2009 Securics, Inc

#ifndef _learn_multi_h_
#define _learn_multi_h_

#include <string>

#include "svm.h"
#include "structs.h"

using namespace std;

typedef struct cat_idx_t
{
    unsigned int idx;
    int cat;
} cat_idx;

// One VS Rest data structure
typedef struct learn_onevsrest_t
{
    struct gallery_entry *gallery;
    unsigned int gal_size;
    string* base_file_name;

    svm_parameter *params;

    int quiet;
    int progress;
    int num_procs;

    cat_idx* idxs;
} learn_onevsrest;

// onevsrest creation, destruction, and operations
learn_onevsrest* learn_onevsrest_create(int quiet, int progress, int num_procs);
void learn_onevsrest_destroy(learn_onevsrest* self);

// Learning functions
int learn_onevsrest_train(learn_onevsrest* self, struct gallery_entry *gallery, unsigned int gal_size, const char* base_file_name);
//int learn_onevsrest_save(learn_onevsrest* self, const char* base_file_name);
int learn_onevsrest_cross_validate(learn_onevsrest* self);

#endif /* _learn_multi_h_ */
