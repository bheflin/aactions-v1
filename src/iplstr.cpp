// iplstr.c
// Author: Chris Eberle
// Copyright 2009 Securics, Inc
//
// Some functions to more easily convert an IplImage to and from a
// string for easier debugging and testing. Should be removed
// when the algorithm is stable.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define _BSD_SOURCE // stupid strdup
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "iplstr.h"

// Prototypes
static CvSize count(const char* str);
static int populate(IplImage* img, char* str);
static char* populateElement(IplImage* img, char* str, int row, int col);
static char* populate8U(IplImage* img, char* str, int row, int col);
static char* populate8S(IplImage* img, char* str, int row, int col);
static char* populate16U(IplImage* img, char* str, int row, int col);
static char* populate16S(IplImage* img, char* str, int row, int col);
static char* populate32S(IplImage* img, char* str, int row, int col);
static char* populate32F(IplImage* img, char* str, int row, int col);
static char* populate64F(IplImage* img, char* str, int row, int col);
static void print_elem(IplImage* img, int row, int col, int pad);
static void fprint_elem(FILE* f, IplImage* img, int row, int col);
static void print_pad(const char* str, int pad);

// Construct an IplImage from a string of the form "[[1,2,3][4,5,6]]"
// Whitespace is ignored.
IplImage* iplFromStr(const char* str, int depth, int channels)
{
    CvSize sz = count(str);
    if(sz.width <= 0 || sz.height <= 0)
    {
        fprintf(stderr, "iplFromStr: Unable to determine dimensions.\n");
        return NULL;
    }

    IplImage* img = cvCreateImage(sz, depth, channels);
    char* s = strdup(str);
    int ret = populate(img, s);
    free(s);

    if(ret != 0)
    {
        fprintf(stderr, "iplFromStr: Error parsing string.\n");
        cvReleaseImage(&img);
        img = NULL;
    }

    return img;
}

IplImage* iplFromFile(const char* file, int depth, int channels)
{
    FILE* f = fopen(file, "r");
    if(f == NULL)
    {
        fprintf(stderr, "iplFromFile: Unable to open file: %s\n", file);
        return NULL;
    }

    int ch, sz = 0;
    while ((ch = fgetc(f)) != EOF)
        ++sz;
    fseek(f, 0L, SEEK_SET);

    char* s = (char*)malloc(sizeof(char) * (sz + 1));
    if(s == NULL)
    {
        fclose(f);
        fprintf(stderr, "iplFromFile: Unable to allocate enough memory.\n");
        return NULL;
    }

    s[sz] = '\0';
    sz = 0; // just re-using a variable here
    while ((ch = fgetc(f)) != EOF)
        s[sz++] = (char)ch;
    fclose(f);

    CvSize size = count(s);
    if(size.width <= 0 || size.height <= 0)
    {
        fprintf(stderr, "iplFromFile: Unable to determine dimensions.\n");
        free(s);
        return NULL;
    }

    IplImage* img = cvCreateImage(size, depth, channels);
    int ret = populate(img, s);
    free(s);

    if(ret != 0)
    {
        fprintf(stderr, "iplFromFile: Error parsing string.\n");
        cvReleaseImage(&img);
        img = NULL;
    }

    return img;
}

int saveIplImage(IplImage* img, const char* file)
{
    if(img == NULL)
    {
        fprintf(stderr, "saveIplImage: Image is null.\n");
        return -1;
    }

    FILE* f = fopen(file, "w");
    if(f == NULL)
    {
        fprintf(stderr, "saveIplImage: Unable to open file for writing: %s\n", file);
        return -2;
    }

    int row, col;
    for(row = 0; row < img->height; ++row)
    {
        for(col = 0; col < img->width; ++col)
        {
            if(col == 0)
            {
                if(row == 0) fprintf(f, "[[");
                else fprintf(f, " [");
            }

            if(col != 0) fprintf(f, ", ");
            fprint_elem(f, img, row, col);
        }

        fprintf(f, "]");
        if(row == (img->height - 1))
            fprintf(f, "]\n");
        else
            fprintf(f, "\n");
    }

    fclose(f);
    return 0;
}

int saveIplToMatlab(IplImage* img, const char* file)
{
    if(img == NULL)
    {
        fprintf(stderr, "saveIplImage: Image is null.\n");
        return -1;
    }

    FILE* f = fopen(file, "w");
    if(f == NULL)
    {
        fprintf(stderr, "saveIplImage: Unable to open file for writing: %s\n", file);
        return -2;
    }

    int row, col;
    fprintf(f, "[");
    for(row = 0; row < img->height; ++row)
    {
        for(col = 0; col < img->width; ++col)
        {
            if(row > 0 && col == 0)
                fprintf(f, " ");
            fprint_elem(f, img, row, col);
            if(col < (img->width - 1))
                fprintf(f, " ");
        }

        if(row < (img->height - 1))
            fprintf(f, ";\n");
    }
    fprintf(f, "]\n");

    fclose(f);
    return 0;
}

// Print an IplImage to stdout with optional padding
void printIplImage(IplImage* img, int pad)
{
    int row, col;
    if(img == NULL)
    {
        printf("Image is null.\n");
        return;
    }

    for(row = 0; row < img->height; ++row)
    {
        for(col = 0; col < img->width; ++col)
        {
            if(col == 0)
            {
                if(row == 0) printf("[[");
                else printf(" [");
            }

            if(col != 0) printf(", ");
            print_elem(img, row, col, pad);
        }

        printf("]");
        if(row == (img->height - 1))
            printf("]\n");
        else
            printf("\n");
    }
}

// Print a section of an IplImage to stdout with optional padding. The
// characters <,>,^,v signify continuation of the iplimage data to the
// left, right, up, and down respectively.
void printIplSegment(IplImage* img, int xLeft, int yTop, int width, int height, int pad)
{
    int row, col, tmp;
    if(img == NULL)
    {
        printf("Image is null!\n");
        return;
    }

    if(xLeft >= img->width || xLeft < 0 || yTop >= img->height || yTop < 0 ||
       width <= 0 || height <= 0)
    {
        printf("Invalid bounds!\n");
        return;
    }

    if(xLeft + width > img->width)
        width = img->width - xLeft;
    if(yTop + height > img->height)
        height = img->height - yTop;

    int yBottom = yTop + height;
    int xRight  = xLeft + width;

    for(row = yTop; row < yBottom; ++row)
    {
        for(col = xLeft; col < xRight; ++col)
        {
            if(col == xLeft)
            {
                if(row == yTop)
                {
                    printf("[[");

                    if(yTop > 0)
                    {
                        if(xLeft > 0)
                        {
                            print_pad("<", 1);
                            printf(", ");
                        }

                        for(tmp = xLeft; tmp < xRight; ++tmp)
                        {
                            if(tmp != xLeft) printf(", ");
                            print_pad("^", pad);
                        }

                        if(xRight < img->width)
                        {
                            printf(", ");
                            print_pad(">", 1);
                        }

                        printf("]\n [");
                        if(xLeft > 0)
                        {
                            print_pad("<", 1);
                            printf(", ");
                        }
                    }
                    else
                    {
                        if(xLeft > 0)
                        {
                            print_pad("<", 1);
                            printf(", ");
                        }                        
                    }
                }
                else
                {
                    printf(" [");
                    if(xLeft > 0)
                    {
                        print_pad("<", 1);
                        printf(", ");
                    }
                }
            }

            if(col != xLeft) printf(", ");
            print_elem(img, row, col, pad);

            if(col == xRight - 1)
            {
                if(xRight < img->width)
                {
                    printf(", ");
                    print_pad(">", 1);
                }
            }
        }

        printf("]");
        if(row == (yBottom - 1))
        {
            if(yBottom < img->height)
            {
                printf("\n [");
                if(xLeft > 0)
                {
                    print_pad("<", 1);
                    printf(", ");
                }

                for(tmp = xLeft; tmp < xRight; ++tmp)
                {
                    if(tmp != xLeft) printf(", ");
                    print_pad("v", pad);
                }

                if(xRight < img->width)
                {
                    printf(", ");
                    print_pad(">", 1);
                }

                printf("]");
            }

            printf("]\n");
        }
        else
        {
            printf("\n");
        }
    }
}

// Count the rows and columns given a string
static CvSize count(const char* str)
{
    char c;
    int cols = -1, rows = -1, tmpcols = -1;
    int inouter = 0;
    int ininner = 0;

    while(*str)
    {
        c = *str;
        ++str;
        if(c == ' ' || c == '\t' || c == '\n' || c == '\r')
            continue;

        if(inouter == 0)
        {
            if(c != '[')
                return cvSize(-1, -1);
            inouter = 1;
            continue;
        }

        if(ininner == 0)
        {
            if(c == ']')
            {
                inouter = 0;
                break;
            }
            else if(c != '[')
            {
                return cvSize(-1, -1);
            }

            ininner = 1;
            if(rows == -1)
            {
                rows = 1;
                cols = 0;
            }
            else
            {
                ++rows;
            }

            tmpcols = 0;

            continue;
        }
        else
        {
            if(c == ']')
            {
                if(rows == 1)
                {
                    cols = tmpcols + 1;
                }
                else
                {
                    if(tmpcols + 1 != cols)
                        return cvSize(-1, -1);
                }

                ininner = 0;
                continue;
            }

            if(c == ',')
            {
                ++tmpcols;
            }
        }
    }

    if(ininner == 1 || inouter == 1)
        return cvSize(-1, -1);

    if(rows < 0 || cols < 0)
        return cvSize(-1, -1);

    return cvSize(cols, rows);
}

// Fill an IplImage with data from a string
static int populate(IplImage* img, char* str)
{
    char c, *tmp;
    int row = -1, col = -1;
    int inouter = 0, ininner = 0;
    int read_item = 0;

    while(*str)
    {
        if(read_item == 1)
        {
            tmp = populateElement(img, str, row, col);
            if(tmp == str)
                return -1;

            str = tmp;
            read_item = 0;

            continue;
        }

        c = *str;
        ++str;
        if(c == ' ' || c == '\t' || c == '\n' || c == '\r')
            continue;

        if(inouter == 0)
        {
            if(c != '[')
                return -1;

            inouter = 1;
            continue;
        }

        if(ininner == 0)
        {
            if(c == ']')
            {
                inouter = 0;
                break;
            }
            else if(c != '[')
                return -1;

            ininner = 1;

            col = 0;
            ++row;

            if(row >= img->height)
                return -1;

            continue;
        }
        else
        {
            if(c == ']')
            {
                ininner = 0;
                continue;
            }
            else if(c == ',')
            {
                col++;
                continue;
            }

            --str;
            read_item = 1;
        }
    }

    return 0;
}

// Populate an element of an IplImage with string data at a given row
// and column. If the return value == str, then nothing useful was
// found and the caller should error out.
static char* populateElement(IplImage* img, char* str, int row, int col)
{
    switch(img->depth)
    {
    case IPL_DEPTH_8U:
        return populate8U(img, str, row, col);
    case IPL_DEPTH_8S:
        return populate8S(img, str, row, col);
    case IPL_DEPTH_16U:
        return populate16U(img, str, row, col);
    case IPL_DEPTH_16S:
        return populate16S(img, str, row, col);
    case IPL_DEPTH_32S:
        return populate32S(img, str, row, col);
    case IPL_DEPTH_32F:
        return populate32F(img, str, row, col);
    case IPL_DEPTH_64F:
        return populate64F(img, str, row, col);
    }

    // Shouldn't reach this
    return str;
}

// Populate an element by parsing an 8-bit unsigned int
static char* populate8U(IplImage* img, char* str, int row, int col)
{
    int nchan;
    char* pos;
    uchar n = (uchar)strtol(str, &pos, 10);

    if(pos > str)
    {
        for(nchan = 0; nchan < img->nChannels; ++nchan)
            ((uchar*)(img->imageData + img->widthStep * row))[col * img->nChannels + nchan] = n;
    }

    return pos;
}

// Populate an element by parsing an 8-bit signed int
static char* populate8S(IplImage* img, char* str, int row, int col)
{
    int nchan;
    char* pos;
    char n = (char)strtol(str, &pos, 10);

    if(pos > str)
    {
        for(nchan = 0; nchan < img->nChannels; ++nchan)
            ((char*)(img->imageData + img->widthStep * row))[col * img->nChannels + nchan] = n;
    }

    return pos;
}

// Populate an element by parsing a 16-bit unsigned int
static char* populate16U(IplImage* img, char* str, int row, int col)
{
    int nchan;
    char* pos;
    unsigned short n = (unsigned short)strtol(str, &pos, 10);

    if(pos > str)
    {
        for(nchan = 0; nchan < img->nChannels; ++nchan)
            ((unsigned short*)(img->imageData + img->widthStep * row))[col * img->nChannels + nchan] = n;
    }

    return pos;
}

// Populate an element by parsing a 16-bit signed int
static char* populate16S(IplImage* img, char* str, int row, int col)
{
    int nchan;
    char* pos;
    short n = (short)strtol(str, &pos, 10);

    if(pos > str)
    {
        for(nchan = 0; nchan < img->nChannels; ++nchan)
            ((short*)(img->imageData + img->widthStep * row))[col * img->nChannels + nchan] = n;
    }

    return pos;
}

// Populate an element by parsing a 32-bit signed int
static char* populate32S(IplImage* img, char* str, int row, int col)
{
    int nchan;
    char* pos;
    int n = (int)strtol(str, &pos, 10);

    if(pos > str)
    {
        for(nchan = 0; nchan < img->nChannels; ++nchan)
            ((int*)(img->imageData + img->widthStep * row))[col * img->nChannels + nchan] = n;
    }

    return pos;
}

// Populate an element by parsing a 32-bit signed float
static char* populate32F(IplImage* img, char* str, int row, int col)
{
    int nchan;
    char* pos;
    float n = (float)strtof(str, &pos);

    if(pos > str)
    {
        for(nchan = 0; nchan < img->nChannels; ++nchan)
            ((float*)(img->imageData + img->widthStep * row))[col * img->nChannels + nchan] = n;
    }

    return pos;
}

// Populate an element by parsing a 64-bit signed float (double)
static char* populate64F(IplImage* img, char* str, int row, int col)
{
    int nchan;
    char* pos;
    double n = (float)strtod(str, &pos);

    if(pos > str)
    {
        for(nchan = 0; nchan < img->nChannels; ++nchan)
            ((double*)(img->imageData + img->widthStep * row))[col * img->nChannels + nchan] = n;
    }

    return pos;
}

// Print the value of an IplImage element to stdout with padding
static void print_elem(IplImage* img, int row, int col, int pad)
{
    char fmt[64];

    switch(img->depth)
    {
    case IPL_DEPTH_8U:
        snprintf(fmt, 64, "%%-%du", pad);
        printf(fmt, ((uchar*)(img->imageData + img->widthStep * row))[col * img->nChannels]);
        break;
    case IPL_DEPTH_8S:
        snprintf(fmt, 64, "%%-%dd", pad);
        printf(fmt, ((char*)(img->imageData + img->widthStep * row))[col * img->nChannels]);
        break;
    case IPL_DEPTH_16U:
        snprintf(fmt, 64, "%%-%du", pad);
        printf(fmt, ((ushort*)(img->imageData + img->widthStep * row))[col * img->nChannels]);
        break;
    case IPL_DEPTH_16S:
        snprintf(fmt, 64, "%%-%dd", pad);
        printf(fmt, ((short*)(img->imageData + img->widthStep * row))[col * img->nChannels]);
        break;
    case IPL_DEPTH_32S:
        snprintf(fmt, 64, "%%-%dd", pad);
        printf(fmt, ((int*)(img->imageData + img->widthStep * row))[col * img->nChannels]);
        break;
    case IPL_DEPTH_32F:
        snprintf(fmt, 64, "%%-%df", pad);
        printf(fmt, ((float*)(img->imageData + img->widthStep * row))[col * img->nChannels]);
        break;
    case IPL_DEPTH_64F:
        snprintf(fmt, 64, "%%-%dlf", pad);
        printf(fmt, ((double*)(img->imageData + img->widthStep * row))[col * img->nChannels]);
        break;
    }
}

// Print the value of an IplImage element to stdout with padding
static void fprint_elem(FILE* f, IplImage* img, int row, int col)
{
    switch(img->depth)
    {
    case IPL_DEPTH_8U:
        fprintf(f, "%u", ((uchar*)(img->imageData + img->widthStep * row))[col * img->nChannels]);
        break;
    case IPL_DEPTH_8S:
        fprintf(f, "%d", ((char*)(img->imageData + img->widthStep * row))[col * img->nChannels]);
        break;
    case IPL_DEPTH_16U:
        fprintf(f, "%u", ((ushort*)(img->imageData + img->widthStep * row))[col * img->nChannels]);
        break;
    case IPL_DEPTH_16S:
        fprintf(f, "%d", ((short*)(img->imageData + img->widthStep * row))[col * img->nChannels]);
        break;
    case IPL_DEPTH_32S:
        fprintf(f, "%d", ((int*)(img->imageData + img->widthStep * row))[col * img->nChannels]);
        break;
    case IPL_DEPTH_32F:
        fprintf(f, "%.20f", ((float*)(img->imageData + img->widthStep * row))[col * img->nChannels]);
        break;
    case IPL_DEPTH_64F:
        fprintf(f, "%.30f", ((double*)(img->imageData + img->widthStep * row))[col * img->nChannels]);
        break;
    }
}

static void print_pad(const char* str, int pad)
{
    char fmt[64];
    snprintf(fmt, 64, "%%-%ds", pad);
    printf(fmt, str);
}

