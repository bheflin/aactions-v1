// capture.h
// Copyright 2009 Securics, Inc

#ifndef _capture_h_
#define _capture_h_

extern "C" {
    #include "gige.h"
    #include "registers.h"
}

#include "cvheaders.h"
#include "parser.h"

// Capture type
typedef enum v1_capture_type_t
{
    FILE_LIST = 0,
    VIDEO     = 1,
    WEBCAM    = 2,
    GIGE      = 3
} v1_capture_type;

typedef union v1_capture_param_t
{
    char* filename;
    int cam_index;
} v1_capture_param;

// V1 Capture Data
typedef struct v1_capture_t
{
    // General data
    IplImage* curImage;
    char* curName;
    char* cacheName;
    char* saveName;
    unsigned int framenum;
    unsigned int frametotal;
    v1_capture_type type;
    char* filename;
    bool eof;
    bool shouldFree;

    // File list data
    filelist* tfList;
    fileitem* listItem;

    // Webcam and video data
    int vgtlabel;
    CvCapture* capture;

    // Gige data
    g_camera* gige_cam;

} v1_capture;

typedef struct v1_capture_gtdata_t
{
    int label;
    int lx;
    int ly;
    int rx;
    int ry;
    int cropX;
    int cropY;
    int cropW;
    int cropH;
} v1_capture_gtdata;

// v1_capture creation, destruction
v1_capture* v1_capture_create(v1_capture_type ftype, const v1_capture_param param);
void v1_capture_destroy(v1_capture* self);

// v1_capture operations
bool         v1_capture_next(v1_capture* self);
IplImage*    v1_capture_get(v1_capture* self);
unsigned int v1_capture_pos(v1_capture* self);
float        v1_capture_progress(v1_capture* self);
bool         v1_capture_getdata(v1_capture* self, v1_capture_gtdata* data);
bool         v1_capture_setdata(v1_capture* self, v1_capture_gtdata* data);
bool         v1_capture_setimg(v1_capture* self, IplImage* img);
void         v1_capture_setvlabel(v1_capture* self, int vlabel);
bool         v1_capture_eof(v1_capture* self);
const char*  v1_capture_name(v1_capture* self);
const char*  v1_capture_cache(v1_capture* self);
const char*  v1_capture_savename(v1_capture* self);

#endif /* _capture_h_ */
