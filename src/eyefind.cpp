// File:    eyefind.cpp
// Author:  Chris Eberle
// Created: Wed Oct 21 15:02:26 2009
// Copyright 2009 Securics, Inc
//
// A simple program to locate eyes in an image

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>

#include "geo.h"
#include "eye_detector.h"
#include "facedetect.h"

#define SPACE 32
#define ESCAPE 27
#define ENTER 10

using namespace std;


// Prototypes
static const char* haarfile = "haarcascade_frontalface_alt2.xml";

#define SAFE_RELEASE(img) do { if((img) != NULL) { cvReleaseImage(&(img)); (img) = NULL; } } while(0);
#define SAFE_FREE(ptr) do { if ((ptr) != NULL) { free((ptr)); (ptr) = NULL; } } while(0);
IplImage* cropIplB(IplImage* in, int x, int y, int width, int height)
{
    if(in == NULL || width == 0 || height == 0 || x < 0 || y < 0 || x >= in->width || y >= in->height)
        return NULL;

    IplImage* cropped = cvCreateImage(cvSize(width, height), in->depth, in->nChannels);
    if(cropped == NULL)
        return NULL;

    int nx, ny, tx, ty;
    if(in->depth == IPL_DEPTH_8U)
    {
        for(ty = 0, ny = y; ty < height; ty++, ny++)
        {
            for(tx = 0, nx = x; tx < width; tx++, nx++)
            {
                CV_IMAGE_ELEM(cropped, uchar, ty, tx) =
                    CV_IMAGE_ELEM(in, uchar, ny, nx);
            }
        }        
    }
    else if(in->depth == IPL_DEPTH_V1)
    {
        for(ty = 0, ny = y; ty < height; ty++, ny++)
        {
            for(tx = 0, nx = x; tx < width; tx++, nx++)
            {
                CV_IMAGE_ELEM(cropped, v1_float, ty, tx) =
                    CV_IMAGE_ELEM(in, v1_float, ny, nx);
            }
        }
    }

    return cropped;
}



int processImage(IplImage* img, CvHaarClassifierCascade* cascade, CvMemStorage* storage)
{
    const int small_size = 180;
    if(img == NULL)
        return 1;

    IplImage* colorImg = cvCloneImage(img);
    IplImage* grayImg = cvCreateImage(cvGetSize(img), img->depth, 1);
      
    if(img->nChannels > 1)
    {
        
        // Convert to grayscale
        cvCvtColor(img, grayImg, CV_BGR2GRAY);
    }
    
    else
    {
        grayImg = cvCloneImage(img);
    }

    CvRect r = detect_face(cascade, storage, grayImg);
    if(r.x >= 0 && r.y >=0 && r.width > 0 && r.height > 0)
    {
        CvPoint pt1 = cvPoint(r.x, r.y);
        CvPoint pt2 = cvPoint(r.x + r.width, r.y + r.height);
        cvRectangle(colorImg, pt1, pt2, cvScalar(0, 0, 255), 2, 8, 0);
    }
    else
    {
        SAFE_RELEASE(colorImg);
        SAFE_RELEASE(grayImg);
        return 1;
    }

    IplImage* tmp = cropIplB(grayImg, r.x, r.y, r.width, r.height);
    
    SAFE_RELEASE(grayImg);
    grayImg = tmp;
    

    if(grayImg == NULL)
    {
        SAFE_RELEASE(colorImg);
        return 1;
    }

    int beforeW, beforeH;
    beforeW = grayImg->width;
    beforeH = grayImg->height;


    //resize image to 180 x N to preserve aspect ratio
    CvSize   new_size;
    int max_edge = small_size;
        
    // -- resize so that the biggest edge is max_edge (keep aspect ratio)
    if(grayImg->width > grayImg->height)
    {
        new_size.width  = max_edge;
        new_size.height = (int)((float)max_edge * (float)grayImg->height / (float)grayImg->width);
    }
    else
    {
        new_size.width  = (int)((float)max_edge * (float)grayImg->width / (float)grayImg->height);
        new_size.height = max_edge;
    }
    
    IplImage* dst = cvCreateImage(new_size, grayImg->depth, grayImg->nChannels);
    
    // Resize the image
    if(grayImg->width == dst->width && grayImg->height == dst->height)
        cvCopy(grayImg, dst);
    else
        // BUG: This does not do a resize the same as python
        cvResize(grayImg, dst, CV_INTER_CUBIC);
    
    SAFE_RELEASE(grayImg);
    grayImg = dst;
    
    if(grayImg == NULL)
    {
        SAFE_RELEASE(colorImg);
        return 1;
    }
    
    int lx = -1, ly = -1, rx = -1, ry = -1;
    int lx1 = -1, ly1 = -1, rx1 = -1, ry1 = -1;
    
    eye_detect(grayImg, &lx, &rx, &ly, &ry, &lx, &rx, &ly, &ry);
    
    if(lx < 0 || ly < 0 || rx < 0 || ry < 0 ||
        lx >= grayImg->width || ly >= grayImg->height ||
        rx >= grayImg->width || ry >= grayImg->height)
    {
        return 1;
    }

    return 0;
}

// Entry point for program
int main(int argc, char **argv)
{
    // Parse the command line arguments
    
    CvHaarClassifierCascade* cascade = (CvHaarClassifierCascade*)cvLoad(haarfile, 0, 0, 0);
    if(cascade == NULL)
    {
        fprintf(stderr,"Error: Unable to load haar classifier: `%s'\n", haarfile);
    }
    
    CvMemStorage* storage = cvCreateMemStorage(0);
    
    //first parameter is the image file name
    IplImage* img = NULL;
    
    if((img = cvLoadImage(argv[1],-1)) == NULL)
    {
        fprintf(stderr,"Error: Unable to load image file \n");
    }

    int ret = processImage(img, cascade, storage);
    
    cvReleaseImage(&img);
    
    return ret;
    
}

