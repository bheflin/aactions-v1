// sqi.h
// Copyright 2009 Securics, Inc

#ifndef SQI_H
#define SQI_H
#include <stdio.h>
#include <fftw3.h>
#include "cvheaders.h"

#define PI 3.14159265

IplImage* securics_sqi_norm(IplImage* input_img);

#endif
