// gabor.h
// Copyright 2009 Securics, Inc

#ifndef GABOR_H
#define GABOR_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "cvheaders.h"
#include "structs.h"

typedef struct gabor_entry_t
{
    IplImage* row;
    IplImage* col;
} gabor_entry;

typedef struct gabor_filter_t
{
    int numEntries;
    gabor_entry* first;
} gabor_filter;

// Fast filters
gabor_filter** make_filters();
gabor_filter** load_filters(const char* dir);
void free_filters(gabor_filter** filters);

// Slow filters
IplImage** make_ipl_filters();
void free_ipl_filters(IplImage** filters);

// Filter generation
gabor_filter* gabor(v1_float gsw, v1_float gsh, v1_float gx0, v1_float gy0, v1_float wfreq,
                    v1_float worient, v1_float wphase, int height, int width, v1_float* gabor);
void free_gabor(gabor_filter* filt);

#endif
