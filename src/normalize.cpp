// File:    normalize.cpp
// Author:  Chris Eberle
// Created: Tue Oct 27 15:44:12 2009
// Copyright 2009 Securics, Inc

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <ctype.h>
#include <libgen.h>
#include <stdarg.h>
#include <errno.h>
#include <sys/stat.h>
#include <iostream>

#include "cvheaders.h"

#include "parser.h"
#include "geo.h"
#include "argparse.h"

using namespace std;

// Typedefs
typedef struct prog_params_t
{
    char* prog_name;
    char* file_name;
    char* out_file;
    int   enable_histnorm;
} prog_params;

// Prototypes
static void usage(const char* name, int val);
static void parseCommandLine(int argc, char **argv, prog_params *parm);
static void fatal(const char* msg, ...);

#define SAFE_RELEASE(img) do { if((img) != NULL) { cvReleaseImage(&(img)); (img) = NULL; } } while(0);
#define SAFE_FREE(ptr) do { if ((ptr) != NULL) { free((ptr)); (ptr) = NULL; } } while(0);

// Entry point for program
int main(int argc, char **argv)
{
    prog_params params;
    filelist* fl_gal = NULL;
    fileitem* gitem  = NULL;
    FILE *output = NULL;
    IplImage* img = NULL, *normed = NULL;
    string newfile;

    // Parse the command line arguments
    parseCommandLine(argc, argv, &params);
    if(params.file_name == NULL || params.out_file == NULL)
        usage(params.prog_name, 1);

    if((fl_gal = parseFileList(params.file_name)) == NULL)
        fatal("Error: Unable to load input gallery!\n");

    if((output = fopen(params.out_file, "w")) == NULL)
        fatal("Error: Unable to open `%s': %s\n", params.out_file, strerror(errno));

    gitem = fl_gal->begin;
    while(gitem != NULL)
    {
        newfile = "";
        if(gitem->leyeX < 0 || gitem->reyeX < 0|| gitem->leyeY < 0 || gitem->reyeY < 0)
        {
            printf("Skipping file: %s\n", gitem->file);
            gitem = gitem->next;
            continue;
        }

        printf("Processing: %s", gitem->file); fflush(stdout);
        if((img = cvLoadImage(gitem->file, 0)) == NULL)
        {
            printf("Can't open file: %s\n", gitem->file);
            gitem = gitem->next;
            continue;
        }

        if((normed = perform_geo_norm(img, gitem->leyeX, gitem->leyeY, gitem->reyeX, gitem->reyeY, params.enable_histnorm, 1)) == NULL)
        {
            printf("\nGeo-norm failed for file: %s\n", gitem->file);
            SAFE_RELEASE(img);
            gitem = gitem->next;
            continue;
        }

        newfile = gitem->file;

        size_t found = newfile.find_last_of(".");
        newfile = newfile.substr(0,found);
        newfile += "_norm.png";

        cvSaveImage(newfile.c_str(), normed);

        SAFE_RELEASE(img);
        SAFE_RELEASE(normed);

        if(gitem->label >= 0)
            fprintf(output, "%s %d\n", newfile.c_str(), gitem->label);
        else
            fprintf(output, "%s\n", newfile.c_str());
        printf(".\n");

        gitem = gitem->next;
    }

    freeList(fl_gal);
    fclose(output);

    return 0;
}

// Print a message to stderr and exit
static void fatal(const char* msg, ...)
{
    va_list ap;
    va_start(ap, msg);
    vfprintf(stderr, msg, ap);
    va_end(ap);
    exit(1);
}

// Print the program's usage
static void usage(const char* name, int val)
{
    FILE* f = (val == 0) ? stdout : stderr;
    fprintf(f,
            "Usage: %s [OPTIONS]\n"
            "  -f, --input-list=FILE             Input file list (required)\n"
            "  -o, --output-list=FILE            Output file list (required)\n"
            "  -H, --hist-norm=BOOL              Enable histogram normalization (default true)\n"
            "  -h, --help                        Show this helpful message.\n"
            , name);
    exit(val);
}

// Parse the command line arguments and load them into a struct
static void parseCommandLine(int argc, char **argv, prog_params *parm)
{
    argv[0] = basename(argv[0]);
    parm->prog_name = argv[0];
    parm->file_name = NULL;
    parm->out_file  = NULL;
    parm->enable_histnorm = 1;

    ArgParser ap(usage, parm->prog_name, false);
    ap.addOption("help", false, "h");
    ap.addOption("hist-norm", true, "H", "yes");
    ap.addOption("input-list", true, "f");
    ap.addOption("output-list", true, "o");

    if(!ap.parseCommandLine(argc, argv, true))
        exit(1);

    if(ap.isSet("help"))
        usage(parm->prog_name, 0);

    if(ap.isSet("hist-norm")) parm->enable_histnorm = ap.getBoolValue("hist-norm");
    if(ap.isSet("input-list")) parm->file_name = strdup(ap.getValue("input-list"));
    if(ap.isSet("output-list")) parm->out_file = strdup(ap.getValue("output-list"));
}
