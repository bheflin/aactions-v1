// preprocessing.cpp
// Copyright 2009 Securics, Inc

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "preprocessing.h"
#include "iplstr.h"
#include "convolve2d.h"
#include "utils.h"
#include <sys/stat.h>
#include <unistd.h>

/**
 * Takes in an image and creates a croped version and frees original
 * @param IplImage* in The image to be cropped and released
 */
static IplImage* cropROIAndRelease(IplImage* in)
{
    IplImage* tmp = cropIplToROI(in);
    if(tmp == NULL)
        return in;

    cvResetImageROI(in);
    cvReleaseImage(&in);
    return tmp;
}
/**
 * Preforms a local normalization on a single image
 * @param IplImage* hin input image to be normalized
 * @param int kh The kernel height
 * @param int kw The kernel width
 * @param v1_float threshold If the magnitude is greater than threshold it is set to threshold
 */

static IplImage* v1s_norm_2d(IplImage *hin, int kh, int kw, v1_float threshold)
{
    int x, y;
    v1_float eps = 1e-5;

    //Below accomplishes the ones command above, it was desinged for IplImage Passing, I think this is very poor but too late in game
    //Questions on improvement are obvious, and Know this can be greatly improved once it is proven, and time is no longer a concern
    //Dynamic allocation is done but at this point there is no real way to extract all malloc calls since tbd all final calls
    IplImage* ker  = cvCreateImage(cvSize(kw, kh), IPL_DEPTH_V1, 1);
    IplImage* hsq  = cvCreateImage(cvGetSize(hin), IPL_DEPTH_V1, 1);
    IplImage* hssq = cvCreateImage(cvGetSize(hin), IPL_DEPTH_V1, 1);
    IplImage* val  = cvCreateImage(cvGetSize(hin), IPL_DEPTH_V1, 1);
    IplImage* hsum = cvCreateImage(cvGetSize(hin), IPL_DEPTH_V1, 1);
    IplImage* hdiv = cvCreateImage(cvGetSize(hin), IPL_DEPTH_V1, 1);

    // Kernel is all ones
    cvSet(ker, cvScalar(1));
 
    // Compute sum-of-square
    int size = kh * kw;
    for(x = 0; x < hin->width; x++)
    {
        for(y = 0; y < hin->height; y++)
        {
            CV_IMAGE_ELEM(hsq, v1_float, y, x) = v1_pow(CV_IMAGE_ELEM(hin, v1_float, y, x), 2.0);
        }
    }

    convolve2d(hsq, hssq, ker, MODE_VALID);
    convolve2d(hin, hsum, ker, MODE_VALID);

    // Crop hssq and hsum to its ROI
    hssq = cropROIAndRelease(hssq);
    hsum = cropROIAndRelease(hsum);

    hin = trimIpl(hin, cvGetSize(hsum));

    IplImage *hout = cvCreateImage(cvGetSize(hsum), IPL_DEPTH_V1, 1);
    IplImage* hnum = cvCreateImage(cvGetSize(hsum), IPL_DEPTH_V1, 1);    

    // Compute hnum and hdiv
    for(x = 0; x < hnum->width; x++)
    {
        for(y = 0; y < hnum->height; y++)
        {
            CV_IMAGE_ELEM(hnum, v1_float, y, x) = CV_IMAGE_ELEM(hin, v1_float, y, x) -
                (CV_IMAGE_ELEM(hsum, v1_float, y, x) / (v1_float)size);
        }
    }

    // Python: val = (hssq - (hsum**2.)/size)
    for(x = 0; x < hin->width; x++)
    {
        for(y = 0; y < hin->height; y++)
        {
            v1_float val_hold = CV_IMAGE_ELEM(hssq, v1_float, y, x) - ((CV_IMAGE_ELEM(hsum, v1_float, y, x)*CV_IMAGE_ELEM(hsum, v1_float, y, x))/(v1_float)size);
            if(val_hold < 0)
                val_hold = 0;
            CV_IMAGE_ELEM(val, v1_float, y, x) = val_hold;
        }
    }

    //  hdiv = val ** (1./2) + eps
    // -- apply normalization
    // 'volume' threshold
    // N.putmask(hdiv, hdiv < (threshold+eps), 1.)
    for(x = 0; x < hin->width; x++)
    {
        for(y = 0; y < hin->height; y++)
        {
            v1_float hdiv_val = v1_pow(CV_IMAGE_ELEM(val, v1_float, y, x), .5) + eps;
            if(hdiv_val < (threshold + eps))
                hdiv_val = 1;
            CV_IMAGE_ELEM(hdiv, v1_float, y, x) = hdiv_val;

        }
    }

    // result = (hnum / hdiv)
    // hout[:] = result
    for(x = 0; x < hin->width; x++)
    {
        for(y = 0; y < hin->height; y++)
        {
            CV_IMAGE_ELEM(hout, v1_float, y, x) = CV_IMAGE_ELEM(hnum, v1_float, y, x) / CV_IMAGE_ELEM(hdiv, v1_float, y, x);
        }
    }

    // So that the image can be trimmed later
    cvSetImageROI(hout, cvGetImageROI(hsum));
      
    cvReleaseImage(&ker);
    cvReleaseImage(&hsq);
    cvReleaseImage(&hssq);
    cvReleaseImage(&val);
    cvReleaseImage(&hdiv);
    cvReleaseImage(&hsum);
    cvReleaseImage(&hnum);
    cvReleaseImage(&hin);

    return hout;
}

/**Takes in an image and a max edge and returns a resized image with the aspect ratio persrved
 * @param IplImage* img the image to resize
 * @param int max_edge The max size of a diagonal of the image
 * @param int call_python 1 if python should be called to resize
 * @param int num A number to allow a unique name and help thread safety
 */
IplImage* get_image(IplImage *img, int max_edge, int call_python, int num)
{
    IplImage *dst = NULL;
    int pid = getpid();
    if(img == NULL)
        return NULL;
    
    if(call_python)
    {
        const char* path = "/tmp/preprocess_%d_%d.png";
        const char* rpath = "/tmp/preprocess_resized_%d_%d.png";
        const char* prog = "./resize.py";

        char mpath[1024];
        char npath[1024];
        char cmd[1024];

        struct stat stFileInfo;
        if(stat(prog, &stFileInfo) != 0)
        {
            fprintf(stderr, "Error: external resize program `%s' not found, using cvResize.\n", prog);
            goto internal;
        }

        snprintf(mpath, 1024, path, num, pid);
        snprintf(npath, 1024, rpath, num, pid);

        int ct = 0;

        cvSaveImage(mpath, img);

        do
        {
            snprintf(cmd, 1024, "%s %s %s", prog, mpath, npath);
            if(system(cmd) != 0)
                fprintf(stderr, "WARNING: command returned non-zero: %s\n", cmd);

            dst = cvLoadImage(npath, CV_LOAD_IMAGE_GRAYSCALE);
            ++ct;
        } while(dst == NULL && ct < 10);

        unlink(mpath);
        unlink(npath);

        if(dst == NULL)
        {
            fprintf(stderr, "ERROR: External program did not work, goodbye.\n");
            exit(1);
        }
    }
    else
    {
    internal:
        CvSize   new_size;

        // -- resize so that the biggest edge is max_edge (keep aspect ratio)
        if(img->width > img->height)
        {
            new_size.width  = max_edge;
            new_size.height = (int)((v1_float)max_edge * (v1_float)img->height / (v1_float)img->width);
        }
        else
        {
            new_size.width  = (int)((v1_float)max_edge * (v1_float)img->width / (v1_float)img->height);
            new_size.height = max_edge;
        }

        dst = cvCreateImage(new_size, img->depth, img->nChannels);

        // Resize the image
        if(img->width == dst->width && img->height == dst->height)
            cvCopy(img, dst);
        else
            // BUG: This does not do a resize the same as python
            cvResize(img, dst, CV_INTER_CUBIC);
    }

    return dst;
}

/*
 *Prepares the image for the v1 operations as described in the v1 paper
 * @param IplImage* img Gray scale source
*/
IplImage* image_preparation(IplImage *img)
{
    v1_float mean;
    v1_float std;
    int i, j;
    std  = 0;

    IplImage* ik1 = cvCreateImage(cvSize(3, 1), IPL_DEPTH_V1, 1);
    CV_IMAGE_ELEM(ik1, v1_float, 0, 0) = 1./3.;
    CV_IMAGE_ELEM(ik1, v1_float, 0, 1) = 1./3.;
    CV_IMAGE_ELEM(ik1, v1_float, 0, 2) = 1./3.;

    IplImage* ik2 = cvCreateImage(cvSize(1, 3), IPL_DEPTH_V1, 1);
    CV_IMAGE_ELEM(ik2, v1_float, 0, 0) = 1./3.;
    CV_IMAGE_ELEM(ik2, v1_float, 1, 0) = 1./3.;
    CV_IMAGE_ELEM(ik2, v1_float, 2, 0) = 1./3.;

    IplImage* src  = cvCreateImage(cvGetSize(img), IPL_DEPTH_V1, 1);
    IplImage* dst1 = cvCreateImage(cvGetSize(img), IPL_DEPTH_V1, 1);
    IplImage* dst2 = cvCreateImage(cvGetSize(img), IPL_DEPTH_V1, 1);

    cvConvertScale(img, src, 1./255., 0.);
    convolve2d(src,  dst1, ik1, MODE_SAME);
    convolve2d(dst1, dst2, ik2, MODE_SAME);

    cvReleaseImage(&src);
    cvReleaseImage(&ik1);
    cvReleaseImage(&ik2);
    cvReleaseImage(&dst1);

    mean = iplMean(dst2);

    for(i = 0; i < dst2->width; i++)
    {
        for(j = 0; j < dst2->height; j++)
            CV_IMAGE_ELEM(dst2, v1_float, j, i) -= mean;
    }

    std = iplStdDev(dst2);

    if(std != 0.0f)
    {
        for(i = 0; i < dst2->width; i++)
        {
            for(j = 0; j < dst2->height; j++)
                CV_IMAGE_ELEM(dst2, v1_float, j, i) /= std;
        }
    }

    IplImage* dst = v1s_norm_2d(dst2, 3, 3, 1.);
    cvReleaseImage(&dst2);
    cvResetImageROI(dst);

    return dst;
}
