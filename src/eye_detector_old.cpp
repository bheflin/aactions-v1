// eye_detector.cpp
// Copyright 2009 Securics, Inc

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "eye_detector.h"
#include <stdio.h>
#include <fftw3.h>

#include "left_eye.h"
#include "right_eye.h"

/**
 *Preforms lighting normalization on an input image
 * @param IplImage* img_in The image to be normalized
 * @param int width The width of the image
 * @param int height The height of the image
 */
IplImage* LIGHTING_NORMALIZATION(IplImage *img_in, int width, int height)
{
    static float         x;
    static float         y[256];
    static int           i, j;
    static unsigned char temp;
    static float         temp_float;
    static float         max_val;
    static float         min_val;
    static float         Factor;


    static IplImage *smoothed_img=0;
    static IplImage *q_img=0;
	static IplImage *SQI_img=0;
	
	uchar temp_s;
	float temp_x, temp_y, temp_z;
    float max, min, factor, mean;



    IplImage *img1 = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
   

    for(i = 0; i < height; i++)
    {
        for(j = 0; j < width; j++)
        {
            (img1->imageData + i * img1->widthStep)[j] = (img_in->imageData + i * img_in->widthStep)[j];
        }
    }
    



    float *new_image = (float*)malloc(sizeof(float) * width * height);
    /*    
    x = 1;
    y[0] = 0;
    
    for(i = 1; i <= 255; i++)
    {
        y[i] = log(x);
        x++;
    }
    
    //log normalize the image
    for(i = 0; i < height; i++)
    {
        for(j = 0; j < width; j++)
        {
            temp = (img1->imageData + i * img1->widthStep)[j];
            // temp +=1;
            //    new_image[i * width + j] = (y[(int)temp]);
              new_image[i * width + j] = temp;
        }
    }


 //gamma correction 
    for(i = 0; i < height; i++)
    {
        for(j = 0; j < width; j++)
        {
            //temp = (new_image->imageData + i * new_image->widthStep)[j];
            new_image[i * width + j] =pow(new_image[i * width + j] , 1/2.2);
        }
    }

    //contrast stretch and convert to uint8
    max_val = 0;
    min_val = 10;

    for(i = 0; i < height; i++)
    {
        for(j = 0; j < width; j++)
        {
            temp_float = new_image[i * width + j];

            if(temp_float > max_val)
                max_val = temp_float;

            if(temp_float < min_val)
                min_val = temp_float;
        }
    }

    Factor = floor((255) / (max_val - min_val));

    //subtract min value and multiply Factor
    for(i = 0; i < height; i++)
    {
        for(j = 0; j < width; j++)
        {
            temp_float = new_image[i * width + j];
            new_image[i * width + j] = temp_float - min_val;

            temp_float = new_image[i * width + j];
            new_image[i * width + j] = (float)floor(temp_float * Factor);

            if(new_image[i * width + j] > 255)
                new_image[i * width + j] = 255;

            if(new_image[i * width + j] < 0)
                new_image[i * width + j] = 0;
        }
    }

    for(i = 0; i < height; i++)
    {
        for(j = 0; j < width; j++)
        {
            (img1->imageData + i * img1->widthStep)[j] = (unsigned char)new_image[i * width + j];
        }
    }
    */
    
	smoothed_img = cvCreateImage(cvSize(img1->width, img1->height), IPL_DEPTH_8U, 1);    
	cvSmooth(img1,smoothed_img,CV_GAUSSIAN,77,0,135);
    q_img = cvCreateImage(cvSize(img1->width, img1->height), IPL_DEPTH_32F, 1);
    SQI_img = cvCreateImage(cvSize(img1->width, img1->height), IPL_DEPTH_8U, 1);    
	max=0;
	min=1000;


	//form quotient image
	for (i=0; i<img1->height; i++)
       for (j=0; j<img1->width; j++)
	   {
		   //get pixel from input image
		   temp=((uchar *)(img1->imageData + i*img1->widthStep))[j];
	       //divide by 255 to make values range from 0.0-1.0 
		   temp_x=(float)temp/255;
		   
		   //get pixel from smoothed image
		   temp_s=((uchar *)(smoothed_img->imageData + i*smoothed_img->widthStep))[j];
	       //divide by 255 to make values range from 0.0-1.0 
		   temp_y=(float)temp_s/255;
		  
           temp_z=temp_x/temp_y;

		   //divide the two pixel values
		   ((float *)(q_img->imageData + i*q_img->widthStep))[j]=temp_z;
	   
           //find max and min of q_img
		   if (temp_z > max)
			   max=temp_z;
		   if (temp_z < min)
			   min=temp_z;
	   }
    
   factor=(max-min);

	//normalize q_image
	for (i=0; i<q_img->height; i++)
       for (j=0; j<q_img->width; j++)
	   {   
		   temp_x =((float *)(q_img->imageData + i*q_img->widthStep))[j];
	       temp_x = temp_x - min;
		   temp_x = temp_x / factor;
	       ((float *)(q_img->imageData + i*q_img->widthStep))[j]=temp_x;
	   }

       //find mean of the image
	   temp_x=0;
  for (i=0; i<q_img->height; i++)
       for (j=0; j<q_img->width; j++)
	   {   
		   temp_x +=((float *)(q_img->imageData + i*q_img->widthStep))[j];
	   }
	   
	   mean=temp_x/(q_img->height*q_img->width);
       
//final normalize q_image
	for (i=0; i<q_img->height; i++)
       for (j=0; j<q_img->width; j++)
	   {   
		   temp_x =((float *)(q_img->imageData + i*q_img->widthStep))[j];
            temp_x = (-1)*temp_x/mean;
		   temp_x = 1-exp((double)temp_x);
             ((float *)(q_img->imageData + i*q_img->widthStep))[j]=temp_x;
	   }
    


  //convert back to uint8
for (i=0; i<q_img->height; i++)
       for (j=0; j<q_img->width; j++)
	   {
		   //get pixel from quotient image
		   temp_x =((float *)(q_img->imageData + i*q_img->widthStep))[j];
	       
		   temp=(uchar)(temp_x*255);
           
		   ((uchar *)(img1->imageData + i*img1->widthStep))[j]=temp;
	   }


 for(i = 0; i < height; i++)
    {
        for(j = 0; j < width; j++)
        {
            temp = (img1->imageData + i * img1->widthStep)[j];
            // temp +=1;
            //    new_image[i * width + j] = (y[(int)temp]);
              new_image[i * width + j] = temp;
        }
    }


 //cvSmooth( img1, img1,CV_GAUSSIAN,3,0,0,0 );

 //gamma correction 
    for(i = 0; i < height; i++)
    {
        for(j = 0; j < width; j++)
        {
            //temp = (new_image->imageData + i * new_image->widthStep)[j];
	  new_image[i * width + j] =pow(new_image[i * width + j] , 1/3.5);
 
        }
    }

    //contrast stretch and convert to uint8
    max_val = 0;
    min_val = 10;

    for(i = 0; i < height; i++)
    {
        for(j = 0; j < width; j++)
        {
            temp_float = new_image[i * width + j];

            if(temp_float > max_val)
                max_val = temp_float;

            if(temp_float < min_val)
                min_val = temp_float;
        }
    }

    Factor = floor((255) / (max_val - min_val));

    //subtract min value and multiply Factor
    for(i = 0; i < height; i++)
    {
        for(j = 0; j < width; j++)
        {
            temp_float = new_image[i * width + j];
            new_image[i * width + j] = temp_float - min_val;

            temp_float = new_image[i * width + j];
            new_image[i * width + j] = (float)floor(temp_float * Factor);

            if(new_image[i * width + j] > 255)
                new_image[i * width + j] = 255;

            if(new_image[i * width + j] < 0)
                new_image[i * width + j] = 0;
        }
    }

    for(i = 0; i < height; i++)
    {
        for(j = 0; j < width; j++)
        {
            (img1->imageData + i * img1->widthStep)[j] = (unsigned char)new_image[i * width + j];
        }
    }
    

    cvReleaseImage(&smoothed_img);
    cvReleaseImage(&q_img);
    cvReleaseImage(&SQI_img);

    free(new_image);
    return img1;
}

IplImage* CROP_EYE (IplImage *img_in, int x_crop_start, int y_crop_start, int size)
{

	static int i,j;
	IplImage        *img_temp = 0;
	img_temp = cvCreateImage( cvSize(size, size), IPL_DEPTH_8U, 1 );
	//crop out eye area
	 for( i = y_crop_start; i < y_crop_start + size; i++ ) {
		 for( j = x_crop_start; j < x_crop_start + size ; j++ ) {

		(img_temp->imageData + (int)(i-y_crop_start) * img_temp ->widthStep)[(int)(j-x_crop_start)]=(img_in->imageData + i * img_in ->widthStep)[j];
		 }
	 
	 }

return img_temp;
cvReleaseImage( &img_temp );

}





/**
 *Preforms lighting normalization on an input image in low light
 * @param IplImage* img_in The image to be normalized
 * @param int width The width of the image
 * @param int height The height of the image
 */


/**
 *Preforms eye detection on an image
 * @param IplImage* input_img The croped face image to do eye detect ion
 * @param int* x_pos_left The returned x position of the left eye
 * @param int* x_pos_right The returned x position of the right eye
 * @param int* y_pos_left The returned y position of the left eye
 * @param int* y_pos_right The returned y position of the right eye
 */
void eye_detect(IplImage *input_img, int *x_pos_left, int *x_pos_right, int *y_pos_left, int *y_pos_right, int *x1_pos_left, int *x1_pos_right, int *y1_pos_left, int *y1_pos_right)
{
    
    int x_pos_left0;
    int y_pos_left0;
    int x_pos_left1;
    int y_pos_left1;
    
    int x_pos_right0;
    int y_pos_right0;
    int x_pos_right1;
    int y_pos_right1;
    
    int y_pos_left_gt;
    int x_pos_left_gt;
    int y_pos_right_gt;
    int x_pos_right_gt;
    
    int y_pos_left_gt1;
    int x_pos_left_gt1;
    int y_pos_right_gt1;
    int x_pos_right_gt1;
    
    int y_pos_left_gttemp;
    int x_pos_left_gttemp;
    int y_pos_right_gttemp;
    int x_pos_right_gttemp;
    
    
    
    int second_peak_left=0;
    int second_peak_right=0;
    
    double max_left = -1000000000;
    double max_left1 = -1000000000;
    double max_right = -1000000000;
    double max_right1 = -1000000000;
    
    float temp_left;
    float temp1_left;
    float temp_right;
    float temp1_right;
    
    
    int WIPE_OUT_SIZE=5;
    
    int WIPE_OUT_X_START=0;
    int WIPE_OUT_Y_START=0;
    int WIPE_OUT_X_FINISH=0;
    int WIPE_OUT_Y_FINISH=0;
    
    int COLUMN_OFFSET=-8;
    
    float PERCENTAGE_Y=0.80;
    float PERCENTAGE_X=1-PERCENTAGE_Y;
    // float PERCENTAGE_THRESHOLD=0.65;
    // float CHANGE_THRESHOLD=.1;
    float PERCENTAGE_THRESHOLD=0.65;
    float CHANGE_THRESHOLD=.0001;
    int i, j, k;
    
    IplImage *blurred_image = NULL;
    IplImage *img1          = NULL;
    IplImage *img_light     = NULL;
    
    float  *img3;
    char   *input_img_data;
    int    width, height, step;
    int    new_width, new_height, new_step;
    int    rows_half, columns_half;
    double temp1_d, temp2_d, temp3_d, temp4_d;
    double dd;
    double temp;
    
    float left_eye_crop_x_start;
    float left_eye_crop_y_start;
    float right_eye_crop_x_start;
    float right_eye_crop_y_start;
    int   ptr_var = 0;
    
    
    
    fftw_complex *data_in;
    fftw_plan    plan_f;
    fftw_plan    plan_r;
    fftw_plan    plan_eye;
    fftw_complex *fft;
    fftw_complex *ifft;

    fftw_complex *eye_in;
    fftw_complex *eye_out;

    width  = 64;
    height = 64;

    // initialize arrays for fftw operations
    data_in  = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * width * height);
    fft      = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * width * height);
    ifft     = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * width * height);
    eye_in   = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * width * height);
    eye_out  = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * width * height);
    
    plan_f   = fftw_plan_dft_1d(64 * 64, data_in, fft, FFTW_FORWARD, FFTW_ESTIMATE);
    plan_eye = fftw_plan_dft_1d(64 * 64, eye_in, eye_out, FFTW_FORWARD, FFTW_ESTIMATE);
    plan_r   = fftw_plan_dft_1d(64 * 64, fft, ifft, FFTW_BACKWARD, FFTW_ESTIMATE);
    
    img3     = (float*)malloc(sizeof(float) * 64 * 64);
    
    // load original image
    blurred_image  = cvCreateImageHeader(cvSize(input_img->width,
                                                input_img->height), IPL_DEPTH_8U, 1);
    blurred_image->widthStep = input_img->width;
    input_img_data = (char*)malloc(input_img->width * input_img->height * sizeof(char));
    int index = 0;
    
    for(i = 0; i < input_img->height; i++)
      {
        for(j = 0; j < input_img->width; j++)
	  {
            input_img_data[index] = CV_IMAGE_ELEM(input_img, uchar, i, j);
            index++;
	  }
      }
    
    blurred_image->imageData = input_img_data;
    
    // get image properties
    width  = blurred_image->width;
    height = blurred_image->height;
    step   = blurred_image->widthStep;
    
    // normalize
    img_light = LIGHTING_NORMALIZATION(blurred_image, width, height);
    
    // form blank new image
    img1 = cvCreateImage(cvSize(64, 64), IPL_DEPTH_8U, 1);
    
    // get new image properties
    new_width  = img1->width;
    new_height = img1->height;
    new_step   = img1->widthStep;
    
    ////////////////// LEFT EYE ///////////////////////////////////
    
    // crop left eye out or original image
    left_eye_crop_x_start = floor(width * .1);
    left_eye_crop_y_start = floor(height * .15);
    
    static int size_crop=64;
    
    img1=CROP_EYE (blurred_image, (int) left_eye_crop_x_start, (int) left_eye_crop_y_start, size_crop);
    
    
    img1=LIGHTING_NORMALIZATION(img1, new_width, new_height);
      
      //cvNamedWindow( "CROP_LEFT", CV_WINDOW_AUTOSIZE );
      //cvShowImage( "CROP_LEFT", img1 ); 	
      
      // cvWaitKey( 0 ); 
      
      cvNamedWindow("CROP",CV_WINDOW_AUTOSIZE);
    cvShowImage("CROP",img1);
    // cvWaitKey(0);
    
    // load eye data to fftw input
    for(i = 0, k = 0; i < new_height; i++)
      {
        for(j = 0; j < new_width; j++)
	  {
            data_in[k][0] = (double)((uchar*)(img1->imageData + i * img1->widthStep))[j];
            data_in[k][1] = 0.0;
            k++;
	  }
      }
    
    // perform FFT
    fftw_execute(plan_f);
    
    ptr_var = 0;
    
    // load eye filter data
    for(i = 0, k = 0; i < new_height; i++)
      {
        for(j = 0; j < new_width; j++)
	  {
            eye_out[k][0] = (double)eye_data[ptr_var];
            ptr_var++;
            eye_out[k][1] = (double)eye_data[ptr_var];
            k++;
            ptr_var++;
	  }
      }
    
    // perfrom correlation
    
    //eye filter already preprocessed with hanning window and conjugated
    for(k = 0; k < new_height * new_width; k++)
      {
        fft[k][0] = ((fft[k][0] * eye_out[k][0]) - (fft[k][1] * eye_out[k][1]));
        fft[k][1] = ((fft[k][0] * eye_out[k][1]) + (fft[k][1] * eye_out[k][0]));
      }
    
    // perfrom ifft
    fftw_execute(plan_r);
    
    // normalize IFFT result
    // copy IFFT result to img_float's data
    
    //added abs to get rid of errors in conversion
    for(i = 0, k = 0; i < new_height; i++)
      {
        for(j = 0; j < new_width; j++)
	  {
            (img3[i * new_width + j]) = ((float)ifft[i * new_width + j][0]);
	  }
      }
    
    //IFFT SHIFT
    //!!!!image rows and columns have to be even!!!!!
    
    //int rows_half, columns_half;
    rows_half    = new_height / 2;
    columns_half = new_width / 2;
    
    for(i = 0; i < 32; i++)
      {
        for(j = 0; j < 32; j++)
	  {
            temp1_d = img3[(i * new_width) + j];
            temp2_d = img3[(i * new_width) + (j + columns_half)];
            temp3_d = img3[((i + rows_half) * new_width) + (j + columns_half)];
            temp4_d = img3[((i + rows_half) * new_width) + j];
	    
            //1=3
            img3[(i * new_width) + j] = temp3_d;
	    
            //2=4
            img3[(i * new_width) + (j + columns_half)] = temp4_d;
	    
            //3=1
            img3[((i + rows_half) * new_width) + (j + columns_half)] = temp1_d;

            //4=2
            img3[((i + rows_half) * new_width) + j] = temp2_d;
	  }
    }
    
    //set borders to 0
    for(i = 0; i < new_height; i++)
      {
        for(j = 0; j < new_width; j++)
	  {
            if(i > 10 && i < 50 && j > 10 && j < 50)
	      dd = 0;
            else
	      img3[i * new_width + j] = 0;
	  }
      }

    max_left = -1000000000;
    
    for(i = 0; i < new_height; i++)
      {
        for(j = 0; j < new_width; j++)
	  {
            temp = img3[i * new_width + j];
	    
            if(temp > max_left)
	      {
                max_left = temp;
                *y_pos_left = i;
                *x_pos_left = j;
                y_pos_left0=i;
                x_pos_left0=j;
	      }
	  }
      }
    
    *x_pos_left = *x_pos_left;
    *y_pos_left = *y_pos_left;
    
    x_pos_left_gt=*x_pos_left + (int)left_eye_crop_x_start;
    y_pos_left_gt=*y_pos_left + (int)left_eye_crop_y_start;    
    
    
    WIPE_OUT_Y_START=(y_pos_left0-WIPE_OUT_SIZE);
    if (WIPE_OUT_Y_START < 0)
      WIPE_OUT_Y_START=0;
    
    WIPE_OUT_Y_FINISH=(y_pos_left0+WIPE_OUT_SIZE);
    if (WIPE_OUT_Y_FINISH > 63)
      WIPE_OUT_Y_FINISH=63;
    
    WIPE_OUT_X_START=(x_pos_left0-WIPE_OUT_SIZE);
    if (WIPE_OUT_X_START < 0)
      WIPE_OUT_X_START=0;
    
    WIPE_OUT_X_FINISH=(x_pos_left0+WIPE_OUT_SIZE);
    if (WIPE_OUT_X_FINISH > 63)
      WIPE_OUT_X_FINISH=63;
    
    //delete region
    for(i = WIPE_OUT_Y_START; i < WIPE_OUT_Y_FINISH; i++)
      {
        for(j = WIPE_OUT_X_START; j < WIPE_OUT_X_FINISH; j++)
	  {
	    img3[i * new_width + j] = 0;
	  }
      }
    
    //find second peak if there is one
    max_left1 = -1000000000;
    
    for(i = 0; i < new_height; i++)
      {
        for(j = 0; j < new_width; j++)
	  {
            temp = img3[i * new_width + j];
	    
            if(temp > max_left1)
	      {
                max_left1 = temp;
                y_pos_left1=i;
                x_pos_left1=j;
	      }
	  }
      }
    
    x_pos_left_gt1=x_pos_left1 + (int)left_eye_crop_x_start;
    y_pos_left_gt1=y_pos_left1 + (int)left_eye_crop_y_start;    
    
    if (max_left1 > max_left*PERCENTAGE_THRESHOLD)    
      second_peak_left=1;
    else
      second_peak_left=0;
    
    
    ////////////////// RIGHT EYE ///////////////////////////////////
    int error_x;
    int error_y;
    
    //crop out right eye search area
    error_x = ((width / 2 +COLUMN_OFFSET) + 64) - width;
    error_y = (*y_pos_left -20 + 64) - height;
    
    if(error_x < 0)
      error_x = 0;

    if(error_y < 0)
        error_y = 0;

    right_eye_crop_x_start = (width / 2 + COLUMN_OFFSET) + error_x;
    right_eye_crop_y_start = (*y_pos_left-20) + error_y;
    
    img1=CROP_EYE (blurred_image, (int) right_eye_crop_x_start, (int) right_eye_crop_y_start, size_crop);
    
    
    img1=LIGHTING_NORMALIZATION(img1, new_width, new_height);
	
    cvNamedWindow("CROP1",CV_WINDOW_AUTOSIZE);
    cvShowImage("CROP1",img1);
    //  cvWaitKey(0);
    // load eye data to fftw input
    for(i = 0, k = 0; i < new_height; i++)
      {
        for(j = 0; j < new_width; j++)
	  {
            data_in[k][0] = (double)((uchar*)(img1->imageData + i * img1->widthStep))[j];
            data_in[k][1] = 0.0;
            k++;
	  }
      }
    
    // perform FFT
    fftw_execute(plan_f);
    
    ptr_var = 0;
    
    // load eye filter data
    for(i = 0, k = 0; i < new_height; i++)
      {
        for(j = 0; j < new_width; j++)
	  {
            eye_out[k][0] = (double)eye_data_right[ptr_var];
            ptr_var++;
            eye_out[k][1] = (double)eye_data_right[ptr_var];
            k++;
            ptr_var++;
	  }
      }
    
    // perfrom correlation
    
    //eye filter already preprocessed with hanning window and conjugated
    for(k = 0; k < new_height * new_width; k++)
      {
        fft[k][0] = ((fft[k][0] * eye_out[k][0]) - (fft[k][1] * eye_out[k][1]));
        fft[k][1] = ((fft[k][0] * eye_out[k][1]) + (fft[k][1] * eye_out[k][0]));
      }
    
    // perfrom ifft
    fftw_execute(plan_r);
    
    // normalize IFFT result
    
    // copy IFFT result to img_float's data
    
    //added abs to get rid of errors in conversion
    for(i = 0, k = 0; i < new_height; i++)
      {
        for(j = 0; j < new_width; j++)
	  (img3[i * new_width + j]) = ((float)ifft[i * new_width + j][0]);
      }
    
    //IFFT SHIFT
    //!!!!image rows and columns have to be even!!!!!
    
    //int rows_half, columns_half;
    rows_half    = new_height / 2;
    columns_half = new_width / 2;
    
    for(i = 0; i < 32; i++)
      {
        for(j = 0; j < 32; j++)
	  {
            temp1_d = img3[(i * new_width) + j];
            temp2_d = img3[(i * new_width) + (j + columns_half)];
            temp3_d = img3[((i + rows_half) * new_width) + (j + columns_half)];
            temp4_d = img3[((i + rows_half) * new_width) + j];
	    
            //1=3
            img3[(i * new_width) + j] = temp3_d;
	    
            //2=4
            img3[(i * new_width) + (j + columns_half)] = temp4_d;
	    
            //3=1
            img3[((i + rows_half) * new_width) + (j + columns_half)] = temp1_d;
	    
            //4=2
            img3[((i + rows_half) * new_width) + j] = temp2_d;
	  }
      }
    
    //set borders to 0
    for(i = 0; i < new_height; i++)
      {
        for(j = 0; j < new_width; j++)
	  {
            if(i > 10 && i < 50 && j > 10 && j < 50)
	      dd = 0;
            else
	      img3[i * new_width + j] = 0;
	  }
      }
    
    max_right = -1000000000;
    
    for(i = 0; i < new_height; i++)
      {
        for(j = 0; j < new_width; j++)
	  {
            temp = img3[i * new_width + j];
	    
            if(temp > max_right)
	      {
                max_right = temp;
                *y_pos_right = i;
                *x_pos_right = j;
                y_pos_right0=i;
                x_pos_right0=j;
	      }
	  }
      }
    
    *x_pos_right = *x_pos_right;
    *y_pos_right = *y_pos_right;
    
    x_pos_right_gt=*x_pos_right + (int)right_eye_crop_x_start;
    y_pos_right_gt=*y_pos_right + (int)right_eye_crop_y_start;
    
    
    WIPE_OUT_Y_START=(y_pos_right0-WIPE_OUT_SIZE);
    if (WIPE_OUT_Y_START < 0)
      WIPE_OUT_Y_START=0;
    
    WIPE_OUT_Y_FINISH=(y_pos_right0+WIPE_OUT_SIZE);
    if (WIPE_OUT_Y_FINISH > 63)
      WIPE_OUT_Y_FINISH=63;
    
    WIPE_OUT_X_START=(x_pos_right0-WIPE_OUT_SIZE);
    if (WIPE_OUT_X_START < 0)
      WIPE_OUT_X_START=0;
    
    WIPE_OUT_X_FINISH=(x_pos_right0+WIPE_OUT_SIZE);
    if (WIPE_OUT_X_FINISH > 63)
      WIPE_OUT_X_FINISH=63;
    
    //delete region
    for(i = WIPE_OUT_Y_START; i < WIPE_OUT_Y_FINISH; i++)
      {
        for(j = WIPE_OUT_X_START; j < WIPE_OUT_X_FINISH; j++)
	  {
	    img3[i * new_width + j] = 0;
	  }
      }
    
    //find second peak if there is one
    max_right1 = -1000000000;
    
    for(i = 0; i < new_height; i++)
      {
        for(j = 0; j < new_width; j++)
	  {
            temp = img3[i * new_width + j];
	    
            if(temp > max_right1)
            {
	      max_right1 = temp;
	      y_pos_right1=i;
	      x_pos_right1=j;
            }
	  }
      }
    
    x_pos_right_gt1=x_pos_right1 + (int)right_eye_crop_x_start;
    y_pos_right_gt1=y_pos_right1 + (int)right_eye_crop_y_start;    
    
    if (max_right1 > max_right*PERCENTAGE_THRESHOLD)    
      second_peak_right=1;
    else
      second_peak_right=0;
    
    
    float offset_right=0;
    float offset_left=0;
    float offset_right1=0;
    float offset_left1=0;
    float offset=0;
    offset_left= (floor(width/2)-x_pos_left_gt)+floor(width/2)+offset;
    offset_right = (floor(width/2)-(x_pos_right_gt - floor(width/2)))-offset;
    offset_left1= (floor(width/2)-x_pos_left_gt1)+floor(width/2)+offset;
    offset_right1 = (floor(width/2)-(x_pos_right_gt1 - floor(width/2)))-offset;
    
    //  fprintf(stderr,"width/2: %f offset_left: %f  offset_left1: %f  offset_right: %f offset_right1: %f\n",floor(width/2),offset_left,offset_left1,offset_right,offset_right1);
      // fprintf(stderr,"xpos_left: %d x_pos_left_gt1: %d x_pos_right_gt: %d x_pos_right_gt1: %d\r\n",x_pos_left_gt,x_pos_left_gt1,x_pos_right_gt,x_pos_right_gt1);
    
    //  fprintf(stderr,"max_left: %f max_right: %f max_left1: %f  max_right1: %f Second LEFT: %d Second RIGHT: %d\r\n",max_left,max_right,max_left1, max_right1,second_peak_left,second_peak_right);
     // cvWaitKey(0);
    /////////////////////////////////////////////////////////
    //////SEARCH FOR EYE WITH LOWEST MAX VALUE///////////////
    /////////////////////////////////////////////////////////
    if (max_left < max_right)
      {
	
	//research for left eye
        if (second_peak_left==1)
	  {
	    
	    
	    temp_left=(abs((float)y_pos_right_gt - (float)y_pos_left_gt)*PERCENTAGE_Y);
            temp1_left=(abs((float)y_pos_right_gt - (float)y_pos_left_gt1)*PERCENTAGE_Y) ;
	    
            temp_left+=abs(((offset_left) - (float) x_pos_right_gt )*PERCENTAGE_X) ;
            temp1_left+=abs(((offset_left1) -(float) x_pos_right_gt)*PERCENTAGE_X) ;
	    
	    //   fprintf(stderr,"LEFT EYE: temp_left: %f   temp1_left: %f\r\n",temp_left,temp1_left);
	    
            if (temp1_left < ((temp_left - (temp_left * CHANGE_THRESHOLD))))
	      {
		
                x_pos_left_gttemp=x_pos_left_gt;
                y_pos_left_gttemp=y_pos_left_gt;
		
                x_pos_left_gt=x_pos_left_gt1;
                y_pos_left_gt=y_pos_left_gt1;
		
                x_pos_left_gt1=x_pos_left_gttemp;
                y_pos_left_gt1=y_pos_left_gttemp;
                offset_left= (floor(width/2)-x_pos_left_gt)+floor(width/2);
                offset_left1= (floor(width/2)-x_pos_left_gt1)+floor(width/2);
		//		fprintf(stderr,"CHANGED LEFT\r\n");
	      }
	    
	  }
	    //research right eye based on left
	    if (second_peak_right==1)
	      {
		
		temp_right=(abs((float)y_pos_left_gt - (float)y_pos_right_gt)*PERCENTAGE_Y)  ;
		temp1_right=(abs((float)y_pos_left_gt - (float)y_pos_right_gt1)*PERCENTAGE_Y) ;
		
		
		temp_right+= abs((offset_right  - (float)x_pos_left_gt)*PERCENTAGE_X) ;
		temp1_right+= abs((offset_right1 - (float)x_pos_left_gt)*PERCENTAGE_X);
		
		//	fprintf(stderr,"RIGHT EYE SECOND AFTER LEFT: temp_right: %f   temp1_right: %f\r\n",temp_right,temp1_right);
					
		if (temp1_right < (temp_right - ((temp_right * CHANGE_THRESHOLD))))
		  {
		    //    fprintf(stderr,"CHANGED COORDINATES!!!!!!!!!!!!!!!\n");
		    x_pos_right_gttemp=x_pos_right_gt;
		    y_pos_right_gttemp=y_pos_right_gt;
		    
		    x_pos_right_gt=x_pos_right_gt1;
		    y_pos_right_gt=y_pos_right_gt1;
		    
		    x_pos_right_gt1=x_pos_right_gttemp;
		    y_pos_right_gt1=y_pos_right_gttemp;
		    offset_right = (floor(width/2)-(x_pos_right_gt - floor(width/2)))-offset;
		    offset_right1 = (floor(width/2)-(x_pos_right_gt1 - floor(width/2)))-offset;
		    //  fprintf(stderr,"CHANGED RIGHT 2nd\r\n");
		  }
	      }  
      }
    
    
    if (max_left >=  max_right)
      {
	
	//research right eye
	if (second_peak_right==1)
	  {
	    
	    
	    //   fprintf(stderr,"SECOND PEAK RIGHT SEARCH!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\r\n");
	    temp_right=(abs((float)y_pos_left_gt - (float)y_pos_right_gt)*PERCENTAGE_Y)  ;
	    temp1_right=(abs((float)y_pos_left_gt - (float)y_pos_right_gt1)*PERCENTAGE_Y) ;
	    
	    
	    temp_right+= abs((offset_right - (float)x_pos_left_gt)*PERCENTAGE_X) ;
	    temp1_right+= abs((offset_right1 - (float)x_pos_left_gt)*PERCENTAGE_X);
	    
	    
	    //   fprintf(stderr,"RIGHT EYE SECOND: temp_right: %f   temp1_right: %f\r\n",temp_right,temp1_right);
	    
	    
	    if (temp1_right < (temp_right - ((temp_right * CHANGE_THRESHOLD))))
	      {
		
		x_pos_right_gttemp=x_pos_right_gt;
		y_pos_right_gttemp=y_pos_right_gt;
		
		x_pos_right_gt=x_pos_right_gt1;
		y_pos_right_gt=y_pos_right_gt1;
		
		x_pos_right_gt1=x_pos_right_gttemp;
		y_pos_right_gt1=y_pos_right_gttemp;
		offset_right = (floor(width/2)-(x_pos_right_gt - floor(width/2)))+offset;
		offset_right1 = (floor(width/2)-(x_pos_right_gt1 - floor(width/2)))+offset;
		//	fprintf(stderr,"CHANGED RIGHT\r\n");
	      }
	    
	  }
	
	//research left  eye based on right
	if (second_peak_left==1)
	  {
	    
	    temp_left=(abs(y_pos_right_gt - y_pos_left_gt)*PERCENTAGE_Y) + (((64-x_pos_left0) - x_pos_right0  - COLUMN_OFFSET)*PERCENTAGE_X) ;
	    temp1_left=(abs(y_pos_right_gt - y_pos_left_gt1)*PERCENTAGE_Y) + (((64-x_pos_left1) - x_pos_right0 - COLUMN_OFFSET)*PERCENTAGE_X) ;
	    
	    temp_left=(abs((float)y_pos_right_gt - (float)y_pos_left_gt)*PERCENTAGE_Y);
	    temp1_left=(abs((float)y_pos_right_gt - (float)y_pos_left_gt1)*PERCENTAGE_Y) ;
	    
	    temp_left+=abs((offset_left - (float)x_pos_right_gt )*PERCENTAGE_X) ;
	    temp1_left+=abs ((offset_left1 - (float)x_pos_right_gt)*PERCENTAGE_X) ;
	    
	    
	    // fprintf(stderr,"LEFT EYE SECOND AFTER RIGHT: temp_left: %f   temp1_left: %f\r\n",temp_left,temp1_left);
	    
	    
	    
	    if (temp1_left < ((temp_left - (temp_left * CHANGE_THRESHOLD))))
	      {
		x_pos_left_gttemp=x_pos_left_gt;
		y_pos_left_gttemp=y_pos_left_gt;
		
		x_pos_left_gt=x_pos_left_gt1;
		y_pos_left_gt=y_pos_left_gt1;
		
		x_pos_left_gt1=x_pos_left_gttemp;
		y_pos_left_gt1=y_pos_left_gttemp;
		//	fprintf(stderr,"CHANGED LEFT 2nd\r\n");
	      }
	  }        
      }

    
    int eye_pos_threshold=50;

    if (second_peak_left==1 && second_peak_right==1)
      {
	if (abs(x_pos_left_gt - x_pos_right_gt) < eye_pos_threshold && (abs(x_pos_left_gt1 - x_pos_right_gt1) >= eye_pos_threshold))
	  {
	  	x_pos_left_gttemp=x_pos_left_gt;
		y_pos_left_gttemp=y_pos_left_gt;
		
		x_pos_left_gt=x_pos_left_gt1;
		y_pos_left_gt=y_pos_left_gt1;
		
		x_pos_left_gt1=x_pos_left_gttemp;
		y_pos_left_gt1=y_pos_left_gttemp;
		
		x_pos_right_gttemp=x_pos_right_gt;
		y_pos_right_gttemp=y_pos_right_gt;
		
		x_pos_right_gt=x_pos_right_gt1;
		y_pos_right_gt=y_pos_right_gt1;
		
		x_pos_right_gt1=x_pos_right_gttemp;
		y_pos_right_gt1=y_pos_right_gttemp;

	  }
      }

    if (second_peak_left==1 && second_peak_right==0)
      {
      	if (abs(x_pos_left_gt - x_pos_right_gt) < eye_pos_threshold && (abs(x_pos_left_gt1 - x_pos_right_gt) >= eye_pos_threshold ))
	  {
	    x_pos_left_gttemp=x_pos_left_gt;
	    y_pos_left_gttemp=y_pos_left_gt;
	    
	    x_pos_left_gt=x_pos_left_gt1;
	    y_pos_left_gt=y_pos_left_gt1;
	    
	    x_pos_left_gt1=x_pos_left_gttemp;
	    y_pos_left_gt1=y_pos_left_gttemp;
	  }
      }
    
    if (second_peak_left==0 && second_peak_right==1)
      {
      	if (abs(x_pos_left_gt - x_pos_right_gt) < eye_pos_threshold  && (abs(x_pos_left_gt - x_pos_right_gt1) >= eye_pos_threshold))
	  {
	    x_pos_right_gttemp=x_pos_right_gt;
	    y_pos_right_gttemp=y_pos_right_gt;
	    
	    x_pos_right_gt=x_pos_right_gt1;
	    y_pos_right_gt=y_pos_right_gt1;
	    
	    x_pos_right_gt1=x_pos_right_gttemp;
	    y_pos_right_gt1=y_pos_right_gttemp;
	    
	  }
      }
    
    
    //	cvWaitKey(0);
	y_pos_left_gt+=4;
	y_pos_right_gt+=4;
	y_pos_left_gt1+=4;
	y_pos_right_gt1+=4;
	
	//	x_pos_left_gt-=2;
	x_pos_right_gt-=2;
	//	x_pos_left_gt1-=2;
	x_pos_right_gt1-=2;

        int y_diff1=0;
	int y_diff2=0;
	int x_diff1=0;
	int x_diff2=0;
       
	*x_pos_left  = x_pos_left_gt;
	*y_pos_left  = y_pos_left_gt;
	*x_pos_right = x_pos_right_gt;
	*y_pos_right = y_pos_right_gt;
	
	*x1_pos_left  = x_pos_left_gt1;
	*y1_pos_left  = y_pos_left_gt1;
	*x1_pos_right = x_pos_right_gt1;
	*y1_pos_right = y_pos_right_gt1;
	
	
	//cvDestroyWindow("CROP");
	//cvDestroyWindow("CROP1");
	CvFont font;
	CvPoint LEFT_pt=cvPoint(x_pos_left_gt,y_pos_left_gt);
	CvPoint RIGHT_pt=cvPoint(x_pos_right_gt,y_pos_right_gt);
	cvInitFont(&font, CV_FONT_HERSHEY_COMPLEX,0.25,0.25,0,1,CV_AA);
	cvPutText(input_img,"X",LEFT_pt,&font,CV_RGB(0xff,0xff,0xff));
	cvPutText(input_img,"X",RIGHT_pt,&font,CV_RGB(0xff,0xff,0xff));
	
	CvFont font1;
	CvPoint LEFT_pt1=cvPoint(x_pos_left_gt1,y_pos_left_gt1);
	CvPoint RIGHT_pt1=cvPoint(x_pos_right_gt1,y_pos_right_gt1);
	cvInitFont(&font1, CV_FONT_HERSHEY_COMPLEX,0.25,0.25,0,1,CV_AA);
	if (second_peak_left==1)
	cvPutText(input_img,"O",LEFT_pt1,&font1,CV_RGB(0xff,0xff,0xff));
	if (second_peak_right==1)
	cvPutText(input_img,"O",RIGHT_pt1,&font1,CV_RGB(0xff,0xff,0xff));
	
	
	
	
	cvNamedWindow("RESIZE",CV_WINDOW_AUTOSIZE);
	cvShowImage("RESIZE",input_img);
	//cvWaitKey(0);
	cvReleaseImage(&img1);
	free(blurred_image->imageData);
	cvReleaseImageHeader(&blurred_image);
	cvReleaseImage(&img_light);
	
	fftw_free(data_in);
	fftw_free(fft);
	fftw_free(ifft);
	fftw_free(eye_in);
	fftw_free(eye_out);
	
	fftw_destroy_plan(plan_f);
	fftw_destroy_plan(plan_eye);
	fftw_destroy_plan(plan_r);
	
	free(img3);
     
}
