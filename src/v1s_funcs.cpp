// v1s_funcs.cpp
// Copyright 2009 Securics, Inc

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "v1s_funcs.h"
#include "iplstr.h"
#include <stdio.h>
#include <math.h>

static IplImage* trim(IplImage* in, CvSize newsize)
{
    int trim_w = in->width - newsize.width;
    int trim_h = in->height - newsize.height;

    if(trim_w < 0 || trim_h < 0)
        return NULL;

    int start_x = (int)v1_floor((v1_float)trim_w / 2.0);
    int end_x   = in->width - (trim_w - start_x);
    int start_y = (int)v1_floor((v1_float)trim_h / 2.0);
    int end_y   = in->height - (trim_h - start_y);

    int x, y, tx, ty;

    IplImage* trimmed = cvCreateImage(newsize, in->depth, in->nChannels);
    if(trimmed == NULL)
        return NULL;

    for(ty = 0, y = start_y; y < end_y; ty++, y++)
    {
        for(tx = 0, x = start_x; x < end_x; tx++, x++)
            CV_IMAGE_ELEM(trimmed, v1_float, ty, tx) =  CV_IMAGE_ELEM(in, v1_float, y, x);
    }

    return trimmed;
}

// Ambiguous enough for ya? Takes a 3d array and trims the 1st and 2nd
// dimensions leaving the 3rd in tact.
static void trim3d2d(IplImage** in, int depth, CvSize newsize)
{
    int i;
    for(i = 0; i < depth; i++)
    {
        IplImage* tmp = in[i];
        if(tmp != NULL)
        {
            in[i] = trim(tmp, newsize);
            cvReleaseImage(&tmp);
        }
    }
}

static int do_convolve2d(IplImage *src, IplImage* dst, IplImage *kernel, convolve_mode mode)
{
    int ret = convolve2d(src, dst, kernel, mode);

    switch(ret)
    {
    case -1:
        fprintf(stderr, "Convolve error: Convolve parameter is null.\n");
        break;
    case -2:
        fprintf(stderr, "Convolve error: Source and destination images differ in size.\n");
        break;
    case -3:
        fprintf(stderr, "Convolve error: Kernel image is larger than source image.\n");
        break;
    case -4:
        fprintf(stderr, "Convolve error: Valid region is too small.\n");
        break;
    case -99:
        fprintf(stderr, "Convolve error: Unknown or unhandled mode.\n");
        break;
    }

    return ret;
}

void v1s_norm(IplImage** hin,IplImage** hout, int kh, int kw, v1_float threshold, int hin_d)
{
    /*""" V1S local normalization
    
    Each pixel in the input image is divisively normalized by the L2 norm
    of the pixels in a local neighborhood around it, and the result of this
    division is placed in the output image.   
    
    Inputs:
      hin -- a 3-dimensional array (width X height X depth)
      kh kw -- kernel shape (tuple) ex: (3,3) for a 3x3 normalization 
                neighborhood
      threshold -- magnitude threshold, if the vector's length is below 
                   it doesn't get resized ex: 1.    
     
    Outputs:
      hout -- a normalized 3-dimensional array (width X height X depth)
      
    """
    */

    v1_float eps = 1e-5;
 
    // -- prepare hout
    int i, x, y;

    // -- compute numerator (hnum) and divisor (hdiv)
    // sum kernel

    //ker = N.ones(kshape3d, dtype=dtype);
    //Below accomplishes the ones command above, it was desinged for IplImage Passing, I think this is very poor but too late in game
    //Questions on improvement are obvious, and Know this can be greatly improved once it is proven, and time is no longer a concern
    //Dynamic allocation is done but at this point there is no real way to extract all malloc calls since tbd all final calls

    CvSize orsize = cvGetSize(hin[0]);

    IplImage* hssq;
    IplImage* hsum;
    IplImage** ker  = (IplImage**)malloc(hin_d * sizeof(IplImage*));
    IplImage** hsq  = (IplImage**)malloc(hin_d * sizeof(IplImage*));
    IplImage** hnum = (IplImage**)malloc(hin_d * sizeof(IplImage*));;

    for(i = 0; i < hin_d; i++)
    {
        ker[i]  = cvCreateImage(cvSize(kw, kh),     IPL_DEPTH_V1, 1);
        hsq[i]  = cvCreateImage(cvGetSize(hin[0]),  IPL_DEPTH_V1, 1);
        hnum[i] = cvCreateImage(cvGetSize(hin[0]), IPL_DEPTH_V1, 1);

        cvSet(ker[i], cvScalar(1));
    }

    int size = kh * kw * hin_d;

    // hsq = hsrc ** 2.
    for(i = 0; i < hin_d; i++)
    {
        for(x = 0; x < hin[0]->width; x++)
        {
            for(y = 0; y < hin[0]->height; y++)
            {
                CV_IMAGE_ELEM(hsq[i], v1_float, y, x) = v1_pow(CV_IMAGE_ELEM(hin[i], v1_float, y, x), 2.0f);
            }
        }
    }

    fast_3d_convolve(hsq, &hssq, ker, hin_d, hin_d);
    fast_3d_convolve(hin, &hsum, ker, hin_d, hin_d);

    trim3d2d(hin, hin_d, cvGetSize(hsum));
    trim3d2d(hout, hin_d, cvGetSize(hsum));
    trim3d2d(hnum, hin_d, cvGetSize(hsum));

    // compute hnum and hdiv      
    // hnum = hsrc[ys:ys+hout_h, xs:xs+hout_w] - (hsum/size)
    for(i = 0; i < hin_d; i++)
    {
        for(x = 0; x < hin[0]->width; x++)
        {
            for(y = 0; y < hin[0]->height; y++)
            {
                CV_IMAGE_ELEM(hnum[i], v1_float, y, x) =
                    CV_IMAGE_ELEM(hin[i], v1_float, y, x) - (CV_IMAGE_ELEM(hsum, v1_float, y, x) / size);
            }
        }
    }

    // val = (hssq - (hsum**2.)/size)
    // N.putmask(val, val<0, 0)
    IplImage *val = cvCreateImage(cvGetSize(hsum),  IPL_DEPTH_V1, 1);
    for(x = 0; x < hsum->width; x++)
    {
        for(y = 0; y < hsum->height; y++)
        {
            v1_float myval = CV_IMAGE_ELEM(hssq, v1_float, y, x) - (v1_pow(CV_IMAGE_ELEM(hsum, v1_float, y, x), 2.0f) / (v1_float)size);
            if(myval < 0.0f)
                myval = 0.0f;
            CV_IMAGE_ELEM(val, v1_float, y, x) = myval;
        }
    }


    // hdiv = val ** (1./2) + eps
    // N.putmask(hdiv, hdiv < (threshold+eps), 1.)
    IplImage *hdiv = cvCreateImage(cvGetSize(val),  IPL_DEPTH_V1, 1);
    for(x = 0; x < val->width; x++)
    {
        for(y = 0; y < val->height; y++)
        {
            v1_float hdiv_val = v1_pow(CV_IMAGE_ELEM(val, v1_float, y, x), 0.5f) + eps;
            if(hdiv_val < (threshold + eps))
                hdiv_val = 1.0f;
            CV_IMAGE_ELEM(hdiv, v1_float, y, x) = hdiv_val;
        }
    }

    // result = (hnum / hdiv)
    // hout[:] = result
    //CvRect roi = cvRect(hsumBounds.startX, hsumBounds.startY, (hsumBounds.endX - hsumBounds.startX) + 1,
    //(hsumBounds.endY - hsumBounds.startY) + 1);

    for(i = 0; i < hin_d; i++)
    {
        for(x = 0; x < hin[0]->width; x++)
        {
            for(y = 0; y < hin[0]->height; y++)
            {
                CV_IMAGE_ELEM(hout[i], v1_float, y, x) = CV_IMAGE_ELEM(hnum[i], v1_float, y, x) / CV_IMAGE_ELEM(hdiv, v1_float, y, x);
            }
        }
    }

    //return hout
    for(i = 0; i < hin_d; i++)
    {
        cvReleaseImage(&ker[i]);
        cvReleaseImage(&hsq[i]);
        cvReleaseImage(&hnum[i]);
    }

    cvReleaseImage(&val);
    cvReleaseImage(&hssq);
    cvReleaseImage(&hdiv);
    cvReleaseImage(&hsum);

    free(hsq);
    free(hnum);
    free(ker);
}

IplImage** sresample(IplImage** src, int outh, int outw, int hin_d)
{
    /*""" Simple 3d array resampling

    Inputs:
      src -- a ndimensional array (dim>2)
      outshape -- fixed output shape for the first 2 dimensions
     
    Outputs:
       hout -- resulting n-dimensional array
    
    """*/
    
    //    inh, inw = inshape = src.shape[:2]
    int inh, inw;
    inh = src[0]->height;
    inw = src[0]->width;
    //      hslice = (N.arange(outh) * (inh-1.)/(outh-1.)).round().astype(int)
    //    wslice = (N.arange(outw) * (inw-1.)/(outw-1.)).round().astype(int)
    //    hout = src[hslice, :][:, wslice]    
    //    return hout.copy()

    IplImage** resampled = (IplImage**)malloc(sizeof(IplImage*) * hin_d);
    for(int i = 0; i < hin_d; i++)
    {
        resampled[i] = cvCreateImage(cvSize(outw, outh), IPL_DEPTH_V1, 1);

        for(int row = 0; row < outh; row++)
        {
            int samp_row = (int)v1_round((v1_float)row * ((v1_float)inh - 1.) / ((v1_float)outh - 1.));

            for(int col = 0; col < outw; col++)
            {
                int samp_col = (int)v1_round((v1_float)col * ((v1_float)inw - 1.) / ((v1_float)outw - 1.));
                CV_IMAGE_ELEM(resampled[i], v1_float, row, col) = CV_IMAGE_ELEM(src[i], v1_float, samp_row, samp_col);
            }
        }
    }

    return resampled;
}

IplImage** v1s_dimr(IplImage** hin, int lsum_ksize, int outh, int outw, int hin_d)
{
    /*""" V1S Image Downsampling
    Low-pass filter and downsample a population "image" (width X height X
    n_channels)
    
    Inputs:
      hin -- a 3-dimensional array (width X height X n_channels)
      lsum_ksize -- kernel size of the local sum ex: 17
      outshape -- fixed output shape (2d slices)
     
    Outputs:
       hout -- resulting 3-dimensional array

    """*/

    // -- local sum
    int hin_h, hin_w, i;
    hin_h = hin[0]->height;
    hin_w = hin[0]->width;
    //  aux = N.empty(hin.shape, dtype)
    IplImage** aux = (IplImage**)malloc(hin_d * sizeof(IplImage*));
    IplImage* kh = cvCreateImage(cvSize(1, lsum_ksize), IPL_DEPTH_V1, 1);
    cvSet(kh, cvScalar(1.));
    IplImage* kw = cvCreateImage(cvSize(lsum_ksize, 1), IPL_DEPTH_V1, 1);
    cvSet(kw, cvScalar(1.));
    IplImage* tmp = cvCreateImage(cvGetSize(hin[0]), IPL_DEPTH_V1, 1);

    //cvNamedWindow("Result", 0);    

    //    for d in xrange(aux.shape[2]):
    //        aux[:,:,d] = conv(conv(hin[:,:,d], k[N.newaxis,:], 'same'), k[:,N.newaxis], 'same')
    for(i = 0; i < hin_d; i++)
    {
        aux[i] = cvCreateImage(cvGetSize(hin[i]), IPL_DEPTH_V1, 1);
        cvZero(aux[i]);
        do_convolve2d(hin[i], tmp, kw, MODE_SAME);
        do_convolve2d(tmp, aux[i], kh, MODE_SAME);
    }

    cvReleaseImage(&kh);
    cvReleaseImage(&kw);
    cvReleaseImage(&tmp);

    //cvDestroyAllWindows();

    // -- resample output
    //    hout = sresample(aux, outshape)
    IplImage** ret = sresample(aux, outh, outw, hin_d);

    //    return hout
    for(i = 0; i < hin_d; i++)
        cvReleaseImage(&aux[i]);

    free(aux);
    return ret;
}
