// recognize.cpp
// Copyright 2009 Securics, Inc

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <time.h>
#include <signal.h>
#include <sys/stat.h>
#include <ctype.h>
#include <libgen.h>
#include <limits.h>
#include <float.h>
#include <iostream>
#include <map>
#include <errno.h>
#include <stdarg.h>
#include <fstream>
#include <vector>
#include <list>
#include <limits>

#include "v1.h"
#include "structs.h"
#include "classify.h"
#include "classify-multi.h"
#include "pca.h"
#include "utils.h"
#include "parser.h"
#include "geo.h"
#include "eye_detector.h"
#include "facedetect.h"
#include "preprocessing.h"
#include "alert.h"
#include "version.h"
#include "atmos_deblur.h"
#include "motion_deblur.h"
#include "capture.h"
#include "argparse.h"
#include "sqi.h"
#include "recognize.h"

#define SPACE 32
#define ESCAPE 27

using namespace std;

// Global variables
static int stop_now = 0;
static CvRect crop_r;
static CvRect orig_r;
// Macros
#define SAFE_RELEASE(img) do { if ((img) != NULL) { cvReleaseImage(&(img)); (img) = NULL; } } while(0);
#define SAFE_FREE(ptr)    do { if ((ptr) != NULL) { free((ptr)); (ptr) = NULL; } } while(0);
#define SAFE_DELETE(ptr)  do { if ((ptr) != NULL) { delete (ptr); (ptr) = NULL; } } while(0);

////////////////////////////////////////////////////////////////////////////////
// General utility functions
////////////////////////////////////////////////////////////////////////////////

/**
 * Allocate memory, or exit if none is available
 * @param size The number of bytes to malloc
 * @return The allocated memory
 */
static inline void* safe_malloc(size_t size)
{
    void* ptr = NULL;
    if((ptr = malloc(size)) == NULL)
        fatal("Fatal: safe_malloc failed.\n");
    return ptr;
}

/**
 * Print a message to stderr and exit
 * @param const char* msg The message to print
 */
static void fatal(const char* msg, ...)
{
    va_list ap;
    va_start(ap, msg);
    vfprintf(stderr, msg, ap);
    va_end(ap);

    exit(1);
}

/**
 * If the user kills us, let's try to die gracefully
 *@param int sig The signal sent to the program
 */
static void onKill(int sig)
{
    static int stop_count = 0;
    stop_now = 1;
    stop_count++;
    if(stop_count == 1)
        printf("\nProgram will stop after the current image is finished...\n");
    else if(stop_count == 2)
        printf("\nI heard you the first time. Patience, I say!\n");
    else
    {
        printf("\nOK, one more and I might just give you what you want.\n");
        signal(sig, SIG_DFL);
    }
}

/**
 *Checks the file type of the file
 *@param const char* path The path to the file.
 *@param int mode File info stat mode
 *@return 1 if okay, 0 if false
 */
static int isFileType(const char* path, const int mode)
{
    struct stat stFileInfo;
    if(stat(path, &stFileInfo) != 0)
        return 0;

    return ((int)(stFileInfo.st_mode & mode) == mode) ? 1 : 0;
}

/**
 * Save an image to an intermediate directory specified by the user. Insert
 * a phase name as part of the new file name if desired. All slashes are
 * replaced by _ so that the files don't need to worry about collisions.
 */
static int saveIntermediateFile(IplImage* img, const char* phaseName, prog_params &params, recogvars &vars)
{
    if(params.save_int_dir == NULL)
        return 0;

    // First create a base filename
    const char* file = v1_capture_savename(vars.cap);
    if(file == NULL)
        return -1;

    string fname(file);
    string replace("/\\");
    size_t position = fname.find_first_of(replace); // find slashes
    while(position != string::npos)
    {
        fname.replace(position, 1, "_");
        position = fname.find_first_of(replace, position + 1);
    }

    string newfile(params.save_int_dir);
    newfile += "/";
    newfile += fname;

    if(phaseName != NULL)
    {
        newfile += "_";
        newfile += phaseName;
    }

    newfile += ".png";
    printf("fname = %s\n", newfile.c_str());

    cvSaveImage(newfile.c_str(), img);

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
// Main program loop
////////////////////////////////////////////////////////////////////////////////

#ifndef TESTING

/**
 * Program entry point for recognition.
 */
int main(int argc, char **argv)
{
    recogvars vars;
    int result;
    prog_params params;
    __verbose = 0;

    parseCommandLine(argc, argv, &params);

    init(params, vars);
    result = runRecognition(params, vars);
    uninit(params, vars);

    return result;
}

#endif // TESTING
/**
 *Runs recognition and sets up the source of the probes
 *@param prog_params &params The program parameters
 *@param recogvars &vars The recognition variables.
 */
static int runRecognition(prog_params &params, recogvars &vars)
{
    clock_t start, end;
    double elapsed = 0.0;
    float progress;
    int total_processed = 0;
    bool do_recognize = true;

    // Catch Ctrl+C
    signal(SIGINT, onKill);
    signal(SIGHUP, onKill);
    signal(SIGQUIT, onKill);
    signal(SIGABRT, onKill);
    signal(SIGPIPE, onKill);
    signal(SIGTERM, onKill);

    if(params.use_webcam)
    {
        cvNamedWindow("Webcam", CV_WINDOW_AUTOSIZE);
        cvStartWindowThread();
    }
    else if(params.use_gige)
    {
        cvNamedWindow("GigE", CV_WINDOW_AUTOSIZE);
        cvStartWindowThread();
    }

    while(stop_now == 0)
    {
        if(params.use_webcam || params.use_gige)
            do_recognize = previewWebcam(params, vars);

        // Have we exhausted the capture device?
        if(v1_capture_eof(vars.cap))
            break;

        if(do_recognize)
        {
            fflush(stdout);
            start = clock();

            // Print the current progress, if the capture source can determine that
            progress = v1_capture_progress(vars.cap);
            if(params.quiet)
                printf("%s:\n", v1_capture_name(vars.cap));
            else if(progress < 0.)
                printf("Processing: %s\n", v1_capture_name(vars.cap));
            else
                printf("[%5.1f%%] Processing: %s\n", progress, v1_capture_name(vars.cap));

            // Attempt recognition
            if(recognize(params, vars) == 0)
            {
                ++total_processed;
                end = clock();
                elapsed += ((double)(end - start)) / CLOCKS_PER_SEC;
            }
        }

        if(!v1_capture_next(vars.cap))
            break;
    }

    if(params.use_webcam)
        cvDestroyWindow("Webcam");
    else if(params.use_gige)
        cvDestroyWindow("GigE");

    printStats(params, vars);

    if(params.quiet == 0)
        printRate(elapsed, total_processed);

    if(params.rpet_file != NULL && params.use_groundtruth)
        saveRPET(params, vars);

    return 0;
}

// Generate the RPET file if requested
static void saveRPET(prog_params &params, recogvars &vars)
{
    vector<point> rpet_data;
    map<string,int>::iterator it;

    printf("\nRPET data:\n");

    for(it = vars.rpet_nums.begin(); it != vars.rpet_nums.end(); it++)
    {
        string name = (*it).first;
        int num = (*it).second;

        string fname = params.rpet_file;
        if(name != "default")
        {
            fname += "_";
            fname += name;
        }

        fname += ".rpet";

        rpet_data.clear();
        vars.rpet.generateCurve(&rpet_data, num);

        unsigned int size = rpet_data.size();

        if(size > 0)
        {
            printf("  Saving data to file `%s'\n", fname.c_str());
            FILE* fh = fopenOrError(fname.c_str(), "w");
            if(fh == NULL)
                continue;

            double diff_save = numeric_limits<double>::max();
            double x_sav, y_sav, thresh_sav;

            for(unsigned int n = 0; n < size; n++)
            {
                double x = rpet_data[n].x;
                double y = rpet_data[n].y;
                double z = rpet_data[n].z;

                double diff = fabs(x - y);
                if(diff < diff_save)
                {
                    diff_save = diff;
                    x_sav = x;
                    y_sav = y;
                    thresh_sav = z;
                }

                fprintf(fh, "%G %G %G\n", x, y, z);
            }

            fclose(fh);
            printf("  Optimum threshold: %G\n", thresh_sav);            
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Core recognition
////////////////////////////////////////////////////////////////////////////////
/**
 *Called for each probe.
 *@param prog_params &params The program parameters
 *@param recogvars &vars The recognition variables.
 */
static int recognize(prog_params &params, recogvars &vars)
{
    IplImage* img = v1_capture_get(vars.cap);

    if(params.multi_face)
    {
        vector<multi_face_result> results = recognizeMulti(params, vars, img);
        answerMulti(params, vars, results);
    }
    else
    {
        IplImage* normalized = NULL;
        IplImage* alert_img = NULL;

        int lbl = -1;

        // Run recognition
        lbl = recognizeBlurred(params, vars, img, &normalized);

        // Set up the alert image
        if(params.show_normalized && normalized != NULL)
            alert_img = normalized;
        else
            alert_img = img;

        // Display the answer / alert
        if(lbl > 0)
        {
            recordAnswer(params, vars, false);
            if(params.enable_alerts)
                show_alert(alert_img, vars.top_ids);
        }
        else
        {
            if(lbl == -100) // failure predicted
                recordAnswer(params, vars, true);
            if(params.enable_alerts)
                hide_alert();
        }

        SAFE_RELEASE(normalized);
    }

    // Clean up
    SAFE_RELEASE(img);

    return 0;
}

/**
 *Called to recognize a single probe and return results which are fused if deblurring is used or determined to be the top result.
 *@param prog_params &params The program parameters
 *@param recogvars &vars The recognition variables.
 *@param vector<id_score> &top The top scores returned
 *@param IplImage* input The image to run recognition on.
 *@param IplImage** normalized normalized image returned from.
 *@param const char* phase For saving off intermediate images, to distinguish motion or atmospheric.
 *@result int -1 if failed, top id otherwise
 */
static int recognizeOne(prog_params &params, recogvars &vars, vector<id_score> &top, IplImage* input,
                        IplImage** normalized, const char* phase, double fp_thresh)
{
    v1_float* hold = NULL;
    int lbl = -1;
    int out_size = 0;
    IplImage* img = NULL;
    v1_capture_gtdata gtdata;
    unsigned int i;

    // Free normalized
    if(normalized != NULL)
    {
        IplImage* freetmp = *normalized;
        SAFE_RELEASE(freetmp);
        *normalized = NULL;
    }

    // Try the cache (then the cheque)
    const char* cache_name = v1_capture_cache(vars.cap);
    if(params.use_cache && cache_name != NULL)
    {
        if(tryCache(cache_name, vars.features) == 0)
            goto recognize;
    }

    // Tres bizarre
    if(input == NULL)
    {
        printf("WARNING: No usable image data, skipping to next image.\n\n");
        return -1;
    }

    img = getGrayscale(input);
    if(img == NULL)
    {
        printf("WARNING: Unable to convert to grayscale (out of memory?)\n\n");
        return -1;        
    }

    if(params.geo_norm)
    {
        if(runGeonorm(params, vars, &img) < 0)
        {
            if(vars.cap->type == FILE_LIST)
                vars.problem_files.push_back(string(v1_capture_name(vars.cap)));
            SAFE_RELEASE(img);
            return -1;
        }
    }

    if(params.use_sqi_norm)
    {
        IplImage* sqi = securics_sqi_norm(img);
        SAFE_RELEASE(img);
        img = sqi;
    }

    if(normalized != NULL)
        *normalized = cvCloneImage(img);

    saveIntermediateFile(img, phase, params, vars);
    get_features(img, vars.features, vars.filters, params.fast_gabor, 1, params.use_python, 1);
    SAFE_RELEASE(img);

    if(params.use_cache && cache_name != NULL)
        saveCache(cache_name, vars.features);

recognize:

    // Get the groundtruth data if there is any
    int use_gt = params.use_groundtruth;
    if(use_gt) use_gt = v1_capture_getdata(vars.cap, &gtdata) ? 1 : 0;

    //clear the top ids
    top.clear();

    // Run recognition
    sphere_probe(vars.features, WINDOW_SIZE*WINDOW_SIZE*GABOR_FEATURES, vars.mean, vars.std);
    hold = projectProbe(vars.pca_training, vars.features, WINDOW_SIZE*WINDOW_SIZE*GABOR_FEATURES, &out_size);

    if(params.onevsrest)
    {
        lbl = classify_onevsrest_classify(vars.covr, hold, out_size, top);
    }
    else
    {
        lbl = classify(hold, out_size, vars.model, top);
    }

    SAFE_FREE(hold);

    // Write out fp groundtruth data
    if(use_gt && vars.fTrainFile != NULL && top.size() > 0)
    {
        if(top[0].id == gtdata.label)
            fprintf(vars.fTrainFile, "1");
        else
            fprintf(vars.fTrainFile, "-1");
        for(i = 0; i < top.size(); i++)
            fprintf(vars.fTrainFile, " %d:%e", i + 1, top[i].score);
        fprintf(vars.fTrainFile, "\n");
        fflush(vars.fTrainFile);
    }

    int feature_num = -1;

    {
        string name;
        if(phase == NULL) name = "default";
        else              name = phase;

        if(vars.rpet_nums.count(name) == 0)
            vars.rpet_nums[name] = ++vars.top_rpet_num;
        feature_num = vars.rpet_nums[name];
    }

    // Do failure prediction
    if(vars.fpmodel != NULL && top.size() > 0)
    {
        resultData fv;
        vector<resultData> data;

        // None if this is even used, but just so you know it's there...
        fv.name = "";
        fv.probeName = "";
        fv.id = "";
        fv.rotation = "";
        fv.probeId = "";

        for(i = 0; i < top.size(); i++)
        {
            fv.score = top[i].score;
            data.push_back(fv);
        }

        if(data.size() >= 10)
        {
            double score = (double)vars.fpmodel->Classify(&data);
            for(i = 0; i < top.size(); i++)
                top[i].failure_score = (float)score;

            bool rpet_file_true = 0;
            if (params.rpet_file != NULL)
               rpet_file_true = 1;

            if(use_gt && params.rpet_file != NULL)
            {
                int gt = (lbl == gtdata.label) ? 1 : 0;
                vars.rpet.addClassifyResult(score, gt, feature_num);
            }

            // params.fp_thresh is 0.0 unless set by the user
            if(score < fp_thresh)
                lbl = -100;
        }
        else
        {
            printf("Warning: not enough scores for failure prediction.\n");
            for(i = 0; i < top.size(); i++)
                top[i].failure_score = -1;
        }
    }

    return lbl;
}

static vector<multi_face_result> recognizeMulti(prog_params &params, recogvars &vars, IplImage* input)
{
    vector<multi_face_result> results;
    params.crop_face = 0;

    IplImage* img = getGrayscale(input);
    if(img == NULL)
    {
        printf("WARNING: Unable to convert to grayscale (out of memory?)\n\n");
        params.crop_face = 1;
        return results;
    }

    vector<CvRect> faceRegions = detect_face_multi(vars.cascade, vars.storage, img);
    size_t i, count = faceRegions.size();
    
    if(count == 0)
    {
        printf("WARNING: Face detection failed, skipping.\n\n");
        params.crop_face = 1;
        return results;
    }

    vector<IplImage*> faces;
    bool failures = false;
    
    for(i = 0; i < count; ++i)
    {
        CvRect crop_bounds = faceRegions[i];
        IplImage* cropped = cropIpl(img, crop_bounds.x, crop_bounds.y, crop_bounds.width, crop_bounds.height);

        if(cropped == NULL)
        {
            failures = true;
            break;
        }

        faces.push_back(cropped);        
    }

    SAFE_RELEASE(img);
    count = faces.size();

    if(failures)
    {
        printf("WARNING: Crop to ROI failed, skipping.\n\n");
        for(i = 0; i < count; ++i)
        {
            IplImage* tmp = faces[i];
            SAFE_RELEASE(tmp);
        }

        params.crop_face = 1;
        return results;
    }

    IplImage* mynormalized = NULL;

    // OK, we've found one or more faces, let's do this thing
    for(i = 0; i < count; ++i)
    {
        multi_face_result result;
        result.top_ids.clear();
        result.label = -1;
        result.faceImage = faces[i];
        faces[i] = NULL;

        v1_capture_setimg(vars.cap, result.faceImage);
        result.label = recognizeBlurred(params, vars, result.faceImage, &mynormalized);

        SAFE_RELEASE(mynormalized);
        if(result.label > 0)
        {
            for(i = 0; i < vars.top_ids.size(); ++i)
                result.top_ids.push_back(vars.top_ids[i]);
        }

        results.push_back(result);
    }

    return results;
}

/**
 *Called to recognize a single probe and return results which are fused if deblurring is used or determined to be the top result.
 *@param prog_params &params The program parameters
 *@param recogvars &vars The recognition variables.
 *@param IplImage* input The image to run recognition on.
 *@param IplImage** normalized normalized image returned from.
 *@result int Negative if failed, top id otherwise
 */
static int recognizeBlurred(prog_params &params, recogvars &vars, IplImage* input, IplImage** normalized)
{
    vector<id_score> fusion_data;
    int lbl, i, size, face_crop = 0;
    int failcount = 0, totalcount = 0;
    IplImage* img = NULL;
    v1_capture_gtdata gtdata;
    bool gt;
    CvRect crop_bounds;
    bool crop = false;

    vector<id_score> top_ids_default;
    vector<id_score> top_ids_atmos;
    vector<id_score> top_ids_motion_1;
    vector<id_score> top_ids_motion_2;
    vector<id_score> top_ids_motion_3;

    IplImage* motion_img[3];
    IplImage* atmos_img = NULL;
    IplImage** face_motion_img = NULL;
    IplImage *anorm = NULL;
    IplImage *m1norm = NULL;
    IplImage *m2norm = NULL;
    IplImage *m3norm=  NULL;
    IplImage *regnorm = NULL;

    bool default_failed = false;
    bool motion1_failed = false;
    bool motion2_failed = false;
    bool motion3_failed = false;
    bool atmos_failed = false;
    bool all_failed = false;
    motion_img[0] = NULL;
    motion_img[1] = NULL;
    motion_img[2] = NULL;

    // Free normalized
    if(normalized != NULL)
    {
        IplImage* freetmp = *normalized;
        SAFE_RELEASE(freetmp);
        *normalized = NULL;
    }

    img = getGrayscale(input);
    if(img == NULL)
    {
        printf("WARNING: Unable to convert to grayscale (out of memory?)\n\n");
        return -1;        
    }

    // Get the groundtruth data if there is any
    gt = v1_capture_getdata(vars.cap, &gtdata);

    if(params.crop_face)
    {
        //no need to crop again
        params.crop_face = 0;
        face_crop = 1;
        CvRect r = detect_face(vars.cascade, vars.storage, img);

        crop_r.x=r.x;
        crop_r.y=r.y;
        crop_r.width=r.width;
        crop_r.height=r.height; 

        if(r.x >=0)
        {
            r.x = floor(r.x - ((float)r.x * 0.20));
            if (r.x < 0)
                r.x=0;
        }

        if(r.y >=0)
        {
            r.y = floor(r.y - ((float)r.y * 0.20));
            if(r.y < 0)
                r.y = 0;
        }

        if(r.width >= 0)
        {
            r.width=floor(r.width + ((float)r.x * 0.53));
            if(r.width + r.x > img->width)
                r.width=img->width - r.x;
        }

        if(r.height >= 0)
        {
            r.height = floor(r.height + ((float)r.y * 0.53));
            if (r.height + r.y  > img->height)
                r.height=img->height - r.y;
        }

        orig_r.x=r.x;
        orig_r.y=r.y;
        orig_r.height=r.height;
        orig_r.width=r.width;

        if(r.x < 0 || r.y < 0 || r.width < 0 || r.height < 0 )
        {
            printf("WARNING: Face detection failed, skipping.\n\n");
            vars.face_detect_fails++;
            if(vars.cap->type == FILE_LIST)
                vars.problem_files.push_back(string(v1_capture_name(vars.cap)));
            if(face_crop == 1)
                params.crop_face = 1;
            return -1;
        }

        int lx = gtdata.lx;
        int ly = gtdata.ly;
        int rx = gtdata.rx;
        int ry = gtdata.ry;

        // We may have some groundtruth data for eyes, a good way to verify the face
        if(lx > 0 && ly > 0 && rx > 0 && ry > 0)
        {
            if(r.x > lx || r.x > rx || r.y > ly || r.y > ry ||
               (r.x + r.width) < lx || (r.x + r.width) < rx ||
               (r.y + r.height) < ly || (r.y + r.height) < ry)
            {
                printf("WARNING: Groundtruth eyes aren't inside of the detected face region, skipping.\n\n");

                vars.face_detect_fails++;
                if(vars.cap->type == FILE_LIST)
                    vars.problem_files.push_back(string(v1_capture_name(vars.cap)));
                if(face_crop ==1)
                    params.crop_face = 1;
                return -1;
            }
        }

        crop_bounds = r;
        crop = true;
    }
    else if(gt && gtdata.cropX >= 0 && gtdata.cropY >= 0 && gtdata.cropW > 0 && gtdata.cropH > 0)
    {
        crop_bounds.x = gtdata.cropX;
        crop_bounds.y = gtdata.cropY;
        crop_bounds.width = gtdata.cropW;
        crop_bounds.height = gtdata.cropH;

        crop_r.x=crop_bounds.x;
        crop_r.y=crop_bounds.y;
        crop_r.width=crop_bounds.width;
        crop_r.height=crop_bounds.height; 

        crop = true;
    }
    else
    {
        crop_r.x=0;
        crop_r.y=0;
        crop_r.width=img->width;
        crop_r.height=img->height;

        crop = false;
    }

    if(crop)
    {
        IplImage* cropped = cropIpl(img, crop_bounds.x, crop_bounds.y,
                    crop_bounds.width, crop_bounds.height);

        SAFE_RELEASE(img);
        if(cropped == NULL)
        {
            printf("WARNING: Crop to ROI failed, skipping.\n\n");
            if(vars.cap->type == FILE_LIST)
                vars.problem_files.push_back(string(v1_capture_name(vars.cap)));
            if(face_crop == 1)
                params.crop_face = 1;
            return -2;
        }

        img = cropped;
        v1_capture_setimg(vars.cap, img);

        // Translate the eye coordinates
        gtdata.lx -= crop_bounds.x;
        gtdata.ly -= crop_bounds.y;
        gtdata.rx -= crop_bounds.x;
        gtdata.ry -= crop_bounds.y;
        v1_capture_setdata(vars.cap, &gtdata);
    
        // cvNamedWindow("RESIZE",CV_WINDOW_AUTOSIZE);
        // cvShowImage("RESIZE",cropped);
        // cvWaitKey(0);
        // cvDestroyWindow("RESIZE");
    }

    // Run atmospheric deblurring
    if(params.use_atmos_deblur == 1)
    {
        printf(" => ATMOSPHERIC\n");
        atmos_img = atmos_deblur(img, params.enable_alerts);
        lbl = recognizeOne(params, vars, top_ids_atmos, atmos_img, &anorm, "atmospheric", params.fp_atmos_thresh);
        totalcount++;

        // Error, discard these results
        if(lbl == -100)
        {
            atmos_failed = true;
            failcount++;
        }
        else if(lbl < 0)
            top_ids_atmos.clear();
        else if(params.enable_alerts)
        {
            cvNamedWindow("Atmospheric Deblurred", 0);
            cvShowImage("Atmospheric Deblurred", atmos_img);
        }

        SAFE_RELEASE(atmos_img);
    }

    // Begin motion deblurring
    if(params.use_motion_deblur)
    {
        int newsize = (int)((double)img->width * 1.7);

        motion_img[0] = cvCreateImage(cvSize(img->width, img->height), IPL_DEPTH_8U, 1);
        cvZero(motion_img[0]);

        motion_img[1] = cvCreateImage(cvSize(img->width, img->height), IPL_DEPTH_8U, 1);
        cvZero(motion_img[1]);

        motion_img[2] = cvCreateImage(cvSize(img->width, img->height), IPL_DEPTH_8U, 1);
        cvZero(motion_img[2]);

        if(params.quiet == 0)
            printf(" => MOTION\n");
        motion_deblur(img, motion_img);

        if((face_motion_img = motionFaces(motion_img, 3, vars)) != NULL)
        {
            if(face_motion_img[0] != NULL)
            {
                lbl = recognizeOne(params, vars, top_ids_motion_1, face_motion_img[0], &m1norm, "motion1", params.fp_motion1_thresh);
                totalcount++;
                if(lbl == -100)
                {
                    motion1_failed = true;
                    failcount++;
                }
                else if(lbl < 0)
                    top_ids_motion_1.clear();
                else if(params.enable_alerts)
                {
                    cvNamedWindow("Motion 1 Deblurred", 0);
                    cvShowImage("Motion 1 Deblurred", face_motion_img[0]);
                }

                SAFE_RELEASE(face_motion_img[0]);
            }

            if(face_motion_img[1] != NULL)
            {
                lbl = recognizeOne(params, vars, top_ids_motion_2, face_motion_img[1], &m2norm, "motion2", params.fp_motion2_thresh);
                totalcount++;
                if(lbl == -100)
                {
                    motion2_failed = true;
                    failcount++;
                }
                else if(lbl < 0)
                    top_ids_motion_2.clear();
                else if(params.enable_alerts)
                {
                    cvNamedWindow("Motion 2 Deblurred", 0);
                    cvShowImage("Motion 2 Deblurred", face_motion_img[1]);
                }

                SAFE_RELEASE(face_motion_img[1]);
            }

            if(face_motion_img[2] != NULL)
            {
                lbl = recognizeOne(params, vars, top_ids_motion_3, face_motion_img[2], &m3norm, "motion3", params.fp_motion3_thresh);
                totalcount++;
                if(lbl == -100)
                {
                    motion3_failed = true;
                    failcount++;
                }
                else if(lbl < 0)
                    top_ids_motion_3.clear();
                else if(params.enable_alerts)
                {
                    cvNamedWindow("Motion 3 Deblurred", 0);
                    cvShowImage("Motion 3 Deblurred", face_motion_img[2]);
                }

                SAFE_RELEASE(face_motion_img[2]);
            }
        }

        SAFE_RELEASE(motion_img[0]);
        SAFE_RELEASE(motion_img[1]);
        SAFE_RELEASE(motion_img[2]);
        SAFE_FREE(face_motion_img);
    }
    // End motion deblurring

    // Do normal recognition
    if(params.quiet == 0)
        printf(" => VANILLA\n");
    lbl = recognizeOne(params, vars, top_ids_default, img, &regnorm, "normal", params.fp_default_thresh);
    totalcount++;
    if(lbl == -100)
    {
        default_failed = true;
        failcount++;
    }
    else if(lbl < 0)
        top_ids_default.clear();

    // Don't need this anymore
    SAFE_RELEASE(img);

    // Prepare data for fusion
    size = top_ids_default.size();
    // Fuse the scores

    // If all images failed, we can still do fusion, we'll just report later
    // on that failure prediction predicted the fused scores as having failed.
    if(failcount >= totalcount)
    {
        default_failed = false;
        motion1_failed = false;
        motion2_failed = false;
        motion3_failed = false;
        atmos_failed = false;
	all_failed = true;
    }

    int fusion_type = params.fusion_type;
    if(vars.fpmodel == NULL)
      {        
	if (params.fusion_type==1)
	  fusion_type=1;
	else if (params.fusion_type==5)
	  fusion_type=5;
	else if (params.fusion_type==7)
	  fusion_type=7;
	else
	  fusion_type = 0;
      }


    
     // Consider all 5 candidate images; choose the ones that haven't failed,
    // and choose the score series with the highest rank 1 score.
    if(fusion_type == 0)
    {
        int best_score_series = -1;
        float max = -1.0*FLT_MAX;
	fprintf(stderr,"fusion 0\r\n");
	

	if((!atmos_failed && (int)top_ids_atmos.size() > 0) && 
	  top_ids_atmos[0].score > max)

	  {
            best_score_series = 1;
	    max = top_ids_atmos[0].score;
	   
	  }

        if((!motion1_failed && (int)top_ids_motion_1.size() > 0) &&
           top_ids_motion_1[0].score > max)
	  {
            best_score_series = 2;
            max = top_ids_motion_1[0].score; 
	  }
        if((!motion2_failed && (int)top_ids_motion_2.size() > 0) &&
           top_ids_motion_2[0].score > max)
        {
	  best_score_series = 3;
	  max = top_ids_motion_2[0].score;
        }
        if((!motion3_failed && (int)top_ids_motion_3.size() > 0) &&
           top_ids_motion_3[0].score > max)
	  {
            best_score_series = 4;
            max = top_ids_motion_3[0].score;
	  }
	
	if((!default_failed && (int)top_ids_default.size() > 0) && top_ids_default[0].score > max)
	  {
            best_score_series = 5;
	    max = top_ids_default[0].score;
	    
	  }
	

        // copy the data for the best score series
        vars.top_ids.clear();
        if(best_score_series == 1)
        {
            for(i = 0; i < (int)top_ids_atmos.size(); i++)
            {
                top_ids_atmos[i].algo = 1;
                vars.top_ids.push_back(top_ids_atmos[i]);
            }
        }
        else if (best_score_series == 2)
        {
            for(i = 0; i < (int)top_ids_motion_1.size(); i++)
            {
                top_ids_motion_1[i].algo = 2;
                vars.top_ids.push_back(top_ids_motion_1[i]);
            }
        }
        else if (best_score_series == 3)
        {
            for(i = 0; i < (int)top_ids_motion_2.size(); i++)
            {
                top_ids_motion_2[i].algo = 3;
                vars.top_ids.push_back(top_ids_motion_2[i]);
            }
        }
        else if (best_score_series == 4)
        {
            for(i = 0; i < (int)top_ids_motion_3.size(); i++)
            {
                top_ids_motion_3[i].algo = 4;
                vars.top_ids.push_back(top_ids_motion_3[i]);            
            }
        }
	
        else
        {
            for (i = 0; i < (int)top_ids_default.size(); i++)
            {
                top_ids_default[i].algo = 5;
                vars.top_ids.push_back(top_ids_default[i]);
            }
        }
	 
 }
    // Consider all 5 candidate images; choose the ones that haven't failed,
    // and select the score set that is "farthest" from failure (best marginal
    // distance.
    else if (fusion_type == 1)
    {
        
	fprintf(stderr,"fusion 1\r\n");
	char chosen[128];
        float max = -1.0*FLT_MAX;

        vars.top_ids.clear();

        //fuse on the one futherest from failure
        if((int)top_ids_default.size() > 0)
        {
          fprintf(stderr,"FAILURE SCORE DEFAULT: %f\r\n",top_ids_default[0].failure_score); 
	  //must be max at this point
            fprintf(stderr,"REGULAR HAS MAX: %f\r\n",top_ids_default[0].failure_score);
	    max = top_ids_default[0].failure_score;
            vars.top_ids.clear();
            for(i = 0; i < size; i++)
                vars.top_ids.push_back(top_ids_default[i]);
        }

        if((int)top_ids_atmos.size() > 0)
        {
	  fprintf(stderr,"FAILURE SCORE ATMOS: %f\r\n",top_ids_atmos[0].failure_score); 
	  //must be max at this point
	  if(max < top_ids_atmos[0].failure_score)
            {
	      fprintf(stderr,"ATMOS HAS MAX: %f\r\n",top_ids_atmos[0].failure_score);
	      max = top_ids_atmos[0].failure_score;
	      vars.top_ids.clear();
	      for(i = 0; i < size; i++)
		vars.top_ids.push_back(top_ids_atmos[i]);
            }
        }
	  
	  if((int)top_ids_motion_1.size() > 0)
        {
	  fprintf(stderr,"FAILURE SCORE MOTION1: %f\r\n",top_ids_motion_1[0].failure_score); 
	  if(max < top_ids_motion_1[0].failure_score)
            {
	      max = top_ids_motion_1[0].failure_score;
	      vars.top_ids.clear();
	      for(i = 0; i < size; i++)
		vars.top_ids.push_back(top_ids_motion_1[i]);
            }
        }
	  
	  if((int)top_ids_motion_2.size() > 0)
	    {
	      fprintf(stderr,"FAILURE SCORE MOTION2: %f\r\n",top_ids_motion_2[0].failure_score); 
	      if(max < top_ids_motion_2[0].failure_score)
		{
		  max = top_ids_motion_2[0].failure_score;
		  vars.top_ids.clear();
	      for(i = 0; i < size; i++)
		vars.top_ids.push_back(top_ids_motion_2[i]);
		}
	    }
	  
	  if((int)top_ids_motion_3.size() > 0)
	    {
	      fprintf(stderr,"FAILURE SCORE MOTION3: %f\r\n",top_ids_motion_3[0].failure_score); 
	      if(max < top_ids_motion_3[0].failure_score)
		{
		  max = top_ids_motion_3[0].failure_score;
		  vars.top_ids.clear();
		  for(i = 0; i < size; i++)
                    vars.top_ids.push_back(top_ids_motion_3[i]);
		}
	    }
    }
    // majority vote - choose the ID that is appears the most for rank 1, 
    // then select the score series with the highest rank 1 score to report.
    // In the case of ties, or no clear winner, the score series with the 
    // highest rank 1 score will be selected.
    else if (fusion_type == 2) 
	  {
	    
	    fprintf(stderr,"fusion 2\r\n");
	    int winner, max = 0;
	    map<int,int> person_votes;
	    int ties[5];
	    int tie_count = 0;
	    int best_score_series = -1;
	    float max_winners = -1.0*FLT_MAX;
	    
	    // collect the votes
	    if (!atmos_failed && (int)top_ids_atmos.size() > 0)
	      {
		vote(person_votes, top_ids_atmos[0].id);
	      }
	    if (!motion1_failed && (int)top_ids_motion_1.size() > 0)
	      {
		vote(person_votes, top_ids_motion_1[0].id);
	      }
	    if (!motion2_failed && (int)top_ids_motion_2.size() > 0)
	      {
		vote(person_votes, top_ids_motion_2[0].id);
	      }
	    if(!motion3_failed && (int)top_ids_motion_3.size() > 0)
	      {
		vote(person_votes, top_ids_motion_3[0].id);
	      }
	    if(!default_failed && (int)top_ids_default.size() > 0)
	      {
		vote(person_votes, top_ids_default[0].id);
	      }
	    
	    // determine the winner
	    for (map<int, int>::iterator it = person_votes.begin(); it != person_votes.end(); ++it)
	      {
		if (it->second > max)
		  {
		    max = it->second;
		  }
	      }
	    
	    // check for ties
	    for (map<int, int>::iterator it = person_votes.begin(); it != person_votes.end(); ++it)
	      {
		if (it->second == max)
		  {
		    tie_count++;
		    ties[tie_count - 1] = it->first;
		  }
	      }
	    
	    for (i = 0; i < tie_count; i++)
	      {
		winner = ties[i];
		
		// first check for the best score series
		if ((int)top_ids_atmos.size() > 0 && winner == top_ids_atmos[0].id && top_ids_atmos[0].score > max_winners)
		  {
		    best_score_series = 1;
		    max_winners = top_ids_atmos[0].score;
		  }
		if ((int)top_ids_motion_1.size() > 0 && winner == top_ids_motion_1[0].id && top_ids_motion_1[0].score > max_winners)
		  {
		    best_score_series = 2;
		    max_winners = top_ids_motion_1[0].score;
		  }
		if ((int)top_ids_motion_2.size() > 0 && winner == top_ids_motion_2[0].id && top_ids_motion_2[0].score > max_winners)
		  {
		    best_score_series = 3;
		    max_winners = top_ids_motion_2[0].score;
		  }
		if((int)top_ids_motion_3.size() > 0 && winner == top_ids_motion_3[0].id && top_ids_motion_3[0].score > max_winners)
		  {
		    best_score_series = 4;
		    max_winners = top_ids_motion_3[0].score;
		  }
		if((int)top_ids_default.size() > 0 && winner == top_ids_default[0].id && top_ids_default[0].score > max_winners)
		  {
		    best_score_series = 5;
		  }
	      }
	    
        // copy the data for the best score series
        vars.top_ids.clear();
        if(best_score_series == 1)
        {
            for(i = 0; i < (int)top_ids_atmos.size(); i++)
            {
                top_ids_atmos[i].algo = 1;
                vars.top_ids.push_back(top_ids_atmos[i]);
            }
        }
        else if (best_score_series == 2)
        {
            for(i = 0; i < (int)top_ids_motion_1.size(); i++)
            {
                top_ids_motion_1[i].algo = 2;
                vars.top_ids.push_back(top_ids_motion_1[i]);
            }
        }
        else if (best_score_series == 3)
        {
            for(i = 0; i < (int)top_ids_motion_2.size(); i++)
            {
                top_ids_motion_2[i].algo = 3;
                vars.top_ids.push_back(top_ids_motion_2[i]);
            }
        }
        else if (best_score_series == 4)
        {
            for(i = 0; i < (int)top_ids_motion_3.size(); i++)
            {
                top_ids_motion_3[i].algo = 4;
                vars.top_ids.push_back(top_ids_motion_3[i]);
            }
        }
        else
        {
            for (i = 0; i < (int)top_ids_default.size(); i++)
            {
                top_ids_default[i].algo = 5;
                vars.top_ids.push_back(top_ids_default[i]);
            }
        }
    }
    // sum (3) & product rule (4)
    else if (fusion_type == 3 || fusion_type == 4)
    {
      	fprintf(stderr,"fusion 3 or 4\r\n");  
      int i;
        int id;
        map<int,id_score> person_score;

        if(!atmos_failed && (int)top_ids_atmos.size() > 0)
        {
            for (i = 0; i < (int)top_ids_atmos.size(); i++)
            {
                id = top_ids_atmos[i].id;
                person_score[id] = top_ids_atmos[i];
                person_score[id].score += 10;   // scale to positive;
            } 
        }
        if(!motion1_failed && (int)top_ids_motion_1.size() > 0)
        {
            for (i = 0; i < (int)top_ids_motion_1.size(); i++)
            {
                id = top_ids_motion_1[i].id;
                if(person_score.count(id) == 0)
                {
                    person_score[id] = top_ids_motion_1[i];
                    person_score[id].score += 10;
                }
                else 
                {
                    if (fusion_type == 3)
                    {
                        person_score[id].score += top_ids_motion_1[i].score + 10;
                    }
                    else 
                    {
                        person_score[id].score *= top_ids_motion_1[i].score + 10;
                    }
                }
            }
        }
        if(!motion2_failed && (int)top_ids_motion_2.size() > 0)
        {
            for (i = 0; i < (int)top_ids_motion_2.size(); i++)
            {
                id = top_ids_motion_2[i].id;
                if(person_score.count(id) == 0)
                {
                    person_score[id] = top_ids_motion_2[i];
                    person_score[id].score += 10;
                }
                else
                {
                    if (fusion_type == 3)
                    {
                        person_score[id].score += top_ids_motion_2[i].score + 10;
                    }
                    else
                    {
                        person_score[id].score *= top_ids_motion_2[i].score + 10;
                    }
                }
            }
        }
        if(!motion3_failed && (int)top_ids_motion_3.size() > 0)
        {
            for (i = 0; i < (int)top_ids_motion_3.size(); i++)
            {
                id = top_ids_motion_3[i].id;
                if(person_score.count(id) == 0)
                {
                    person_score[id] = top_ids_motion_3[i];
                    person_score[id].score += 10;
                }
                else
                {
                    if (fusion_type == 3)
                    {
                        person_score[id].score += top_ids_motion_3[i].score + 10;
                    }
                    else
                    {
                        person_score[id].score *= top_ids_motion_3[i].score + 10;
                    }
                }
            }
        }
        if(!default_failed && (int)top_ids_default.size() > 0)
        {
            for (i = 0; i < (int)top_ids_default.size(); i++)
            {
                id = top_ids_default[i].id;
                if(person_score.count(id) == 0)
                {
                    person_score[id] = top_ids_default[i];
                    person_score[id].score += 10;
                }
                else
                {
                    if (fusion_type == 3)
                    {
                        person_score[id].score += top_ids_default[i].score + 10;
                    }
                    else
                    {
                        person_score[id].score *= top_ids_default[i].score + 10;
                    }
                }
            }
        } 

        // Now put the scores into a list
        list<id_score> scorelist;

        {
            map<int,id_score>::iterator it;
            for(it = person_score.begin(); it != person_score.end(); it++)
            {
                scorelist.push_back((*it).second);
            }
        }

        // Now sort it
        scorelist.sort(fuseCompare);

        // Return the results
        vars.top_ids.clear();
        {
            list<id_score>::iterator it;
            for(it = scorelist.begin(); it != scorelist.end(); it++)
            {
                vars.top_ids.push_back(*it);
            }
        }
    }
    ///////////////////////////////////////////FUSION 5/////////////////////////////////////////////////////
 

  else if (fusion_type == 5)
      {
	int best_score_series = -1;
        float max = -1.0*FLT_MAX;
	fprintf(stderr,"fusion 5\r\n");
	
        //print which ones failed
	if (all_failed==false)
	  {
	    if (params.use_motion_deblur==1 && params.use_atmos_deblur==1)
	      fprintf(stderr,"ATMOS_FAILED: %d  MOTION1 FAILED: %d  MOTION2 FAILED: %d  MOTION3 FAILED: %d REGULAR_FAILED: %d\r\n",(int)atmos_failed,(int)motion1_failed,(int)motion2_failed,(int)motion3_failed,(int)default_failed);
	    else if (params.use_motion_deblur==1 && params.use_atmos_deblur==0)
	      fprintf(stderr,"MOTION1 FAILED: %d  MOTION2 FAILED: %d  MOTION3 FAILED: %d REGULAR_FAILED: %d\r\n",(int)motion1_failed,(int)motion2_failed,(int)motion3_failed,(int)default_failed);
	    else if (params.use_motion_deblur==0 && params.use_atmos_deblur==1)
	      fprintf(stderr,"ATMOS_FAILED: %d  REGULAR_FAILED: %d\r\n",(int)atmos_failed,(int)default_failed);
	    else
	      fprintf(stderr,"REGULAR_FAILED: %d\r\n",(int)default_failed);
	  }
	
	else
	  fprintf(stderr,"ALL FAILED!!!!!\r\n");
	// first check for the best score series 
        
	//print atmos scores
	if(params.use_atmos_deblur==1 && !atmos_failed && (int)top_ids_atmos.size() > 0)
	  {
	    for (i=0; i < 10; i++)
	      {
		fprintf(stderr,"TEST IDS ATMOS[%d]: %f\r\n",i,top_ids_atmos[i].score);
	      }
	    
	    fprintf(stderr,"\r\n"); 
	  }
	
	
	//print motion blur scores
	if(params.use_motion_deblur==1 && !motion1_failed && (int)top_ids_motion_1.size() > 0)
	  {
	    for (i=0; i < 10; i++)
	      {
		fprintf(stderr,"TEST IDS MOTION1[%d]: %f\r\n",i,top_ids_motion_1[i].score);
	      }
	    
	    fprintf(stderr,"\r\n"); 
	  }
	
	if(params.use_motion_deblur==1 && !motion2_failed && (int)top_ids_motion_2.size() > 0)
	  {
	    for (i=0; i < 10; i++)
	      {
		fprintf(stderr,"TEST IDS MOTION2[%d]: %f\r\n",i,top_ids_motion_2[i].score);
	      }
	    
	    fprintf(stderr,"\r\n"); 
	  }
	
	
	if(params.use_motion_deblur==1 && !motion3_failed && (int)top_ids_motion_3.size() > 0)
	  {
	    for (i=0; i < 10; i++)
	      {
		fprintf(stderr,"TEST IDS MOTION3[%d]: %f\r\n",i,top_ids_motion_3[i].score);
	      }
	    
	    fprintf(stderr,"\r\n"); 
	  }
	
	
	if(!default_failed && (int)top_ids_default.size() > 0)
	  {
	    for (i=0; i < 10; i++)
	      {
		fprintf(stderr,"TEST IDS REGULAR[%d]: %f\r\n",i,top_ids_default[i].score);
	      }
	  }
	
	float atmos_distance;
	float regular_distance;
        float motion1_distance;
	float motion2_distance;
	float motion3_distance;
	
	atmos_distance=0;
	motion1_distance=0;
	motion2_distance=0;
	motion3_distance=0;
	regular_distance=0;
	
        if (params.use_atmos_deblur==1 && !atmos_failed)
	  atmos_distance=top_ids_atmos[0].score-top_ids_atmos[1].score;
	
	if (params.use_motion_deblur==1 && !motion1_failed)
	  motion1_distance=top_ids_motion_1[0].score-top_ids_motion_1[1].score;
	
	if (params.use_motion_deblur==1 && !motion2_failed)
	  motion2_distance=top_ids_motion_2[0].score-top_ids_motion_2[1].score;
	
	if (params.use_motion_deblur==1 && !motion3_failed)
	  motion3_distance=top_ids_motion_3[0].score-top_ids_motion_3[1].score;
	
	if (!default_failed)
	  regular_distance=top_ids_default[0].score-top_ids_default[1].score;
		
	if (params.use_atmos_deblur==1)
	  fprintf(stderr,"atmos_distance: %f\r\n",atmos_distance);
	if (params.use_motion_deblur==1)
	  fprintf(stderr,"motion1: %f motion2: %f  motion3: %f\r\n",motion1_distance,motion2_distance,motion3_distance);
	
	fprintf(stderr,"regular_distance: %f\r\n",regular_distance);
	
	if((!atmos_failed && (int)top_ids_atmos.size() > 0) &&  atmos_distance > max)      
	  {
            best_score_series = 1;
	    max = atmos_distance;
	  }
	
	if((!motion1_failed && (int)top_ids_motion_1.size() > 0) && motion1_distance > max)
	  {
	    best_score_series = 2;
	    max = motion1_distance; 
	  }
	
	if((!motion2_failed && (int)top_ids_motion_2.size() > 0) && motion2_distance > max)
	  {
	    best_score_series = 3;
	    max = motion2_distance;
	  }
	
	if((!motion3_failed && (int)top_ids_motion_3.size() > 0) && motion3_distance > max)
	  {
	    best_score_series = 4;
	    max = motion3_distance;
	  }
	/*
	if((!default_failed && (int)top_ids_default.size() > 0) && regular_distance > max)      
	  {
            best_score_series = 5;
	    max = regular_distance;
	  }
	*/
        // copy the data for the best score series
        vars.top_ids.clear();
        if(best_score_series == 1)
	  {
	    for(i = 0; i < (int)top_ids_atmos.size(); i++)
	      {
		top_ids_atmos[i].algo = 1;
		vars.top_ids.push_back(top_ids_atmos[i]);
	      }
	  }
        else if (best_score_series == 2)
	  {
            for(i = 0; i < (int)top_ids_motion_1.size(); i++)
	      {
                top_ids_motion_1[i].algo = 2;
                vars.top_ids.push_back(top_ids_motion_1[i]);
	      }
	  }
        else if (best_score_series == 3)
	  {
            for(i = 0; i < (int)top_ids_motion_2.size(); i++)
	      {
                top_ids_motion_2[i].algo = 3;
                vars.top_ids.push_back(top_ids_motion_2[i]);
	      }
	  }
        else if (best_score_series == 4)
	  {
            for(i = 0; i < (int)top_ids_motion_3.size(); i++)
	      {
                top_ids_motion_3[i].algo = 4;
                vars.top_ids.push_back(top_ids_motion_3[i]);            
	      }
	  }
	/*
        else
	  {
            for (i = 0; i < (int)top_ids_default.size(); i++)
	      {
                top_ids_default[i].algo = 5;
                vars.top_ids.push_back(top_ids_default[i]);
	      }
	  }
	*/
      }
    //////////////////////////////////////////////fusion 6/////////////////////////////////////////////
     else if (fusion_type == 6)
      {
	int best_score_series = -1;
        float max = -1.0*FLT_MAX;
	fprintf(stderr,"fusion 6\r\n");
	
        //print which ones failed
	if (all_failed==false)
	  {
	    if (params.use_motion_deblur==1 && params.use_atmos_deblur==1)
	      fprintf(stderr,"ATMOS_FAILED: %d  MOTION1 FAILED: %d  MOTION2 FAILED: %d  MOTION3 FAILED: %d REGULAR_FAILED: %d\r\n",(int)atmos_failed,(int)motion1_failed,(int)motion2_failed,(int)motion3_failed,(int)default_failed);
	    else if (params.use_motion_deblur==1 && params.use_atmos_deblur==0)
	      fprintf(stderr,"MOTION1 FAILED: %d  MOTION2 FAILED: %d  MOTION3 FAILED: %d REGULAR_FAILED: %d\r\n",(int)motion1_failed,(int)motion2_failed,(int)motion3_failed,(int)default_failed);
	    else if (params.use_motion_deblur==0 && params.use_atmos_deblur==1)
	      fprintf(stderr,"ATMOS_FAILED: %d  REGULAR_FAILED: %d\r\n",(int)atmos_failed,(int)default_failed);
	    else
	      fprintf(stderr,"REGULAR_FAILED: %d\r\n",(int)default_failed);
	  }
	
	else
	  fprintf(stderr,"ALL FAILED!!!!!\r\n");
	// first check for the best score series 
        
	//print atmos scores
	if(params.use_atmos_deblur==1 && !atmos_failed && (int)top_ids_atmos.size() > 0)
	  {
	    for (i=0; i < 10; i++)
	      {
		fprintf(stderr,"TEST IDS ATMOS[%d]: %f\r\n",i,top_ids_atmos[i].score);
	      }
	    
	    fprintf(stderr,"\r\n"); 
	  }
	
	
	//print motion blur scores
	if(params.use_motion_deblur==1 && !motion1_failed && (int)top_ids_motion_1.size() > 0)
	  {
	    for (i=0; i < 10; i++)
	      {
		fprintf(stderr,"TEST IDS MOTION1[%d]: %f\r\n",i,top_ids_motion_1[i].score);
	      }
	    
	    fprintf(stderr,"\r\n"); 
	  }
	
	if(params.use_motion_deblur==1 && !motion2_failed && (int)top_ids_motion_2.size() > 0)
	  {
	    for (i=0; i < 10; i++)
	      {
		fprintf(stderr,"TEST IDS MOTION2[%d]: %f\r\n",i,top_ids_motion_2[i].score);
	      }
	    
	    fprintf(stderr,"\r\n"); 
	  }
	
	
	if(params.use_motion_deblur==1 && !motion3_failed && (int)top_ids_motion_3.size() > 0)
	  {
	    for (i=0; i < 10; i++)
	      {
		fprintf(stderr,"TEST IDS MOTION3[%d]: %f\r\n",i,top_ids_motion_3[i].score);
	      }
	    
	    fprintf(stderr,"\r\n"); 
	  }
	
	
	if(!default_failed && (int)top_ids_default.size() > 0)
	  {
	    for (i=0; i < 10; i++)
	      {
		fprintf(stderr,"TEST IDS REGULAR[%d]: %f\r\n",i,top_ids_default[i].score);
	      }
	  }
	
	float atmos_distance;
	float regular_distance;
        float motion1_distance;
	float motion2_distance;
	float motion3_distance;
	
	atmos_distance=0;
	motion1_distance=0;
	motion2_distance=0;
	motion3_distance=0;
	regular_distance=0;
	
        if (params.use_atmos_deblur==1 && !atmos_failed)
	  {
	    atmos_distance=top_ids_atmos[0].score-top_ids_atmos[1].score;
	    atmos_distance += top_ids_atmos[0].failure_score; 	 
	  }

	if (params.use_motion_deblur==1 && !motion1_failed)
	  {
	    motion1_distance=top_ids_motion_1[0].score-top_ids_motion_1[1].score;
	    motion1_distance += top_ids_motion_1[0].failure_score; 
	  }
	
	if (params.use_motion_deblur==1 && !motion2_failed)
	  {	 
	    motion2_distance=top_ids_motion_2[0].score-top_ids_motion_2[1].score;
	    motion2_distance += top_ids_motion_2[0].failure_score; 
	  }
	
	if (params.use_motion_deblur==1 && !motion3_failed)
	  {	 
	    motion3_distance=top_ids_motion_3[0].score-top_ids_motion_3[1].score;
	    motion3_distance += top_ids_motion_3[0].failure_score; 
	  }
	
	if (!default_failed)
	  {	  
	    regular_distance=top_ids_default[0].score-top_ids_default[1].score;
	    regular_distance += top_ids_default[0].failure_score; 
	  }
	
	
	if (params.use_atmos_deblur==1)
	  fprintf(stderr,"atmos_distance: %f failure score: %f Total: %f\r\n",top_ids_atmos[0].score-top_ids_atmos[1].score,top_ids_atmos[0].failure_score,atmos_distance);
	if (params.use_motion_deblur==1)
	  {
	  fprintf(stderr,"motion1: %f motion2: %f  motion3: %f\r\n",motion1_distance,motion2_distance,motion3_distance);
	  fprintf(stderr,"motion score: %f motion score: %f  motionscore: %f\r\n",top_ids_motion_1[0].failure_score,top_ids_motion_2[0].failure_score,top_ids_motion_3[0].failure_score);
	  }
	fprintf(stderr,"regular_distance: %f failure_score: %f Total: %f\r\n",top_ids_default[0].score-top_ids_default[1].score,top_ids_default[0].failure_score,regular_distance);
	
	if((!atmos_failed && (int)top_ids_atmos.size() > 0) &&  atmos_distance > max)      
	  {
            best_score_series = 1;
	    max = atmos_distance;
	  }
	
	if((!motion1_failed && (int)top_ids_motion_1.size() > 0) && motion1_distance > max)
	  {
	    best_score_series = 2;
	    max = motion1_distance; 
	  }
	
	if((!motion2_failed && (int)top_ids_motion_2.size() > 0) && motion2_distance > max)
	  {
	    best_score_series = 3;
	    max = motion2_distance;
	  }
	
	if((!motion3_failed && (int)top_ids_motion_3.size() > 0) && motion3_distance > max)
	  {
	    best_score_series = 4;
	    max = motion3_distance;
	  }
	
	if((!default_failed && (int)top_ids_default.size() > 0) && regular_distance > max)      
	  {
            best_score_series = 5;
	    max = regular_distance;
	  }
	
        // copy the data for the best score series
        vars.top_ids.clear();
        if(best_score_series == 1)
	  {
	    for(i = 0; i < (int)top_ids_atmos.size(); i++)
	      {
		top_ids_atmos[i].algo = 1;
		vars.top_ids.push_back(top_ids_atmos[i]);
	      }
	  }
        else if (best_score_series == 2)
	  {
            for(i = 0; i < (int)top_ids_motion_1.size(); i++)
	      {
                top_ids_motion_1[i].algo = 2;
                vars.top_ids.push_back(top_ids_motion_1[i]);
	      }
	  }
        else if (best_score_series == 3)
	  {
            for(i = 0; i < (int)top_ids_motion_2.size(); i++)
	      {
                top_ids_motion_2[i].algo = 3;
                vars.top_ids.push_back(top_ids_motion_2[i]);
	      }
	  }
        else if (best_score_series == 4)
	  {
            for(i = 0; i < (int)top_ids_motion_3.size(); i++)
	      {
                top_ids_motion_3[i].algo = 4;
                vars.top_ids.push_back(top_ids_motion_3[i]);            
	      }
	  }
	
        else
	  {
            for (i = 0; i < (int)top_ids_default.size(); i++)
	      {
                top_ids_default[i].algo = 5;
                vars.top_ids.push_back(top_ids_default[i]);
	      }
	  }
	
      }
    
    ////////////////////////////////////fusion 7 deblur images only/////////////////////////////////////////
     else if(fusion_type == 7)
    {
        int best_score_series = -1;
        float max = -1.0*FLT_MAX;
	fprintf(stderr,"fusion 7\r\n");
	

	if((!atmos_failed && (int)top_ids_atmos.size() > 0) && 
	  top_ids_atmos[0].score > max)

	  {
            best_score_series = 1;
	    max = top_ids_atmos[0].score;
	   
	  }

        if((!motion1_failed && (int)top_ids_motion_1.size() > 0) &&
           top_ids_motion_1[0].score > max)
	  {
            best_score_series = 2;
            max = top_ids_motion_1[0].score; 
	  }
        if((!motion2_failed && (int)top_ids_motion_2.size() > 0) &&
           top_ids_motion_2[0].score > max)
        {
	  best_score_series = 3;
	  max = top_ids_motion_2[0].score;
        }
        if((!motion3_failed && (int)top_ids_motion_3.size() > 0) &&
           top_ids_motion_3[0].score > max)
	  {
            best_score_series = 4;
            max = top_ids_motion_3[0].score;
	  }


        // copy the data for the best score series
        vars.top_ids.clear();
        if(best_score_series == 1)
        {
            for(i = 0; i < (int)top_ids_atmos.size(); i++)
            {
                top_ids_atmos[i].algo = 1;
                vars.top_ids.push_back(top_ids_atmos[i]);
            }
        }
        else if (best_score_series == 2)
        {
            for(i = 0; i < (int)top_ids_motion_1.size(); i++)
            {
                top_ids_motion_1[i].algo = 2;
                vars.top_ids.push_back(top_ids_motion_1[i]);
            }
        }
        else if (best_score_series == 3)
        {
            for(i = 0; i < (int)top_ids_motion_2.size(); i++)
            {
                top_ids_motion_2[i].algo = 3;
                vars.top_ids.push_back(top_ids_motion_2[i]);
            }
        }
        else if (best_score_series == 4)
        {
            for(i = 0; i < (int)top_ids_motion_3.size(); i++)
            {
                top_ids_motion_3[i].algo = 4;
                vars.top_ids.push_back(top_ids_motion_3[i]);            
            }
        }
	
     
 }

    // Used to select the best normalized image
int best_algo = 0;
 if(vars.top_ids.size() > 0)
   best_algo = vars.top_ids[0].algo;
 
 if(best_algo == 1) *normalized = anorm;
 else SAFE_RELEASE(anorm);
 if(best_algo == 2) *normalized = m1norm;
 else SAFE_RELEASE(m1norm);
 if(best_algo == 3) *normalized = m2norm;
 else SAFE_RELEASE(m2norm);
 if(best_algo == 4) *normalized = m3norm;
 else SAFE_RELEASE(m3norm);
 if(best_algo == 5) *normalized = regnorm;
 else SAFE_RELEASE(regnorm);
 
 if(params.quiet == 0)
   {
     if(best_algo == 1)
       printf("Rank 1 generated using: atmospheric deblur\n");
     else if(best_algo == 2)
       printf("Rank 1 generated using: motion deblur (variant 1)\n");
     else if(best_algo == 3)
       printf("Rank 1 generated using: motion deblur (variant 2)\n");
     else if(best_algo == 4)
       printf("Rank 1 generated using: motion deblur (variant 3)\n");
     else if(best_algo == 5)
       printf("Rank 1 generated using: vanilla recognition\n");
   }
 
 if(face_crop == 1)
   params.crop_face = 1;
 
 if(failcount >= totalcount)
   {
     // Wow, all images failed!
     return -100;
     //	return vars.top_ids[0].id;
   }
 else if(vars.top_ids.size() > 0)
   {
     // We have an answer
     return vars.top_ids[0].id;
   }
 else
   {
     // Ok, something went horribly wrong
     return -1;
   }
}
////////////////////////////////////////////////////////////////////////////////
// Recognition utility functions
////////////////////////////////////////////////////////////////////////////////

// comparison, not case sensitive.
static bool fuseCompare(id_score first, id_score second)
{
    if(first.score > second.score)
        return true;
    return false;
}

// voting utility fuction
static void vote(map<int, int> &person_votes, int id)
{
    int i;
    int found = 0;

    if (person_votes.count(id) == 0)
    {
        person_votes[id] = 1;
    }
    else
    {
        person_votes[id]++;
    }
}

/**
 * XXX No longer needed
 *Fuses the results of deblurred and undeblurred results
 *@param vector<id_score> &src The vector containing all recognition id score pairs.
 *@param vector<id_score> &top The vector to store the fusion results.
 *@param int num The number of scores to keep.
 */
static void fuseData(vector<id_score>& src, vector<id_score>& dst, int num)
{
    int i, j, id, k, pass;
    float max;
    vector<id_score> sorted;
    dst.clear();

    map<int,id_score> person_score;

    if(src.size() < (unsigned int)num)
    {
        printf("\nFusing fewer scores than requested because there are not enough scores. Scores %d requested %d\n", (int)src.size(), num);
        num = src.size();
    }

    // First pick the best score for each ID
    for(i = 0; i < src.size(); i++)
    {
        int id = src[i].id;
        if(person_score.count(id) == 0)
            person_score[id] = src[i];
        else if(src[i].score > person_score[id].score)
            person_score[id] = src[i];
    }

    // Now put the scores into a list
    list<id_score> scorelist;

    {
        map<int,id_score>::iterator it;
        for(it = person_score.begin(); it != person_score.end(); it++)
            scorelist.push_back((*it).second);
    }

    // Now sort it
    scorelist.sort(fuseCompare);

    // Return the results
    {
        list<id_score>::iterator it;
        for(it = scorelist.begin(); it != scorelist.end(); it++)
            dst.push_back(*it);
    }
}

/**
 *Checks if the motion image has a face that the detector can find, if so process it if not discard it
 *@param IplImage** motion_images The motion deblurred images
 *@param int size The number of deblurred images
 *@result IplImage** The detected faces in the motion deblurred images
 */
IplImage** motionFaces(IplImage** motion_images, int size, recogvars &vars)
{
    // TODO: is this a bug? Should this be a 2 or should it be size?
    IplImage** results = (IplImage**)safe_malloc(size * sizeof(IplImage*));

        for(int i = 0; i < size; i++)
      {      
        //      CvRect r = detect_face(motion_images[i]);
        //      if(r.x < 0 || r.y < 0 || r.width < 0 || r.height < 0)
        //        {
        //      printf("MOTION IMAGE %d FAILED TO FIND FACE DISCARDING\n", i);
        //      results[i] = NULL;
        //      continue;
        //        }
        results[i] = cvCloneImage(motion_images[i]);
        
        
      }
    return results;
}
/**
 *Computes the statistics of recognition and if the anwser was right or not.
 *@param prog_params &params The program parameters
 *@param recogvars &vars The recognition variables.
 *@param bool failed True if failed, false if otherwise.
 */
static void recordAnswer(prog_params &params, recogvars &vars, bool failed)
{
    if(vars.top_ids.size() == 0)
        return;

    int tidsz = vars.top_ids.size();
    int keep_scores = (params.keep_scores > tidsz) ? tidsz : params.keep_scores;
    int lbl, i, rank = -1;
    float score;
    int gt = params.use_groundtruth;
    v1_capture_gtdata gtdata;

printf("WEIBULL HERE!!!!!\r\n");    

// Print out the top N scores
    if(params.keep_scores > 0 && params.quiet == 0)
        printf("Top %d scores:\n", keep_scores);
    for(i = 0; i < keep_scores; i++)
    {
        lbl = vars.top_ids[i].id;

        if(params.onevsrest)
            score = vars.top_ids[i].score;
        else
            score = vars.top_ids[i].score * 100.;

        if(vars.cat_names.count(lbl) > 0)
        {
            if(params.onevsrest)
            {
                printf("  %4d => %-10.3f [%s]\n", lbl, score, vars.cat_names[lbl].c_str());
            }
            else
            {
                char* tmpstr = NULL;
                if(asprintf(&tmpstr, "%.3f%%", score) > 0)
                    printf("  %4d => %-10s [%s]\n", lbl, tmpstr, vars.cat_names[lbl].c_str());
                else
                    printf("  %4d => %-10.3f%% [%s]\n", lbl, score, vars.cat_names[lbl].c_str());
                SAFE_FREE(tmpstr);
            }
        }
        else
        {
            if(params.onevsrest)
                printf("  %4d => %.3f\n", lbl, score);
            else
                printf("  %4d => %.3f%%\n", lbl, score);
        }
    }

    lbl = vars.top_ids[0].id;

    if(gt) gt = v1_capture_getdata(vars.cap, &gtdata) ? 1 : 0;
    if(gt && gtdata.label >= 0)
    {
        // Determine the rank
        for(i = 0; i < tidsz; i++)
        {
            if(vars.top_ids[i].id == gtdata.label)
            {
                rank = i + 1;
                break;
            }
        }

        if(vars.cat_total.count(gtdata.label) == 0)
        {
            vars.cat_total[gtdata.label] = 0;
            vars.cat_correct[gtdata.label] = 0;
        }
        
        vars.total++;
        vars.cat_total[gtdata.label]++;
        



        if(failed)
        {
            if(rank == 1)
            {
                vars.fp_fail_incorrect++;
                printf("Answer: FAILURE PREDICTED (incorrect)\n");
            }
            else
            {
                vars.correct++;
                vars.cat_correct[gtdata.label]++;
                vars.fp_fail_correct++;
                printf("Answer: FAILURE PREDICTED (correct)\n");
            }
        }
        else
        {
            if(rank != -1)
            {
                if(vars.rank_count.count(rank) == 0)
                    vars.rank_count[rank] = 1;
                else
                    vars.rank_count[rank] += 1;
                if(rank > vars.maxrank)
                    vars.maxrank = rank;
                vars.rank_counter++;
            }

            if(lbl == gtdata.label)
            {
                if(params.quiet == 0)
                {
                    if(vars.cat_names.count(lbl) > 0)
                        printf("Answer: %d [%s] (correct, rank = %d)\n", lbl, vars.cat_names[lbl].c_str(), rank);
                    else
                        printf("Answer: %d (correct, rank = %d)\n", lbl, rank);
                }

                vars.correct++;
                vars.cat_correct[gtdata.label]++;
                if(vars.fpmodel != NULL)
                    vars.fp_pass_correct++;
            }
            else
            {
                if(params.quiet == 0)
                {
                    if(vars.cat_names.count(lbl) > 0)
                        printf("Answer: %d [%s] ", lbl, vars.cat_names[lbl].c_str());
                    else
                        printf("Answer: %d ", lbl);
                    if(vars.cat_names.count(gtdata.label) > 0)
                        printf("(incorrect, expected %d [%s], rank = %d)\n",
                               gtdata.label, vars.cat_names[gtdata.label].c_str(), rank);
                    else
                        printf("(incorrect, expected %d, rank = %d)\n", gtdata.label, rank);
                }

                if(vars.fpmodel != NULL)
                    vars.fp_pass_incorrect++;
            }
        }

        if(params.quiet == 0)
        {
            if(vars.cat_names.count(gtdata.label) > 0)
                printf("Category %d [%s] success rate: %d / %d (%.2f%%)\n",
                       gtdata.label, vars.cat_names[gtdata.label].c_str(),
                       vars.cat_correct[gtdata.label], vars.cat_total[gtdata.label],
                       100.0f * (float)vars.cat_correct[gtdata.label] / (float)vars.cat_total[gtdata.label]);
            else
                printf("Category %d success rate: %d / %d (%.2f%%)\n", gtdata.label,
                       vars.cat_correct[gtdata.label], vars.cat_total[gtdata.label],
                       100.0f * (float)vars.cat_correct[gtdata.label] / (float)vars.cat_total[gtdata.label]);
            if(vars.total > 0)
                printf("Global success rate: %d / %d (%.2f%%)\n\n", vars.correct, vars.total,
                       (float)((float)100.0 * (float)vars.correct / (float)vars.total));
        }
        else
            printf("\n");
    }
    else
    {
        if(failed)
        {
            printf("Answer: FAILURE PREDICTED\n\n");
        }
        else
        {
            if(params.quiet == 0)
            {
                if(vars.cat_names.count(lbl) > 0)
                    printf("Answer: %d [%s]\n\n", lbl, vars.cat_names[lbl].c_str());
                else
                    printf("Answer: %d\n\n", lbl);
            }
        }
    }
}

/**
 * 
 *
 *
 */
static void answerMulti(prog_params &params, recogvars &vars, vector<multi_face_result> &results)
{
    
}

/**
 * Run geometric normalization on an image
 *@param prog_params &params The program parameters
 *@param recogvars &vars The recognition variables.
 *@param IplImage** input The input image, that is replaced with the geonormalized image
 *@return int 0 if success, negative if failure.
 */

static int runGeonorm(prog_params &params, recogvars &vars, IplImage **input)
{
    IplImage* geo_normed;
    IplImage* cropped;
    IplImage* test_cropped;
    // First face detection if necessary
    int lx = -1, ly = -1, rx = -1, ry = -1;
    int lx1 = -1, ly1 = -1, rx1 = -1, ry1 = -1;
    int doeyedetect = params.use_eye_detect;
    int dofacedetect = params.crop_face;
    bool crop = false, gt;
    CvRect crop_bounds;
    v1_capture_gtdata gtdata;

    if((gt = v1_capture_getdata(vars.cap, &gtdata)) == true)
    {
        lx = gtdata.lx;
        ly = gtdata.ly;
        rx = gtdata.rx;
        ry = gtdata.ry;
    }

    if((doeyedetect == 0) && (lx < 0 || rx < 0 || ly < 0 || ry < 0))
    {
        printf("WARNING: Invalid eye coordinates given (%d, %d), (%d, %d), running eye detection.\n", lx, ly, rx, ry);
        doeyedetect = 1;
    }

    // No point in doing face detection if eye detection is turned off
    if(doeyedetect)
    {
        if(dofacedetect)
        {
            CvRect r = detect_face(vars.cascade, vars.storage, *input);
            if(r.x < 0 || r.y < 0 || r.width < 0 || r.height < 0)
            {
                printf("WARNING: Face detection failed, skipping.\n\n");
                vars.face_detect_fails++;
                if(vars.cap->type == FILE_LIST)
                    vars.problem_files.push_back(string(v1_capture_name(vars.cap)));
                return -1;
            }

            // We may have some groundtruth data for eyes, a good way to verify the face
            if(lx > 0 && ly > 0 && rx > 0 && ry > 0)
            {
                if(r.x > lx || r.x > rx || r.y > ly || r.y > ry ||
                   (r.x + r.width) < lx || (r.x + r.width) < rx ||
                   (r.y + r.height) < ly || (r.y + r.height) < ry)
                {
                    printf("WARNING: Groundtruth eyes aren't inside of the detected face region.\n");
                    vars.face_fails++;
                }
            }

            crop_bounds = r;
            crop = true;
        }
        else if(gtdata.cropX >= 0 && gtdata.cropY >= 0 && gtdata.cropW > 0 && gtdata.cropH > 0 &&
                params.use_motion_deblur == 0 && params.use_atmos_deblur == 0)
        {
            crop_bounds.x = gtdata.cropX;
            crop_bounds.y = gtdata.cropY;
            crop_bounds.width = gtdata.cropW;
            crop_bounds.height = gtdata.cropH;
            crop = true;
        }
        else
        {
            crop_bounds.x = 0;
            crop_bounds.y = 0;
            crop_bounds.width = (*input)->width;
            crop_bounds.height = (*input)->height;
            crop = false;
        }

        cropped = *input;
        if(crop)
        {
            cropped = cropIpl(*input, crop_bounds.x, crop_bounds.y, crop_bounds.width, crop_bounds.height);
            if(cropped == NULL)
            {
                printf("WARNING: Crop to ROI failed, skipping.\n\n");
                if(vars.cap->type == FILE_LIST)
                    vars.problem_files.push_back(string(v1_capture_name(vars.cap)));
                return -2;
            }
        }

        int recrop_x;
        int recrop_y;

        recrop_x=crop_r.x-orig_r.x;
        if (recrop_x < 0)
            recrop_x=0;

        recrop_y=crop_r.y-orig_r.y;
        if (recrop_y < 0)
            recrop_y=0;

        test_cropped = cvCreateImage(cvSize(abs(crop_r.width-recrop_x), abs(crop_r.height-recrop_y)), IPL_DEPTH_8U, 1);
        test_cropped = cropIpl(cropped, recrop_x, recrop_y, crop_r.width, crop_r.height);

        // Aye eye
        IplImage* resized = get_image(test_cropped, 150, params.use_python, 1);
        // IplImage* resized = get_image(cropped, 150, params.use_python, 1);

        eye_detect(resized, &lx, &rx, &ly, &ry, &lx1, &rx1, &ly1, &ry1);

        // Now scale the points back to their proper size
        double scaleX = (double)(test_cropped->width) / (double)(resized->width);
        double scaleY = (double)(test_cropped->height) / (double)(resized->height);
        lx = (int)(((double)lx * scaleX + recrop_x)) + crop_bounds.x;
        ly = (int)(((double)ly * scaleY + recrop_y)) + crop_bounds.y;
        rx = (int)(((double)rx * scaleX + recrop_x)) + crop_bounds.x;
        ry = (int)(((double)ry * scaleY + recrop_y)) + crop_bounds.y;

        if(cropped != *input)
            SAFE_RELEASE(cropped);
        SAFE_RELEASE(resized);
    }

    // Make sure we have valid coordinates
    if(lx < 0 || rx < 0 || ly < 0 || ry < 0)
    {
        printf("WARNING: eye coordinates invalid (%d,%d),(%d,%d), skipping.\n\n", lx, ly, rx, ry);
        if(vars.cap->type == FILE_LIST)
            vars.problem_files.push_back(string(v1_capture_name(vars.cap)));
        return -3;
    }

    // Run geo-normalization
    if((geo_normed = perform_geo_norm(*input, lx, ly, rx, ry, params.hist_norm, 1)) == NULL)
    {
        printf("WARNING: geo normalization failed, skipping.\n\n");
        if(vars.cap->type == FILE_LIST)
            vars.problem_files.push_back(string(v1_capture_name(vars.cap)));
        return -4;
    }

    IplImage* tmp = *input;
    SAFE_RELEASE(tmp);

    *input = geo_normed;

    return 0;
}

/**
 *Prints the recognition time rate
 *@param double elapsed Time since program started.
 *@param int total_processed The number processed.
 */
// Print the average recognition rate
static void printRate(double elapsed, int total_processed)
{
    if(elapsed > 10.0 && total_processed > 0)
    {
        double rate = (double)total_processed / elapsed;
        printf("Average recognition rate:\n  %.2g images per second\n", rate);
    }    
}

// 
/**
 *Print the recognition statistics. Only really useful with groundtruth
 *@param prog_params &params The program parameters
 *@param recogvars &vars The recognition variables.
 */

static void printStats(prog_params &params, recogvars &vars)
{
    if(params.use_groundtruth)
    {
        printf("Recognition rates per category for groundtruth data:\n");
        map<int ,int>::iterator it;
        for(it=vars.cat_total.begin(); it != vars.cat_total.end(); it++)
        {
            int cat = (*it).first;
            int ccorrect = vars.cat_correct[cat];
            int ctotal = vars.cat_total[cat];

            if(vars.cat_names.count(cat) > 0)
                printf("  Category %4d rate:  %4d / %4d (%.2f%%)\t[%s]\n", cat, ccorrect, ctotal,
                       100.0f * (float)ccorrect / (float)ctotal, vars.cat_names[cat].c_str());
            else
                printf("  Category %4d rate:  %4d / %4d (%.2f%%)\n", cat, ccorrect, ctotal,
                       100.0f * (float)ccorrect / (float)ctotal);
        }

        if(vars.total > 0)
            printf("  Global success rate: %4d / %4d (%.2f%%)\n\n", vars.correct, vars.total,
                   (float)((float)100.0 * (float)vars.correct / (float)vars.total));

        if(vars.rank_counter > 0) // division by zero? I'll pass
        {
            int rankt = 0;
            printf("Ranking for groundtruth data:\n");

            for(int rank = 1; rank <= vars.maxrank; rank++)
            {
                if(vars.rank_count.count(rank) > 0)
                    rankt += vars.rank_count[rank];
                printf("  Rank %4d: %4d / %4d (%.2f%%)\n", rank, rankt, vars.rank_counter,
                       100.0f * (float)rankt / (float)vars.rank_counter);
            }

            printf("\n");
        }
    }

    if(vars.fp_fail_correct > 0 || vars.fp_fail_incorrect > 0 ||
       vars.fp_pass_correct > 0 || vars.fp_pass_incorrect > 0)
    {
        int fails = vars.fp_fail_correct + vars.fp_fail_incorrect;
        int passes = vars.fp_pass_correct + vars.fp_pass_incorrect;
        int total = fails + passes;
        int correct = vars.fp_fail_correct + vars.fp_pass_correct;
        int incorrect = vars.fp_fail_incorrect + vars.fp_pass_incorrect;

        printf("Failure prediction:\n\n");
        printf("             Fails   Passes\n");
        printf("  Correct:   %-8d%d\n", vars.fp_fail_correct, vars.fp_pass_correct);
        printf("  Incorrect: %-8d%d\n\n", vars.fp_fail_incorrect, vars.fp_pass_incorrect);

        printf("  Total fails:     %4d / %4d (%.2f%%)\n",
               fails, total, 100. * (float)fails/(float)total);
        printf("  Total passes:    %4d / %4d (%.2f%%)\n",
               passes, total, 100. * (float)passes/(float)total);
        printf("  Total correct:   %4d / %4d (%.2f%%)\n",
               correct, total, 100. * (float)correct/(float)total);
        printf("  Total incorrect: %4d / %4d (%.2f%%)\n\n",
               incorrect, total, 100. * (float)incorrect/(float)total);
    }

    if(vars.face_fails > 0 || vars.face_detect_fails > 0)
    {
        printf("Face detection:\n");
        if(vars.face_detect_fails > 0)
            printf("  Total images with no faces detected: %d\n", vars.face_detect_fails);
        if(vars.face_fails > 0)
            printf("  Total faces detected incorrectly: %d\n", vars.face_fails);
        printf("\n");
    }

    unsigned int psize = vars.problem_files.size();
    if(psize > 0)
    {
        printf("Found %u problematic files (face detection failed, etc):\n", psize);
        for(unsigned int i = 0; i < psize; ++i)
            printf("  * %s\n", vars.problem_files[i].c_str());
        printf("\n");
    }
}

////////////////////////////////////////////////////////////////////////////////
// Program initialization and uninitialization
////////////////////////////////////////////////////////////////////////////////
/**
 *Load training, allocate memory, check parameters, etc
 *@param prog_params &params The program parameters
 *@param recogvars &vars The recognition variables.
 */
static void init(prog_params &params, recogvars &vars)
{
    vars.total      = 0;
    vars.correct    = 0;
    vars.rank_counter = 0;
    vars.maxrank    = -1;
    vars.face_fails = 0;
    vars.face_detect_fails = 0;
    vars.rpet_nums["default"] = 1;
    vars.top_rpet_num = 1;
    vars.cascade    = NULL;
    vars.storage    = NULL;
    vars.fpmodel    = NULL;
    vars.fTrainFile = NULL;
    vars.mean       = NULL;
    vars.std        = NULL;
    vars.filters    = NULL;
    vars.pca_training = NULL;
    vars.features   = NULL;
    vars.model      = NULL;
    vars.cap        = NULL;
    vars.fInterFile = NULL;
    vars.covr       = NULL;

    vars.fp_fail_correct   = 0;
    vars.fp_fail_incorrect = 0;
    vars.fp_pass_correct   = 0;
    vars.fp_pass_incorrect = 0;

    int error = 0;
    map<int,string> cat_files;

    if(params.file_name == NULL && params.use_webcam == 0 && params.use_gige == 0)
        ++error;

    if(params.easy_base && !error)
    {
        char tname[1024];
        if(params.pca_fname == NULL)
        {
            snprintf(tname, 1024, "%s.pca", params.easy_base);
            params.pca_fname = strdup(tname);
        }

        if(params.svm_model_name == NULL)
        {
            if(params.onevsrest)
                snprintf(tname, 1024, "%s", params.easy_base);
            else
                snprintf(tname, 1024, "%s.svm", params.easy_base);
            params.svm_model_name = strdup(tname);
        }

        if(params.sphere_fname == NULL)
        {
            snprintf(tname, 1024, "%s.val", params.easy_base);
            params.sphere_fname = strdup(tname);
        }
    }

    if(params.pca_fname == NULL)
        ++error;

    if(params.sphere_fname == NULL)
        ++error;

    if(params.svm_model_name == NULL)
        ++error;

    if(params.keep_scores < 0)
    {
        fprintf(stderr, "Invalid value for top n scores\n");
        ++error;
    }

    if(error)
        usage(params.progName, 1);

    if(params.loadfilt_dir != NULL)
        params.fast_gabor = 1;

    if(params.demo_mode)
    {
        IplImage* splash = cvLoadImage("splash.png", -1);
        if(splash != NULL)
        {
            cvNamedWindow("V1 Recognition");
            cvShowImage("V1 Recognition", splash);
            cvWaitKey(500);
            SAFE_RELEASE(splash);
        }
    }

    printSummary(&params);
    printf("Initializing: "); fflush(stdout);

    // Parse the map file
    if(params.map_name != NULL)
    {
        int cat_id;
        char cat_tmp[256];
        char fname_tmp[1024];

        ifstream myfile;
        myfile.open(params.map_name);
        if(!myfile.is_open())
        {
            fprintf(stderr, "Unable to open `%s': %s\n",
                    params.map_name, strerror(errno));
        }
        else
        {
            string line;
            int lineno = 0;
            while(!myfile.eof())
            {
                ++lineno;
                getline(myfile, line);
                int num = sscanf(line.c_str(), "%d %s %s", &cat_id, cat_tmp, fname_tmp);
                if(num < 2)
                    continue;
                vars.cat_names[cat_id] = string(cat_tmp);
                if(num > 2)
                    cat_files[cat_id] = string(fname_tmp);
            }

            myfile.close();
        }
    }

    // Setup the alert system
    if(params.enable_alerts)
        setup_alert_gallery(&cat_files, &vars.cat_names);

    // Parse the value file
    FILE* f = fopenOrError(params.sphere_fname, "r");
    if(!f) exit(1);

    size_t sz = WINDOW_SIZE*WINDOW_SIZE*GABOR_FEATURES;
    vars.mean     = (v1_float*)safe_malloc(sz * sizeof(v1_float));
    vars.std      = (v1_float*)safe_malloc(sz * sizeof(v1_float));
    vars.features = (v1_float*)safe_malloc(sz * sizeof(v1_float));

    unsigned int i = 0;
#ifdef USE_DOUBLE
    while(fscanf(f, "%lf %lf", &(vars.mean[i]), &(vars.std[i])) !=EOF)
#else
    while(fscanf(f, "%f %f", &(vars.mean[i]), &(vars.std[i])) !=EOF)
#endif
    {
        if(i >= sz) fatal("\nOverflow detected, goodbye!\n");
        i++;
    }

    fclose(f);

    // Load the haar classifier if necessary
    if(params.crop_face)
    {
        vars.cascade = (CvHaarClassifierCascade*)cvLoad(haarfile, 0, 0, 0);
        if(vars.cascade == NULL)
            fatal("\nError: Unable to load haar classifier: `%s'\n", haarfile);
        vars.storage = cvCreateMemStorage(0);
    }

    // Load the failure prediction model
    if(params.failmodel_name != NULL)
    {
        vars.fpmodel = new MlClassification();
        if(vars.fpmodel->LoadModel(params.failmodel_name) == false)
            fatal("\nUnable to load model file: %s\n", params.failmodel_name);
        vars.fpmodel->SetNormFactor(1.0);
    }

    // Open the failpredict file for writing
    if(params.failpredict_file)
    {
        if((vars.fTrainFile = fopenOrError(params.failpredict_file, "w")) == NULL)
            exit(1);
    }

    // Generate the gabor filters
    if(params.loadfilt_dir != NULL)
    {
        vars.filters = (void**)load_filters(params.loadfilt_dir);
    }
    else
    {
        if(params.fast_gabor)
            vars.filters = (void**)make_filters();
        else
            vars.filters = (void**)make_ipl_filters();
    }

    if(vars.filters == NULL)
        fatal("\nError: nUnable to create gabor filters.\n");

    // Initialize the capture
    v1_capture_type ctype;
    v1_capture_param cparam;

    if(params.use_webcam)
    {
        ctype = WEBCAM;
        cparam.cam_index = 0;
    }
    else if(params.use_gige)
    {
        ctype = GIGE;
    }
    else if(params.use_as_video)
    {
        ctype = VIDEO;
        cparam.filename = params.file_name;
    }
    else
    {
        ctype = FILE_LIST;
        cparam.filename = params.file_name;
    }

    if((vars.cap = v1_capture_create(ctype, cparam)) == NULL)
        fatal("Unable to initialize capture source.\n");

    if(params.force_id >= 0)
        v1_capture_setvlabel(vars.cap, params.force_id);

    // Set up intermediate file saving
    if(params.save_int_dir != NULL)
    {
        if(isFileType(params.save_int_dir, S_IFDIR) == 0)
        {
            fprintf(stderr, "WARNING: Intermediate directory `%s' doesn't exist.\n", params.save_int_dir);
            SAFE_FREE(params.save_int_dir);
        }
    }

    if(params.save_int_file != NULL)
    {
        if(params.save_int_dir == NULL)
        {
            // Silently disable the option
            SAFE_FREE(params.save_int_file);
        }
        else
        {
            if((vars.fInterFile = fopenOrError(params.save_int_file, "w")) == NULL)
                SAFE_FREE(params.save_int_file);
        }
    }

    printf("done.\n");

    printf("Load PCA: "); fflush(stdout);

    // Load the PCA training data, may take a while
    if((vars.pca_training = loadTrainingData(params.pca_fname, params.show_progress)) == NULL)
        fatal("\nError: Unable to load PCA training.\n");

    printf("done.\n");

    // Load the main V1 SVM training file
    printf("Load SVM: "); fflush(stdout);
    if(params.onevsrest)
    {
        if((vars.covr = classify_onevsrest_create(params.quiet, params.show_progress)) == NULL)
            fatal("\nError: Unable to initialize one vs rest svm classifier.\n");
        if(classify_onevsrest_load(vars.covr, params.svm_model_name) < 0)
            fatal("\nError: Unable to load one vs rest svm classifiers.\n");
    }
    else
    {
        if((vars.model = svm_load_model(params.svm_model_name)) == NULL)
            fatal("\nUnable to load model file: %s\n", params.svm_model_name);
    }

    printf("done.\n\n");

    if(params.demo_mode)
    {
        cvWaitKey(3000);
        cvDestroyWindow("V1 Recognition");
    }
}


/**
 * Free any allocated memory
 *@param prog_params &params The program parameters
 *@param recogvars &vars The recognition variables.
 */
static void uninit(prog_params &params, recogvars &vars)
{
    if(params.enable_alerts)
        cleanup_alert_gallery();

    if(vars.cap)
    {
        v1_capture_destroy(vars.cap);
        vars.cap = NULL;
    }

    if(vars.fTrainFile)
    {
        fclose(vars.fTrainFile);
        vars.fTrainFile = NULL;
    }

    if(vars.cascade != NULL)
    {
        cvReleaseHaarClassifierCascade(&(vars.cascade));
        vars.cascade = NULL;
    }

    if(vars.storage != NULL)
    {
        cvReleaseMemStorage(&(vars.storage));
        vars.storage = NULL;
    }

    if(vars.filters != NULL)
    {
        if(params.fast_gabor)
            free_filters((gabor_filter**)vars.filters);
        else
            free_ipl_filters((IplImage**)vars.filters);
        vars.filters = NULL;
    }

    SAFE_FREE(vars.mean);
    SAFE_FREE(vars.std);
    SAFE_FREE(vars.features);
    SAFE_DELETE(vars.fpmodel);

    if(vars.pca_training != NULL)
    {
        cvReleaseMat(&(vars.pca_training));
        vars.pca_training = NULL;
    }

    if(vars.model != NULL)
    {
        svm_destroy_model(vars.model);
        vars.model = NULL;
    }

    if(vars.covr != NULL)
    {
        classify_onevsrest_destroy(vars.covr);
        vars.covr = NULL;
    }

    if(vars.fTrainFile != NULL)
    {
        fclose(vars.fTrainFile);
        vars.fTrainFile = NULL;
    }

    SAFE_FREE(params.file_name);
    SAFE_FREE(params.pca_fname);
    SAFE_FREE(params.sphere_fname);
    SAFE_FREE(params.svm_model_name);
    SAFE_FREE(params.easy_base);
    SAFE_FREE(params.loadfilt_dir);
    SAFE_FREE(params.map_name);
    SAFE_FREE(params.failpredict_file);
    SAFE_FREE(params.failmodel_name);
    SAFE_FREE(params.save_int_dir);
    SAFE_FREE(params.save_int_file);
}

/**
 * Parses the command line
 * @param int argc Same as main
 * @param char**argv Same as main
 * @param prog_params *parm The program parameters
 */
static void parseCommandLine(int argc, char **argv, prog_params *parm)
{
    argv[0] = basename(argv[0]);

    // Set the default values
    parm->progName           = argv[0];
    parm->file_name          = NULL;
    parm->pca_fname          = NULL;
    parm->sphere_fname       = NULL;
    parm->svm_model_name     = NULL;
    parm->easy_base          = NULL;
    parm->loadfilt_dir       = NULL;
    parm->map_name           = NULL;
    parm->failpredict_file   = NULL;
    parm->failmodel_name     = NULL;
    parm->save_int_dir       = NULL;
    parm->save_int_file      = NULL;
    parm->rpet_file          = NULL;
    parm->keep_scores        = 10;
    parm->use_groundtruth    = 0;
    parm->fast_gabor         = 1;
    parm->show_progress      = 1;
    parm->show_normalized    = 1;
    parm->use_cache          = 0;
    parm->geo_norm           = 0;
    parm->use_eye_detect     = 1;
    parm->crop_face          = 1;
    parm->multi_face         = 0;
    parm->hist_norm          = 1;
    parm->use_as_video       = 0;
    parm->use_python         = 0;
    parm->enable_alerts      = 1;
    parm->use_webcam         = 0;
    parm->use_atmos_deblur   = 0;
    parm->use_sqi_norm       = 0;
    parm->fusion_type        = 0;
    parm->use_motion_deblur  = 0;
    parm->webcam_trigger_time = -1;
    parm->demo_mode          = 0;
    parm->quiet              = 0;
    parm->force_id           = -1;
    parm->onevsrest          = 1;
    parm->use_gige           = 0;
    parm->fp_atmos_thresh    = 0.;
    parm->fp_motion1_thresh  = 0.;
    parm->fp_motion2_thresh  = 0.;
    parm->fp_motion3_thresh  = 0.;
    parm->fp_default_thresh  = 0.;

    ArgParser ap(usage, argv[0], false);

    // Available switches: J, k, K, l, L, t, u, U, W, x, X, y, Z

    // Simple switches
    ap.addOption("help", false, "h");
    ap.addOption("info", false, "i");

    // Regular options
    ap.addOption("config",       true, "C");
    ap.addOption("file",         true, "f");
    ap.addOption("pca",          true, "p");
    ap.addOption("value",        true, "v");
    ap.addOption("svm",          true, "s");
    ap.addOption("num-scores",   true, "n");
    ap.addOption("base",         true, "b");
    ap.addOption("filter-dir",   true, "d");
    ap.addOption("map",          true, "m");
    ap.addOption("fp-save",      true, "O");
    ap.addOption("fp-model",     true, "F");
    ap.addOption("fp-rpet",      true, "R");
    ap.addOption("force-id",     true, "I");
    ap.addOption("intermediate-dir", true, NULL);
    ap.addOption("intermediate-file", true, NULL);
    ap.addOption("save-config",  true, "o");
    ap.addOption("default-thresh", true, NULL);
    ap.addOption("atmos-thresh", true, NULL);
    ap.addOption("motion1-thresh", true, NULL);
    ap.addOption("motion2-thresh", true, NULL);
    ap.addOption("motion3-thresh", true, NULL);

    // Boolean options (rightmost column indicates that "-G" is the same as "-G true")
    ap.addOption("geo-norm",       true, "G", "true");
    ap.addOption("eye-detect",     true, "e", "true");
    ap.addOption("hist-norm",      true, "H", "true");
    ap.addOption("video",          true, "V", "true");
    ap.addOption("webcam",         true, "w", "true");
    ap.addOption("gige",           true, "E", "true");
    ap.addOption("crop-face",      true, "c", "true");
    ap.addOption("multi-face",     true, "z", "true");
    ap.addOption("groundtruth",    true, "g", "true");
    ap.addOption("python-resize",  true, "P", "true");
    ap.addOption("alerts",         true, "a", "true");
    ap.addOption("webcam-trigger", true, "T", "true");
    ap.addOption("deblur-atmos",   true, "A", "true");
    ap.addOption("deblur-motion",  true, "M", "true");
    ap.addOption("demo",           true, "D", "true");
    ap.addOption("show-norm",      true, "N", "true");
    ap.addOption("cache",          true, "Q", "true");
    ap.addOption("fast-gabor",     true, "S", "true");
    ap.addOption("progress",       true, "r", "true");
    ap.addOption("normalize-SQI",  true, "B", "true");
    ap.addOption("fusion-type",    true, "Y", "true");
    ap.addOption("quiet",          true, "q", "true");
    ap.addOption("one-vs-rest",    true, "j", "true");

    if(!ap.parseCommandLine(argc, argv, true))
        exit(1);

    if(ap.isSet("help"))
        usage(parm->progName, 0);

    if(ap.isSet("info"))
    {
        printFileVersions(stdout);
        exit(0);
    }

    // Parse the config file if specified
    if(ap.isSet("config"))
    {
        if(!ap.parseConfigFile(ap.getValue("config"), true))
            exit(1);
    }

    // Extract the command line options
    if(ap.isSet("file")) parm->file_name = strdup(ap.getValue("file"));
    if(ap.isSet("pca")) parm->pca_fname = strdup(ap.getValue("pca"));
    if(ap.isSet("value")) parm->sphere_fname = strdup(ap.getValue("value"));
    if(ap.isSet("svm")) parm->svm_model_name = strdup(ap.getValue("svm"));
    if(ap.isSet("num-scores")) parm->keep_scores = atoi(ap.getValue("num-scores"));
    if(ap.isSet("base")) parm->easy_base = strdup(ap.getValue("base"));
    if(ap.isSet("filter-dir")) parm->loadfilt_dir = strdup(ap.getValue("filter-dir"));
    if(ap.isSet("map")) parm->map_name = strdup(ap.getValue("map"));
    if(ap.isSet("fp-model")) parm->failmodel_name = strdup(ap.getValue("fp-model"));
    if(ap.isSet("progress")) parm->show_progress = ap.getBoolValue("progress");
    if(ap.isSet("geo-norm")) parm->geo_norm = ap.getBoolValue("geo-norm");
    if(ap.isSet("fast-gabor")) parm->fast_gabor = ap.getBoolValue("fast-gabor");
    if(ap.isSet("cache")) parm->use_cache = ap.getBoolValue("cache");
    if(ap.isSet("show-norm")) parm->show_normalized = ap.getBoolValue("show-norm");
    if(ap.isSet("eye-detect")) parm->use_eye_detect = ap.getBoolValue("eye-detect");
    if(ap.isSet("hist-norm")) parm->hist_norm = ap.getBoolValue("hist-norm");
    if(ap.isSet("video")) parm->use_as_video = ap.getBoolValue("video");
    if(ap.isSet("webcam")) parm->use_webcam = ap.getBoolValue("webcam");
    if(ap.isSet("gige")) parm->use_gige = ap.getBoolValue("gige");
    if(ap.isSet("crop-face")) parm->crop_face = ap.getBoolValue("crop-face");
    if(ap.isSet("multi-face")) parm->multi_face = ap.getBoolValue("multi-face");
    if(ap.isSet("groundtruth")) parm->use_groundtruth = ap.getBoolValue("groundtruth");
    if(ap.isSet("python-resize")) parm->use_python = ap.getBoolValue("python-resize");
    if(ap.isSet("alerts")) parm->enable_alerts = ap.getBoolValue("alerts");
    if(ap.isSet("deblur-atmos")) parm->use_atmos_deblur = ap.getBoolValue("deblur-atmos");
    if(ap.isSet("deblur-motion")) parm->use_motion_deblur = ap.getBoolValue("deblur-motion");
    if(ap.isSet("normalize-SQI")) parm->use_sqi_norm = ap.getBoolValue("normalize-SQI");
    if(ap.isSet("fusion-type")) parm->fusion_type = atoi(ap.getValue("fusion-type"));
    if(ap.isSet("demo")) parm->demo_mode = ap.getBoolValue("demo");
    if(ap.isSet("quiet")) parm->quiet = ap.getBoolValue("quiet");
    if(ap.isSet("force-id")) parm->force_id = atoi(ap.getValue("force-id"));
    if(ap.isSet("intermediate-dir")) parm->save_int_dir = strdup(ap.getValue("intermediate-dir"));
    if(ap.isSet("intermediate-file")) parm->save_int_file = strdup(ap.getValue("intermediate-file"));
    if(ap.isSet("one-vs-rest")) parm->onevsrest = ap.getBoolValue("one-vs-rest");
    if(ap.isSet("fp-rpet")) parm->rpet_file = strdup(ap.getValue("fp-rpet"));

    if(ap.isSet("fp-save"))
    {
        parm->failpredict_file = strdup(ap.getValue("fp-save"));
        parm->use_groundtruth = 1;
    }

    if(ap.isSet("default-thresh"))
    {
        char* end = NULL;
        const char* begin = ap.getValue("default-thresh");
        parm->fp_default_thresh = (double)strtof(begin, &end);  

        if(begin == end || *end != '\0')
        {
            fprintf(stderr, "Invalid value for default-thresh.\n");
            exit(1);
        }
    }

    if(ap.isSet("motion1-thresh"))
    {
        char* end = NULL;
        const char* begin = ap.getValue("motion1-thresh");
        parm->fp_motion1_thresh = (double)strtof(begin, &end);

        if(begin == end || *end != '\0')
        {
            fprintf(stderr, "Invalid value for motion1-thresh.\n");
            exit(1);
        }
    }

    if(ap.isSet("motion2-thresh"))
    {
        char* end = NULL;
        const char* begin = ap.getValue("motion2-thresh");
        parm->fp_motion2_thresh = (double)strtof(begin, &end);

        if(begin == end || *end != '\0')
        {
            fprintf(stderr, "Invalid value for motion2-thresh.\n");
            exit(1);
        }
    }

    if(ap.isSet("motion3-thresh"))
    {
        char* end = NULL;
        const char* begin = ap.getValue("motion3-thresh");
        parm->fp_motion3_thresh = (double)strtof(begin, &end);

        if(begin == end || *end != '\0')
        {
            fprintf(stderr, "Invalid value for motion3-thresh.\n");
            exit(1);
        }
    }

    if(ap.isSet("atmos-thresh"))
    {
        char* end = NULL;
        const char* begin = ap.getValue("atmos-thresh");
        parm->fp_atmos_thresh = (double)strtof(begin, &end);  

        if(begin == end || *end != '\0')
        {
            fprintf(stderr, "Invalid value for atmos-thresh.\n");
            exit(1);
        }
    }

    if(ap.isSet("webcam-trigger"))
    {
        parm->webcam_trigger_time = atoi(ap.getValue("webcam-trigger"));
        if(parm->webcam_trigger_time < 0)
            parm->webcam_trigger_time = -1;
    }

    if(parm->quiet)
        parm->show_progress = 0;

    //    if(parm->use_atmos_deblur || parm->use_motion_deblur)
    //  parm->crop_face = 1;

    if(parm->use_webcam)
    {
        SAFE_FREE(parm->file_name);
        parm->use_cache = 0;
        parm->use_as_video = 0;
        parm->use_gige = 0;

        if(parm->force_id < 0)
        {
            parm->use_groundtruth = 0;
            SAFE_FREE(parm->failpredict_file);
        }
    }
    else if(parm->use_gige)
    {
        SAFE_FREE(parm->file_name);
        parm->use_webcam = 0;
        parm->use_cache = 0;
        parm->use_as_video = 0;

        if(parm->force_id < 0)
        {
            parm->use_groundtruth = 0;
            SAFE_FREE(parm->failpredict_file);
        }
    }
    else if(parm->use_as_video)
    {
        parm->use_gige = 0;
        parm->use_webcam = 0;
        parm->use_cache = 0;

        if(parm->force_id < 0)
        {
            parm->use_groundtruth = 0;
            SAFE_FREE(parm->failpredict_file);
        }
    }

    if(parm->multi_face)
    {
        if(!parm->crop_face)
            fatal("\nYou must also set crop-face to use multi-face\n");
        if(parm->use_groundtruth)
            fatal("\nmulti-face mode cannot be used in conjunction with groundtruth\n");
    }

    if(ap.isSet("save-config"))
    {
        vector<string> excludes;
        excludes.push_back("help");
        excludes.push_back("info");
        excludes.push_back("config");
        excludes.push_back("save-config");
        if(!ap.writeConfigFile(ap.getValue("save-config"), excludes))
            exit(1);
        printf("All options saved to %s.\n", ap.getValue("save-config"));
        exit(0);
    }
}

/**
 * Print out the program usage and exit
 *@param const char* name The program name
 *@param int ret The exit value.
 */
static void usage(const char* name, int ret)
{
    fprintf(stderr, "Usage: %s [OPTIONS]\n"
            "General options:\n"
            "  -i, --info             Print out file versions and exit.\n"
            "  -h, --help             Show this helpful message.\n"
            "  -C, --config=FILE      Load extra configuration options from FILE.\n"
            "  -o, --save-config=FILE Dump all configuration options to FILE.\n"
#ifndef HIDE_PROGRESS
            "  -r, --progress=BOOL    Show the progress bar (default true).\n"
#endif
            "  -q, --quiet=BOOL       Quiet mode, implies -r0 (default false).\n"
            "  -D, --demo=BOOL        Demo mode (default false).\n\n"
            "V1 recognition options:\n"
            "  -f, --file=FILE        Probe file. Required unless using a webcam.\n"
            "  -V, --video=BOOL       Treat -f as a video file. groundtruth, cache, and\n"
            "                         fp-save, are ignored (default false).\n"
            "  -w, --webcam=BOOL      Use a webcam instead of a file. -f, -q, and -O\n"
            "                         are ignored (default false).\n"
            "  -E, --gige=BOOL        Use a gige camera instead of a file. -f, -q, and -O\n"
            "                         are ignored (default false).\n"
            "  -b, --base=NAME        Use easy file mode where -p, -s, and -v are\n"
            "                         automatically determined using NAME as the base.\n"
            "  -p, --pca=FILE         PCA subspace file. Required unless -b specified.\n"
            "  -s, --svm=FILE         SVM training file. Required unless -b specified.\n"
            "  -v, --value=FILE       Sphered values file. Required unless -b specified.\n"
            "  -m, --map=FILE         Load a category map from FILE  to map ids to names.\n"
            "  -n, --num-scores=NUM   Number of scores to display (default 10).\n"
            "  -I, --force-id=NUM     If using a video or webcam, force all frames to\n"
            "                         have a particular groundtruth id.\n"
            "  -g, --groundtruth=BOOL Use groundtruth labels if available to calculate\n"
            "                         statistics (default false).\n"
            "  -d, --filter-dir=DIR   Load gabor filters from DIR. Ignores -S.\n"
            "  -S, --fast-gabor=BOOL  Use the fast gabor convolve (default true).\n"
            "  -Q, --cache=BOOL       Enable feature caching (default false).\n"
            "  -P, --python-resize=BOOL\n"
            "                         Call off to python for resizing rather than cvResize\n"
            "                         (default false).\n"
            "  -a, --alerts=BOOL      Enable the alerts window (default true).\n"
            "  -N, --show-norm=BOOL   Show the normalized image instead of the original probe\n"
            "                         (default true).\n"
            "  -T, --webcam-trigger=VAL\n"
            "                         Webcam trigger. -1 means the webcam only captures when\n"
            "                         the user presses SPACE, 0 means the webcam captures\n"
            "                         when it finds a face, and >0 means the webcam captures\n"
            "                         after N seconds (default -1).\n\n"
            "  -B, --normalize-SQI=BOOL\n"
            "                         Enable sqi-normalization of the images (default false).\n"
            "  --intermediate-dir=DIR\n"
            "                         Save intermediate images into DIR.\n"
            "  --intermediate-file=FILE\n"
            "                         Create a gallery-like file that lists the intermediate\n"
            "                         images created by the intermediate-dir option.\n"
            "  -j, --one-vs-rest=BOOL Use one vs rest classification (default true).\n\n"
            "Geo-norm options (all options only effective if -G is specified):\n"
            "  -G, --geo-norm=BOOL    Enable geo-normalization of the images (default false).\n"
            "  -H, --hist-norm=BOOL   Enable histogram normalization (default true).\n"
            "  -c, --crop-face=BOOL   Enable face cropping (default true). If disabled,\n"
            "                         the program will use any cropping coordinates given in\n"
            "                         in the probe file if available. If none are available,\n"
            "                         no cropping will be done.\n"
            "  -z, --multi-face=BOOL  Enable processing of more than one face (default false).\n"
            "  -e, --eye-detect=BOOL  Enable eye detection (default true). If disabled, then\n"
            "                         any eye coordinates given in the probe file are used.\n\n"
            "Failure prediction options:\n"
            "  -O, --fp-save=FILE     Save libsvm failure prediction file. Train with\n"
            "                         svm-learn.\n"
            "  -F, --fp-model=FILE    Use libsvm model FILE for failure prediction.\n"
            "  -R, --fp-rpet=FILE     Save RPET data to FILE. Only used if fp-model is used.\n"
            "  --default-thresh=NUM   The default FP threshold (default 0.0)\n"
            "  --atmos-thresh=NUM     The FP threshold for atmospheric deblur (default 0.0)\n"
            "  --motion1-thresh=NUM   The FP threshold for motion deblur type 1 (default 0.0)\n"
            "  --motion2-thresh=NUM   The FP threshold for motion deblur type 2 (default 0.0)\n"
            "  --motion3-thresh=NUM   The FP threshold for motion deblur type 3 (default 0.0)\n\n"
            "Debluring options:\n"
            "  -A, --deblur-atmos=BOOL\n"
            "                         Enable atmospheric deblurring (default false).\n"
            "  -M, --deblur-motion=BOOL\n"
            "                         Enable motion deblurring (default false).\n"
            "  -y, --fusion-type=BOOL Enable failure fusion as oppose to most confident\n"
            "                         recognition score (default false).\n\n"
            "Examples:\n"
            "  %s -C config.cfg -r0 --file=probe.avi --video=True -w no -mmap.txt -aY\n\n"
            , name, name);
    exit(ret);
}

// Print a summary of the user's input parameters
/**
 *Prints out what parameters are set in the current run.
 *@param prog_params &params The program parameters
 */
static void printSummary(prog_params *params)
{
    if(params->quiet)
        return;

    printf("V1 Recognition\n");
    printf("Probe source:  ");
    if(params->file_name != NULL)
        printf("%s", params->file_name);
    else if(params->use_gige)
        printf("GigE Network Camera");
    else if(params->use_webcam)
        printf("Webcam");
    else
        printf("(None)");
    printf("\n");

    printf("PCA file:      %s\n", params->pca_fname);

    if(params->onevsrest)
        printf("SVM file:      %s_#.svm\n", params->svm_model_name);
    else
        printf("SVM file:      %s\n", params->svm_model_name);

    printf("Value file:    %s\n", params->sphere_fname);
    printf("Map file:      %s\n", (params->map_name == NULL) ? "(None)" : params->map_name);
    printf("Fail model:    %s\n", (params->failmodel_name == NULL) ? "(None)" : params->failmodel_name);

/*    
    if(params->failmodel_name != NULL)
    {
        printf("Fail thresh:   %f\n", params->fp_thresh);
    }
*/

    printf("Caching:       %s\n", (params->use_cache == 1) ? "On" : "Off");
    printf("Gabor mode:    %s\n", (params->fast_gabor == 1) ? "Fast" : "Slow");
    printf("Atmos deblur:  %s\n", (params->use_atmos_deblur == 1) ? "On" : "Off");
    printf("Motion deblur: %s\n", (params->use_motion_deblur == 1) ? "On" : "Off");
    printf("Geo-norm:      ");

    if(params->geo_norm == 1)
    {
        printf("On (hist-norm = %s)\n", (params->hist_norm == 1) ? "On" : "Off");
        printf("Crop face:     %s\n", (params->crop_face == 1) ? "On" : "Off");
        printf("Eye detect:    %s\n", (params->use_eye_detect == 1) ? "On" : "Off");
    }
    else
        printf("Off\n");

    printf("SQI-norm:      %s\n", (params->use_sqi_norm == 1) ? "On" : "Off");
    printf("Fusion type:   %s\n", (params->fusion_type == 0) ? "Default" : "Failure-score");
    printf("Resize:        %s\n", (params->use_python == 1) ? "Python" : "cvResize");
    printf("Groundtruth:   %s\n", (params->use_groundtruth == 1) ? "Yes" : "No");

#ifdef USE_DOUBLE
    printf("Precision:     double\n");
#else
    printf("Precision:     float\n");
#endif

#ifdef DEBUG
    printf("Debugging:     On\n\n");
#else
    printf("Debugging:     Off\n\n"); 
#endif
    
}

////////////////////////////////////////////////////////////////////////////////
// Caching functions
////////////////////////////////////////////////////////////////////////////////
/**
 *Tries to load a cache file
 *@param const char* file The location of the cache file
 *@param v1_float* feat The loaded features
 *@result int -1 if failed, 0 if success
 */
static int tryCache(const char* file, v1_float* feat)
{
    const char* suffix = ".cache";
    char* cachefile = (char*)safe_malloc((strlen(file) + strlen(suffix) + 1) * sizeof(char));
    sprintf(cachefile, "%s%s", file, suffix);

    struct stat stFileInfo;
    if(stat(cachefile, &stFileInfo) != 0)
    {
        SAFE_FREE(cachefile);
        return -1;
    }

    FILE *fp = fopen(cachefile, "r");
    SAFE_FREE(cachefile);
    if(fp == NULL)
        return -1;

    int sz = WINDOW_SIZE * WINDOW_SIZE * GABOR_FEATURES;
    for(int i = 0; i < sz; ++i)
    {
#ifdef USE_DOUBLE
        if(fscanf(fp, "%lf", &feat[i]) != 1)
#else
        if(fscanf(fp, "%f", &feat[i]) != 1)
#endif
        {
            fclose(fp);
            return -1;
        }
    }

    fclose(fp);
    return 0;
}

// Save features to a cache file
/**
 *Saves a cache file of the features
 *@param const char* file The file to save the cache at
 *@param v1_float* feat The features to save.
 */

static void saveCache(const char* file, v1_float* feat)
{
    const char* suffix = ".cache";
    char* cachefile = (char*)safe_malloc((strlen(file) + strlen(suffix) + 1) * sizeof(char));
    sprintf(cachefile, "%s%s", file, suffix);

    FILE *fp = fopen(cachefile, "w");
    SAFE_FREE(cachefile);
    if(fp == NULL)
        return;

    int sz = WINDOW_SIZE * WINDOW_SIZE * GABOR_FEATURES;
    for(int i = 0; i < sz; ++i)
#ifdef USE_DOUBLE
        fprintf(fp, "%g\n", feat[i]);
#else
        fprintf(fp, "%f\n", feat[i]);
#endif

    fclose(fp);
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Webcam functions
////////////////////////////////////////////////////////////////////////////////
/**
 *Checks if the webcam frame is good to process on.
 *@param prog_params &params The program parameters
 *@param recogvars &vars The recognition variables.
 *@result bool Returns true if process current frame, false otherwise
 */
static bool previewWebcam(prog_params &params, recogvars &vars)
{
    int try_next = 0;
    int showres, key;
    IplImage* frame;

    clock_t wstart = clock();
    double welapsed;

    while(!v1_capture_eof(vars.cap) && stop_now == 0)
    {
        frame = v1_capture_get(vars.cap);
        if(params.use_webcam)
            showres = showFace(frame, vars.cascade, vars.storage, params.use_python, "Webcam");
        else if(params.use_gige)
            showres = showFace(frame, vars.cascade, vars.storage, params.use_python, "GigE");
        else
            return true;

        SAFE_RELEASE(frame);

        if(showres && (params.webcam_trigger_time == 0 || try_next == 1))
            return true;

        if(params.webcam_trigger_time > 0)
        {
            welapsed = ((double)(clock() - wstart)) / CLOCKS_PER_SEC;
            if(welapsed > (double)params.webcam_trigger_time)
                return true;
        }
        else
        {
            key = cvWaitKey(100) & 255;
            if(key == SPACE)
            {
                if(showres)
                    return true;
                else
                    try_next = 1;
            }
            else if(key == ESCAPE)
                return false;
        }

        if(!v1_capture_next(vars.cap))
            break;
    }

    return false;
}
/**
 *Detects and shows a face on an image
 *@param CvHaarClassifierCascade* cascade The haar cascade
 *@param CvMemStorage* storage The storage for the detected faces.
 *@param int use_python 1 to use python to resize 0 otherwise
 *@result int number of found faces
 */
static int showFace(IplImage* img, CvHaarClassifierCascade* cascade, CvMemStorage* storage, int use_python, const char* windowName)
{
    int found = 0;
    if(img == NULL)
        return found;

    IplImage* dup = cvCloneImage(img);
    if(dup == NULL)
        return found;

    IplImage* gray = getGrayscale(dup);
    if(gray != NULL)
    {
        CvRect r = detect_face(cascade, storage, gray);
        if(r.x >= 0 && r.y >=0 && r.width > 0 && r.height > 0)
        {
            CvPoint pt1 = cvPoint(r.x, r.y);
            CvPoint pt2 = cvPoint(r.x + r.width, r.y + r.height);
            cvRectangle(dup, pt1, pt2, cvScalar(0, 0, 255), 2, 8, 0);
            found = 1;
        }

        IplImage* cropped = cropIpl(gray, r.x, r.y, r.width, r.height);
        if(cropped != NULL)
        {
            int beforeW, beforeH;
            beforeW = cropped->width;
            beforeH = cropped->height;

            IplImage* resized = get_image(cropped, 150, use_python, 1);
            if(resized != NULL)
            {
                int lx = -1, ly = -1, rx = -1, ry = -1;
        int lx1 = -1, ly1 = -1, rx1 = -1, ry1 = -1;
                eye_detect(resized, &lx, &rx, &ly, &ry, &lx1, &rx1, &ly1, &ry1);
                if(lx < 0 || ly < 0 || rx < 0 || ry < 0 ||
                   lx >= resized->width || ly >= resized->height ||
                   rx >= resized->width || ry >= resized->height)
                {
                    // No eyes for you
                }
                else
                {
                    double scaleX, scaleY;
                    scaleX = (double)beforeW / (double)resized->width;
                    scaleY = (double)beforeH / (double)resized->height;

                    CvPoint center;

                    int flx = (int)((double)lx * scaleX) + r.x;
                    int fly = (int)((double)ly * scaleY) + r.y;
                    int frx = (int)((double)rx * scaleX) + r.x;
                    int fry = (int)((double)ry * scaleY) + r.y;

                    center.x = flx; center.y = fly;
                    cvCircle(dup, center, 10, cvScalar(0, 0, 255), 2, 8, 0);

                    center.x = frx; center.y = fry;
                    cvCircle(dup, center, 10, cvScalar(0, 0, 255), 2, 8, 0);
                }

                SAFE_RELEASE(resized);
            }

            SAFE_RELEASE(cropped);
        }

        SAFE_RELEASE(gray);
    }
    else
    {
        fprintf(stderr, "Gray is NULL!\n");
    }

    cvShowImage(windowName, dup);
    SAFE_RELEASE(dup);

    return found;
}
