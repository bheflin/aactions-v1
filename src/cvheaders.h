// cvheaders.h
// Copyright 2009 Securics, Inc
//
// OpenCV can't seem to handle having HAVE_CONFIG_H set
// so this wrapper solves that problem.

#ifndef _cvheaders_h_
#define _cvheaders_h_

#ifdef HAVE_CONFIG_H
#define HAD_CONFIG_H
#undef HAVE_CONFIG_H
#endif

#include <opencv/cv.h>
#include <opencv/highgui.h>

#ifdef HAD_CONFIG_H
#define HAVE_CONFIG_H
#undef HAD_CONFIG_H
#endif

#endif /* _cvheaders_h_ */
