// motion_deblur.h
// Copyright 2009 Securics, Inc

#ifndef MOTION_DEBLUR_H
#define MOTION_DEBLUR_H

void motion_deblur(IplImage * blurred_image, IplImage** dst_images);

#endif
