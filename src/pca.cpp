// pca.cpp
// Copyright 2009 Securics, Inc

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <sys/stat.h>
#include "cvheaders.h"

#include "pca.h"
#include "utils.h"
#include "asciibar.h"

// Not really faster, just needed a different signature than fastnorm
static v1_float fasternorm(CvMat *dat, int row)
{
    v1_float return_val = 0;
    int i;

    for(i = 0; i < dat->cols; i++)
    {
        v1_float val = ((v1_float*)(dat->data.ptr + dat->step * row))[i];
        return_val += val * val;
    }

    return_val = v1_pow(return_val, .5);
    return return_val;
}

void bytesToString(long int bytes)
{
    if(bytes < 1024)
        printf("%ld B\n", bytes);
    else if(bytes < 1048576)
        printf("%.2f KiB\n", (float)bytes / 1024.);
    else if(bytes < 1073741824)
        printf("%.2f MiB\n", (float)bytes / 1048576.);
    else
        printf("%.2f GiB\n", (float)bytes / 1073741824.);
}
/**
 *Loads the Pca training data into a matrix
 *@param const char* fname The file where the pca subspace is.
 *@param int show_progress 1 to show progress bar 0 otherwise.
 *@return CvMat* The matrix containing the subspace.
 */
CvMat* loadTrainingData(const char* fname, int show_progress)
{
    int rows, cols, i, j, count;
    v1_float ftemp;

    FILE* f = fopenOrError(fname, "r");
    if(f == NULL)
        return NULL;

    count = fscanf(f, "%d %d", &rows, &cols);
    if(count != 2)
    {
        fprintf(stderr, "Read failure (1)\n");
        fclose(f);
        return NULL;
    }

#ifndef HIDE_PROGRESS
    ascii_bar_params params;
    if(show_progress)
    {
        init_ascii_bar(&params, 0.0f, (float)(rows / 100 - 1), 1.0f);
        params.len = 50;
        params.show_eta = 1;
        show_ascii_bar(&params);
    }
#endif

    CvMat* training = cvCreateMat(rows, cols, CV_V1);
    v1_float* tptr = NULL;
    for(i = 0; i < rows; i++)
    {
        tptr = (v1_float*)(training->data.ptr + training->step * i);
        for(j = 0; j < cols; j++)
        {
#ifdef USE_DOUBLE
            if((count = fscanf(f, "%lf", &ftemp)) != 1)
#else
            if((count = fscanf(f, "%f", &ftemp)) != 1)
#endif
            {

                fprintf(stderr, "Read failure (2): Size mismatch\n");
                fclose(f);
                cvReleaseMat(&training);
                return NULL;
            }

            *tptr = ftemp;
            ++tptr;
        }

#ifndef HIDE_PROGRESS
        if(i % 100 == 0 && i > 0 && show_progress)
            step_ascii_bar(&params);
#endif
    }

    fclose(f);

#ifndef HIDE_PROGRESS
    if(show_progress)
        hide_ascii_bar(&params);
#endif

    return training;
}

/**
 *Projects the probe into the subspace
 *@param CvMat* training The pca subspace.
 *@param v1_float* fdata The data to project.
 *@param int in_size The size of the data to project.
 *@param int out_size The size of the returned data.
 *@return v1_float* the projected data.
 */
v1_float* projectProbe(CvMat* training, v1_float* fdata, int in_size, int* out_size)
{
    int i;
    CvMat* data = cvCreateMat(1, in_size, CV_V1);

    for(i = 0; i < in_size; i++)
        ((v1_float*)(data->data.ptr))[i] = fdata[i];

    // Project the data into PCA space
    CvMat* projected = cvCreateMat(1, training->cols, CV_V1);
    cvGEMM(data, training, 1, NULL, 1, projected, 0);

    v1_float* output = (v1_float*)malloc(sizeof(v1_float) * training->cols);
    for(i = 0; i < training->cols; i++)
        output[i] = ((v1_float*)(projected->data.ptr))[i];

    if(out_size != NULL)
        *out_size = training->cols;

    cvReleaseMat(&projected);
    cvReleaseMat(&data);

    return output;
}

/**
 *Projects a gallery into a subspace
 *@param CvMat* subspace Pca subspace
 *@param CvMat* data Data to project
 *@param struct gallery_entry* gallery The gallery to project(Data generated from it)
 *@param int size The size of the gallery
 */
void project(CvMat* subspace, CvMat* data, struct gallery_entry* gallery, int size)
{
    CvMat* projected = cvCreateMat(data->rows, subspace->cols, CV_V1);
    cvGEMM(data, subspace, 1, NULL, 1, projected, 0);

    // Copy the projected data back to the gallery
    for(int i = 0; i < size; i++)
    {
        gallery[i].pca_features = (v1_float*)malloc(projected->cols * sizeof(v1_float));
        for(int j = 0; j < projected->cols; j++)
        {
            gallery[i].pca_features[j] = ((v1_float*)(projected->data.ptr + projected->step * i))[j];
            gallery[i].feature_count = projected->cols;
        }
    }

    cvReleaseMat(&projected);
}


/**
 *Projects a gallery into a subspace
 *@param CvMat* subspace Pca subspace
 *@param CvMat* data Data to project
 *@param struct gallery_entry* gallery The gallery to project(Data generated from it)
 *@param int size The size of the gallery
 */
void project_validate(CvMat* subspace, CvMat* data, struct validate_gallery* gallery, int size)
{
    CvMat* projected = cvCreateMat(data->rows, subspace->cols, CV_V1);
    cvGEMM(data, subspace, 1, NULL, 1, projected, 0);

    // Copy the projected data back to the gallery
    for(int i = 0; i < size; i++)
    {
        gallery[i].pca_features = (v1_float*)malloc(projected->cols * sizeof(v1_float));
        for(int j = 0; j < projected->cols; j++)
        {
            gallery[i].pca_features[j] = ((v1_float*)(projected->data.ptr + projected->step * i))[j];
            gallery[i].feature_count = projected->cols;
        }
    }

    cvReleaseMat(&projected);
}

/**
 *Projects a gallery using only gallery structure rather than data
 *@param CvMat* training The trained subspace.
 *@param struct gallery_entry* gallery The gallery to project.
 *@param int size the size of the gallery
 */
void projectMulti(CvMat* training, struct gallery_entry* gallery, int size)
{
    int i, j;
    size_t rows = size;
    size_t cols = WINDOW_SIZE * WINDOW_SIZE * GABOR_FEATURES;

    CvMat* data = cvCreateMat(rows, cols, CV_V1);

    for(i = 0; i < (int)rows; i++)
    {
        for(j = 0; j < (int)cols; j++)
            ((v1_float*)(data->data.ptr + data->step*i))[j] = gallery[i].features[j];
    }

    project(training, data, gallery, size);
    cvReleaseMat(&data);
}
/**
 *Creates a pca subspace
 *@param struct gallery_entry* gallery The gallery used to create subspace
 *@param int size The size of the gallery
 *@param v1_float pca_thresh The Eigan value threshold.
 *@param const char* pca_fname File to save subspace to.
 *@param int show_progress 1 to show progress bar 0 otherwise.
 *@param int quiet 0 to surpress all messages.
 */
void pca(struct gallery_entry* gallery, int size, v1_float pca_thresh, const char* pca_fname, int show_progress, int quiet, int limit)
{
    clock_t start = clock();
    double diff = 0.0;

#ifndef TESTING
    printf("PCA: ");
    fflush(stdout);
#endif

    int i, j;
    size_t rows = size;
    size_t cols = WINDOW_SIZE * WINDOW_SIZE * GABOR_FEATURES;

    CvMat* data = cvCreateMat(rows, cols, CV_V1);

    for(i = 0; i < (int)rows; i++)
    {
        for(j = 0; j < (int)cols; j++)
            ((v1_float*)(data->data.ptr + data->step*i))[j] = gallery[i].features[j];
    }

    // dot = N.dot(M, M.T)
    CvMat* dot = cvCreateMat(rows, rows, CV_V1);
    cvGEMM(data, data, 1, NULL, 1, dot, CV_GEMM_B_T);

    // U, S, NULL = N.linalg.svd(dot)
    CvMat* U = cvCreateMat(rows, rows, CV_V1);
    CvMat* S = cvCreateMat(rows, 1, CV_V1);
    CvMat* V = cvCreateMat(rows, rows, CV_V1);
    cvSVD(dot, S, U, V, CV_SVD_U_T | CV_SVD_V_T);

    // V = N.dot(U.T, M)
    cvReleaseMat(&V);
    V = cvCreateMat(rows, cols, CV_V1);
    cvGEMM(U, data, 1, NULL, 1, V, 0);

    // Normalize, square, and calculate S.sum() at the same time
    v1_float S_sum = 0.0;
    for(i = 0; i < (int)rows; i++)
    {
        v1_float norm = fasternorm(V, i);
        ((v1_float*)(S->data.ptr + S->step * i))[0] = v1_pow(norm, 2.0);
        S_sum += v1_pow(norm, 2.0);
        for(j = 0; j < (int)cols; j++)
            ((v1_float*)(V->data.ptr + V->step * i))[j] /= norm;
    }

    // eigvectors = V.T
    CvMat* eigenvectors = cvCreateMat(cols, rows, CV_V1);
    cvTranspose(V, eigenvectors);

    int num = 0;
    v1_float tot = 0.0f;

    while(tot <= pca_thresh && num < (int)rows)
    {
        v1_float S_i = ((v1_float*)(S->data.ptr + S->step * num))[0];
        tot += S_i / S_sum;
        if(limit > 0 && num >= limit)
            break;
        num++;
    }

    if(num < (int)rows && (limit <= 0 || num < limit))
        num++;

    // Trim down the eigenvectors
    CvMat* reduced = cvCreateMat(cols, num, CV_V1);
    for(i = 0; i < (int)cols; i++)
    {
        for(j = 0; j < num; j++)
        {
            ((v1_float*)(reduced->data.ptr + reduced->step * i))[j] =
                ((v1_float*)(eigenvectors->data.ptr + eigenvectors->step * i))[j];
        }
    }

    clock_t end = clock();
    diff = ((double)(end - start)) / CLOCKS_PER_SEC;

    int min = (int)diff / 60;
    int sec = (int)diff % 60;

#ifndef TESTING
    if(quiet == 0)
    {
        printf("done. (eigen size = %d)\n", num);
        printf("Time spent in PCA:");
        if(min > 0)
            printf(" %d minute%s", min, (min != 1) ? "s" : "");
        if(sec > 0 || min == 0)
            printf (" %d second%s", sec, (sec != 1) ? "s" : "");
        printf("\n");

        printf("Saving PCA: "); fflush(stdout);
    }
#endif

    FILE* f;
    if(pca_fname == NULL)
        goto out;

    // Save off the eigenvectors
    f = fopenOrError(pca_fname, "w");
    if(f == NULL)
        goto out;

#ifndef HIDE_PROGRESS
    ascii_bar_params params;
    if(show_progress)
    {
        init_ascii_bar(&params, 0.0f, (float)(cols / 100 - 1), 1.0f);
        params.len = 50;
        show_ascii_bar(&params);
    }
#endif

    fprintf(f, "%d %d\n", reduced->rows, reduced->cols);
    for(i = 0; i < (int)cols; i++)
    {
        for(j = 0; j < num; j++)
#ifdef USE_DOUBLE
            fprintf(f, "%g ", ((v1_float*)(reduced->data.ptr + reduced->step * i))[j]);
#else
            fprintf(f, "%f ", ((v1_float*)(reduced->data.ptr + reduced->step * i))[j]);
#endif
        fprintf(f, "\n");

#ifndef HIDE_PROGRESS
        if(i % 100 == 0 && i > 0 && show_progress)
            step_ascii_bar(&params);
#endif

    }

#ifndef HIDE_PROGRESS
    if(show_progress)
        hide_ascii_bar(&params);
#endif

    fclose(f);

out:
#ifndef TESTING
    if(quiet == 0)
    {
        printf("done.\n");

        struct stat stFileInfo;
        if(stat(pca_fname, &stFileInfo) == 0)
        {
            printf("File size: ");
            bytesToString((long int)stFileInfo.st_size);
        }

        printf("Projecting data: "); fflush(stdout);
    }

    start = clock();
#endif

    project(reduced, data, gallery, size);

#ifndef TESTING
    printf("done.\n");

    end = clock();
    diff = ((double)(end - start)) / CLOCKS_PER_SEC;

    min = (int)diff / 60;
    sec = (int)diff % 60;

    if(quiet == 0)
    {
        printf("Time spent projecting:");
        if(min > 0)
            printf(" %d minute%s", min, (min != 1) ? "s" : "");
        if(sec > 0 || min == 0)
            printf (" %d second%s", sec, (sec != 1) ? "s" : "");
        printf("\n");
    }

#endif

    // Clean up
    cvReleaseMat(&eigenvectors);
    cvReleaseMat(&reduced);
    cvReleaseMat(&dot);
    cvReleaseMat(&U);
    cvReleaseMat(&V);
    cvReleaseMat(&S);
    cvReleaseMat(&data);
}

/**
 *Creates a pca subspace
 *@param struct gallery_entry* gallery The gallery used to create subspace
 *@param int size The size of the gallery
 *@param v1_float pca_thresh The Eigan value threshold.
 *@param const char* pca_fname File to save subspace to.
 *@param int show_progress 1 to show progress bar 0 otherwise.
 *@param int quiet 0 to surpress all messages.
 */
void pca_validate(struct validate_gallery* gallery, int size, v1_float pca_thresh, const char* pca_fname, int show_progress, int quiet, int limit)
{
    clock_t start = clock();
    double diff = 0.0;

#ifndef TESTING
    printf("PCA: ");
    fflush(stdout);
#endif

    int i, j;
    size_t rows = size;
    size_t cols = WINDOW_SIZE * WINDOW_SIZE * GABOR_FEATURES;

    CvMat* data = cvCreateMat(rows, cols, CV_V1);

    for(i = 0; i < (int)rows; i++)
    {
        for(j = 0; j < (int)cols; j++)
            ((v1_float*)(data->data.ptr + data->step*i))[j] = gallery[i].features[j];
    }

    // dot = N.dot(M, M.T)
    CvMat* dot = cvCreateMat(rows, rows, CV_V1);
    cvGEMM(data, data, 1, NULL, 1, dot, CV_GEMM_B_T);

    // U, S, NULL = N.linalg.svd(dot)
    CvMat* U = cvCreateMat(rows, rows, CV_V1);
    CvMat* S = cvCreateMat(rows, 1, CV_V1);
    CvMat* V = cvCreateMat(rows, rows, CV_V1);
    cvSVD(dot, S, U, V, CV_SVD_U_T | CV_SVD_V_T);

    // V = N.dot(U.T, M)
    cvReleaseMat(&V);
    V = cvCreateMat(rows, cols, CV_V1);
    cvGEMM(U, data, 1, NULL, 1, V, 0);

    // Normalize, square, and calculate S.sum() at the same time
    v1_float S_sum = 0.0;
    for(i = 0; i < (int)rows; i++)
    {
        v1_float norm = fasternorm(V, i);
        ((v1_float*)(S->data.ptr + S->step * i))[0] = v1_pow(norm, 2.0);
        S_sum += v1_pow(norm, 2.0);
        for(j = 0; j < (int)cols; j++)
            ((v1_float*)(V->data.ptr + V->step * i))[j] /= norm;
    }

    // eigvectors = V.T
    CvMat* eigenvectors = cvCreateMat(cols, rows, CV_V1);
    cvTranspose(V, eigenvectors);

    int num = 0;
    v1_float tot = 0.0f;

    while(tot <= pca_thresh && num < (int)rows)
    {
        v1_float S_i = ((v1_float*)(S->data.ptr + S->step * num))[0];
        tot += S_i / S_sum;
        if(limit > 0 && num >= limit)
            break;
        num++;
    }

    if(num < (int)rows && (limit <= 0 || num < limit))
        num++;

    // Trim down the eigenvectors
    CvMat* reduced = cvCreateMat(cols, num, CV_V1);
    for(i = 0; i < (int)cols; i++)
    {
        for(j = 0; j < num; j++)
        {
            ((v1_float*)(reduced->data.ptr + reduced->step * i))[j] =
                ((v1_float*)(eigenvectors->data.ptr + eigenvectors->step * i))[j];
        }
    }

    clock_t end = clock();
    diff = ((double)(end - start)) / CLOCKS_PER_SEC;

    int min = (int)diff / 60;
    int sec = (int)diff % 60;

#ifndef TESTING
    if(quiet == 0)
    {
        printf("done. (eigen size = %d)\n", num);
        printf("Time spent in PCA:");
        if(min > 0)
            printf(" %d minute%s", min, (min != 1) ? "s" : "");
        if(sec > 0 || min == 0)
            printf (" %d second%s", sec, (sec != 1) ? "s" : "");
        printf("\n");

        printf("Saving PCA: "); fflush(stdout);
    }
#endif

    FILE* f;
    if(pca_fname == NULL)
        goto out;

    // Save off the eigenvectors
    f = fopenOrError(pca_fname, "w");
    if(f == NULL)
        goto out;

#ifndef HIDE_PROGRESS
    ascii_bar_params params;
    if(show_progress)
    {
        init_ascii_bar(&params, 0.0f, (float)(cols / 100 - 1), 1.0f);
        params.len = 50;
        show_ascii_bar(&params);
    }
#endif

    fprintf(f, "%d %d\n", reduced->rows, reduced->cols);
    for(i = 0; i < (int)cols; i++)
    {
        for(j = 0; j < num; j++)
#ifdef USE_DOUBLE
            fprintf(f, "%g ", ((v1_float*)(reduced->data.ptr + reduced->step * i))[j]);
#else
            fprintf(f, "%f ", ((v1_float*)(reduced->data.ptr + reduced->step * i))[j]);
#endif
        fprintf(f, "\n");

#ifndef HIDE_PROGRESS
        if(i % 100 == 0 && i > 0 && show_progress)
            step_ascii_bar(&params);
#endif

    }

#ifndef HIDE_PROGRESS
    if(show_progress)
        hide_ascii_bar(&params);
#endif

    fclose(f);

out:
#ifndef TESTING
    if(quiet == 0)
    {
        printf("done.\n");

        struct stat stFileInfo;
        if(stat(pca_fname, &stFileInfo) == 0)
        {
            printf("File size: ");
            bytesToString((long int)stFileInfo.st_size);
        }

        printf("Projecting data: "); fflush(stdout);
    }

    start = clock();
#endif

    project_validate(reduced, data, gallery, size);

#ifndef TESTING
    printf("done.\n");

    end = clock();
    diff = ((double)(end - start)) / CLOCKS_PER_SEC;

    min = (int)diff / 60;
    sec = (int)diff % 60;

    if(quiet == 0)
    {
        printf("Time spent projecting:");
        if(min > 0)
            printf(" %d minute%s", min, (min != 1) ? "s" : "");
        if(sec > 0 || min == 0)
            printf (" %d second%s", sec, (sec != 1) ? "s" : "");
        printf("\n");
    }

#endif

    // Clean up
    cvReleaseMat(&eigenvectors);
    cvReleaseMat(&reduced);
    cvReleaseMat(&dot);
    cvReleaseMat(&U);
    cvReleaseMat(&V);
    cvReleaseMat(&S);
    cvReleaseMat(&data);
}
