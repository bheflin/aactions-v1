/// \file enroll.cpp
/// \author Chris Eberle
/// \author Ryan Thomas
/// \brief Main method for the enroll program
// Copyright 2009 Securics, Inc

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifndef WIN32
# include <pthread.h>
#endif

#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/stat.h>
#include <string.h>
#include <libgen.h>
#include "learn.h"
#include "v1.h"
#include "structs.h"
#include "pca.h"
#include "asciibar.h"
#include "gabor.h"
#include "utils.h"
#include "parser.h"
#include "geo.h"
#include "version.h"
#include "atmos_deblur.h"
#include "sqi.h"
#include "argparse.h"
#include "eye_detector.h"
#include "learn-multi.h"

static int incoming[2];
static int done[2];

// Structs
typedef struct prog_params_t
{
    char* prog_name;
    char* file_name;
    char* pca_fname;
    char* svm_fname;
    char* train_fname;
    char* sphere_fname;
    char* easy_base;
    char* loadfilt_dir;
    int   cross_validate;
    int   num_procs;
    int   fast_gabor;
    int   show_progress;
    int   use_cache;
    int   use_python;
    int   geo_norm;
    int   hist_norm;
    int   atmos_deblur;
    int   geo_norm_after;
    int   motion_deblur;
    int   sqi_norm;
    int   augment_training;
    int   quiet;
    int   use_eyedetect;
    int   eye_detection_thresh;
    int   onevsrest;
    int   pca_limit;
} prog_params;

// Lets us silence libsvm
extern int __verbose;

// Local prototypes
static void usage(const char* name, int val);
static void parseCommandLine(int argc, char **argv, prog_params *parm);
static int try_cache(struct tparam* param, int idx);
static void save_cache(struct tparam* param, int idx);
static void compute_features(struct tparam* param, int idx);
static void* feature_thread(void* p);
static void eye_detect_gallery(gallery_entry* gallery, int gallery_size, int thresh, int use_python, int show_progress);

#define SAFE_RELEASE(img) do { if((img) != NULL) { cvReleaseImage(&(img)); (img) = NULL; } } while(0);
#define SAFE_FREE(ptr) do { if ((ptr) != NULL) { free((ptr)); (ptr) = NULL; } } while(0);
#define SAFE_DELETE(ptr) do { if ((ptr) != NULL) { delete (ptr); (ptr) = NULL; } } while(0);

#ifndef TESTING

/**
 * The entry point for the train program
 */
int main(int argc, char **argv)
{
    prog_params params;
    int  c;
    int  gallery_size  = 0;
    void** filters     = NULL;
    clock_t start, end;
    double elapsed;
    struct gallery_entry *gallery;

    // Only used in augment mode
    v1_float* mean = NULL;
    v1_float* std  = NULL;

    // Shut the hell up, libsvm
    __verbose = 0;

#ifdef WIN32
    IplImage *input;
#endif

    parseCommandLine(argc, argv, &params);

    int error = 0;

    if(params.atmos_deblur == 1 && params.num_procs > 1)
    {
        printf("\nWARNING: Only 1 thread possible with atmospheric deblurring.\n");
        params.num_procs = 1;
    }

    if(params.file_name == NULL)
        ++error;

    if(params.easy_base && !error)
    {
        char tname[1024];
        if(params.pca_fname == NULL)
        {
            snprintf(tname, 1024, "%s.pca", params.easy_base);
            params.pca_fname = strdup(tname);
        }

        if(params.svm_fname == NULL)
        {
            if(params.onevsrest)
                snprintf(tname, 1024, "%s", params.easy_base);
            else
                snprintf(tname, 1024, "%s.svm", params.easy_base);
            params.svm_fname = strdup(tname);
        }

        if(params.sphere_fname == NULL)
        {
            snprintf(tname, 1024, "%s.val", params.easy_base);
            params.sphere_fname = strdup(tname);
        }
    }

    if(params.pca_fname == NULL)
        ++error;

    if(params.svm_fname == NULL)
        ++error;

    if(params.sphere_fname == NULL)
        ++error;

    if(params.num_procs < 1 || params.num_procs > 32)
    {
        fprintf(stderr, "Unreasonable number of threads: %d\n", params.num_procs);
        ++error;
    }

    if(error)
        usage(params.prog_name, 1);

    if(params.loadfilt_dir != NULL)
        params.fast_gabor = 1;

    if(params.quiet == 0)
    {
        printf("V1 Enrollment\n");
        printf("Gallery file: %s\n", params.file_name);
        printf("PCA file:     %s\n", params.pca_fname);

        if(params.onevsrest)
            printf("SVM file:     %s_#.svm\n", params.svm_fname);
        else
            printf("SVM file:     %s\n", params.svm_fname);

        printf("Value file:   %s\n", params.sphere_fname);
        printf("SVM features: %s\n", (params.train_fname == NULL) ? "(None)" : params.train_fname);
        printf("Augment:      %s\n", (params.augment_training == 1) ? "Yes" : "No");
        printf("Num workers:  %d\n", params.num_procs);
        printf("Caching:      %s\n", (params.use_cache == 1) ? "On" : "Off");
        printf("Gabor mode:   %s\n", (params.fast_gabor == 1) ? "Fast" : "Slow");
        printf("Geo-norm:     ");

        if(params.geo_norm == 1)
        {
            printf("On (hist-norm = %s)\n", (params.hist_norm == 1) ? "On" : "Off");
            printf("Geo-norm:     ");
            printf("Eye detect:   ");

            if(params.use_eyedetect == 1)
            {
                printf("On\n");
                printf("Eye thresh:   %d\n", params.eye_detection_thresh);
            }
            else
                printf("Off\n");
        }
        else
        {
            printf("Off\n");
        }

        printf("SQI-norm:     %s\n", (params.sqi_norm == 1) ? "On" : "Off");
        printf("Atmos-deblur: %s\n", (params.atmos_deblur == 1) ? "On" : "Off");
        if(params.atmos_deblur == 1)
            printf("Geo-after:    %s\n", (params.geo_norm_after == 1) ? "On" : "Off");
        printf("Resize:       %s\n", (params.use_python == 1) ? "Python" : "cvResize");

#ifdef USE_DOUBLE
        printf("Precision:    double\n");
#else
        printf("Precision:    float\n");
#endif

#ifdef DEBUG
        printf("Debugging:    On\n\n");
#else
        printf("Debugging:    Off\n\n"); 
#endif

    }

#ifdef DEBUG
    if(params.num_procs > 1)
        printf("WARNING: You are using > 1 worker. This causes problems for cvShowImage.\n");
#endif

    // Open the training file
    filelist* tf_list = parseFileList(params.file_name);
    if(tf_list == NULL)
        exit(-2);
    gallery_size = tf_list->num;

    if(gallery_size == 0)
    {
        fprintf(stderr, "Got no valid lines from the file %s\n", params.file_name);
        exit(-3);
    }

    if(params.atmos_deblur && params.motion_deblur)
        gallery_size = gallery_size * 4;
    else if(params.motion_deblur)
        gallery_size = gallery_size * 3;
    else if(params.atmos_deblur)
        gallery_size = gallery_size * 2;

    if(params.quiet == 0)
        printf("Gallery size is %d\n", gallery_size);

    start = clock();
    init(&gallery, gallery_size);

    //store the gallery information
    fileitem* item = tf_list->begin;
    int mf = 1;

    if(params.motion_deblur && params.atmos_deblur)
        mf = 4;
    else if(params.motion_deblur)
        mf = 3;
    else if(params.atmos_deblur)
        mf = 2;

    for(c = 0; c < tf_list->num; c++)
    {
        if(item == NULL)
            break;

        gallery[c*mf].path  = strdup(item->file);
        gallery[c*mf].id    = item->label;
        gallery[c*mf].leyeX = item->leyeX;
        gallery[c*mf].leyeY = item->leyeY;
        gallery[c*mf].reyeX = item->reyeX;
        gallery[c*mf].reyeY = item->reyeY;
        gallery[c*mf].atmos_deblur = 0;
        gallery[c*mf].motion_deblur = 0;

        int i;

        for(i = 1; i < mf; i++)
        {
            gallery[c*mf+i].path  = strdup(item->file);
            gallery[c*mf+i].id    = gallery[c*mf].id;
            gallery[c*mf+i].leyeX = gallery[c*mf].leyeX;
            gallery[c*mf+i].leyeY = gallery[c*mf].leyeY;
            gallery[c*mf+i].reyeX = gallery[c*mf].reyeX;
            gallery[c*mf+i].reyeY = gallery[c*mf].reyeY;
            gallery[c*mf+i].atmos_deblur = 0;
            gallery[c*mf+i].motion_deblur = 0;

            if(params.atmos_deblur == 1&& i == 1)
                gallery[c*mf+i].atmos_deblur = 1;
            else if(params.motion_deblur == 1 && params.atmos_deblur == 0)
                gallery[c*mf+i].motion_deblur = 1;
            else
                gallery[c*mf+i].motion_deblur = 1;
        }

        item = item->next;        
    }
    //do eye detection to replace poor ground truth
    if(params.use_eyedetect == 1)
    {
        eye_detect_gallery(gallery, gallery_size, params.eye_detection_thresh, params.use_python, params.show_progress);
    }
    if(params.geo_norm == 1)
    {
        int gn;
        if(params.atmos_deblur == 1 && params.geo_norm_after == 1)
        {
            //ONLY 1 proc with fftw
            //            num_procs = 1;
            gn = perform_geo_norm_gallery(gallery, gallery_size, params.hist_norm, params.show_progress, 1);
        }
        else
        {
            gn = perform_geo_norm_gallery(gallery, gallery_size, params.hist_norm, params.show_progress, 0);
        }

        if(gn == -1)
        {
            free_v1(&gallery, gallery_size);
            printf("Error in Geo-norm\n");
            return -1;
        }
    }
    

    if(params.num_procs > gallery_size)
        params.num_procs = gallery_size;

    // No point in having all the threads make their own
    if(params.loadfilt_dir != NULL)
        filters = (void**)load_filters(params.loadfilt_dir);
    else
    {
        if(params.fast_gabor)
            filters = (void**)make_filters();
        else
            filters = (void**)make_ipl_filters();
    }

    if(filters == NULL)
        exit(-4);

    struct tparam thread_param;
    thread_param.gallery = gallery;
    thread_param.fast_gabor = params.fast_gabor;
    thread_param.filters = filters;
    thread_param.use_cache = params.use_cache;
    thread_param.use_python = params.use_python;
    thread_param.tnum = 0;
    thread_param.geo_norm_after = params.geo_norm_after;
    thread_param.geo_norm = params.geo_norm;
    thread_param.hist_norm = params.hist_norm;
    thread_param.sqi_norm = params.sqi_norm;
#ifdef WIN32

    if(params.quiet == 0)
        printf("Getting features...\n");

#else

    pthread_t *threads = (pthread_t*)malloc(params.num_procs * sizeof(pthread_t));
    pthread_attr_t pthread_custom_attr;
    pthread_attr_init(&pthread_custom_attr);

    if(pipe(incoming) != 0)
    {
        perror("pipe");
        exit(1);
    }

    if(pipe(done) != 0)
    {
        perror("pipe");
        exit(1);
    }

    for(c = 0; c < params.num_procs; c++)
    {
        thread_param.tnum++;
        struct tparam* tp = (struct tparam*)malloc(sizeof(struct tparam));
        memcpy(tp, &thread_param, sizeof(struct tparam));
        pthread_create(&threads[c], &pthread_custom_attr, feature_thread, (void*)tp);
    }

    if(params.quiet == 0)
        printf("Started %d worker%s\n", params.num_procs, (params.num_procs == 1) ? "" : "s");

#endif // !WIN32

    for(c = 0; c < gallery_size; c++)
    {
#ifdef WIN32
        compute_features(&thread_param, c);
        if(params.quiet == 0)
            printf(" --> %s\n", gallery[c].path);
#else
        if(write(incoming[1], &c, sizeof(int)) != sizeof(int))
            perror("write");
#endif
    }

    freeList(tf_list);

#ifndef WIN32
    int n = -1;
    for(c = 0; c < params.num_procs; c++)
    {
        if(write(incoming[1], &n, sizeof(int)) != sizeof(int))
            perror("write");
    }

# ifndef HIDE_PROGRESS
    struct ascii_bar_params abp;
    if(params.show_progress)
    {
        init_ascii_bar(&abp, 0.0, (float)gallery_size, 1.0f);
        abp.len = 45;
        abp.show_eta = 1;
        printf("Get features: ");
        show_ascii_bar(&abp);
    }
    else
# endif
    {
        if(params.quiet == 0)
            printf("Getting features...\n");
        else
            printf("Get features:");
        fflush(stdout);
    }

    for(c = 0; c < gallery_size; c++)
    {
        if(read(done[0], &n, sizeof(int)) != sizeof(int))
            perror("read");

# ifndef HIDE_PROGRESS
        if(params.show_progress)
            step_ascii_bar(&abp);
        else
# endif
            if(params.quiet == 0)
            {
                printf("[%5.1f%%] %s\n", 100. * (float)(c + 1) / (float)gallery_size, gallery[n].path);
                fflush(stdout);
            }
    }

# ifndef HIDE_PROGRESS
    if(params.show_progress)
    {
        hide_ascii_bar(&abp);
        printf("done.\n");
        print_elapsed_time(&abp, "image");
    }
    else
# endif
    {
        if(params.quiet)
            printf(" done.\n");
    }

    for(c = 0; c < params.num_procs; c++)
        pthread_join(threads[c], NULL);
    free(threads);

    close(incoming[0]);
    close(incoming[1]);
    close(done[0]);
    close(done[1]);

    pthread_attr_destroy(&pthread_custom_attr);
#endif // !WIN32

    // Here's where we differentiate between augmenting and
    // non-augmenting mode.

    int feat_size = WINDOW_SIZE * WINDOW_SIZE * GABOR_FEATURES;
    if(params.augment_training)
    {
        if(params.quiet == 0)
        {
            printf("Load training: ");
            fflush(stdout);
        }

        FILE* f = fopenOrError(params.sphere_fname, "r");
        if(f == NULL)
        {
            printf("Augmenting disabled.\n");
            goto noaugment;
        }

        mean = (v1_float*)malloc(feat_size * sizeof(v1_float));
        std = (v1_float*)malloc(feat_size * sizeof(v1_float));

        if(mean == NULL || std == NULL)
        {
            if(mean != NULL) free(mean);
            if(std != NULL) free(std);
            fclose(f);
            printf("Augmenting disabled.\n");
            goto noaugment;
        }

        int i = 0;

#ifdef USE_DOUBLE
        while(fscanf(f, "%lf %lf", &(mean[i]), &(std[i])) !=EOF)
#else
        while(fscanf(f, "%f %f", &(mean[i]), &(std[i])) !=EOF)
#endif
        {
            if(i >= feat_size)
            {
                fclose(f);
                free(mean);
                free(std);
                printf("Overflow detected, Augmenting disabled.\n");
                goto noaugment;
            }

            i++;
        }

        fclose(f);

        // Load the PCA subspace
        CvMat* training = loadTrainingData(params.pca_fname, params.show_progress);
        if(training == NULL)
        {
            free(mean);
            free(std);
            printf("Augmenting disabled.\n");
            goto noaugment;
        }

        if(params.quiet == 0)
            printf("done.\n");

        // Now sphere the data
        if(params.quiet == 0)
        {
            printf("Sphere: ");
            fflush(stdout);
        }

        for(i = 0; i < feat_size; i++)
        {
            if(std[i] != 0)
            {
                for(c = 0; c < gallery_size; c++)
                    gallery[c].features[i] = (gallery[c].features[i] - mean[i]) / std[i];
            }
            else
            {
                for(c = 0; c < gallery_size; c++)
                    gallery[c].features[i] = gallery[c].features[i] - mean[i];
            }
        }

        free(mean);
        free(std);

        printf("done.\n");

        // Project data into pca subspace
        if(params.quiet == 0)
        {
            printf("Projecting: ");
            fflush(stdout);
        }

        projectMulti(training, gallery, gallery_size);
        cvReleaseMat(&training);

        if(params.quiet == 0)
            printf("done\n");
    }
    else
    {
    noaugment:
        // Sphere the data
        printf("Sphere: "); fflush(stdout);
        sphere_data(gallery, gallery_size, feat_size, params.sphere_fname);
        printf("done.\n");

        // Run PCA reduction
        pca(gallery, gallery_size, 1., params.pca_fname, params.show_progress, params.quiet, params.pca_limit);
    }

    if(params.onevsrest)
    {
        // Train the PCA-projected data using a one-vs-rest classifier
        learn_onevsrest* onevrest = NULL;
        if((onevrest = learn_onevsrest_create(params.quiet, params.show_progress, params.num_procs)) == NULL)
        {
            fprintf(stderr, "ERROR: Unable to create one vs rest svm learning model.\n");
            exit(42);
        }

        if(learn_onevsrest_train(onevrest, gallery, (unsigned int)gallery_size, params.svm_fname) < 0)
        {
            fprintf(stderr, "ERROR: One vs rest training failed.\n");
            exit(43);
        }

        learn_onevsrest_destroy(onevrest);
        onevrest = NULL;
    }
    else
    {
        // Initialize the libsvm parameters
        svm_parameter svm_params;
        svm_params.svm_type     = C_SVC;
        svm_params.kernel_type  = LINEAR;
        svm_params.degree       = 2;
        svm_params.gamma        = 0; // 1/k
        svm_params.coef0        = 0;
        svm_params.nu           = 0.1;
        svm_params.cache_size   = 256;
        svm_params.C            = 10;
        svm_params.eps          = 0.01;
        svm_params.p            = 0.1;
        svm_params.shrinking    = 1;
        svm_params.probability  = 1;
        svm_params.nr_weight    = 0;
        svm_params.weight_label = NULL;
        svm_params.weight       = NULL;

        // Train the PCA-projected data
        learn(gallery, gallery_size, &svm_params, params.svm_fname, params.train_fname, params.cross_validate);        
    }

    // Clean up
    free_v1(&gallery, gallery_size);
    if(params.fast_gabor)
        free_filters((gabor_filter**)filters);
    else
        free_ipl_filters((IplImage**)filters);

    end = clock();
    elapsed = ((double)(end - start)) / CLOCKS_PER_SEC;

    int hours = (int)elapsed / 3600;
    int minutes = ((int)elapsed - (hours * 3600))  / 60;
    int seconds = (int)elapsed - (hours * 3600) - (minutes * 60);

    if(params.quiet == 0)
    {
        printf("Enrollment time: ");
        if(hours == 1)
            printf(" 1 hour");
        else if(hours > 1)
            printf(" %d hours", hours);

        if(minutes == 1)
            printf(" 1 minute");
        else if(minutes > 1)
            printf(" %d minutes", minutes);
        else if(hours > 0 && minutes == 0 && seconds > 0)
            printf(" 0 minutes");

        if(seconds == 1)
            printf(" 1 second");
        else if(seconds > 1)
            printf(" %d seconds", seconds);

        printf("\nEffective rate: %.3f seconds per image\n", (float)elapsed / (float)gallery_size);
    }

    SAFE_FREE(params.file_name);
    SAFE_FREE(params.pca_fname);
    SAFE_FREE(params.svm_fname);
    SAFE_FREE(params.train_fname);
    SAFE_FREE(params.sphere_fname);
    SAFE_FREE(params.easy_base);
    SAFE_FREE(params.loadfilt_dir);

    return 0;
}

#endif // TESTING

/**
 *Run eye detection on a gallery, and if the new eyes are within than ground truth, replace the ground truth
 * @param gallery_entry* gallery The gallery to enroll
 * @param int gallery_size The size of the gallery
 * @param int thresh The maximum distance detected eyes can be found and used to replace ground truth
 * @param int use_python 1 to use python resize 0 otherwise.
 */
static void eye_detect_gallery(gallery_entry* gallery, int gallery_size, int thresh, int use_python, int show_progress)
{
    printf("Detecting eyes: ");
    fflush(stdout);

#ifndef HIDE_PROGRESS
    ascii_bar_params params;
    if(show_progress)
    {
        init_ascii_bar(&params, 0.0f, (float)gallery_size, 1.0f);
        params.len = 50;
        show_ascii_bar(&params);
    }
#endif

    int reyeX, reyeY, leyeX, leyeY, i;
    int reyeX1, reyeY1, leyeX1, leyeY1;
    float l_distance, r_distance;
    IplImage* input;
    int beforeW, beforeH;
    double scaleX, scaleY;
    const int small_size = 150;
    for(i = 0; i < gallery_size; i++)
    {
        reyeX = 0;
        reyeY = 0;
        leyeX = 0;
        leyeY = 0;
        input = cvLoadImage(gallery[i].path, 0);
        int width = gallery[i].reyeX -gallery[i].leyeX;
        CvRect r;
        r.x = gallery[i].leyeX - width;

        if(r.x < 0)
            r.x = 0;

        r.y = gallery[i].leyeY - width;
        if(r.y < 0)
            r.y = 0;

        r.width = 3 * width;
        if(r.width > input->width)
            r.width = input->width;

        r.height = 3 * width;
        if(r.height > input->height)
            r.height = input->height;

        IplImage* tmp = cropIpl(input, r.x, r.y, r.width, r.height);
        SAFE_RELEASE(input);
        input = tmp;

        beforeW = input->width;
        beforeH = input->height;
        tmp = get_image(input, small_size, use_python, 1);
        SAFE_RELEASE(input);
        input = tmp;
        eye_detect(input, &leyeX, &reyeX, &leyeY, &reyeY, &leyeX1, &reyeX1, &leyeY1, &reyeY1);
        scaleX = (double)beforeW / (double)input->width;
        scaleY = (double)beforeH / (double)input->height;
        leyeX = (int)((double)leyeX * scaleX) + r.x;
        leyeY = (int)((double)leyeY * scaleY) + r.y;
        reyeX = (int)((double)reyeX * scaleX) + r.x;
        reyeY = (int)((double)reyeY * scaleY) + r.y;

        //check if old ground truth is better
        l_distance = sqrt(pow(leyeX-gallery[i].leyeX,2)+pow(leyeY-gallery[i].leyeY,2));
        r_distance = sqrt(pow(reyeX-gallery[i].reyeX,2)+pow(reyeY-gallery[i].reyeY,2));

        if(l_distance < thresh && r_distance < thresh)
        {
            gallery[i].leyeX = leyeX;
            gallery[i].leyeY = leyeY;
            gallery[i].reyeX = reyeX;
            gallery[i].reyeY = reyeY;
        }

        SAFE_RELEASE(input);

#ifndef HIDE_PROGRESS
        if(show_progress)
            step_ascii_bar(&params);
#endif

    }

#ifndef HIDE_PROGRESS
    if(show_progress)
        hide_ascii_bar(&params);
#endif

    printf("done.\n");
}

/**
 * Try to load features from a cache file. Return 0 on success
 * @param struct tparam* param The thread specific parameters on the program options for feature generation.
 * @param int idx The index of the thread 
 */
static int try_cache(struct tparam* param, int idx)
{
    const char* file = param->gallery[idx].path;
    const char* suffix = ".cache";
    char* cachefile = (char*)malloc((strlen(file) + strlen(suffix) + 1) * sizeof(char));
    sprintf(cachefile, "%s%s", file, suffix);

    struct stat stFileInfo;
    if(stat(cachefile, &stFileInfo) != 0)
    {
        free(cachefile);
        return -1;
    }

    v1_float* feat = param->gallery[idx].features;
    FILE *fp = fopen(cachefile, "r");
    free(cachefile);
    if(fp == NULL)
        return -1;

    int sz = WINDOW_SIZE * WINDOW_SIZE * GABOR_FEATURES;

    for(int i = 0; i < sz; ++i)
    {
#ifdef USE_DOUBLE
        if(fscanf(fp, "%lf", &feat[i]) != 1)
#else
        if(fscanf(fp, "%f", &feat[i]) != 1)
#endif
        {
            fclose(fp);
            return -1;
        }
    }

    fclose(fp);
    return 0;
}

/**
 * Save features to a cache file. Problems are not reported -- it's a cache.
 * @param struct tparam* param The thread specific parameters on the program options for feature generation.
 * @param int idx The index of the thread 
 */
static void save_cache(struct tparam* param, int idx)
{
    const char* file = param->gallery[idx].path;
    const char* suffix = ".cache";
    char* cachefile = (char*)malloc((strlen(file) + strlen(suffix) + 1) * sizeof(char));
    sprintf(cachefile, "%s%s", file, suffix);

    FILE *fp = fopen(cachefile, "w");
    free(cachefile);
    if(fp == NULL)
        return;

    int sz = WINDOW_SIZE * WINDOW_SIZE * GABOR_FEATURES;
    v1_float* feat = param->gallery[idx].features;

    for(int i = 0; i < sz; ++i)
    {
        fprintf(fp, "%f\n", feat[i]);
    }

    fclose(fp);
    return;
}

/**
 *  Load an image and call get_features
 * @param struct tparam* param The thread specific parameters on the program options for feature generation.
 * @param int idx The thread id.
 */
static void compute_features(struct tparam* param, int idx)
{
    IplImage *input;
    const char* file = param->gallery[idx].path;

    if(param->use_cache)
    {
        if(try_cache(param, idx) == 0)
            return;
    }

    if((input = cvLoadImage(file, 0)) == NULL)
    {
        fprintf(stderr, "Unable to open file: `%s'\n", file);
        return;
    }

    if(param->gallery[idx].atmos_deblur == 1)
    {

        IplImage* atmos_img = atmos_deblur(input,0);
        cvReleaseImage(&input);
        input = atmos_img;
        //check if geo norm should
        if(param->geo_norm_after && param->geo_norm)
        {
	  IplImage* norm = perform_geo_norm(input, param->gallery[idx].leyeX, param->gallery[idx].leyeY, param->gallery[idx].reyeX,param->gallery[idx].reyeY, param->hist_norm, param->tnum);
            cvReleaseImage(&input);
            input = norm;
        }
    }
    else if(param->gallery[idx].motion_deblur == 1)
    {

    }
    if(param->sqi_norm)
    {
        IplImage* sqi = securics_sqi_norm(input);
        cvReleaseImage(&input);
        input = sqi;
    }

    get_features(input, param->gallery[idx].features, param->filters, param->fast_gabor, 1,
                 param->use_python, param->tnum);

    cvReleaseImage(&input);

    if(param->use_cache)
        save_cache(param, idx);
}

/**
 *  Used for pthreads to speed up feature generation
 * @param void* p The void pointer containing the thread structure.
 */
static void* feature_thread(void* p)
{
    struct tparam* param = (struct tparam*)p;

    int n;

    while(1)
    {
        if(read(incoming[0], &n, sizeof(int)) != sizeof(int))
            perror("read");

        if(n < 0)
            break;
        compute_features(param, n);

        if(write(done[1], &n, sizeof(int)) != sizeof(int))
            perror("write");
    }

    free(p);

    return NULL;
}

/**
 * Parses the command line
 * @param int argc Same as main
 * @param char**argv Same as main
 * @param prog_params *parm The program parameters
 */
static void parseCommandLine(int argc, char **argv, prog_params *parm)
{
    argv[0] = basename(argv[0]);

    parm->prog_name        = argv[0];
    parm->file_name        = NULL;
    parm->pca_fname        = NULL;
    parm->svm_fname        = NULL;
    parm->train_fname      = NULL;
    parm->sphere_fname     = NULL;
    parm->easy_base        = NULL;
    parm->loadfilt_dir     = NULL;
    parm->cross_validate   = 0;
    parm->use_eyedetect    = 0;
    parm->eye_detection_thresh = 10;
    parm->fast_gabor       = 1;
    parm->show_progress    = 1;
    parm->use_cache        = 0;
    parm->use_python       = 0;
    parm->geo_norm         = 0;
    parm->hist_norm        = 1;
    parm->atmos_deblur     = 0;
    parm->geo_norm_after   = 0;
    parm->motion_deblur    = 0;
    parm->sqi_norm         = 0;
    parm->augment_training = 0;
    parm->quiet            = 0;
    parm->onevsrest        = 1;
    parm->pca_limit        = -1;

#ifdef DEBUG
    parm->num_procs        = 1;
#else
# ifdef WIN32
    parm->num_procs        = 1;
# else
    parm->num_procs        = 2;
# endif
#endif

    ArgParser ap(usage, parm->prog_name, false);

    // Simple switches
    ap.addOption("help", false, "h");
    ap.addOption("info", false, "i");

    // Regular options
    ap.addOption("config",        true, "C");
    ap.addOption("file",          true, "f");
    ap.addOption("pca",           true, "p");
    ap.addOption("value",         true, "v");
    ap.addOption("svm",           true, "s");
    ap.addOption("base",          true, "b");
    ap.addOption("features-save", true, "F");
    ap.addOption("num-threads",   true, "t");
    ap.addOption("filter-dir",    true, "d");
    ap.addOption("pca-limit",     true, "l");
    ap.addOption("save-config",   true, "o");

    // Boolean options (rightmost column indicates that "-G" is the same as "-G yes")
    ap.addOption("fast-gabor",     true, "S", "yes");
    ap.addOption("deblur-atmos",   true, "A", "yes");
    ap.addOption("geo-after",      true, "a", "yes");
    ap.addOption("geo-norm",       true, "G", "yes");
    ap.addOption("cross-validate", true, "c", "yes");    
    ap.addOption("progress",       true, "r", "yes");
    ap.addOption("cache",          true, "Q", "yes");
    ap.addOption("hist-norm",      true, "H", "yes");
    ap.addOption("python-resize",  true, "P", "yes");
    ap.addOption("normalize-SQI",  true, "B", "yes");
    ap.addOption("augment",        true, "u", "yes");
    ap.addOption("quiet",          true, "q", "yes");
    ap.addOption("eye-detect",     true, "E", "yes");    
    ap.addOption("eye-thresh",     true, "R", "yes");
    ap.addOption("one-vs-rest",    true, "j", "yes");

    if(!ap.parseCommandLine(argc, argv, true))
        exit(1);
    
    if(ap.isSet("help"))
        usage(argv[0], 0);
    
    if(ap.isSet("info"))
    {
        printFileVersions(stdout);
        exit(0);
    }

    // Parse the config file if specified
    if(ap.isSet("config"))
    {
        if(!ap.parseConfigFile(ap.getValue("config"), true))
            exit(1);
    }

    if(ap.isSet("file")) parm->file_name = strdup(ap.getValue("file"));
    if(ap.isSet("pca")) parm->pca_fname = strdup(ap.getValue("pca"));
    if(ap.isSet("value")) parm->sphere_fname = strdup(ap.getValue("value"));
    if(ap.isSet("svm")) parm->svm_fname = strdup(ap.getValue("svm"));
    if(ap.isSet("base")) parm->easy_base = strdup(ap.getValue("base"));
    if(ap.isSet("features-save")) parm->train_fname = strdup(ap.getValue("features-save"));
    if(ap.isSet("cross-validate")) parm->cross_validate = ap.getBoolValue("cross-validate");
    if(ap.isSet("fast-gabor")) parm->fast_gabor = ap.getBoolValue("fast-gabor");
    if(ap.isSet("progress")) parm->show_progress = ap.getBoolValue("progress");
    if(ap.isSet("filter-dir")) parm->loadfilt_dir = strdup(ap.getValue("filter-dir"));
    if(ap.isSet("cache")) parm->use_cache = ap.getBoolValue("cache");
    if(ap.isSet("geo-norm")) parm->geo_norm = ap.getBoolValue("geo-norm");
    if(ap.isSet("hist-norm")) parm->hist_norm = ap.getBoolValue("hist-norm");
    if(ap.isSet("geo-after")) parm->geo_norm_after = ap.getBoolValue("geo-after");
    if(ap.isSet("deblur-atmos")) parm->atmos_deblur = ap.getBoolValue("deblur-atmos");
    if(ap.isSet("python-resize")) parm->use_python = ap.getBoolValue("python-resize");
    if(ap.isSet("normalize-SQI")) parm->sqi_norm = ap.getBoolValue("normalize-SQI");
    if(ap.isSet("augment")) parm->augment_training = ap.getBoolValue("augment");
    if(ap.isSet("quiet")) parm->quiet = ap.getBoolValue("quiet");
    if(ap.isSet("eye-detect")) parm->use_eyedetect = ap.getBoolValue("eye-detect");
    if(ap.isSet("eye-thresh")) parm->eye_detection_thresh = atoi(ap.getValue("eye-thresh"));
    if(ap.isSet("one-vs-rest")) parm->onevsrest = ap.getBoolValue("one-vs-rest");
    if(ap.isSet("pca-limit")) parm->pca_limit = atoi(ap.getValue("pca-limit"));

    if(ap.isSet("num-threads"))
    {
#ifdef WIN32
        fprintf(stderr, "Sorry, number of threads is only for use by cool people.\n");
        exit(1);
#endif
        parm->num_procs = atoi(ap.getValue("num-threads"));
    }

    if(parm->quiet)
        parm->show_progress = 0;

    if(ap.isSet("save-config"))
    {
        vector<string> excludes;
        excludes.push_back("help");
        excludes.push_back("info");
        excludes.push_back("config");
        excludes.push_back("save-config");
        if(!ap.writeConfigFile(ap.getValue("save-config"), excludes))
            exit(1);
        printf("All options saved to %s.\n", ap.getValue("save-config"));
        exit(0);
    }
}

/**
 * prints the usage of the program if command line is wrong
 * @param const char* name The program name.
 * @param int val The program exit value
 */
static void usage(const char* name, int val)
{
    fprintf(stderr, "Usage: %s [OPTIONS]\n"
            "General options:\n"
            "  -i, --info            Print out file versions and exit.\n"
            "  -h, --help            Show this helpful message.\n"
            "  -C, --config=FILE     Load extra configuration options from FILE.\n"
            "  -o, --save-config=FILE\n"
            "                        Dump all configuration options to FILE.\n"
#ifndef HIDE_PROGRESS
            "  -r, --progress=BOOL   Show the progress bar (default true).\n"
#endif
            "  -q, --quiet=BOOL      Less output (default false). If set, implies -r0.\n\n"
            "V1 training options:\n"
            "  -f, --file=FILE       Gallery file with ids and eye coordinates (required).\n"
            "  -b, --base=NAME       Use easy file mode where -p, -s, and -v are\n"
            "                        automatically determined using NAME as the base.\n"
            "  -p, --pca=FILE        PCA subspace file. Required unless -b specified.\n"
            "  -s, --svm=FILE        SVM training file. Required unless -b specified.\n"
            "  -v, --value=FILE      Sphered values file. Required unless -b specified.\n"
            "  -u, --augment=BOOL    Re-use prior PCA subspace and sphere value files\n"
            "                        to speed up training. (default false).\n"
            "  -F, --features-save=FILE\n"
            "                        File in which to store SVM features.\n"
            "  -t, --num-threads=NUM Number of threads to spawn (default 2).\n"
            "  -l, --pca-limit=NUM   Maximum number of PCA features to keep.\n"
            "  -d, --filter-dir=DIR  Load gabor filters from DIR. Ignores -S.\n"
            "  -S, --fast-gabor=BOOL Use the fast gabor convolve (default true).\n"
            "  -c, --cross-validate=BOOL\n"
            "                        Enable cross validation of svm training (default false).\n"
            "  -Q, --cache=BOOL      Enable feature caching (default false).\n"
            "  -P, --python-resize=BOOL\n"
            "                        Call off to python for resizing rather than cvResize\n"
            "                        (default false).\n"
            "  -B, --normalize-SQI=BOOL\n"
            "                        Enable sqi-normalization of the images (default false).\n"
            "  -j, --one-vs-rest=BOOL\n"
            "                        Create a multi-class classifier using one-vs-rest\n"
            "                        method (default false). This will slow down training.\n\n"
            "Geo-norm options (all options only effective if -G is specified):\n"
            "  -G, --geo-norm=BOOL   Enable geo-normalization of the images (default false).\n"
            "  -H, --hist-norm=BOOL  Enable histogram normalization (default true).\n"
            "  -E, --eye-detect=BOOL Enable eye detection (default false).\n"
            "  -R, --eye-thresh=NUM  Enable eye detection threshold. If the detected position\n"
            "                        is futher than NUM pixels from the ground-truthed position,\n"
            "                        then the ground truth position is used (default 10).\n\n" 
            "Debluring options:\n"
            "  -A, --deblur-atmos=BOOL\n"
            "                        Enable atmospheric deblurring (default false).\n"
            "  -a, --geo-after=BOOL  Enable geo-norm after atmos-deblurring (default false)\n\n"
            "Examples:\n"
            "  %s -C config.cfg -r0 --file=gallery.txt -G1 -H0 --cache=no -S false\n\n"
            , name, name);
    exit(val);
}
