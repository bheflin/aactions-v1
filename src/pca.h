// pca.h
// Copyright 2009 Securics, Inc

#ifndef PCA_H
#define PCA_H

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "structs.h"

typedef struct _trainingData
{
    unsigned int eigen_count; // How many principle components did we keep
    unsigned int image_count; // How many images were there in the training set
    unsigned int mat_size;    // How large was a single image (flattened)
    unsigned int img_width;   // The width of the normalized image
    unsigned int img_height;  // The height of the normalized image
    CvMat* means;             // The averages for the images
    CvMat* eigenVectors;      // The eigenvectors
    CvMat* eigenValues;       // The eigenvalues
} trainingData;

void pca(struct gallery_entry* gallery, int size, v1_float pca_thresh, const char* pca_fname, int show_progress, int quiet, int limit);
void pca_validate(struct validate_gallery* gallery, int size, v1_float pca_thresh, const char* pca_fname, int show_progress, int quiet, int limit);
CvMat* loadTrainingData(const char* pca_fname, int show_progress);
v1_float* projectProbe(CvMat* training, v1_float* fdata, int in_size, int* out_size);
void projectMulti(CvMat* training, struct gallery_entry* gallery, int size);

#endif // PCA_H
