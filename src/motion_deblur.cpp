// motion_deblur.cpp
// Copyright 2009 Securics, Inc

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include "cvheaders.h"

#include <fftw3.h>
#define PI 3.14159265

//User Parameters
#define INPUT_IMAGE "10.jpg"
#define MAX_ESTIMATES 6
#define MIN_ESTIMATES 4
#define MAX_BLUR_LENGTH 30
#define GAMMA_VALUE 0.00325

void CROP_REGION (IplImage * img_out, IplImage *img_in, int x_crop_start, int y_crop_start, int size_x, int size_y)
{

	static int i,j;
	
	//crop out eye area
	for( i = y_crop_start; i < y_crop_start + size_y; i++ ) {
	  for( j = x_crop_start; j < x_crop_start + size_x ; j++ ) {
	    
	    (img_out->imageData + (int)(i-y_crop_start) * img_out ->widthStep)[(int)(j-x_crop_start)]=(img_in->imageData + i * img_in ->widthStep)[j];
	  }
	  
	}
}


int BACKGROUND_SUB_CROP(IplImage *original_crop, IplImage *deblurred_crop)
{


   int flag=0;
   int width, height, step;
   int i,j;
   unsigned char  THRESHOLD=10;
   unsigned char* temp_value_original;
   unsigned char* temp_value_deblurred;
   unsigned char abs_value;
   int above_threshold_counter=0;
 
    width  	  = original_crop->width;
    height 	  = original_crop->height;
    step	  = original_crop->widthStep;
    
    //subtract original crop and deblurred
    for( i = 0; i < height ; i++ ) {
      for( j = 0 ; j < width ; j++ ) {
	
	temp_value_original=((unsigned char*) (original_crop->imageData + i * step) [j]);
	temp_value_deblurred=((unsigned char*) (deblurred_crop->imageData + i * step) [j]);
	abs_value=abs(temp_value_original-temp_value_deblurred);
	if (abs_value > THRESHOLD)
	  above_threshold_counter++;
      }
    }
    
    if (above_threshold_counter > ((width*height)/1.5))
      flag=1;
    else
      flag=0;
    
    //printf("FLAG: %d BACK_DEBUG ABOVE_COUNTER: %d  CUTOFF: %d\n",flag,above_threshold_counter,((width*height)/3));
    // cvWaitKey(0);
    //flag=0;
    
    return (flag);

}


/**
 * Extends image boarders before hamming window.
 * @param IplImage* img_in The input image.
 * @param int NEW_IMAGE_SIZE The size of the border replacated image.
 * @return IplImage* The new image.
 */
float IMAGE_QUALITY_MOTION(IplImage *input_image, int apertureSize, float Threshold)
{
  
  static float temp_sum;
  static float temp_mean;
  static float temp_sobel;
  static float temp_2u;
  static IplImage        *sobel_x;
  static IplImage        *sobel_y;
  static IplImage        *sobel_z;
  static int dx;
  static int dy;
  static short temp_x, temp_y;
  int temp_x_sq, temp_y_sq;
  int temp_z;
  float Image_Quality;
  int i,j;
  sobel_x=cvCreateImage( cvSize( input_image->width, input_image->height ), IPL_DEPTH_16S, 1 );
  sobel_y=cvCreateImage( cvSize( input_image->width, input_image->height ), IPL_DEPTH_16S, 1 );
  sobel_z=cvCreateImage( cvSize( input_image->width, input_image->height ), IPL_DEPTH_32F, 1 );
  
  dx=1;
  dy=0;
  
  cvSobel(input_image,sobel_x,dx,dy,apertureSize);
  
  dx=0;
  dy=1;
  
  cvSobel(input_image,sobel_y,dx,dy,apertureSize);
  
  //combine sobel_x and sobel_y
  for (i=0; i< sobel_x->height; i++)
    for (j=0; j < sobel_x->width; j++)
      {
	temp_x=((short *)(sobel_x->imageData + i*sobel_x->widthStep))[j];
	temp_y=((short *)(sobel_y->imageData + i*sobel_y->widthStep))[j];
	temp_x_sq=((int)temp_x * (int)temp_x);
	temp_y_sq=((int)temp_y * (int)temp_y);
	temp_z=pow((float)temp_x_sq+(float)temp_y_sq,(float).5);
	//temp_z=pow((float)temp_x_sq,(float).5);
	((float *)(sobel_z->imageData + i*sobel_z->widthStep))[j]=temp_z;
      }
  
  //find sum and mean of edge pixels
  temp_sum=0;
  temp_sobel=0;
  
  for (i=0; i< sobel_z->height; i++)
    for (j=0; j < sobel_z->width; j++)
      {
	temp_sobel=((float *)(sobel_z->imageData + i*sobel_z->widthStep))[j];
	temp_sum+=temp_sobel;
      }
  
  temp_mean=(temp_sum)/(sobel_z->width*sobel_z->height);
  
  //find sum of pixels above 2U of temp_mean
  temp_mean*=Threshold;
  temp_2u=0;
  for (i=0; i< sobel_z->height; i++)
    for (j=0; j < sobel_z->width; j++)
      {
	temp_sobel=((float *)(sobel_z->imageData + i*sobel_z->widthStep))[j];
	if (temp_sobel > temp_mean)
	  temp_2u+=temp_sobel;
      }
  
  Image_Quality=(float)(temp_2u/temp_mean);
  
  
  
  cvReleaseImage (&sobel_x);
  cvReleaseImage (&sobel_y);
  cvReleaseImage (&sobel_z);
  
  
  return (Image_Quality);
  
}

IplImage* BORDER_REPLICATION(IplImage *img_in, int NEW_IMAGE_SIZE)
{
  
  IplImage        *img1;
  int             width, height, step;
  int             new_width, new_height, new_step;
  
  unsigned char* x_top; 
  unsigned char* x_bottom;
  unsigned char* x_left; 
  unsigned char* x_right;
  
  int vertical_edge_size, horizontal_edge_size;
  int vertical_start_bottom, horizontal_start_right;
  int i,j;
  
  img1 = cvCreateImage( cvSize(NEW_IMAGE_SIZE, NEW_IMAGE_SIZE), IPL_DEPTH_8U, 1 );
  /* get input image properties */
  width  	  = img_in->width;
  height 	  = img_in->height;
  step	  = img_in->widthStep;
  
  
  
  /* get new image properties */
  new_width  	  = img1->width;
  new_height 	  = img1->height;
  new_step	  = img1->widthStep;
  
  vertical_edge_size=(NEW_IMAGE_SIZE - height)/2;
  vertical_start_bottom=(vertical_edge_size + height);
  
  horizontal_edge_size=(NEW_IMAGE_SIZE - width)/2;
  horizontal_start_right = horizontal_edge_size + width;
  
  //get border pixels
  x_top=(unsigned char *)malloc(sizeof(unsigned char)* width);
  if (!x_top) {printf("MALLOC Failed x_top\r\n");}
  x_bottom=(unsigned char *)malloc(sizeof(unsigned char)* width);
  if (!x_bottom) {printf("MALLOC Failed x_bottom\r\n");}
  x_left=(unsigned char *)malloc(sizeof(unsigned char)* height);
  if (!x_left) {printf("MALLOC Failed x_left\r\n");}
  x_right=(unsigned char *)malloc(sizeof(unsigned char)* height);
  if (!x_right) {printf("MALLOC Failed x_right\r\n");}
  
  //get edge pixels
  for (i=0; i < width; i++)
    {
      x_top[i]=(unsigned char)(img_in->imageData)[i];
      x_bottom[i]=(unsigned char)(img_in->imageData)[i + ((height-1) * width)];
      //printf("x_bottom[%d]: %d\r\n",i,x_bottom[i]);
    }
  
  for (i=0; i < height; i++)
    {
      x_left[i]=(unsigned char)(img_in->imageData)[i * width];	
      x_right[i]=(unsigned char)(img_in->imageData + (i * width))[(width-1)];	
      //printf("x_right[%d]: %d\r\n",i,x_right[i]);
    }
  
  //insert image into new image
  for (i=0; i < new_height; i++)
    for (j=0; j< new_width; j++)
      {
	
	//top border
	if (i < vertical_edge_size && (j >= horizontal_edge_size && j < horizontal_start_right))
	  (img1 ->imageData)[i * new_step + j]=(unsigned char) x_top[j-horizontal_edge_size];
	
	//bottom border
	else if (i >= vertical_start_bottom && (j >= horizontal_edge_size && j < horizontal_start_right))
	  (img1 ->imageData)[i * new_step + j]=(unsigned char) x_bottom[j-horizontal_edge_size];
	
	//left border
	else if (j <= horizontal_edge_size && (i >= vertical_edge_size && i < vertical_start_bottom))
	  (img1 ->imageData)[i * new_step + j]=(unsigned char) x_left[i-vertical_edge_size];
	
	//right border
	else if (j >= horizontal_start_right && (i >= vertical_edge_size && i < vertical_start_bottom))
	  (img1 ->imageData)[i * new_step + j]=(unsigned char) x_right[i-vertical_edge_size];
	
	else if (i >= vertical_edge_size && i < vertical_start_bottom && j >= horizontal_edge_size && j < horizontal_start_right)
	  (img1 ->imageData)[i * new_step + j]=(unsigned char)(img_in->imageData)[((i - vertical_edge_size) * step) + (j-horizontal_edge_size)];	
	
	else
	  (img1 ->imageData)[i * new_step + j]=0;
	
      }
  
  free(x_top);
  free(x_bottom);
  free(x_left);
  free(x_right);
  
  return img1;
}

/**
 *Raise cosine window that reduces border discontunities
 * @param IplImage* img1 The input image
 * @param int width The width of the image.
 * @param int height The height of the image.
 */
void Hanning_Window(IplImage *img1, int width, int height)
{
  int i,j;
  float temp1_hanning, temp2_hanning;
  float temp_cos, temp_cos1, temp_cos2;
  float *hanning_window_array, *hanning_window_array2;
  
  unsigned char temp_hanning;
  hanning_window_array = (float *)malloc(sizeof(float)* width);
  if (!hanning_window_array) {printf("MALLOC Failed hanning_window_array\r\n");}
  hanning_window_array2 =(float *)malloc(sizeof(float)* height);
  if (!hanning_window_array2) {printf("MALLOC Failed hanning_window_array2\r\n");}
  //vertical hanning window
  for( j = 0 ; j < width; j++ ) { 
    temp_cos=cos((2*PI*j)/width);
    temp_cos1=1-temp_cos;
    temp_cos2=.5*temp_cos1;
    hanning_window_array[j]=temp_cos2;
  }
  
  //horizontal hanning window
  for( j = 0 ; j < height; j++ ) { 
    temp_cos=cos((2*PI*j)/height);
    temp_cos1=1-temp_cos;
    temp_cos2=.5*temp_cos1;
    hanning_window_array2[j]=temp_cos2; 
  }
  
  
  //multiply windows to image//
  for( i = 0; i < height ; i++ ) {
    for( j = 0 ; j < width ; j++ ) {
      temp_hanning =((uchar *)(img1->imageData + i*img1->widthStep))[j];
      temp1_hanning=hanning_window_array[j];
      temp2_hanning=temp_hanning*temp1_hanning;
      ((uchar *)(img1->imageData + i*img1->widthStep))[j]= temp2_hanning;
      
      temp_hanning =((uchar *)(img1->imageData + i*img1->widthStep))[j];
      temp1_hanning=hanning_window_array2[i];
      temp2_hanning=temp_hanning*temp1_hanning;
      ((uchar *)(img1->imageData + i*img1->widthStep))[j]= temp2_hanning;
    }
  }
  
  //multiply windows to image//
  for( i = 0; i < height ; i++ ) {
    for( j = 0 ; j < width ; j++ ) {
      temp_hanning =((uchar *)(img1->imageData + i*img1->widthStep))[j];
      temp1_hanning=hanning_window_array[j];
      temp2_hanning=temp_hanning*temp1_hanning;
      ((uchar *)(img1->imageData + i*img1->widthStep))[j]= temp2_hanning;
      
      temp_hanning =((uchar *)(img1->imageData + i*img1->widthStep))[j];
      temp1_hanning=hanning_window_array2[i];
      temp2_hanning=temp_hanning*temp1_hanning;
      ((uchar *)(img1->imageData + i*img1->widthStep))[j]= temp2_hanning;
    }
  }
  
  free(hanning_window_array);
  free(hanning_window_array2);
}


void Edge_Taper(IplImage *img1, int width, int height)
{
  

  float alpha=0.18;
  int i,j;
  float temp1_hanning, temp2_hanning;
  float temp1, temp2, temp3, temp4, temp5, temp6, temp7;
  float Tl, Th;
  float factor;

  float *hanning_window_array, *hanning_window_array2;
  IplImage *smoothed_img;
  unsigned char temp_hanning, temp_smooth;
  
  smoothed_img=cvCreateImage(cvSize(img1->width,img1->height),IPL_DEPTH_8U,1);
  cvSmooth(img1,smoothed_img,CV_GAUSSIAN,7,0,10);
 
  hanning_window_array = (float *)malloc(sizeof(float)* width);
  if (!hanning_window_array) {printf("MALLOC Failed hanning_window_array\r\n");}
  hanning_window_array2 =(float *)malloc(sizeof(float)* height);
  if (!hanning_window_array2) {printf("MALLOC Failed hanning_window_array2\r\n");}
 
 //vertical Tukey window
  factor=(1/(float)width);
  Tl=floor((alpha*(float)width)/2);
 Th=floor(width-Tl);
 
 for( j = 0 ; j < width; j++ ) { 
   
   if (j <= Tl)
     {
       temp1=(factor*j)-factor;
       temp2=temp1-(alpha/2);
       temp3=temp2/(alpha/2);
       temp4=PI*temp3;
       temp5=cos(temp4);
       temp6=1+temp5;
       temp7=temp6/2;
       hanning_window_array[j]=temp7;
     }
   else if (j > Tl && j < Th)
     {
       hanning_window_array[j]=1;
     }
   else
     {
       temp1=(factor*j)-factor;
       temp2=temp1+(alpha/2);
       temp3=temp2/(alpha/2);
       temp4=PI*temp3;
       temp5=cos(temp4);
       temp6=1+temp5;
       temp7=temp6/2;
       hanning_window_array[j]=temp7;
     }
   
 }
  
  //horizontal Tukey  window
 factor=(1/(float)height);
 Tl=floor((alpha*(float)height)/2);
 Th=floor(height-Tl);
  
for( j = 0 ; j < height; j++ ) { 
   
if (j <= Tl)
     {
       temp1=(factor*j)-factor;
       temp2=temp1-(alpha/2);
       temp3=temp2/(alpha/2);
       temp4=PI*temp3;
       temp5=cos(temp4);
       temp6=1+temp5;
       temp7=temp6/2;
       hanning_window_array2[j]=temp7;
     }
   else if (j > Tl && j < Th)
     {
       hanning_window_array2[j]=1;
     }
   else
     {
       temp1=(factor*j)-factor;
       temp2=temp1+(alpha/2);
       temp3=temp2/(alpha/2);
       temp4=PI*temp3;
       temp5=cos(temp4);
       temp6=1+temp5;
       temp7=temp6/2;
       hanning_window_array2[j]=temp7;
     }
  }
  
  
  //multiply windows to image//
  for( i = 0; i < height ; i++ ) {
    for( j = 0 ; j < width ; j++ ) {
      temp_hanning =((uchar *)(img1->imageData + i*img1->widthStep))[j];
      temp_smooth =((uchar *)(smoothed_img->imageData + i*smoothed_img->widthStep))[j];
      temp1_hanning=hanning_window_array[j]*hanning_window_array2[i];
      temp2_hanning=(temp_hanning*temp1_hanning)+(temp_smooth*(1-temp1_hanning));
      ((uchar *)(img1->imageData + i*img1->widthStep))[j]= temp2_hanning;
     
    }
  }
  
  
  free(hanning_window_array);
  free(hanning_window_array2);
  cvReleaseImage(&smoothed_img);
}



/**
 *Swaps the first and third quadriants of the image, and the second and fourth
 * @param IplImage* img2 The input image.
 */
void IFFT_Shift_Char(IplImage *img2)
{
  //!!!!image rows and columns have to be even!!!!! 
  unsigned char           *ptr1;
  int             rows_half, columns_half;
  char temp1_c,temp2_c;
  int i,j;
  
  rows_half=img2->height/2;
  columns_half=img2->width/2;
  ptr1=(unsigned char *)img2->imageData;
  
  for (i=0; i < img2->height/2; i++)
    {
      for (j=0; j < img2->width/2; j++)
        {
	  temp1_c=ptr1[i*img2->widthStep+j];
	  temp2_c=ptr1[i*img2->widthStep+(j+columns_half)];
	  //1=3
	  ptr1[i*img2->widthStep+j]=ptr1[(i+rows_half)*img2->widthStep+(j+columns_half)];
	  //2=4
	  ptr1[i*img2->widthStep+(j+columns_half)]=ptr1[(i+rows_half)*img2->widthStep+j];
	  //3=1
	  ptr1[(i+rows_half)*img2->widthStep+(j+columns_half)]=temp1_c;
	  //4=2
	  ptr1[(i+rows_half)*img2->widthStep+j]=temp2_c;
        }
    }
}

/**
 *Computes the complex spectrum of the image
 * @param fftw_complex* fft The fft of the image
 * @param int width The width of the image
 * @param int height The height of the image
 */
void Compute_Cepstrum (fftw_complex *fft, int width, int height)
{
  int i;
  //compute cepstrum
  for( i = 0 ; i < ( width * height ) ; i++ ) { 
    
    //cepstrum computation
    fft[i][0] = fft[i][0] * fft[i][0];
    fft[i][1] = fft[i][1] * fft[i][1];
    fft[i][0] = sqrt(fft[i][0] + fft[i][1]);
    fft[i][0] = log10(abs(1+fft[i][0]));
    fft[i][0] =fft[i][0] ;
  }
}

/**
 *Converts image to 0 or 255 value image.
 * @param IplImage* img2 The input image.
 */
void Threshold_Image (IplImage *img2)
{
  int i,j;
  
  //Threshold image to 0 or 255 values
  for (i=0; i < img2->height; i++)
    {
      for (j=0; j < img2->width; j++)
        {
	  if ((unsigned char *)(img2->imageData +i*img2->widthStep)[j] > 0)
	    (img2->imageData + i * img2->widthStep)[j] = 255;
	  else
	    (img2->imageData + i * img2->widthStep)[j] = 0;
        }
    }
}

/**
 *Preforms Hough transform to detect motion blur angle
 * @param float* h_array Contains scores of top n angles
 * @param IplImage* img2 The input image.
 * @param int widht The width of the image.
 * @param int height The height of the image.
 * @param int rl Hough transform size.
 */
void Hough_Transform(float* h_array, IplImage *img2, int width, int height, int rl)
{
  ////////////HOUGH TRANSFORM////////////////
  float theta;
  float temp0;
  float r;
  
  unsigned char* pixel_data;
  unsigned char pixel_value;
  int i,j;
  int temp1;
  pixel_data=(unsigned char*)img2->imageData;
  
  //initilize matrix
  for (i=0; i<rl; i++)
    for (j=0; j<360;j++)
      {
	h_array[i*360+j]=0;
      }
  
  //iterate through image
  for (i=0; i< height; i++)
    {
      for (j=0; j<width;j++)
        {
	  pixel_value=pixel_data[i*img2->widthStep+j];
	  if (pixel_value==255)
	    for (theta=0; theta < 360; theta++)
	      {
		r=cvRound((i*cos(theta*PI/180))+ (j*sin(theta*PI/180)));
		if (r >= 1)
		  {
		    temp0=h_array[(int)r*360+(int)theta];
		    temp1=(int)temp0;
		    temp1++;
		    h_array[(int)r*360+(int)theta]=(float)temp1;
		  }
	      }
        }
    }
}

/**
 *Rotate image
 * @param IplImage* src_image The input image.
 * @param double Phi Angle to rotate by
 * @param float SCALE_FACTOR Value to scale image by.
 * @param int width The width of the image.
 * @param int height The height of the image.
 * @param IplImage* The rotated image.
 */
IplImage* Image_Rotation(IplImage *src_image, double Phi, float SCALE_FACTOR, int width, int height)
{
  IplImage *dst_image;
  dst_image = cvCreateImage( cvSize( width, height ), IPL_DEPTH_32F, 1 );    
  
  CvPoint2D32f Center;
  Center.x = src_image->width*0.5f;
  Center.y = src_image->height*0.5f;
  CvMat *N = cvCreateMat(2,3, CV_64F);
  
  cv2DRotationMatrix(Center, Phi, SCALE_FACTOR, N); //Phi is the angle
  //cvWarpAffine(src_image, dst_image, N, CV_INTER_LINEAR+CV_WARP_FILL, cvScalarAll(0) );
  cvWarpAffine(src_image, dst_image, N );
  cvReleaseMat(&N);
    
  return dst_image;
}

/**
 *Swaps quandrants on float images 1->3 2->4
 * @param IplImage* src_image The input image
 * @param int width The width of the image.
 * @param int height The height of the image.
 */
void IFFT_Shift_Float(IplImage *src_image, int width, int height)
{
  ////////////////IFFTSHIFT////////////////////////
  int             rows_half, columns_half;
  double temp1_d, temp2_d, temp3_d, temp4_d;
  int i,j;
  
  //!!!!image rows and columns have to be even!!!!! 
  
  //int rows_half, columns_half;
  rows_half=height/2;
  columns_half=width/2;
  
  for (i=0; i < src_image->height/2; i++)
    {
      for (j=0; j < src_image->width/2; j++)
        {
	  
	  temp1_d=((float *)(src_image->imageData + i*src_image->widthStep))[j];
	  temp2_d=((float *)(src_image->imageData + i*src_image->widthStep))[j+columns_half];
	  temp3_d=((float *)(src_image->imageData + (i+rows_half)*src_image->widthStep))[j+columns_half];
	  temp4_d=((float *)(src_image->imageData + (i+rows_half)*src_image->widthStep))[j];
          
	  
	  //1=3
	  ((float *)(src_image->imageData + i*src_image->widthStep))[j]=temp3_d;
	  //2=4
	  ((float *)(src_image->imageData + i*src_image->widthStep))[j+columns_half]=temp4_d;
	  //3=1
	  ((float *)(src_image->imageData + (i+rows_half)*src_image->widthStep))[j+columns_half]=temp1_d;
	  //4=2
	  ((float *)(src_image->imageData + (i+rows_half)*src_image->widthStep))[j]=temp2_d;
        }
    }
}
/**
 *Computes the motion blur point spread function(PSF)
 * @param CvMat* PSF array of 0's to be populated
 * @param double Phi Angle of motion blur
 * @param int Length Length of blur
 * @param int even_flag If kernel size is even set to 1 otherwise 0
 */

void Compute_PSF (CvMat* PSF, double Phi, int LENGTH, int even_flag)
{
  
  int middle_row;
  int i,j;
  float SCALE_FACTOR=1.00;
  float temp, sum;
  
  middle_row=(LENGTH/2);
  
  CvMat* PSF_temp = cvCreateMat(LENGTH,LENGTH,CV_32F);
  
  //initilize matrix
  for (i=0; i<LENGTH; i++) {
    for (j=0; j<LENGTH;j++)
      {
	CV_MAT_ELEM(*PSF_temp,float,i,j)=0;
	CV_MAT_ELEM(*PSF,float,i,j)=0;  
      }
  }
  //put 1's in middle row
  i=middle_row;
  for (j=0; j < LENGTH; j++)
    {
      if ((j==0 || j==LENGTH-1) && even_flag==1)
	CV_MAT_ELEM(*PSF_temp,float,i,j)=0.5;
      else
	CV_MAT_ELEM(*PSF_temp,float,i,j)=1; 
    }
  
  
  //rotate kernal to specified angle
  CvPoint2D32f Center;
  Center.x = middle_row;
  Center.y = middle_row;
  CvMat *N = cvCreateMat(2,3, CV_32F);
  cv2DRotationMatrix(Center, Phi, SCALE_FACTOR, N); //Phi is the angle
  cvWarpAffine(PSF_temp, PSF, N, CV_INTER_LINEAR+CV_WARP_FILL_OUTLIERS, cvScalarAll(0) );
  
  cvReleaseMat(&PSF_temp);
  cvReleaseMat(&N);
  
  sum=0;
  //find sum of the kernal
  for (i=0; i<PSF->rows; i++)
    for (j=0; j<PSF->cols;j++)
      {
	temp=CV_MAT_ELEM(*PSF,float,i,j);
	sum+=temp;
      }
  
  //normalize kernal
  for (i=0; i<PSF->rows; i++)
    for (j=0; j<PSF->cols;j++)
      {
	temp=CV_MAT_ELEM(*PSF,float,i,j);
	temp/=sum;
	CV_MAT_ELEM(*PSF,float,i,j)=temp;
      }
  
}

/**
 *Computes the optical transfer function(OTF) or the fft of the psf
 * @param float* OTF_array Array of 0's to be populated
 * @param float* temp_OTF_array Used for intermediate computation
 * @param float* PSF_array atmospheric point spread function
 * @param int kernal_size The size of the kernal
 * @param int kernal_middle Basis for x and y offsets
 * @param int height The height of the image
 * @param int width The widht of the image
 */

void  Compute_OTF_MOTION (float* OTF_array, float* temp_OTF_array, float* PSF_array, int kernal_size, int kernal_middle, int height, int width)
{
  int i,j;
  int shift_up;
  int shift_left;
  float temp;
  //initilize OTF Array
  for (i=0; i<height; i++)
    for (j=0; j<width;j++)
      {
	OTF_array[i*width+j]=0;
	temp_OTF_array[i*width+j]=0;
      }
  
  //insert PSF into OTF array
  for (i=0; i<kernal_size; i++)
    for (j=0; j<kernal_size;j++)
      {
	temp=PSF_array[i*kernal_size+j];
	OTF_array[i*width+j]=temp;	
      }
  
  //circularly shift the OTF so that the center of 
  //the PSF is at the (0,0) element of the array
  shift_up=kernal_middle;
  shift_left=shift_up;
  
  ///CIRCULAR SHIFT UP///////////////// 
  for (i=shift_up; i < height; i++)
    for (j=0; j < width; j++)
      {
	temp_OTF_array[((i-shift_up)*width)+j]=OTF_array[i*width+j];
      }
  
  
  for (i=(height)-(shift_up); i < height; i++)
    for (j=0; j < width; j++)
      {
	temp_OTF_array[i*width+j]=OTF_array[((i-(height-shift_up))*width)+j];
      }
  
  //CIRCULAR SHIFT LEFT
  for (i=shift_left; i < width; i++)
    for (j=0; j < height; j++)
      {
	OTF_array[(j*width)+(i-shift_left)]=temp_OTF_array[(j*width)+i];	  
      }
  
  for (i=width-shift_left; i < width; i++)
    for (j=0; j < height; j++)
      {
	OTF_array[((j*width)+i)]=temp_OTF_array[(j*width)+(i-(width-shift_left))];   
      }
  
}
/**
 * Deconvolves the image with estimated blur(OTF)
 * @param IplImage* img2 The original Blurred image
 * @param fftw_complex **fft The fft of the original blurred image.
 * @param float* OTF_array The blur OTF.
 * @param int height The height of the image.
 * @param int widht The width of the image.
 */

inline void Wiener_Filter_Image(IplImage *img2, fftw_complex *fft, float* OTF_array, fftw_complex *OTF_Laplacian_fft, int height, int width, float gamma)
{
  int i,j,k;
  float temp;
  
  fftw_complex    *OTF_in;
  fftw_complex    *OTF_fft;
  fftw_complex    *temp_wiener;
  fftw_complex    *recovered;
  fftw_complex    *recovered_out;
  
  fftw_plan       plan_OTF;
  fftw_plan       plan_ifft_wiener;
  
  /*initialize arrays for fftw operations */
  recovered     = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width * height );
  if (!recovered) {printf("MALLOC Failed recovered\r\n");}
  recovered_out     = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width * height );
  if (!recovered_out) {printf("MALLOC Failed recovered_out\r\n");}
  OTF_in  = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width * height );
  if (!OTF_in) {printf("MALLOC Failed OTF_in\r\n");}
  OTF_fft = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width * height );
  if (!OTF_fft) {printf("MALLOC Failed OTF_fft\r\n");}
  temp_wiener = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width * height );
  if (!temp_wiener) {printf("MALLOC Failed temp_wiener\r\n");}
  
  /* create plans */
  plan_OTF = fftw_plan_dft_1d( width * height, OTF_in, OTF_fft,  FFTW_FORWARD,  FFTW_ESTIMATE );
  plan_ifft_wiener = fftw_plan_dft_1d( width * height, recovered, recovered_out, FFTW_BACKWARD, FFTW_ESTIMATE );
  
    /* load OTF data to fftw input */
  for( i = 0, k = 0 ; i < height ; i++ ) {
    for( j = 0 ; j < width ; j++ ) {
      temp=OTF_array[i*width+j];
      OTF_in[k][0] = ( double )temp;
      OTF_in[k][1] = 0.0;
      k++;
    }
  }
  
  /* perform FFT */
  fftw_execute(plan_OTF);
  
  //modify OTF to avoid divide by 0 error
  for (k=0; k < height*width; k++)
    {
      if(OTF_fft[k][0]==0)
	OTF_fft[k][0]=0.0000000000001;
    }
  
  for (k=0; k < height*width; k++)
    {
      temp_wiener[k][0]=OTF_fft[k][0]/((OTF_fft[k][0]*OTF_fft[k][0]) + (gamma*(OTF_Laplacian_fft[k][0]*2)));
    }
  
  //Convolve Image and Wiener Kernal in Frequency Domain
  for (k=0; k < height*width; k++)
    {
      recovered[k][0]=fft[k][0]*temp_wiener[k][0];
      recovered[k][1]=fft[k][1]*temp_wiener[k][0];
    }
  
  /* perform IFFT */
  fftw_execute( plan_ifft_wiener );
  
  /* normalize IFFT result */
  for( k = 0 ; k < ( width * height ) ; k++ ) {
    recovered_out[k][0] = ((recovered_out[k][0])/(width*height));
    if (recovered_out[k][0] > 255)
      recovered_out[k][0]=255;
    if (recovered_out[k][0] < 0)
      recovered_out[k][0]=0;
  }
  
  /* copy IFFT result to img_float's data */
  //added abs to get rid of errors in conversion
  for( i = 0, k = 0 ; i < height ; i++ ) {
    for( j = 0 ; j < width ; j++ ) {
      ((uchar *)(img2->imageData + i*img2->widthStep))[j]= ((uchar)recovered_out[k++][0]);
    }
  }
  fftw_destroy_plan( plan_OTF );
  fftw_destroy_plan( plan_ifft_wiener );
  
  fftw_free( recovered );
  fftw_free( recovered_out );
  fftw_free( OTF_in );
  fftw_free( OTF_fft );
  fftw_free( temp_wiener );       
}

/**
 * Computes Historgram Metrics of an image
 * @param float *metric_array Holds the results
 * @param IplImage* img1 The input image
 * @param int height The height of the image.
 * @param int width The width of the image.
 */




/**
 *Preforms motion deblur
 * @params IplImage* blurred_image The motion blurred image
 * @params IplImage** dst_images The 2 motion unblurred images, must be created before called
 */
void motion_deblur(IplImage * blurred_image, IplImage** dst_images)
{
  
  char first_crossing_title [100];
  
  float gamma;
  gamma=(float)GAMMA_VALUE;
  
  int even_flag;
  int NEW_IMAGE_SIZE;
  
  // int SD_array_ptr;
  
  IplImage        *img1 = 0;
  IplImage        *img2 = 0;
  IplImage        *img_float = 0;
  IplImage        *img_float_scale = 0;
  IplImage        *dst_image = 0;
  IplImage        *src_image = 0;
  
  
  fftw_complex    *data_in;
  fftw_complex    *fft; 
  fftw_complex    *ifft;
  fftw_complex    *Image_fft; 
  fftw_complex    *fft_temp=0;
  fftw_complex    *OTF_Laplacian_in=0;
  fftw_complex    *OTF_Laplacian_fft=0;
  fftw_plan       plan_f;
  fftw_plan       plan_b;
  fftw_plan       plan_OTF_Laplacian;
  
  double          Phi;
  
  int             width, height, step;
  int             width2, height2, step2;
  int             i, j, k;
  int             deblur_loops;
  int rows_sq, columns_sq;
  int rl;
  int loop_counter;
  int angle_estimate;
  int temp_h;
  int loops;
  float global_min;
  int global_min_position;	
  int image_number;
  int array_ptr;
  int length_flag;
  int first_crossing;
  int LENGTH;
  int first_crossing_found=0;
  
  float temp0, max_val;
  float* h_array;
  float  CEPSTRUM_SCALE=100;
  float  temp_pixel; 
  //    float  average_array[2048];
  float*  average_array;
  float  SCALE_FACTOR=1.00;
  float  temp;
  float* PSF_array;
  float* PSF_array_1;
  float* OTF_array;
  float* temp_OTF_array;
  
  float* PSF_Laplacian;
  
  
  IplImage *img_out = 0;
  int width1, height1, step1;
  fftw_complex *data_in1;
  fftw_complex *fft1;
  fftw_complex *Image_fft1;
  fftw_plan plan_f1;
  
  
  IplImage        *img_out0;
  IplImage        *img_out1;
  IplImage        *img_out2;
  IplImage        *img_out3;
  IplImage        *img_out4;
  IplImage        *img_out5;
  IplImage        *img_out6;
  IplImage        *img_out7;
  IplImage        *img_out8;
  IplImage        *img_out9;
  IplImage        *img_out10;
  IplImage        *img_out11;
  
  IplImage        *output_image;

  IplImage        *crop_img0;
  IplImage        *crop_img1;
  IplImage        *crop_img2;
  IplImage        *crop_img3;
  IplImage        *crop_img4;
  IplImage        *crop_img5;
  IplImage        *crop_img6;
  IplImage        *crop_img7;
  IplImage        *crop_img8;
  IplImage        *crop_img9;
  IplImage        *crop_img10;
  IplImage        *crop_img11;
  
  
  IplImage        *original_crop;
  
  float temp_score;
  int image_deblur_counter=0;
  int APERTURE=3;
  static float THRESHOLD=12;
  float CROP_PERCENTAGE=0.15;
  int gt_zero_counter;
  
  int size_y_crop=64;
  
  PSF_Laplacian=(float *)malloc(sizeof(float)*3*3);
  float* metric_original = (float*)malloc(sizeof(float)*1);
  

  /////////SNR STUFF////////
  IplImage *snr_image = 0;
  IplImage *img_float_snr = 0;
  
  fftw_complex *snr_fft_image;
  fftw_complex    *ifft_snr;	
  
  fftw_plan       plan_r_snr;  

  float max_acf_val;
  float nf;
  float factor1, factor2;
  static float SNR_ACF=0.003;
  static float previous_SNR_ACF=0.003;
  float temp_SNR;
  float ABS_temp_SNR;
  float SNR_negative_flag;
  float mean_acf;
  float temp_acf;
  float snr_temp;
  float abs_snr_temp;
  float temp_pixel_snr;
 
  //populate PSF_Laplacian
  PSF_Laplacian[0]= 0;
  PSF_Laplacian[1]= -1;
  PSF_Laplacian[2]= 0;
  PSF_Laplacian[3]= -1;
  PSF_Laplacian[4]= 4;
  PSF_Laplacian[5]=-1;
  PSF_Laplacian[6]= 0;
  PSF_Laplacian[7]=-1;
  PSF_Laplacian[8]= 0;
  
  float* OTF_Laplacian;
  float* temp_OTF_Laplacian;
  
  unsigned char count=0;
  unsigned char HOLDER[MAX_ESTIMATES][3]; 	
  unsigned char max_array [2][MAX_ESTIMATES];
  loop_counter=0;
  
  
  
  /* get image properties */
  width  	  = blurred_image->width;
  height 	  = blurred_image->height;
  step	          = blurred_image->widthStep;
  
  img_out=cvCreateImage(cvSize(width,height),IPL_DEPTH_8U,1);
  output_image=cvCreateImage(cvSize(width,height),IPL_DEPTH_8U,1);

  // cvNamedWindow( "BEFORE HANNING", CV_WINDOW_AUTOSIZE );
  //      cvShowImage( "BEFORE HANNING", blurred_image ); 	
  Edge_Taper(blurred_image,width,height);  
  //        cvNamedWindow( "HANNING", CV_WINDOW_AUTOSIZE );
  //       cvShowImage( "HANNING", blurred_image ); 
  // 	cvWaitKey(0);

  
  


 cvCopy(blurred_image,img_out);
   snr_image = cvCreateImage( cvSize( blurred_image->width, blurred_image->height ), IPL_DEPTH_8U, 1 );   
  
 
  

   original_crop=cvCreateImage(cvSize(img_out->width,int(64)),IPL_DEPTH_8U,1);
  CROP_REGION (original_crop, img_out, int (0), img_out->height*CROP_PERCENTAGE, img_out->width, 64);
  

  img_out0=cvCreateImage(cvSize(img_out->width,img_out->height),IPL_DEPTH_8U,1);
  img_out1=cvCreateImage(cvSize(img_out->width,img_out->height),IPL_DEPTH_8U,1);
  img_out2=cvCreateImage(cvSize(img_out->width,img_out->height),IPL_DEPTH_8U,1);
  img_out3=cvCreateImage(cvSize(img_out->width,img_out->height),IPL_DEPTH_8U,1);
  img_out4=cvCreateImage(cvSize(img_out->width,img_out->height),IPL_DEPTH_8U,1);
  img_out5=cvCreateImage(cvSize(img_out->width,img_out->height),IPL_DEPTH_8U,1);
  img_out6=cvCreateImage(cvSize(img_out->width,img_out->height),IPL_DEPTH_8U,1);
  img_out7=cvCreateImage(cvSize(img_out->width,img_out->height),IPL_DEPTH_8U,1);
  img_out8=cvCreateImage(cvSize(img_out->width,img_out->height),IPL_DEPTH_8U,1);
  img_out9=cvCreateImage(cvSize(img_out->width,img_out->height),IPL_DEPTH_8U,1);
  img_out10=cvCreateImage(cvSize(img_out->width,img_out->height),IPL_DEPTH_8U,1);
  img_out11=cvCreateImage(cvSize(img_out->width,img_out->height),IPL_DEPTH_8U,1);
  
  crop_img0=cvCreateImage(cvSize(img_out->width,int(64)),IPL_DEPTH_8U,1);
  crop_img1=cvCreateImage(cvSize(img_out->width,int(64)),IPL_DEPTH_8U,1);
  crop_img2=cvCreateImage(cvSize(img_out->width,int(64)),IPL_DEPTH_8U,1);
  crop_img3=cvCreateImage(cvSize(img_out->width,int(64)),IPL_DEPTH_8U,1);
  crop_img4=cvCreateImage(cvSize(img_out->width,int(64)),IPL_DEPTH_8U,1);
  crop_img5=cvCreateImage(cvSize(img_out->width,int(64)),IPL_DEPTH_8U,1);
  crop_img6=cvCreateImage(cvSize(img_out->width,int(64)),IPL_DEPTH_8U,1);
  crop_img7=cvCreateImage(cvSize(img_out->width,int(64)),IPL_DEPTH_8U,1);
  crop_img8=cvCreateImage(cvSize(img_out->width,int(64)),IPL_DEPTH_8U,1);
  crop_img9=cvCreateImage(cvSize(img_out->width,int(64)),IPL_DEPTH_8U,1);
  crop_img10=cvCreateImage(cvSize(img_out->width,int(64)),IPL_DEPTH_8U,1);
  crop_img11=cvCreateImage(cvSize(img_out->width,int(64)),IPL_DEPTH_8U,1);
  
  
    /* get image properties */
  width1  	  = img_out->width;
  height1 	  = img_out->height;
  step1	          = img_out->widthStep;
  
  if (width > height)
  NEW_IMAGE_SIZE=(height*1.3);
  else
   NEW_IMAGE_SIZE=(width*1.3);
img1 = cvCreateImage( cvSize( blurred_image->width, blurred_image->height ), IPL_DEPTH_8U, 1 );
  // cvCopy(blurred_image, img1);
  
  img1 = BORDER_REPLICATION(blurred_image,NEW_IMAGE_SIZE);
 
  
  //cvCopy(blurred_image,img1);
  // cvNamedWindow( "border replication", CV_WINDOW_AUTOSIZE );
  // cvShowImage( "border replication", img1 ); 	
  /* create new image for IFFT result */
  img2 = cvCreateImage( cvSize( img1->width, img1->height ), IPL_DEPTH_8U, 1 );
  img_float = cvCreateImage( cvSize( img1->width, img1->height ), IPL_DEPTH_32F, 1 );
  img_float_scale = cvCreateImage( cvSize( img1->width, img1->height ), IPL_DEPTH_32F, 1 );
 
  
  /* get image properties */
  width  	  = img1->width;
  height 	  = img1->height;
  step	          = img1->widthStep;
  
  rows_sq=height*height;
  columns_sq=width*width;
  temp=rows_sq + columns_sq;
  rl=ceil(sqrt((double)temp));
  
  h_array        =(float *)malloc(sizeof(float)*rl*360);
  if (!h_array) {printf("MALLOC Failed h_array\r\n");}
  
  OTF_array      =(float *)malloc(sizeof(float)*width1*height1);
  if (!OTF_array) {printf("MALLOC Failed OTF_array\r\n");}
  
  temp_OTF_array =(float *)malloc(sizeof(float)*width1*height1);
  if (!temp_OTF_array) {printf("MALLOC Failed temp_OTF_Laplacian\r\n");}
  
  OTF_Laplacian  =(float *)malloc(sizeof(float)*width1*height1);
  if (!OTF_Laplacian) {printf("MALLOC Failed OTF_Laplacian\r\n");}
  
  temp_OTF_Laplacian =(float *)malloc(sizeof(float)*width1*height1);
  if (!temp_OTF_Laplacian) {printf("MALLOC Failed temp_OTF_Laplacian\r\n");}
  
  
  Image_fft     = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * img1->width * img1->height );
  if (!Image_fft) {printf("MALLOC Failed Image_fft\r\n");}
  
  Image_fft1     = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * img_out->width * img_out->height );
  if (!Image_fft1) {printf("MALLOC Failed Image_fft1\r\n");}
  
  
  data_in = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width * height );
  if (!data_in) {printf("MALLOC Failed data_in\r\n");}
  
  data_in1 = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width1 * height1 );
  if (!data_in1) {printf("MALLOC Failed data_in1\r\n");}
  
  snr_fft_image   =  ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width1 * height1 );
  ifft_snr   =  ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width1 * height1 );
  plan_r_snr = fftw_plan_dft_1d( width1*height1, snr_fft_image, ifft_snr,  FFTW_BACKWARD,  FFTW_ESTIMATE );

  fft     = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width * height );
  if (!fft) {printf("MALLOC Failed fft\r\n");}
  
  fft1     = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width1 * height1 );
  if (!fft1) {printf("MALLOC Failed fft1\r\n");}
  
  ifft    = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width * height );
  if (!ifft) {printf("MALLOC Failed ifft\r\n");}
  OTF_Laplacian_in = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width1 * height1 );
  if (!OTF_Laplacian_in) {printf("MALLOC Failed OTF_Laplacian_in\r\n");}
  OTF_Laplacian_fft = ( fftw_complex* )fftw_malloc( sizeof( fftw_complex ) * width1 * height1 );
  if (!OTF_Laplacian_fft) {printf("MALLOC Failed OTF_Laplacian_fft\r\n");}
  
  /* create plans */
  plan_b = fftw_plan_dft_1d( width * height, fft,ifft, FFTW_BACKWARD, FFTW_ESTIMATE );
  plan_f = fftw_plan_dft_1d( width * height, data_in, fft,  FFTW_FORWARD,  FFTW_ESTIMATE );
  plan_f1 = fftw_plan_dft_1d( width1 * height1, data_in1, fft1,  FFTW_FORWARD,  FFTW_ESTIMATE );
  
  
  plan_OTF_Laplacian = fftw_plan_dft_1d( width1 * height1, OTF_Laplacian_in, OTF_Laplacian_fft,  FFTW_FORWARD,  FFTW_ESTIMATE );
  
  
  Compute_OTF_MOTION(OTF_Laplacian,temp_OTF_Laplacian,PSF_Laplacian,3,3/2,height1,width1);
  
  
  for( i = 0, k = 0 ; i < height1 ; i++ ) {
    for( j = 0 ; j < width1 ; j++ ) {
      temp=OTF_Laplacian[i*width1+j];
      OTF_Laplacian_in[k][0] = ( double )temp;
      OTF_Laplacian_in[k][1] = 0.0;
      k++;
    }
  }
  
  
  fftw_execute(plan_OTF_Laplacian);
  
  //modify OTF to avoid divide by 0 error
  
  for (k=0; k < height1*width1; k++)
    {
      if(OTF_Laplacian_fft[k][0]==0)
	OTF_Laplacian_fft[k][0]=0.0000000000001;
    }
  //     cvNamedWindow( "BEFORE HANNING", CV_WINDOW_AUTOSIZE );
  //       cvShowImage( "BEFORE HANNING", img1 ); 	
  /* preprocess the image with a hanning window */
	Hanning_Window(img1,width,height);
   
    //       cvNamedWindow( "HANNING", CV_WINDOW_AUTOSIZE );
    //      cvShowImage( "HANNING", img1 ); 
    //	cvWaitKey(0);
  	cvDestroyWindow("BEFORE HANNING");
  	cvDestroyWindow("HANNING");	
  
  /* FFT Input Image */
  /* load img1's data to fftw input */
  for( i = 0, k = 0 ; i < height ; i++ ) {
    for( j = 0 ; j < width ; j++ ) {
      data_in[k][0] = ( double )((uchar*)(img1->imageData + i * step))[j];
      data_in[k][1] = 0.0;
      k++;
    }
  }
  
  /* perform FFT */
  fftw_execute( plan_f );
  
  for( i = 0 ; i < ( width * height ) ; i++ ) { 
    //image holder for wiener filter
    Image_fft[i][0]=fft[i][0];
    Image_fft[i][1]=fft[i][1];
  }
  


  /* FFT Input Image */
  /* load img1's data to fftw input */
  for( i = 0, k = 0 ; i < height1 ; i++ ) {
    for( j = 0 ; j < width1 ; j++ ) {
      data_in1[k][0] = ( double )((uchar*)(img_out->imageData + i * step1))[j];
      data_in1[k][1] = 0.0;
      k++;
    }
  }
  
  /* perform FFT */
  fftw_execute( plan_f1 );
  
  for( i = 0 ; i < ( width1 * height1 ) ; i++ ) { 
    //image holder for wiener filter
    Image_fft1[i][0]=fft1[i][0];
    Image_fft1[i][1]=fft1[i][1];
  }
  
  
for( k = 0 ; k < height1 * width1 ; k++ ) {
    snr_fft_image[k][0]=((fft1[k][0]*fft1[k][0])-(fft1[k][1]*(-1*fft1[k][1])));
    snr_fft_image[k][1]=((fft1[k][0]* (-1*fft1[k][1]))+(fft1[k][1]* fft1[k][0]));
  }

 
/* perfrom ifft */
  fftw_execute( plan_r_snr );
  
  
  img_float_snr = cvCreateImage( cvSize( blurred_image->width, blurred_image->height ), IPL_DEPTH_32F, 1 );
  
  /* copy IFFT result to 2D float data */
  for( i = 0, k = 0 ; i < height1 ; i++ ) {
    for( j = 0 ; j < width1 ; j++ ) {
      ((float *)(img_float_snr->imageData + i*img_float_snr->widthStep))[j]=((float)ifft_snr[i*width1+j][0]/(width1*height1));
    }
  }


  float* average_array_snr = (float *)malloc(sizeof(float)*width1);
  
  //initilize average array
  for (i=0; i<width1; i++)
    average_array_snr[i]=0;
  
  //average columns (collapse 2D power spectrum)
  for (i=0; i < width1; i++)
    {
      for (j=0; j < height1; j++)
	{
	  temp_pixel_snr=((float *)(img_float_snr->imageData + j*img_float_snr->widthStep))[i];
	  average_array_snr[i]+=temp_pixel_snr;
	}     
      average_array_snr[i]/=height;  
    }	
 
  float max_value=0;
  int max_ind=0;
  mean_acf=0;
  float temp_snr;
  for (i=0; i < width1; i++){
    temp_snr=average_array_snr[i];
    mean_acf+=temp_snr;
    if (temp_snr > max_value)
      {
      max_value=temp_snr;
      max_ind=i;
      }
  }
  
 



   mean_acf/=width1;
   //max_value=average_array[0];
   //	float nf;
   nf=average_array_snr[max_ind+1];
   
   //	float factor1, factor2;
   
   factor1=nf-mean_acf;
   factor2=max_value-nf;
   //float SNR;
   
   
   
   // fprintf(stderr,"max_acf_val: %f mean_acf: %f nf: %f factor1: %f factor2: %f\r\n",max_value,mean_acf,nf,factor1,factor2);
  
   //some feedback to prevent wild SNR value swings
  
   
       previous_SNR_ACF=SNR_ACF;
       SNR_ACF=factor2/factor1;
       fprintf(stderr,"RAW SNR: %f\r\n",SNR_ACF);
       // if (SNR_ACF >= 0.001)       
	 //	 SNR_ACF*=0.1;
       //SNR_ACF*=0.001;
       snr_temp=previous_SNR_ACF-SNR_ACF;
         fprintf(stderr,"SNR_ACF: %f previous: %F\r\n",SNR_ACF,previous_SNR_ACF);
       if (snr_temp < 0)
	 abs_snr_temp=snr_temp*-1;
       else
	 abs_snr_temp=snr_temp;
       
       if (abs_snr_temp > 0.0001)
	 {
	   if (snr_temp < 0)
	     SNR_ACF=previous_SNR_ACF+0.0001;
	   else
	    SNR_ACF=previous_SNR_ACF-0.0001;
	   
	 }
          fprintf(stderr,"snr_temp: %f abs_temp: %f new_SNR_ACF: %f\r\n",snr_temp,abs_snr_temp,SNR_ACF);
      
      if (SNR_ACF < 0.0001)
	SNR_ACF=0.0001;
    

      fprintf(stderr,"SNR: %f \r\n",SNR_ACF);



      gamma=SNR_ACF;




  /* Compute Cepstrum */
  Compute_Cepstrum(fft,width,height);
  
  /* perform IFFT */
  fftw_execute( plan_b );
  
  /* normalize IFFT result and copy IFFT result to img_float's data */
  for( i = 0, k = 0 ; i < height ; i++ ) {
    for( j = 0 ; j < width ; j++ ) {
      /* normalize IFFT result */
      ifft[k][0] = (ifft[k][0])/(width*height);
      
      ((float *)(img_float->imageData + i*img_float->widthStep))[j]=( float )ifft[k++][0];
      //amplify spectrum for better angle estimation//
      ((float *)(img_float_scale->imageData + i*img_float_scale->widthStep))[j] = (((float *)(img_float->imageData + i*img_float->widthStep))[j])*CEPSTRUM_SCALE;
      // copy scale result to img2's data //
      ((uchar *)(img2->imageData + i*img2->widthStep))[j]= (uchar)((float *)(img_float_scale->imageData + i*img_float_scale->widthStep))[j];
    }
  }
  
  Threshold_Image(img2);
  IFFT_Shift_Char(img2);
  cvNamedWindow( "IFFT SHIFT", CV_WINDOW_AUTOSIZE );
  cvShowImage( "IFFT SHIFT", img2 ); 
  //cvWaitKey(0);
  // cvDestroyWindow("IFFT SHIFT");
  Hough_Transform(h_array,img2,width,height,rl);
  
  //initilize max_array
  for (i=0; i < 2; i++)
    {
      for (j=0; j<MAX_ESTIMATES; j++)
	{
	  max_array[i][j]=0;
	}
    }
  
  while (loop_counter !=MAX_ESTIMATES)
    {
      max_val=0;
      angle_estimate=0;
      //find maximum value in Hough Array (Angle Estimation)
      for (i=0; i<rl; i++)
	{
	  for (j=0; j<360;j++)
	    {
	      temp0=h_array[i*360+j];
	      if (temp0 >= max_val)
		{
		  max_val=temp0;	 
		  angle_estimate=j;
		}
	    }
	}
      max_array [0][loop_counter]=max_val;    
      
      if (angle_estimate > 180)
	max_array[1][loop_counter]=angle_estimate - 180;
      else
	max_array[1][loop_counter]=angle_estimate;
      
      //set bins in Hough array with angle estimate to 0
      for (i=0; i< rl; i++)
	{
	  if (angle_estimate > 180)
	    temp_h=angle_estimate-180;
	  else
	    temp_h=angle_estimate+180;
	  
	  h_array[i*360+angle_estimate]=(float)0;
	  h_array[i*360+temp_h]=(float)0;
	}
      
      //check conditions (Min_Estimates and Ties)
      if (loop_counter > MIN_ESTIMATES)
	if (max_array[0][loop_counter] != max_array[0][loop_counter-1])
	  break;
      
      loop_counter++;
    }//end while loop
  
  
  
     //count entries in max array
  count=0;
  
  //initilize holder array
  for (i=0; i<MAX_ESTIMATES; i++)
    for (j=0; j<3; j++)
      HOLDER[i][j]=0;
  
  for (i=0; i<MAX_ESTIMATES; i++)
    {
      if (max_array[1][i] > 0)
	count++;
    }
  printf("COUNT IS %d\n", count);
  //put angle data into HOLDER array
  for (i=0; i<count; i++)
    {
      HOLDER[i][0]= max_array[1][i];
    }
  
  
     ///////////////LENGTH ESTIMATION/////////////////////////////
  average_array = (float*)malloc(sizeof(float)*width);
  for (loops=0; loops < count; loops++)
    {
      
      Phi=(float)HOLDER[loops][0];
      
      if (Phi > 180)
	Phi=(360-(Phi-180));
      else
	Phi=360-Phi;
      
      
      src_image=cvCloneImage(img_float);
      IFFT_Shift_Float(src_image,width,height);
      dst_image=Image_Rotation(src_image,Phi,SCALE_FACTOR,width,height);
      IFFT_Shift_Float(dst_image,width,height);
      
      cvReleaseImage(&src_image);
      
      memset(average_array, 0, sizeof(float)*width);
      //average columns (collapse 2D power spectrum)
      for (i=0; i < width; i++)
	{
	  for (j=0; j < height; j++)
	    {
                 temp_pixel=((float *)(dst_image->imageData + j*dst_image->widthStep))[i];
                 average_array[i]+=temp_pixel;
	    }     
	  average_array[i]/=height;  
	}	
      cvReleaseImage(&dst_image);
      
      //find first crossing and global min
      first_crossing=1;
      first_crossing_found=0;
      global_min=1000;
      
      for (i=0; i< MAX_BLUR_LENGTH; i++)
	{
	  // if (average_array[i] <= 0.0001 && first_crossing_found==0)
	     if (average_array[i] <= 0 && first_crossing_found==0)		
	    {
	      first_crossing=i;
	      first_crossing_found=1;
	    }
	  
	  if (average_array[i] < global_min)
	    
	    {
	      global_min=average_array[i];
	      global_min_position=i;
	    }	
	}
      
      //put global min and first crossing into holder array
      HOLDER[loops][1]=first_crossing;
      HOLDER[loops][2]=global_min_position;
    }//end loops
  
  
    
     /////////////////////////////////DEBLUR IMAGES////////////////////////////////////
  for (loops=0; loops < count; loops++)
    {
      printf("Angle: %d first_crossing: %d Global Min: %d \r\n",HOLDER[loops][0],HOLDER[loops][1],HOLDER[loops][2]);
    }
  
  array_ptr=0;
  
  length_flag=0;
  image_number=2;
  float* mean_array=(float *)malloc(sizeof(float) * count * 2);
  float* skewness_first_crossing =(float *)malloc(sizeof(float)* count * 2);
  float* theta_first_crossing = (float *)malloc(sizeof(float)* count * 2);
  float* length_first_crossing = (float *)malloc(sizeof(float)* count * 2);           
  int*   background_sub_flag = (int *)malloc(sizeof(int) * count * 2);
  
  float LENGTH_TEMP;
  
  int metric_counter=0;
  image_deblur_counter=0;
  
  //first crossing deblurring
  
  for (deblur_loops=0; deblur_loops < count; deblur_loops++)
    {
      
      Phi=HOLDER[deblur_loops][0];
      LENGTH=HOLDER[deblur_loops][1];
      LENGTH_TEMP=LENGTH;
      length_flag=1;
      
      
      //modify even lengths and set flag before PSF creation
      even_flag=0;
      
      if ((int)LENGTH%2==0)
	{
	  LENGTH=LENGTH+1;
	  even_flag=1;
	}
      
      CvMat* PSF = cvCreateMat(LENGTH,LENGTH,CV_32F);
      Compute_PSF(PSF,Phi,LENGTH,even_flag);
      PSF_array=(float *)malloc(sizeof(float)*LENGTH*LENGTH);
      //populate PSF_array
      for (i=0; i<PSF->rows; i++)
	for (j=0; j<PSF->cols;j++)
	  {
	    PSF_array[i*(int)LENGTH+j]=CV_MAT_ELEM(*PSF,float,i,j);
	  }
      cvReleaseMat(&PSF);
      
      //create OTF matrix
      Compute_OTF_MOTION(OTF_array,temp_OTF_array,PSF_array,LENGTH,(LENGTH/2),height1,width1);
      
 
      switch (image_deblur_counter){
      case 0: Wiener_Filter_Image(img_out0,Image_fft1,OTF_array,OTF_Laplacian_fft,height1,width1,gamma);
	CROP_REGION (crop_img0, img_out0, int (0), img_out->height*CROP_PERCENTAGE, img_out->width, 64);
	background_sub_flag[image_deblur_counter] =   BACKGROUND_SUB_CROP(original_crop,crop_img0);
	break;
	
      case 2: Wiener_Filter_Image(img_out2,Image_fft1,OTF_array,OTF_Laplacian_fft,height1,width1,gamma);
	CROP_REGION (crop_img2, img_out2, int (0), img_out->height*CROP_PERCENTAGE, img_out->width, 64);
	background_sub_flag[image_deblur_counter] =   BACKGROUND_SUB_CROP(original_crop,crop_img2);
	break;
	
	
      case 4: Wiener_Filter_Image(img_out4,Image_fft1,OTF_array,OTF_Laplacian_fft,height1,width1,gamma);
	CROP_REGION (crop_img4, img_out4, int (0), img_out->height*CROP_PERCENTAGE, img_out->width, 64);
	background_sub_flag[image_deblur_counter] =   BACKGROUND_SUB_CROP(original_crop,crop_img4);
	break;
	
      case 6: Wiener_Filter_Image(img_out6,Image_fft1,OTF_array,OTF_Laplacian_fft,height1,width1,gamma);
	CROP_REGION (crop_img6, img_out6, int (0), img_out->height*CROP_PERCENTAGE, img_out->width, 64);
	background_sub_flag[image_deblur_counter] =   BACKGROUND_SUB_CROP(original_crop,crop_img6);
	break;
	
      case 8: Wiener_Filter_Image(img_out8,Image_fft1,OTF_array,OTF_Laplacian_fft,height1,width1,gamma);
	CROP_REGION (crop_img8, img_out8, int (0), img_out->height*CROP_PERCENTAGE, img_out->width, 64);
	background_sub_flag[image_deblur_counter] =   BACKGROUND_SUB_CROP(original_crop,crop_img8);
	break;
	
      case 10: Wiener_Filter_Image(img_out10,Image_fft1,OTF_array,OTF_Laplacian_fft,height1,width1,gamma);
	CROP_REGION (crop_img10, img_out10, int (0), img_out->height*CROP_PERCENTAGE, img_out->width, 64);
	background_sub_flag[image_deblur_counter] =   BACKGROUND_SUB_CROP(original_crop,crop_img10);
	break;
	
      }//end switch
      
      
      
      
      image_deblur_counter++;
        
       
      free(PSF_array);
      
      
      //////////deblur and find best global min image/////////////////////////////////////
        
      Phi=HOLDER[deblur_loops][0];
      LENGTH=HOLDER[deblur_loops][2];
      LENGTH_TEMP=LENGTH;
      length_flag=1;
      
      //modify even lengths and set flag before PSF creation
      even_flag=0;
      
      if ((int)LENGTH%2==0)
	{
	  LENGTH=LENGTH+1;
	  even_flag=1;
	}
      
      PSF = cvCreateMat(LENGTH,LENGTH,CV_32F);
      Compute_PSF(PSF,Phi,LENGTH,even_flag);
      PSF_array=(float *)malloc(sizeof(float)*LENGTH*LENGTH);
      //populate PSF_array
      for (i=0; i<PSF->rows; i++)
	for (j=0; j<PSF->cols;j++)
	  {
	    PSF_array[i*(int)LENGTH+j]=CV_MAT_ELEM(*PSF,float,i,j);
	  }
      cvReleaseMat(&PSF);
      
      //create OTF matrix
      Compute_OTF_MOTION(OTF_array,temp_OTF_array,PSF_array,LENGTH,(LENGTH/2),height1,width1);
        
        
      switch (image_deblur_counter){
	
      case 1: Wiener_Filter_Image(img_out1,Image_fft1,OTF_array,OTF_Laplacian_fft,height1,width1,gamma);
	CROP_REGION (crop_img1, img_out1, int (0), img_out->height*CROP_PERCENTAGE, img_out->width, 64);
	background_sub_flag[image_deblur_counter] =   BACKGROUND_SUB_CROP(original_crop,crop_img1);
	break;
	
      case 3: Wiener_Filter_Image(img_out3,Image_fft1,OTF_array,OTF_Laplacian_fft,height1,width1,gamma);
	CROP_REGION (crop_img3, img_out3, int (0), img_out->height*CROP_PERCENTAGE, img_out->width, 64);
	background_sub_flag[image_deblur_counter] =   BACKGROUND_SUB_CROP(original_crop,crop_img3);
	break;
	
	
      case 5: Wiener_Filter_Image(img_out5,Image_fft1,OTF_array,OTF_Laplacian_fft,height1,width1,gamma);
	CROP_REGION (crop_img5, img_out5, int (0), img_out->height*CROP_PERCENTAGE, img_out->width, 64);
	background_sub_flag[image_deblur_counter] =   BACKGROUND_SUB_CROP(original_crop,crop_img5);
	break;
	
      case 7: Wiener_Filter_Image(img_out7,Image_fft1,OTF_array,OTF_Laplacian_fft,height1,width1,gamma);
	CROP_REGION (crop_img7, img_out7, int (0), img_out->height*CROP_PERCENTAGE, img_out->width, 64);
	background_sub_flag[image_deblur_counter] =   BACKGROUND_SUB_CROP(original_crop,crop_img7);
	break;
	
      case 9: Wiener_Filter_Image(img_out9,Image_fft1,OTF_array,OTF_Laplacian_fft,height1,width1,gamma);
	CROP_REGION (crop_img9, img_out9, int (0), img_out->height*CROP_PERCENTAGE, img_out->width, 64);
	background_sub_flag[image_deblur_counter] =   BACKGROUND_SUB_CROP(original_crop,crop_img9);
	break;
	
      case 11: Wiener_Filter_Image(img_out11,Image_fft1,OTF_array,OTF_Laplacian_fft,height1,width1,gamma);
	CROP_REGION (crop_img11, img_out11, int (0), img_out->height*CROP_PERCENTAGE, img_out->width, 64);
	background_sub_flag[image_deblur_counter] =   BACKGROUND_SUB_CROP(original_crop,crop_img11);
	break;
	
      }//end switch
      
      
      image_deblur_counter++;
      free(PSF_array);
    }//end deblur loops
  
  
  
  
  //THRESHOLD =12.0;
  float all_zero=1;
  
  
  while (all_zero==1)
    {
      THRESHOLD-=0.5;
      metric_counter=0;
      
      
      for (i=0; i<count; i++)
	{
	  
	  Phi=HOLDER[i][0];
	  LENGTH=HOLDER[i][1];
	  LENGTH_TEMP=LENGTH;
	  switch (metric_counter){
	  case 0: 
	    skewness_first_crossing[metric_counter]  = IMAGE_QUALITY_MOTION(crop_img0,APERTURE,(float)THRESHOLD);
	    theta_first_crossing[metric_counter]     = Phi;
	    length_first_crossing[metric_counter]    = LENGTH_TEMP;
	    metric_counter++;
	    break;
	    
	  case 2:
	    skewness_first_crossing[metric_counter]  = IMAGE_QUALITY_MOTION(crop_img2,APERTURE,(float)THRESHOLD); 	   
	    theta_first_crossing[metric_counter]     = Phi;
	    length_first_crossing[metric_counter]    = LENGTH_TEMP;
	    metric_counter++;
	    break;
	    
	  case 4:
 	    skewness_first_crossing[metric_counter]  = IMAGE_QUALITY_MOTION(crop_img4,APERTURE,(float)THRESHOLD);	   
	    theta_first_crossing[metric_counter]     = Phi;
	    length_first_crossing[metric_counter]    = LENGTH_TEMP;
	    metric_counter++;
	    break;

	  case 6:  
	    skewness_first_crossing[metric_counter]  = IMAGE_QUALITY_MOTION(crop_img6,APERTURE,(float)THRESHOLD);
	    background_sub_flag[metric_counter] =   BACKGROUND_SUB_CROP(original_crop,crop_img6);
	    theta_first_crossing[metric_counter]     = Phi;
	    length_first_crossing[metric_counter]    = LENGTH_TEMP;
	    metric_counter++;
	    break;

	  case 8:
	    skewness_first_crossing[metric_counter]  = IMAGE_QUALITY_MOTION(crop_img8,APERTURE,(float)THRESHOLD);;
	    theta_first_crossing[metric_counter]     = Phi;
	    length_first_crossing[metric_counter]    = LENGTH_TEMP;
	    metric_counter++;
	    break;
	    
	  case 10:
	    skewness_first_crossing[metric_counter]  = IMAGE_QUALITY_MOTION(crop_img10,APERTURE,(float)THRESHOLD);
	    theta_first_crossing[metric_counter]     = Phi;
	    length_first_crossing[metric_counter]    = LENGTH_TEMP;
	    metric_counter++;
	    break;
	  }
	  
	  Phi=HOLDER[i][0];
	  LENGTH=HOLDER[i][2];
	  LENGTH_TEMP=LENGTH;
	  
	  switch (metric_counter){
	  case 1:
	    skewness_first_crossing[metric_counter]  = IMAGE_QUALITY_MOTION(crop_img1,APERTURE,(float)THRESHOLD);
	    theta_first_crossing[metric_counter]     = Phi;
	    length_first_crossing[metric_counter]    = LENGTH_TEMP;
	    metric_counter++;
	    break;
	    
	  case 3: 
	    skewness_first_crossing[metric_counter]  = IMAGE_QUALITY_MOTION(crop_img3,APERTURE,(float)THRESHOLD);
	    theta_first_crossing[metric_counter]     = Phi;
	    length_first_crossing[metric_counter]    = LENGTH_TEMP;
	    metric_counter++;
	    break;
	    
	  case 5: 
	    skewness_first_crossing[metric_counter]  = IMAGE_QUALITY_MOTION(crop_img5,APERTURE,(float)THRESHOLD);
	    theta_first_crossing[metric_counter]     = Phi;
	    length_first_crossing[metric_counter]    = LENGTH_TEMP;
	    metric_counter++;
	    break;
	    
	  case 7:  
	    skewness_first_crossing[metric_counter]  = IMAGE_QUALITY_MOTION(crop_img7,APERTURE,(float)THRESHOLD);
	    theta_first_crossing[metric_counter]     = Phi;
	    length_first_crossing[metric_counter]    = LENGTH_TEMP;
	    metric_counter++;
	    break;
	    
	  case 9: 
	    skewness_first_crossing[metric_counter]  = IMAGE_QUALITY_MOTION(crop_img9,APERTURE,(float)THRESHOLD);
	    theta_first_crossing[metric_counter]     = Phi;
	    length_first_crossing[metric_counter]    = LENGTH_TEMP;
	    metric_counter++;
	    break;
	    
	  case 11:  
	    skewness_first_crossing[metric_counter]  = IMAGE_QUALITY_MOTION(crop_img11,APERTURE,(float)THRESHOLD);
	    theta_first_crossing[metric_counter]     = Phi;
	    length_first_crossing[metric_counter]    = LENGTH_TEMP;
	    metric_counter++;
	    break;
	  }
	}
      
      
      gt_zero_counter=0;
      //check to see if all values are 0
      for (i=0; i < count*2; i++)
	{
	  temp_score=skewness_first_crossing[i];
	  if (temp_score > 0)
	    {
	      gt_zero_counter++;
	    }
	  
	}
      
      //  if (gt_zero_counter >=9 || THRESHOLD==0)
 if (gt_zero_counter >=6 || THRESHOLD==0)
	{ 
	  all_zero=0;
	  printf("FINAL THRESHOLD: %f\n",THRESHOLD);
	}
      
    }//end while all zero = 1;
  THRESHOLD+=2;
  
  float BEST_ANGLE;
  float BEST_LENGTH;
  float highest_score;
  float highest_score1;
  float highest_score2;
  int best_image_index;    
  //float temp_score;
  
  BEST_LENGTH=2;
  BEST_ANGLE=0;
  best_image_index=-1;
  highest_score=-100000;
  for (i=0; i < metric_counter; i++)
    {
      temp_score=skewness_first_crossing[i];
      printf("SCORE[%d]: %f  FLAG[%d]: %d THETA: %f  LENGTH: %f\n",i,temp_score,i,background_sub_flag[i],theta_first_crossing[i],length_first_crossing[i]);
      
      if (temp_score > highest_score && temp_score !=0 && background_sub_flag[i]==0)
        {
	  highest_score=temp_score;
	  best_image_index=i;
	  BEST_ANGLE=theta_first_crossing[i];
	  BEST_LENGTH=length_first_crossing[i];
        }   
      
    }
  
  printf("BEST IMAGE: %d\n",best_image_index);
  
  switch (best_image_index){
    
  case -1: cvCopy(img_out, dst_images[0]);
    break;
    
  case 0: cvCopy(img_out0, dst_images[0]);
    break;
    
  case 1:  cvCopy(img_out1, dst_images[0]);
    break;
    
  case 2: cvCopy(img_out2, dst_images[0]);
    break;
    
  case 3: cvCopy(img_out3, dst_images[0]);
    break;
    
  case 4:  cvCopy(img_out4, dst_images[0]);
    break;
    
  case 5: cvCopy(img_out5, dst_images[0]);
    break;
    
  case 6:  cvCopy(img_out6, dst_images[0]);
    break;
    
  case 7:  cvCopy(img_out7, dst_images[0]);
    break;
    
  case 8: cvCopy(img_out8, dst_images[0]);
    break;
    
  case 9: cvCopy(img_out9, dst_images[0]);
    break;
    
  case 10: cvCopy(img_out10, dst_images[0]);
    break;
    
  case 11:  cvCopy(img_out11, dst_images[0]);
    break;
  }
    
  
  BEST_LENGTH=2;
  BEST_ANGLE=0;
  //best_image_index=-1;
  highest_score1=-100000;
  for (i=0; i < metric_counter; i++)
    {
      temp_score=skewness_first_crossing[i];
      
      if (temp_score > highest_score1 && temp_score != highest_score && temp_score !=0 && background_sub_flag[i]==0)
        {
	  highest_score1=temp_score;
	  best_image_index=i;
	  BEST_ANGLE=theta_first_crossing[i];
	  BEST_LENGTH=length_first_crossing[i];
        }
      
    }
  
  
  switch (best_image_index){
  case -1: cvCopy(img_out, dst_images[1]);
    break;
  case 0: cvCopy(img_out0, dst_images[1]);
    break;
    
  case 1:  cvCopy(img_out1, dst_images[1]);
    break;
    
  case 2: cvCopy(img_out2, dst_images[1]);
    break;
    
  case 3: cvCopy(img_out3, dst_images[1]);
    break;
    
  case 4:  cvCopy(img_out4, dst_images[1]);
    break;
    
  case 5: cvCopy(img_out5, dst_images[1]);
    break;
    
  case 6:  cvCopy(img_out6, dst_images[1]);
    break;
    
  case 7:  cvCopy(img_out7, dst_images[1]);
    break;
    
  case 8: cvCopy(img_out8, dst_images[1]);
    break;
    
  case 9: cvCopy(img_out9, dst_images[1]);
    break;
    
  case 10: cvCopy(img_out10, dst_images[1]);
    break;
    
  case 11:  cvCopy(img_out11, dst_images[1]);
    break;
    
  }

  
  BEST_LENGTH=2;
  BEST_ANGLE=0;
  //best_image_index=-1;
  highest_score2=-100000;
  for (i=0; i < metric_counter; i++)
    {
      temp_score=skewness_first_crossing[i];
      
      if (temp_score > highest_score2 && temp_score != highest_score && temp_score !=highest_score1 && temp_score !=0 && background_sub_flag[i]==0)
        {
	  highest_score2=temp_score;
	  best_image_index=i;
	  BEST_ANGLE=theta_first_crossing[i];
	  BEST_LENGTH=length_first_crossing[i];
        }
      
    }
  
  
  
  switch (best_image_index){
  case -1: cvCopy(img_out, dst_images[2]);
    break;
  case 0: cvCopy(img_out0, dst_images[2]);
    break;
    
  case 1:  cvCopy(img_out1, dst_images[2]);
    break;
    
  case 2: cvCopy(img_out2, dst_images[2]);
    break;
    
  case 3: cvCopy(img_out3, dst_images[2]);
    break;
    
  case 4:  cvCopy(img_out4, dst_images[2]);
    break;
    
  case 5: cvCopy(img_out5, dst_images[2]);
    break;
    
  case 6:  cvCopy(img_out6, dst_images[2]);
    break;
    
  case 7:  cvCopy(img_out7, dst_images[2]);
    break;
    
  case 8: cvCopy(img_out8, dst_images[2]);
    break;
    
  case 9: cvCopy(img_out9, dst_images[2]);
    break;
    
  case 10: cvCopy(img_out10, dst_images[2]);
    break;
    
  case 11:  cvCopy(img_out11, dst_images[2]);
    break;
    
  }
  
    cvReleaseImage( &img1 );
    cvReleaseImage( &img2 );
    cvReleaseImage( &img_out );
    cvReleaseImage( &img_out0 );
    cvReleaseImage( &img_out1 );
    cvReleaseImage( &img_out2 );
    cvReleaseImage( &img_out3 );
    cvReleaseImage( &img_out4 );
    cvReleaseImage( &img_out5 );
    cvReleaseImage( &img_out6 );
    cvReleaseImage( &img_out7 );
    cvReleaseImage( &img_out8 );
    cvReleaseImage( &img_out9 );
    cvReleaseImage( &img_out10 );
    cvReleaseImage( &img_out11 );
    cvReleaseImage( &original_crop);
    cvReleaseImage( &crop_img0);
    cvReleaseImage( &crop_img1);
    cvReleaseImage( &crop_img2);
    cvReleaseImage( &crop_img3);
    cvReleaseImage( &crop_img4);
    cvReleaseImage( &crop_img5);
    cvReleaseImage( &crop_img6);
    cvReleaseImage( &crop_img7);
    cvReleaseImage( &crop_img8);
    cvReleaseImage( &crop_img9);
    cvReleaseImage( &crop_img10);
    cvReleaseImage( &crop_img11);

    cvReleaseImage(&snr_image);
    cvReleaseImage(&img_float_snr);
    fftw_destroy_plan(plan_r_snr);
    fftw_free(snr_fft_image);
    fftw_free  (ifft_snr);	
    free(average_array_snr);
  
    cvReleaseImage( &img_float );
    cvReleaseImage( &img_float_scale );
    free(metric_original);
    free(PSF_array_1);
    free(h_array);
    free(OTF_array);
    free(average_array);
    free(temp_OTF_array);
    free(OTF_Laplacian);
    free(temp_OTF_Laplacian);
    free(skewness_first_crossing);
    free(theta_first_crossing);
    free(length_first_crossing);
    free(background_sub_flag);
    fftw_destroy_plan( plan_b );
    fftw_destroy_plan( plan_f );
    fftw_destroy_plan( plan_f1 );
    fftw_destroy_plan( plan_OTF_Laplacian );

    fftw_free(Image_fft); 
    fftw_free(data_in);
    fftw_free(fft);
    fftw_free(Image_fft1); 
    fftw_free(data_in1);
    fftw_free(fft1); 
 
    fftw_free(ifft);
    fftw_free(fft_temp);
    fftw_free(OTF_Laplacian_in);
    fftw_free(OTF_Laplacian_fft);
    fftw_cleanup();

    
    free(PSF_Laplacian);
    
}



