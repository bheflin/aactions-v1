// classify.cpp
// Copyright 2009 Securics, Inc

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <math.h>
#include "classify.h"

#define MIN(a, b) (((a) < (b)) ? a : b)

using namespace std;

// A struct used for sorting the top N
struct label_and_score
{
    int label;
    double score;
};

/** Compare two label_and_score structs, used for sorting the top N
 *@param const void *p1 value to compare
 *@param const void *p2 second value to compare
 *@return p1<p2 return 1 p1==p2 return 0 p1>p2 return -1
 */
static int cmpdouble(const void *p1, const void *p2)
{
    struct label_and_score *s1 = (struct label_and_score*)p1;
    struct label_and_score *s2 = (struct label_and_score*)p2;

    if(s1->score < s2->score)
        return 1;
    else if(s1->score > s2->score)
        return -1;
    else
        return 0;
}

/**
 * Runs classification of a single probe against the gallery
 * @param  v1_float* features The features of the probe
 * @param int  feature_count The number of pca features kept
 * @param struct svm_model* model_name The svm model to be saved off
 * @param int target_label For ground truth of data
 * @param FILE* ftrain File to save future failure training data(Can be NULL)
 * @param Classification* fpmodel Failure prediction model (Can be NULL)
 * @param vector<id_score> &topids The location to store the top n score id pairs to be returned.
*/
int classify(v1_float *features, int feature_count, struct svm_model *model, vector<id_score> &topids)
{
    struct svm_node *x;
    int i = 0;
    int nr_class = svm_get_nr_class(model);

    x = (struct svm_node*)malloc((feature_count + 1) * sizeof(struct svm_node));

    double v;

    for(i = 0; i < feature_count; i++)
    {
        x[i].index = i + 1;
        x[i].value = features[i];
    }

    x[feature_count].index = -1;

    int *labels = (int*)malloc(sizeof(int) * nr_class);
    svm_get_labels(model, labels);

    double *scores = (double*)malloc(sizeof(double) * nr_class);
    v = svm_predict_probability(model, x, scores);

    struct label_and_score *lscores = (struct label_and_score*)malloc(sizeof(struct label_and_score) * nr_class);

    for(i = 0; i < nr_class; i++)
    {
        lscores[i].label = labels[i];
        lscores[i].score = scores[i];
    }

    free(labels);
    free(scores);

    qsort(lscores, nr_class, sizeof(struct label_and_score), cmpdouble);

    topids.clear();
    for(i = 0; i < nr_class; i++)
    {
        id_score entry;
        entry.id = lscores[i].label;
        entry.score = lscores[i].score;
        entry.algo = 0;
        topids.push_back(entry);
    }

    free(lscores);
    free(x);

    return (int)v;
}
