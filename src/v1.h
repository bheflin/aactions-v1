// v1.h
// Copyright 2009 Securics, Inc

#ifndef V1_H
#define V1_H

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include "structs.h"
#include "preprocessing.h"
#include "convolve2d.h"
#include "gabor.h"

void init(struct gallery_entry** gallery, int gallery_size);
void free_v1(struct gallery_entry** gallery, int gallery_size);
void get_features(IplImage* img, v1_float* features, void** gfilters, int fast_gabor, int norm_image, int use_python, int num);
void sphere_data(struct gallery_entry* gallery, int gallery_size, int size, char* file_values);
void sphere_data_validate(struct validate_gallery* gallery, int gallery_size, int size, char* file_values);
void sphere_probe(v1_float* features, int size, v1_float* mean, v1_float* std);
void addIpl(IplImage* dst, IplImage* add);
void v1s_filter_fast(IplImage* img, gabor_filter **filterbank, IplImage** results);
void v1s_filter_slow(IplImage* img, IplImage**filterbank_real, IplImage** results);

#endif
