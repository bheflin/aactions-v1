// facedetect.h
// Copyright 2009 Securics, Inc

#ifndef _facedetect_h_
#define _facedetect_h_

#ifdef HAVE_CONFIG_H
#define HAD_CONFIG_H
#undef HAVE_CONFIG_H
#endif

#include "our_cv.h"
//#include "opencv/highgui.h"

#ifdef HAD_CONFIG_H
#define HAVE_CONFIG_H
#undef HAD_CONFIG_H
#endif

int init_face(const char* haarfile);
void free_face();
CvRect detect_face(IplImage* input);

#endif /* _facedetect_h_ */
