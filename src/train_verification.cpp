#ifndef WIN32
# include <pthread.h>
#endif
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/stat.h>
#include <string.h>
#include <libgen.h>
#include <vector>
#include "learn.h"
#include "v1.h"
#include "structs.h"
#include "pca.h"
#include "asciibar.h"
#include "gabor.h"
#include "utils.h"
#include "geo.h"
#include "version.h"
#include "sqi.h"
#include "argparse.h"
#include "eye_detector.h"
#include "parser.h"


static int incoming[2];
static int done[2];
typedef struct file_entry
{
    int id;
    string name;
    string name_2;
    int lx1;
    int ly1;
    int rx1;
    int ry1;
    int lx2;
    int ly2;
    int rx2;
    int ry2;
}file_entry;

vector<file_entry> lines;
// Structs
typedef struct prog_params_t
{
    char* prog_name;
    char* file_name;
    char* pca_fname;
    char* svm_fname;
    char* train_fname;
    char* sphere_fname;
    char* easy_base;
    char* loadfilt_dir;
    int   cross_validate;
    int   num_procs;
    int   fast_gabor;
    int   show_progress;
    int   use_python;
    int   geo_norm;
    int   hist_norm;
    int   sqi_norm;
    int   quiet;
    int   use_eyedetect;
    int   eye_detection_thresh;
    int   pca_limit;
} prog_params;

typedef struct thread_validate_params
{
    struct validate_gallery* gallery;
    void** filters;
    int fast_gabor;
    int use_python;
    int tnum;
  int sqi_norm;
  int geo_norm;
  int hist_norm;
}thread_validate_params;

static void usage(const char* name, int val);
static void parseCommandLine(int argc, char **argv, prog_params *parm);
static void init_validate(struct validate_gallery **gallery, int gallery_size);
static void free_validate(struct gallery_entry **gallery, int gallery_size);
static int parseFile(struct validate_gallery* gallery);
static void combine_features(v1_float* feature_one, v1_float* feature_two, validate_gallery gallery, int features);
static void eye_detect_gallery(validate_gallery* gallery, int gallery_size, int thresh, int use_python);

#define SAFE_RELEASE(img) do { if((img) != NULL) { cvReleaseImage(&(img)); (img) = NULL; } } while(0);
#define SAFE_FREE(ptr) do { if ((ptr) != NULL) { free((ptr)); (ptr) = NULL; } } while(0);
#define SAFE_DELETE(ptr) do { if ((ptr) != NULL) { delete (ptr); (ptr) = NULL; } } while(0);

static void eye_detect_gallery(validate_gallery* gallery, int gallery_size, int thresh, int use_python)
{
    int reyeX, reyeY, leyeX, leyeY, i;
    int reyeX1, reyeY1, leyeX1, leyeY1;
    float l_distance, r_distance;
    IplImage* input;
    int beforeW, beforeH;
    double scaleX, scaleY;
    IplImage* tmp;
    CvRect r;
    int width;
    const int small_size = 150;
    for(i = 0; i < gallery_size; i++)
    {
        reyeX = 0;
        reyeY = 0;
        leyeX = 0;
        leyeY = 0;
        input = cvLoadImage(gallery[i].path, 0);
        width = gallery[i].reyeX_file1 -gallery[i].leyeX_file1;
	r.x = gallery[i].leyeX_file1 - width;
        if(r.x < 0)
        {
            r.x = 0;
        }
        r.y = gallery[i].leyeY_file1 - width;
        if(r.y < 0)
        {
            r.y = 0;
        }
        r.width = 3 * width;
        if(r.width > input->width)
        {
            r.width = input->width;
        }
        r.height = 3 * width;
        if(r.height > input->height)
        {
            r.height = input->height;
        }
        tmp = cropIpl(input, r.x, r.y, r.width, r.height);
        SAFE_RELEASE(input);
        input = tmp;

        beforeW = input->width;
        beforeH = input->height;
        tmp = get_image(input, small_size, use_python, 1);
        SAFE_RELEASE(input);
        input = tmp;

        //now returns 4 eye coordinates
	eye_detect(input, &leyeX, &reyeX, &leyeY, &reyeY, &leyeX1, &reyeX1, &leyeY1, &reyeY1);
        
	scaleX = (double)beforeW / (double)input->width;
        scaleY = (double)beforeH / (double)input->height;
        leyeX = (int)((double)leyeX * scaleX) + r.x;
        leyeY = (int)((double)leyeY * scaleY) + r.y;
        reyeX = (int)((double)reyeX * scaleX) + r.x;
        reyeY = (int)((double)reyeY * scaleY) + r.y;
        //check if old ground truth is better
        l_distance = sqrt(pow(leyeX-gallery[i].leyeX_file1,2)+pow(leyeY-gallery[i].leyeY_file1,2));
        r_distance = sqrt(pow(reyeX-gallery[i].reyeX_file1,2)+pow(reyeY-gallery[i].reyeY_file1,2));
        if(l_distance < thresh && r_distance < thresh)
        {
            printf("CHANGING EYE COORDINATES FOR %s\n", gallery[i].path);
            gallery[i].leyeX_file1 = leyeX;
            gallery[i].leyeY_file1 = leyeY;
            gallery[i].reyeX_file1 = reyeX;
            gallery[i].reyeY_file1 = reyeY;
        }

        SAFE_RELEASE(input);

	// now do the second eye
        reyeX = 0;
        reyeY = 0;
        leyeX = 0;
        leyeY = 0;
        input = cvLoadImage(gallery[i].second_path, 0);
        width = gallery[i].reyeX_file2 -gallery[i].leyeX_file2;
	r.x = gallery[i].leyeX_file2 - width;
        if(r.x < 0)
        {
            r.x = 0;
        }
        r.y = gallery[i].leyeY_file2 - width;
        if(r.y < 0)
        {
            r.y = 0;
        }
        r.width = 3 * width;
        if(r.width > input->width)
        {
            r.width = input->width;
        }
        r.height = 3 * width;
        if(r.height > input->height)
        {
            r.height = input->height;
        }
        tmp = cropIpl(input, r.x, r.y, r.width, r.height);
        SAFE_RELEASE(input);
        input = tmp;

        beforeW = input->width;
        beforeH = input->height;
        tmp = get_image(input, small_size, use_python, 1);
        SAFE_RELEASE(input);
        input = tmp;
        eye_detect(input, &leyeX, &reyeX, &leyeY, &reyeY, &leyeX, &reyeX, &leyeY, &reyeY);
        scaleX = (double)beforeW / (double)input->width;
        scaleY = (double)beforeH / (double)input->height;
        leyeX = (int)((double)leyeX * scaleX) + r.x;
        leyeY = (int)((double)leyeY * scaleY) + r.y;
        reyeX = (int)((double)reyeX * scaleX) + r.x;
        reyeY = (int)((double)reyeY * scaleY) + r.y;
        //check if old ground truth is better
        l_distance = sqrt(pow(leyeX-gallery[i].leyeX_file2,2)+pow(leyeY-gallery[i].leyeY_file2,2));
        r_distance = sqrt(pow(reyeX-gallery[i].reyeX_file2,2)+pow(reyeY-gallery[i].reyeY_file2,2));
        if(l_distance < thresh && r_distance < thresh)
        {
            printf("CHANGING EYE COORDINATES FOR %s\n", gallery[i].second_path);
            gallery[i].leyeX_file2 = leyeX;
            gallery[i].leyeY_file2 = leyeY;
            gallery[i].reyeX_file2 = reyeX;
            gallery[i].reyeY_file2 = reyeY;
        }

        SAFE_RELEASE(input);

    }
}

static void combine_features(v1_float* features_one, v1_float* features_two, validate_gallery gallery, int features)
{
    int j;
    for(j = 0; j < features; j++)
    {
        gallery.features[j] = pow((features_one[j]-features_two[j]),2.0);
    }
}
static void usage(const char* name, int val)
{
    fprintf(stderr, "Usage: %s [OPTIONS]\n"
            "General options:\n"
            "  -i, --info            Print out file versions and exit.\n"
            "  -h, --help            Show this helpful message.\n"
            "  -C, --config=FILE     Load extra configuration options from FILE.\n"
            "  -o, --save-config=FILE\n"
            "                        Dump all configuration options to FILE.\n"
            "V1 validation training options:\n"
            "  -f, --file=FILE       Gallery file with ids and eye coordinates (required).\n"
            "  -b, --base=NAME       Use easy file mode where -p, -s, and -v are\n"
            "                        automatically determined using NAME as the base.\n"
            "  -p, --pca=FILE        PCA subspace file. Required unless -b specified.\n"
            "  -s, --svm=FILE        SVM training file. Required unless -b specified.\n"
            "  -v, --value=FILE      Sphered values file. Required unless -b specified.\n"
            "  -F, --features-save=FILE\n"
            "                        File in which to store SVM features.\n"
            "  -t, --num-threads=NUM Number of threads to spawn (default 2).\n"

            , name);
    exit(val);
}

//This is a really simple file reader that is inteded to get program going fast it expects prenormalized images and format is
// 1 path path for match
// -1 path path for non match
static int parseFile(struct validate_gallery* gallery)
{
    FILE* file;
    char name[128];
    char name_2[128];
    int id;
    int i;
    for(i = 0; i < lines.size(); i++)
      {
        gallery[i].id = lines[i].id;
        gallery[i].path = strdup(lines[i].name.c_str());
        gallery[i].second_path =strdup(lines[i].name_2.c_str());
        gallery[i].leyeX_file1 = lines[i].lx1;  
        gallery[i].leyeY_file1 = lines[i].ly1;   
        gallery[i].reyeX_file1 = lines[i].rx1;   
        gallery[i].reyeY_file1 = lines[i].ry1;  
        gallery[i].leyeX_file2 = lines[i].lx2;   
        gallery[i].leyeY_file2 = lines[i].ly2;  
        gallery[i].reyeX_file2 = lines[i].rx2;  
        gallery[i].reyeY_file2 = lines[i].ry2;  

      }

    return 0;
}
static int count_lines(char* file_name)
{
    char line_str[512];
    char* line_no_newline;
    FILE* file;
    IplImage* check;
    file_entry line;
    vector<string> tokens;
    lines.clear();

    file = fopen(file_name,"r");
    if(!file)
    {
        printf("ERROR INPUT FILE NOT FOUND\n");
        return -1;
    }
    while(fgets(line_str, sizeof(line_str), file) !=NULL)
    {
        int len = strlen(line_str);
        line_no_newline = (char*) malloc((len) * sizeof(char));
        memcpy(line_no_newline, line_str, (len-1)*sizeof(char));
        line_no_newline[len-1] = '\0';
        tokenize(line_no_newline, tokens);
        SAFE_FREE(line_no_newline);
        if(tokens.size() != 3 && tokens.size() !=11)
        {
            printf("INVALID LINE %s\n Need either all ground truth or no ground truth\n", line_str);
            continue;
        }
        check = cvLoadImage(tokens[1].c_str());
        if(!check)
        {
            printf("Line %s is invalid due to %s\n",line_str, tokens[1].c_str()); 
            continue;
        }
        else
        {
            cvReleaseImage(&check);
            check = cvLoadImage(tokens[2].c_str());
            if(!check)
            {
                printf("Line %s is invalid due to %s\n",line_str, tokens[2].c_str()); 
              continue;
            }
	    cvReleaseImage(&check);
            line.id = atoi(tokens[0].c_str());
            line.name = tokens[1].c_str();
            line.name_2 = tokens[2].c_str();
            if(tokens.size() ==3)
            {
              line.lx1 = -1;
              line.ly1 = -1;
              line.rx1 = -1;
              line.ry1 = -1;
              line.lx2 = -1;
              line.ly2 = -1;
              line.rx2 = -1;
              line.ry2 = -1;
          }
          else
          {
              line.lx1 = atoi(tokens[3].c_str());
              line.ly1 = atoi(tokens[4].c_str());
              line.rx1 = atoi(tokens[5].c_str());
              line.ry1 = atoi(tokens[6].c_str());
              line.lx2 = atoi(tokens[7].c_str());
              line.ly2 = atoi(tokens[8].c_str());
              line.rx2 = atoi(tokens[9].c_str());
              line.ry2 = atoi(tokens[10].c_str());

          }
          lines.push_back(line);
      }
      tokens.clear();
    }

    fclose(file);
    return lines.size();
}
/**
 *Initilaze memory for program including all images
 *This cannot be done unless you remove dynamic feature reduction and set up all gabors willing to use at the time
 * @param struct gallery_entry ** gallery The gallery to initialize
 * @param int gallery_size The size of the gallery
 */
static void init_validate(struct validate_gallery **gallery, int gallery_size)
{
    int i = 0;
    *gallery = (struct validate_gallery*)malloc(gallery_size * sizeof(struct validate_gallery));
    for(i = 0; i < gallery_size; i++)
    {
        //for resample 30x30x96
        (*gallery)[i].features = (v1_float*)malloc(WINDOW_SIZE*WINDOW_SIZE*GABOR_FEATURES*sizeof(v1_float));
        (*gallery)[i].pca_features = NULL;
        (*gallery)[i].path = NULL;
        (*gallery)[i].second_path = NULL;
        (*gallery)[i].id = 0;
        (*gallery)[i].feature_count = 0;
    }
}

/**
 *free all memory allocated in init
 * @param struct gallery_entry ** gallery The gallery to free
 * @param int gallery_size The size of the gallery
 */
static void free_validate(struct validate_gallery **gallery, int gallery_size)
{
    int i;
    for(i = 0; i < gallery_size; i++)
    {
        free((*gallery)[i].features);
        if((*gallery)[i].pca_features != NULL)
            free((*gallery)[i].pca_features);

        if((*gallery)[i].path != NULL)
            free((*gallery)[i].path);

        if((*gallery)[i].second_path != NULL)
            free((*gallery)[i].second_path);
    }

    free(*gallery);
    *gallery = NULL;
}

static void parseCommandLine(int argc, char **argv, prog_params *parm)
{
    argv[0] = basename(argv[0]);

    parm->prog_name        = argv[0];
    parm->file_name        = NULL;
    parm->pca_fname        = NULL;
    parm->svm_fname        = NULL;
    parm->train_fname      = NULL;
    parm->sphere_fname     = NULL;
    parm->easy_base        = NULL;
    parm->loadfilt_dir     = NULL;
    parm->cross_validate   = 0;
    parm->use_eyedetect    = 0;
    parm->eye_detection_thresh = 10;
    parm->fast_gabor       = 1;
    parm->show_progress    = 1;
    parm->use_python       = 0;
    parm->geo_norm         = 0;
    parm->hist_norm        = 1;
    parm->sqi_norm         = 0;
    parm->quiet            = 0;
    parm->pca_limit        = -1;

#ifdef DEBUG
    parm->num_procs        = 1;
#else
# ifdef WIN32
    parm->num_procs        = 1;
# else
    parm->num_procs        = 2;
# endif
#endif

    ArgParser ap(usage, parm->prog_name, false);

    // Simple switches
    ap.addOption("help", false, "h");
    ap.addOption("info", false, "i");

    // Regular options
    ap.addOption("config",        true, "C");
    ap.addOption("file",          true, "f");
    ap.addOption("pca",           true, "p");
    ap.addOption("value",         true, "v");
    ap.addOption("svm",           true, "s");
    ap.addOption("base",          true, "b");
    ap.addOption("features-save", true, "F");
    ap.addOption("num-threads",   true, "t");
    ap.addOption("filter-dir",    true, "d");
    ap.addOption("pca-limit",     true, "l");
    ap.addOption("save-config",   true, "o");

    // Boolean options (rightmost column indicates that "-G" is the same as "-G yes")
    ap.addOption("fast-gabor",     true, "S", "yes");
    ap.addOption("geo-norm",       true, "G", "yes");
    ap.addOption("cross-validate", true, "c", "yes");    
    ap.addOption("progress",       true, "r", "yes");
    ap.addOption("hist-norm",      true, "H", "yes");
    ap.addOption("python-resize",  true, "P", "yes");
    ap.addOption("normalize-SQI",  true, "B", "yes");
    ap.addOption("quiet",          true, "q", "yes");
    ap.addOption("eye-detect",     true, "E", "yes");    
    ap.addOption("eye-thresh",     true, "R", "yes");

    if(!ap.parseCommandLine(argc, argv, true))
        exit(1);
    
    if(ap.isSet("help"))
        usage(argv[0], 0);
    
    if(ap.isSet("info"))
    {
        printFileVersions(stdout);
        exit(0);
    }

    // Parse the config file if specified
    if(ap.isSet("config"))
    {
        // The last option means that the command line always gets
        // preference if there is a conflict.
        if(!ap.parseConfigFile(ap.getValue("config"), false))
            exit(1);
    }

    if(ap.isSet("file")) parm->file_name = strdup(ap.getValue("file"));
    if(ap.isSet("pca")) parm->pca_fname = strdup(ap.getValue("pca"));
    if(ap.isSet("value")) parm->sphere_fname = strdup(ap.getValue("value"));
    if(ap.isSet("svm")) parm->svm_fname = strdup(ap.getValue("svm"));
    if(ap.isSet("base")) parm->easy_base = strdup(ap.getValue("base"));
    if(ap.isSet("features-save")) parm->train_fname = strdup(ap.getValue("features-save"));
    if(ap.isSet("cross-validate")) parm->cross_validate = ap.getBoolValue("cross-validate");
    if(ap.isSet("fast-gabor")) parm->fast_gabor = ap.getBoolValue("fast-gabor");
    if(ap.isSet("progress")) parm->show_progress = ap.getBoolValue("progress");
    if(ap.isSet("filter-dir")) parm->loadfilt_dir = strdup(ap.getValue("filter-dir"));
    if(ap.isSet("geo-norm")) parm->geo_norm = ap.getBoolValue("geo-norm");
    if(ap.isSet("hist-norm")) parm->hist_norm = ap.getBoolValue("hist-norm");
    if(ap.isSet("python-resize")) parm->use_python = ap.getBoolValue("python-resize");
    if(ap.isSet("normalize-SQI")) parm->sqi_norm = ap.getBoolValue("normalize-SQI");
    if(ap.isSet("quiet")) parm->quiet = ap.getBoolValue("quiet");
    if(ap.isSet("eye-thresh")) parm->eye_detection_thresh = atoi(ap.getValue("eye-thresh"));
    if(ap.isSet("pca-limit")) parm->pca_limit = atoi(ap.getValue("pca-limit"));

    if(ap.isSet("num-threads"))
    {
#ifdef WIN32
        fprintf(stderr, "Sorry, number of threads is only for use by cool people.\n");
        exit(1);
#endif
        parm->num_procs = atoi(ap.getValue("num-threads"));
    }

    if(parm->quiet)
        parm->show_progress = 0;

    if(ap.isSet("save-config"))
    {
        vector<string> excludes;
        excludes.push_back("help");
        excludes.push_back("info");
        excludes.push_back("config");
        excludes.push_back("save-config");
        if(!ap.writeConfigFile(ap.getValue("save-config"), excludes))
            exit(1);
        printf("All options saved to %s.\n", ap.getValue("save-config"));
        exit(0);
    }
}


void compute_features_validate(struct thread_validate_params* params, int index)
{
  v1_float* features_one;
  v1_float* features_two;
  IplImage* img;
  features_one = (v1_float*)malloc(WINDOW_SIZE*WINDOW_SIZE*GABOR_FEATURES*sizeof(v1_float));
  features_two = (v1_float*)malloc(WINDOW_SIZE*WINDOW_SIZE*GABOR_FEATURES*sizeof(v1_float));
  //Get first set
  img = cvLoadImage(params->gallery[index].path,0);
  if(params->geo_norm)
    {
      IplImage* geo = perform_geo_norm(img, params->gallery[index].leyeX_file1, params->gallery[index].leyeY_file1, params->gallery[index].reyeX_file1,params->gallery[index].reyeY_file1, params->hist_norm, params->tnum);
      if(!geo)
	{
	  printf("GEO NORMALIZATION FAILED");
	}
      else
	{
	  cvReleaseImage(&img);
	  img = geo;      
	}
    }
  if(params->sqi_norm)
    {
      IplImage* sqi = securics_sqi_norm(img);
      if(!sqi)
	{
	  printf("sqi NORMALIZATION FAILED");
	}
      else
	{
	  cvReleaseImage(&img);
	  img = sqi;      
	}

    }
  printf("%s %s\n", params->gallery[index].path, params->gallery[index].second_path);
  get_features(img, features_one, params->filters, params->fast_gabor, 1, params->use_python, 1);
  cvReleaseImage(&img);
  //Get second set
  img = cvLoadImage(params->gallery[index].second_path,0);
  if(params->geo_norm)
    {
      IplImage* geo = perform_geo_norm(img, params->gallery[index].leyeX_file2, params->gallery[index].leyeY_file2, params->gallery[index].reyeX_file2,params->gallery[index].reyeY_file2, params->hist_norm, params->tnum);
      cvReleaseImage(&img);
      if(!geo)
	{
	  printf("GEO NORMALIZATION FAILED");
	}
      else
	{
	  cvReleaseImage(&img);
	  img = geo;      
	}
      
    }

  if(params->sqi_norm)
    {
      IplImage* sqi = securics_sqi_norm(img);
      if(!sqi)
	{
	  printf("sqi NORMALIZATION FAILED");
	}
      else
	{
	  cvReleaseImage(&img);
	  img = sqi;      
	}
    }
  get_features(img, features_two, params->filters, params->fast_gabor, 1, params->use_python, 1);
  cvReleaseImage(&img);
  combine_features(features_one, features_two,params->gallery[index], WINDOW_SIZE*WINDOW_SIZE*GABOR_FEATURES);
  free(features_one);
  free(features_two);
}

static void* feature_thread(void* p)
{
    struct thread_validate_params* param = (struct thread_validate_params*)p;

    int n;

    while(1)
    {
        if(read(incoming[0], &n, sizeof(int)) != sizeof(int))
            perror("read");

        if(n < 0)
            break;
        compute_features_validate(param, n);

        if(write(done[1], &n, sizeof(int)) != sizeof(int))
            perror("write");
    }

    free(p);

    return NULL;
}


int main(int argc, char** argv)
{
    prog_params params;
    struct validate_gallery *gallery;
    int gallery_size;
    int i, c;
    int error = 0;
    int feat_size = WINDOW_SIZE * WINDOW_SIZE * GABOR_FEATURES;
    void** filters;

    parseCommandLine(argc, argv, &params);
    if(params.easy_base && !error)
    {
        char tname[1024];
        if(params.pca_fname == NULL)
        {
            snprintf(tname, 1024, "%s.pca", params.easy_base);
            params.pca_fname = strdup(tname);
        }

        if(params.svm_fname == NULL)
        {
	  snprintf(tname, 1024, "%s.svm", params.easy_base);
	  params.svm_fname = strdup(tname);
        }

        if(params.sphere_fname == NULL)
        {
            snprintf(tname, 1024, "%s.val", params.easy_base);
            params.sphere_fname = strdup(tname);
        }
    }
    if(params.num_procs < 1 || params.num_procs > 32)
    {
        fprintf(stderr, "Unreasonable number of threads: %d\n", params.num_procs);
        ++error;
        return error;
    }

    if(params.fast_gabor)
        filters = (void**)make_filters();
    else
        filters = (void**)make_ipl_filters();
    if(filters == NULL)
    {
        exit(-4);
    }
    gallery_size = count_lines(params.file_name);
    if(gallery_size ==-1)
    {
        return gallery_size;
    }
    init_validate(&gallery, gallery_size);
    if(parseFile(gallery) == -1)
    {
        free_validate(&gallery, gallery_size);
        return -1;
    }
    if(params.num_procs > gallery_size)
        params.num_procs = gallery_size;
    if(params.use_eyedetect)
      {
	eye_detect_gallery(gallery, gallery_size, params.eye_detection_thresh, params.use_python);
      }
    struct thread_validate_params thread_param;
    thread_param.tnum =0;
    thread_param.filters = filters;
    thread_param.fast_gabor = params.fast_gabor;
    thread_param.use_python = params.use_python;
    thread_param.gallery = gallery;
    thread_param.sqi_norm = params.sqi_norm;
    thread_param.geo_norm = params.geo_norm;
    thread_param.hist_norm = params.hist_norm;
#ifdef WIN32
    for(i = 0; i < gallery_size; i++)
    {
        compute_features_validate(&thread_param, i);
    }
#else
    pthread_t *threads = (pthread_t*)malloc(params.num_procs * sizeof(pthread_t));
    pthread_attr_t pthread_custom_attr;
    pthread_attr_init(&pthread_custom_attr);

    if(pipe(incoming) != 0)
    {
        perror("pipe");
        exit(1);
    }

    if(pipe(done) != 0)
    {
        perror("pipe");
        exit(1);
    }

    for(c = 0; c < params.num_procs; c++)
    {
        thread_param.tnum++;
        struct tparam* tp = (struct tparam*)malloc(sizeof(struct tparam));
        memcpy(tp, &thread_param, sizeof(struct tparam));
        pthread_create(&threads[c], &pthread_custom_attr, feature_thread, (void*)tp);
    }

    if(params.quiet == 0)
        printf("Started %d worker%s\n", params.num_procs, (params.num_procs == 1) ? "" : "s");
    for(c = 0; c < gallery_size; c++)
    {
#ifdef WIN32
        compute_features_validate(&thread_param, c);
        if(params.quiet == 0)
            printf(" --> %s\n", gallery[c].path);
#else
        if(write(incoming[1], &c, sizeof(int)) != sizeof(int))
            perror("write");
#endif
    }

    int n = -1;
    for(c = 0; c < params.num_procs; c++)
    {
        if(write(incoming[1], &n, sizeof(int)) != sizeof(int))
            perror("write");
    }

    for(c = 0; c < gallery_size; c++)
    {
        if(read(done[0], &n, sizeof(int)) != sizeof(int))
            perror("read");
            if(params.quiet == 0)
            {
                printf("[%5.1f%%] %s\n", 100. * (float)(c + 1) / (float)gallery_size, gallery[n].path);
                fflush(stdout);
            }
    }

    if(params.quiet)
        printf(" done.\n");
    

    for(c = 0; c < params.num_procs; c++)
        pthread_join(threads[c], NULL);
    free(threads);

    close(incoming[0]);
    close(incoming[1]);
    close(done[0]);
    close(done[1]);

    pthread_attr_destroy(&pthread_custom_attr);

#endif
    sphere_data_validate(gallery, gallery_size, feat_size, params.sphere_fname);
    pca_validate(gallery, gallery_size, 1., params.pca_fname, params.show_progress, params.quiet, params.pca_limit);
    svm_parameter svm_params;
    svm_params.svm_type     = C_SVC;
    svm_params.kernel_type  = LINEAR;
    svm_params.degree       = 2;
    svm_params.gamma        = 0; // 1/k
    svm_params.coef0        = 0;
    svm_params.nu           = 0.1;
    svm_params.cache_size   = 256;
    svm_params.C            = 10;
    svm_params.eps          = 0.01;
    svm_params.p            = 0.1;
    svm_params.shrinking    = 1;
    svm_params.probability  = 1;
    svm_params.nr_weight    = 0;
    svm_params.weight_label = NULL;
    svm_params.weight       = NULL;
    learn_validate(gallery, gallery_size, &svm_params, params.svm_fname, params.train_fname, params.cross_validate);        
    free_validate(&gallery, gallery_size);
    return 0;
}
