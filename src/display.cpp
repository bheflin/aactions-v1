// File:    display.cpp
// Author:  Chris Eberle
// Created: Fri Nov 13 14:58:46 2009
// Copyright 2009 Securics, Inc
//
// This program is for viewing gallery / probe file lists in a much easier format
// than the search / click / open method that others seem to like.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <ctype.h>
#include <libgen.h>

#include "argparse.h"
#include "parser.h"
#include "cvheaders.h"

#define SPACE 32
#define ESCAPE 27
#define ENTER 10
#define RIGHT 83
#define LEFT 81
#define HOME 80
#define END 87
#define PAGE_UP 85
#define PAGE_DOWN 86
#define EKEY 101
#define YKEY 121

// Macros
#define SAFE_RELEASE(img) do { if ((img) != NULL) { cvReleaseImage(&(img)); (img) = NULL; } } while(0);
#define SAFE_FREE(ptr)    do { if ((ptr) != NULL) { free((ptr)); (ptr) = NULL; } } while(0);

// Typedefs
typedef struct prog_params_t
{
    char* progName;
    char* fileName;
    int   showEyes;
} prog_params;

// Prototypes
static void usage(const char* name, int val);
static void parseCommandLine(int argc, char **argv, prog_params *params);

static fileitem* getIndex(filelist* list, int idx)
{
    if(list == NULL)
        return NULL;

    fileitem* item = list->begin;
    if(item == NULL)
        return NULL;

    int myidx = 0;

    if(idx < 0)
        return item;

    while(myidx < idx)
    {
        if(item->next == NULL)
            break;
        item = item->next;
        myidx++;
    }

    return item;
}

static IplImage* resize(IplImage* img, int max_edge)
{
    IplImage *dst = NULL;
    CvSize new_size;

    // -- resize so that the biggest edge is max_edge (keep aspect ratio)
    if(img->width > img->height)
    {
        new_size.width  = max_edge;
        new_size.height = (int)((float)max_edge * (float)img->height / (float)img->width);
    }
    else
    {
        new_size.width  = (int)((float)max_edge * (float)img->width / (float)img->height);
        new_size.height = max_edge;
    }

    dst = cvCreateImage(new_size, img->depth, img->nChannels);

    // Resize the image
    if(img->width == dst->width && img->height == dst->height)
        cvCopy(img, dst);
    else
        cvResize(img, dst, CV_INTER_CUBIC);

    return dst;
}

// Entry point for program
int main(int argc, char **argv)
{
    // Parse the command line arguments
    prog_params params;
    parseCommandLine(argc, argv, &params);

    if(params.fileName == NULL)
        usage(params.progName, 0);

    filelist* list = parseFileList(params.fileName);
    SAFE_FREE(params.fileName);
    if(list == NULL)
        exit(1);

    int idx = 0;
    int key = -1;
    fileitem* item = NULL;
    int width, height;
    IplImage* img = NULL;
    bool reload = true;

    cvNamedWindow("Display", CV_WINDOW_AUTOSIZE);

    do
    {
        item = getIndex(list, idx);
        if(item == NULL)
            break;

        if(reload)
        {
            if((img = cvLoadImage(item->file, -1)) == NULL)
            {
                fprintf(stderr, "Error: Unable to load image file: `%s'\n", item->file);
                img = cvCreateImage(cvSize(300, 300), IPL_DEPTH_8U, 1);
                cvZero(img);
            }

            if(params.showEyes && item->leyeX >= 0 && item->leyeY >= 0 && item->reyeX >= 0 && item->reyeY >= 0)
            {
                CvPoint center;
                center.x = item->leyeX;
                center.y = item->leyeY;
                cvCircle(img, center, 10, cvScalar(0, 0, 255), 2, 8, 0);

                center.x = item->reyeX;
                center.y = item->reyeY;
                cvCircle(img, center, 10, cvScalar(0, 0, 255), 2, 8, 0);
            }

            if(img->width > 1000 || img->height > 1000)
            {
                IplImage* resized = resize(img, 1000);
                cvReleaseImage(&img);
                img = resized;
            }
        }

        reload = true;
        width = img->width;
        height = img->height;

        cvShowImage("Display", img);
        key = cvWaitKey(-1) & 255;

        if(key == RIGHT)
            idx++;
        else if(key == LEFT)
            idx--;
        else if(key == EKEY || key == YKEY)
            params.showEyes = 1 - params.showEyes;
        else if(key == HOME)
            idx = 0;
        else if(key == END)
            idx = list->num - 1;
        else if(key == PAGE_UP)
        {
            if(item->label < 0)
                idx--;
            else
            {
                int tmp = idx;
                int lbl = -1;

                while(tmp >= 0)
                {
                    lbl = (getIndex(list, --tmp))->label;
                    if(lbl != item->label)
                    {
                        idx = tmp;
                        break;
                    }
                }

                while(tmp >= 0)
                {
                    if((getIndex(list, tmp))->label != lbl)
                    {
                        idx = tmp + 1;
                        break;
                    }
                    tmp--;
                }
            }
        }
        else if(key == PAGE_DOWN)
        {
            if(item->label < 0)
                idx++;
            else
            {
                int tmp = idx;
                while(tmp < list->num)
                {
                    if((getIndex(list, ++tmp))->label != item->label)
                    {
                        idx = tmp;
                        break;
                    }
                }
            }
        }
        else if(key == ENTER || key == SPACE)
        {
            printf("%s:\n", item->file);
            printf("  -> Size     :  %d x %d\n", width, height);
            printf("  -> Channels :  %d\n", img->nChannels);
            printf("  -> Depth    :  ");

            switch(img->depth)
            {
            case IPL_DEPTH_8U:
                printf("8U");
                break;
            case IPL_DEPTH_8S:
                printf("8S");
                break;
            case IPL_DEPTH_16U:
                printf("16U");
                break;
            case IPL_DEPTH_16S:
                printf("16S");
                break;
            case IPL_DEPTH_32S:
                printf("32S");
                break;
            case IPL_DEPTH_32F:
                printf("32F");
                break;
            case IPL_DEPTH_64F:
                printf("64F");
                break;
            }

            printf("\n");

            if(item->label > 0)
                printf("  -> ID       :  %d\n", item->label);

            if(item->leyeX >= 0 && item->leyeY >= 0 && item->reyeX >= 0 && item->reyeY >= 0)
            {
                printf("  -> Eyes     :  Left   (%d, %d)\n"
                       "                 Right  (%d, %d)\n",
                       item->leyeX, item->leyeY, item->reyeX, item->reyeY);
            }

            printf("\n");
        }
        else
        {
            reload = false;
        }

        if(idx < 0)
            idx = 0;
        else if(idx >= list->num)
            idx = list->num - 1;

        if(reload)
        {
            cvReleaseImage(&img);
            img = NULL;
        }

    } while(key != ESCAPE);

    cvDestroyWindow("Display");

    freeList(list);
    return 0;
}

// Print the program's usage
static void usage(const char* name, int val)
{
    FILE* f = (val == 0) ? stdout : stderr;
    fprintf(f,
        "Usage: %s [OPTIONS]\n"
        "  -h, --help             Show this helpful message.\n"
        "  -f, --file=FILE        Load a list of files for viewing.\n"
        "  -y, --eyes             Show eye positions if available.\n"
        , name);
    exit(val);
}

// Parse the command line arguments and load them into a struct
static void parseCommandLine(int argc, char **argv, prog_params *params)
{
    argv[0] = basename(argv[0]);
    params->progName = argv[0];
    params->fileName = NULL;
    params->showEyes = 0;

    ArgParser ap(usage, params->progName, false);
    ap.addOption("help", false, "h");
    ap.addOption("file", true,  "f");
    ap.addOption("eyes", false, "y");

    if(!ap.parseCommandLine(argc, argv, false))
        exit(1);

    if(ap.isSet("help"))
        usage(params->progName, 0);

    if(ap.isSet("file"))
        params->fileName = strdup(ap.getValue("file"));

    if(ap.isSet("eyes"))
        params->showEyes = 1;
}
