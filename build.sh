#!/bin/bash

mydir=`dirname $0`
cd "$mydir"
mydir=`pwd`

if [[ ! -e "lib/libmr/build.sh" ]]; then
    git submodule init
    git submodule update
    cd lib/libmr
    git checkout master
    cd $mydir
    cd lib/libgige
    git checkout master
    cd $mydir
fi

if [[ ! -e "lib/libgige/build.sh" ]]; then
    git submodule init
    git submodule update
    cd lib/libgige
    git checkout master
    cd $mydir
    cd lib/libmr
    git checkout master
    cd $mydir
fi

if [[ ! -e "lib/libmr/configure" ]]; then
    cd lib/libmr
    echo "Creating libmr configure script"
    ./build.sh
    cd $mydir
fi

if [[ ! -e "lib/libgige/configure" ]]; then
    cd lib/libgige
    echo "Creating libgige configure script"
    ./build.sh
    cd $mydir
fi

echo "Creating v1 configure script"

if [[ ! -d "m4" ]]; then
    mkdir "m4"
fi

if [[ ! -d "lib" ]]; then
    mkdir "lib"
fi

if [[ ! -d "tests" ]]; then
    mkdir "tests"
fi

if [[ ! -d "scripts" ]]; then
    mkdir "scripts"
fi

if [[ ! -d "dist" ]]; then
    mkdir "dist"
fi

autoreconf -i

echo "Now you can run ./configure && make"
